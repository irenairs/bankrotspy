<?php
defined('DS_ENGINE') or die('web_demon laughs');

class loader_category
{
    private $name;
    private $descr;

    function __construct($params)
    {
        $this->name = isset($params[0]) ?  trim($params[0]) : '';
        $this->descr = isset($params[1]) ?  trim($params[1]) : '';
    }

    private function merge_name($data_name, $description)
    {
        if ($data_name != $description) {
            $name = (strlen($data_name) >= strlen($description)?$data_name.' '.$description:$description.' '.$data_name);
        } else {
            $name = $data_name;
        }
        return $name;
    }

    private function get_categories()
    {
        $res = core::$db->query('SELECT *, `ds_maindata_category`.`name` FROM `ds_maindata_category_keys` LEFT JOIN `ds_maindata_category` ON `ds_maindata_category_keys`.`id` = `ds_maindata_category`.`id` ORDER BY `ds_maindata_category_keys`.`sort` ASC;');
        $keys = array();
        while($cat = $res->fetch_assoc())
        {
            $loc = array();
            $loc['id'] = $cat['id'];
            $loc['name'] = $cat['name'];
            $loc['anti'] = array();
            $keywords = explode("\n", htmlspecialchars_decode(str_replace("\r\n", "\n", $cat['keys'])));
            foreach ($keywords as $key => $kt) {
                if (strpos($kt, '__') === 0) {
                    $loc['anti'][] = $kt;
                } else {
                    $loc['keys'][] = $kt;
                }
            }
            $keys[$cat['id']] = $loc;
        }
        return $keys;
    }
    private function catFind($category,$lotdata)
    {
        if (isset($category['anti']) && count($category['anti'])) {
            foreach ($category['anti'] as $key => $value) {
                $qw = substr($value, 2);
                $big = preg_match_all("/[А-Я]/Usu",$qw);
                if ($big) {
                    $findanti = preg_match_all("/".$qw."/Usu", $lotdata);
                } else {
                    $findanti = preg_match_all("/".$qw."/Uisu", $lotdata);
                }
                if ($findanti) {
                    return false;
                }
            }
        }
        foreach ($category['keys'] as $key => $qw) {
            $big = preg_match_all("/[А-Я]/Usu",$qw);
            if ($big) {
                $find = preg_match_all("/".$qw."/Usu", $lotdata);
            } else {
                $find = preg_match_all("/".$qw."/Uisu", $lotdata);
            }
            if ($find) {
                return true;
            }
        }
        return false;
    }
    public function process()
    {
        $categories = $this->get_categories();

        $lotdata = $this->merge_name($this->name, $this->descr);
        $catId = 0;
        foreach ($categories as $kk => $category) {
            $find = $this->catFind($category,$lotdata);
            if ($find) {
                $catId = $category['id'];
                break;
            }
        }
        return $catId;
    }
}