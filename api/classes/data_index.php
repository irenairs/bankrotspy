<?php
/**
 * bankrot-spy.ru
 * Author: Vladimir S. <guyasyou@gmail.com>
 * www.SiteCreate54.ru
 * © 2016
 */

class data_index {

    //Данные о таблице индекса
    const indexTableName = 'ds_maindata_index';
    const keyColumnName = 'lotID'; //primary, unique
    const contentColumnName = 'content'; //fulltext

    /**
     * Возвращает массив с lotID и контентом индекса
     * @param integer $lotID
     * @return array
     */
    public static function getIndexByLotID($lotID) {
        /** @var SQLResult $result */
        if (is_numeric($lotID)) {
            $result = core::$db->query('SELECT * FROM `'.self::indexTableName.'` WHERE `'.self::keyColumnName.'` = '.$lotID.';');
            if ($result->num_rows != 0) {
                return $result->fetch_assoc();
            }
        }
    }

    public static function fullReindex($offset = 0, $limit = 100) {
        /** @var SQLResult $result */
        $result = core::$db->query('SELECT `ds_maindata`.id FROM `ds_maindata` LIMIT '.$offset.','.$limit.';');
        if ($result->num_rows != 0) {
            $lotIDs = $result->fetch_all();
            foreach ($lotIDs as $lotID) {
                self::reindex($lotID['id']);
            }
        }
    }

    /**
     * Создает или обновляет индек лота по его номеру
     * @param integer $lotID
     */
    public static function reindex($lotID = 0) {
        if ($lotID && is_numeric($lotID)) {
            //Получаем данные лота
            $lotDataArray = self::getLotData($lotID);
            if ($lotDataArray) {
                //Склеиваем данные в одну строку
                $lotDataString = implode(' ', $lotDataArray);
                if (self::getIndexByLotID($lotID)) {
                    //Если индекс уже есть, обновляем его
                    self::update($lotID, $lotDataString);
                } else {
                    //Если индекса нет, создаем новую запись
                    self::create($lotID, $lotDataString);
                }
            }
        }
    }

    /**
     * Возвращает массив данных о лоте по номеру
     * @param integer $lotID
     * @return array
     */
    private static function getLotData($lotID) {
        /** @var SQLResult $result */
        $result = core::$db->query('
            SELECT
               `ds_maindata`.name,
               `ds_maindata`.description,
               `ds_maindata`.code,
               `ds_maindata`.case_number,
               `ds_maindata_regions`.name AS region,
               `ds_maindata_debtors`.dept_name,
               `ds_maindata_debtors`.inn AS dept_inn,
               `ds_maindata_organizers`.org_name,
               `ds_maindata_organizers`.inn AS org_inn
            FROM
               `ds_maindata`
                LEFT JOIN `ds_maindata_regions` ON `ds_maindata`.`place` = `ds_maindata_regions`.`number`
                LEFT JOIN `ds_maindata_debtors` ON `ds_maindata`.`debtor` = `ds_maindata_debtors`.`id`
                LEFT JOIN `ds_maindata_organizers` ON `ds_maindata`.`organizer` = `ds_maindata_organizers`.`id`
            WHERE `ds_maindata`.`id` = "'.$lotID.'"
        ;');
        if ($result->num_rows != 0) {
            return $result->fetch_assoc();
        }
    }

    /**
     * Создает новый индекс
     * @param integer $lotID
     * @param string $lotDataString
     */
    private static function create($lotID, $lotDataString) {
        core::$db->query('INSERT INTO `'.self::indexTableName.'` (`'.self::keyColumnName.'`, `'.self::contentColumnName.'`) VALUES ('.$lotID.', "'.core::$db->res($lotDataString).'");');
    }

    /**
     * Обновляет существующий индекс
     * @param integer $lotID
     * @param string $lotDataString
     */
    private static function update($lotID, $lotDataString) {
        core::$db->query('UPDATE `'.self::indexTableName.'` SET `'.self::contentColumnName.'`="'.core::$db->res($lotDataString).'" WHERE `'.self::keyColumnName.'`='.$lotID.';');
    }
}