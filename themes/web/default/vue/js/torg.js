    Object.defineProperty(window, 'Storage', {writable: false, value: {
            external: {},
            internal: {},
            local: {}
        }});

    Vue.use(VC, {
        url: {
            prefix: '/themes/web/default/vue/templates/',
            postfix: '.vue'
        },
        app: 'app-torg',
        vc: ['torg', 'msg', 'input-edit']
    });
