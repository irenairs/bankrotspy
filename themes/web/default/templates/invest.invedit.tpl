<table>
    <tr>
        <td valign="top">

            <?if ($error):?>
            <div class="error">
                <?foreach($error as $error): ?>
                <?=$error?><br/>
                <?endforeach?>
            </div>
            <?endif?>

            <form name="mess" action="<?=$home?>/invest/invedit?id=<?=$id?>" method="post" enctype="multipart/form-data">
                <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>

                <div class="content">
                    <div class="conthead"><h2><i class="icon-docs"></i> Редактирование актива</h2></div>
                    <table cellspacing = "0"><tr>
                  <td>
                  <div class="contbody_forms">
                        <b>Номер актива</b><br/>
                        <input type="text" name="number" value="<?=$number?>" />
                   </div>
                  <div class="contbody_forms">
                        <b>Актив</b><br/>
                        <input type="text" name="active" value="<?=$active?>" />
                   </div>
                     </td>
                    </tr></table>
                    <div class="contbody_forms">
                        <b>Сумма инвестиций, руб.</b><br/>
                        <input type="text" name="sum_invest" value="<?=$sum_invest?>" style="width:98%;"/>
                    </div>
                    <div class="contbody_forms">
                        <b>Срок инвестиций, мес.</b><br/>
                        <input type="text" name="time_invest" value="<?=$time_invest?>" style="width:98%;"/>
                    </div>
                    <div class="contbody_forms">
                        <b>Ориентир по доходности, %</b><br/>
                        <input type="text" name="profit_proc" value="<?=$profit_proc?>" style="width:98%;"/>
                    </div>
                    <div class="contbody_forms">
                        <b>Ответственный менеджер</b><br/>
                        <?=func::tagspanel('messarea');?>
                        <div class="texta"><textarea id="messarea" name="manager" rows="15"><?=$manager?></textarea></div>
                    </div>
                    <div class="contbody_forms">
                        <b>Ссылка на презентацию</b><br/>
                        <input type="text" name="present" value="<?=$present?>" style="width:98%;"/>
                    </div>
                    <div class="contfin_forms">
                        <input name="submit" type="submit" value="<?=lang('save')?>" />
                    </div>
                </div>
            </form>

        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню</div>
                <div class="elmenu"><a href="<?=$home?>/control/invest">Отмена</a></div>
                <div class="elmenu"><a href="<?=$home?>">На главную</a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>
<script type="text/javascript">
    var messagetext ="Данный псевдоним уже<br/> используется! <br/>Выберите другой псевдоним.";
    $(document).ready(function()
    {
        
        $("form[name='mess']").submit(
        
        function()
        {
        var res=0;
            var this_p = $(this);           
            $.ajax({
            url:"http://bankrot-spy.ru/verifyvalue.php",
            type:"POST",
            data:"entered_value_alias_old_page="+$("input[name = 'page_alias']").val()+"&id=<?=$id?>",
            async:false,
            success:function(data)
            {
                res=data;               
            }});
            if(res=="1") 
                {
                    $("input[name = 'page_alias']").val("");
                    EasyMessage.show("name","page_alias",messagetext);
                    return false;
                }
            
        }); 
            
            
        
        
        $("input[name = 'page_alias']").on('focusout',function()
        {var this_p = $(this);
        if(this_p.val()!="")
        {
            $.post("http://bankrot-spy.ru/verifyvalue.php",{entered_value_alias_old_page:this_p.val(),id:<?=$id?>},function(data)
            {
                if(data=="1") 
                {
                    this_p.val("");
                    EasyMessage.show("name","page_alias",messagetext);
                }
            });
        }
            
        }).on('focus',function()
        {
            
            EasyMessage.hide();
            
            
        });
    });

</script>