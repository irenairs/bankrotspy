<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2 class="icon-doc-inv">Справка</h2>
                </div>
                <div class="contbody_forms">
                    <a href="https://docs.google.com/document/d/1j3AclC0SLtxGmqK1OmgRUMp-bsitaO6WJ5wrIC0N5P4/edit">
                        Краткое описание движка bankrot-spy.ru, часть 1
                    </a><br/>
                    <a href="https://docs.google.com/document/d/1S9pdHruYQUyFw-zj_DEQ4bdu-XLkA73J7Vi4lZCww2Q/edit">
                        Описание движка Bankrot-Spy - БС (он же АС и КС), часть 2
                    </a>
                </div>
            </div>
        </td>
        <td class="right_back_menu">
            <? temp::include('control.index.right.tpl') ?>
        </td>
    </tr>
</table>
