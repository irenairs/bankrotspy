<table>
    <tr>
        <td valign="top">

            <?if ($error):?>
                <div class="error">
                    <?foreach($error as $error): ?>
                        <?=$error?><br/>
                    <?endforeach?>
                </div>
            <?endif?>

            <div class="content">
                <div class="conthead"><h2><i class="icon-newspaper"></i> <?=lang('edit_st')?></h2></div>

                <?if($preview):?>

                <div class="conthead" style="border-bottom: 1px dotted #E4E4E4;background: white">
                    <table>
                        <tr>
                            <td width="50px">
                                <a title="<?=lang('us_create')?> <?=$arr['user']?>"
                                   href="<?=$home?>/user/profile?id=<?=$arr['userid']?>"><img class="avatar"
                                                                                              src="<?=$arr['avatar']?>"/></a>
                            </td>
                            <td>
                                <b><?=$arr['name']?></b><br/>
                                <?if($arr['keys']):?>
                                <?$a=0;?>
                                <?foreach ($arr['keys'] AS $ot_key=>$ot_value):?>
                                <?=$ot_value?><?if(count($arr['keys']) != ($a+1)):?>,<?endif?>
                                <?$a++;?>
                                <?endforeach?>
                                <?endif?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contbodytext" style="border-bottom: 0px;">
                    <div class="image_resizer"><?=$arr['text']?></div>
                </div>
                <div class="contfintext"
                     style="border-top: 1px dotted #E4E4E4;border-bottom: 1px solid #E4E4E4;background: white">
                    <table>
                        <tr>
                            <td width="100%"></td>
                            <td title="<?=lang('stat_autor')?>"><a
                                        href="<?=$home?>/user/profile?id=<?=$arr['userid']?>"><i
                                            class="icon-user-1"></i><?=$arr['user']?></a></td>
                        </tr>
                    </table>
                </div>

                <div class="conthead">
                    <h2><i class="icon-newspaper"></i> Редактирование:</h2>
                </div>

                <?endif?>


                <form name="mess" action="<?=$home?>/articles/edit?id=<?=$id?>" method="post"
                      enctype="multipart/form-data">
                    <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>


                    <table cellspacing="0">
                        <tr>
                            <td>
                                <div class="contbody_forms">
                                    <b><?=lang('name_stat')?></b><br/>
                                    <input type="text" style="width: 350px" name="art_name" value="<?=$name?>"/>
                                </div>
                            </td>
                            <td align="right">
                                <?// Поле ввода адресного имени для ЧПУ адреса ?>
                                <div class="contbody_forms">
                                    <table>
                                        <tr>
                                            <td style="width:25%"></td>
                                            <td align="left" style="width:75%"><b><?=lang('alias_stat')?></b></td>
                                        <tr/>
                                        <tr>
                                            <td style="width:25%"></td>
                                            <td align="left" style="width:75%"><input type="text" class="input_class_in"
                                                                                      name="art_alias"
                                                                                      value="<?=$art_alias?>" style=""/>
                                            </td>
                                        <tr/>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div style="clear:both"></div>


                    <div class="contbody_forms">
                        <b><?=lang('data_opis')?></b><br/>
                        <?=func::tagspanel('messarea');?>
                        <div class="texta"><textarea id="messarea" name="art_text" rows="15"><?=$text?></textarea></div>
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('data_keyw')?></b><br/>
                        <div class="texta"><textarea name="art_keywords" rows="2"><?=$text_keys?></textarea></div>
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('data_description')?></b><br/>
                        <div class="texta"><textarea name="art_description" rows="2"><?=$description?></textarea></div>
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('pr_file')?></b><br/>
                        <input type="file" name="file"/>
                        <input type="submit" name="add_attachment" value="<?=lang('do_file')?>"/>
                    </div>

                    <?if($att_true):?>

                    <div class="contbody_forms">
                        <b><?=lang('pr_files_n')?></b><br/>
                        <?foreach($out as $data):?>
                        <hr/>
                        <i class="icon-attach"></i>
                        <input type="text"
                               value="[<?=$data['type']?>=<?=$data['filename']?>]<?=$data['name']?>[/<?=$data['type']?>]"/>
                        <input type="submit" name="del_attachment[<?=$data['id']?>]" value="<?=lang('del_th')?>"/>
                        <b><a target="_blank"
                              href="<?=$home?>/load/file<?=$data['id']?>/<?=$data['nameraw']?>"><?=$data['name']?></a></b>

                        <?endforeach?>

                    </div>
                    <?endif?>

                    <div class="contfin_forms">
                        <input name="submit" type="submit" value="<?=lang('save')?>"/>
                        <?if($preview):?>
                        <input name="preview" class="button_noright" type="submit" value="<?=lang('preview')?>"/><input
                                title="<?=lang('exitpreview')?>" class="button_noleft" name="exitpreview" type="submit"
                                value="X"/>
                        <?else:?>
                        <input name="preview" type="submit" value="<?=lang('preview')?>"/>
                        <?endif?>
                        <!--<?if($hide_stat):?>
                          <input name="public" type="submit" value="<?=lang('to_public')?>" />
                        <?endif?>-->
                    </div>

                </form>

            </div>
        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Статьи:</div>
                <? temp::include('articles._lang.head.tpl') ?>
                <div class="elmenu"><a href="<?=$home?>/articles/<?=$art_alias?>/"><?=lang('turn_back')?></a></div>
                <div class="down_rmenu"></div>
            </div>
        </td>
    </tr>
</table>
<script type="text/javascript">
    var messagetext = "Данный псевдоним уже<br/> используется! <br/>Выберите другой псевдоним.";
    $(document).ready(function () {
        $("form[name='mess']").submit(function () {
            var res = 0;
            var this_p = $(this);
            $.ajax({
                url: window.location.origin + "/verifyvalue.php",
                type: "POST",
                data: "entered_value_alias_old=" + $("input[name = 'art_alias']").val() + "&id=<?=$id?>",
                async: false,
                success: function (data) {
                    res = data;
                }
            });
            if (res == "1") {
                EasyMessage.show("name", "art_alias", messagetext);
                return false;
            }
        });

        $("input[name='art_alias']").on('focusout', function () {
            var this_p = $(this);
            if (this_p.val() != "") {
                $.post(window.location.origin + "/verifyvalue.php",{entered_value_alias_old:this_p.val(),id:<?=$id?>},function(data) {
                    if (data=="1") {
                        this_p.val("");
                        EasyMessage.show("name","art_alias",messagetext);
                    }
                });
            }
        }).on('focus', function () {
            EasyMessage.hide();
        });
    });
</script>