<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-cog-alt"></i> Платежи</h2>
                </div>
                
                <div class="contbody_forms" >
	<table><tr><td>
                    <form style="display:block;margin-bottom:20px;" method="get" id="searchForm">
                        <input type="text" placeholder="логин или email" name="search" value="<?=$search?>" style="height: 15px; width:120px;">
                        <!--input type="text" name="date" value="<?=$date?>" style="margin-left: 10px; margin-right: 10px;width:100px; height: 15px;"-->
                        <input class="button_no_top_index" type="submit" value="искать">
                    </form></td><td align="right">
 	 <form  action="/control/pays" style="display:block;margin-bottom:20px;" method="get" id="addPays_hohol">
                        <input type="text" placeholder="Имя" name="fio" style="height: 15px; width:110px;;">
                        <input type="text" placeholder="Сумма" name="summa" style="height: 15px; width:110px;;">
                        <input type="text" placeholder="Дата" name="date" autocomplete="off" style="height: 15px; width:110px;;">
                        <?=$tarrif_select?>
 <input type="hidden"  name="action" value="addPay">

                        <input class="button_no_top_index" type="submit" value="Добавить">
                    </form>
</td></tr></table>
                </div>
                <style>
                    b.bold-row {
                        margin-right: 15px;
                        font-size: 13px;
                    }
                </style>
                <div class="contbody_forms">
                    <?if($out):?>
                        <b class="bold-row">Транзакции: <?=$uall?> </b> 
                        <b class="bold-row">Подписки: <?=$clients_with?></b>
                        <b class="bold-row">Уникальные клиенты: <?=$clients?></b>
                        <b class="bold-row">Повт оплат: <?=$repays?> (<?= round( ($repays/$clients_with) * 100, 2)?>%)</b>
                        <b class="bold-row">Макс. повт. оплат: <?=$max_repays - 1?></b>
                        <b class="bold-row">Ср. вр. жизни клиента: <?=$avg_live?> дней</b>

                        <br/>
                    <?else:?>
                        <br/>Нет оплат<br/><br/>
                    <?endif?>
                </div>
            </div>

            <?if($out):?>
            <style>
                .pays_t tr:hover{ background:#f9f8f8;}
            </style>
                <?foreach($out as $key_year => $year): ?>

                <?if($year):?>
                <?foreach($year as $key_month => $month): ?>

                <div class="content">
                    <div class="conthead">
                        <h2><?=$marr[$key_month]?> <?=$key_year?></h2>
                    </div>
                    <div class="contbody_forms">
                        <table class="pays_t">

                        <?if($month):?>
                        <?foreach($month as $id => $data): ?>
                        <tr id="<?=$data['id']?>">
                            <td width="10" style="text-align:center; pading:0 3px;"><?= $id+1 ?></td>
                            <td><a class="icon-cancel" onclick="return confirmDelete(<?=$data['id']?>);" title="Удалить транзакцию."></a></td>
                            <td style="text-align:left;">
                                <?if($data['userid']):?>
                                    <b><a href="<?=$home?>/user/profile?id=<?=$data['userid']?>"><?=$data['username']?></a></b>
                                    <?if($data['mail']):?><br/><?=$data['mail']?><?endif?>
                                <?else:?>
                                <b><?=$data['username']?></b>
                                <?endif?>
                            </td>
                            <td style="text-align:left;"><?=$data['summ']?> р.</td>
                            <td style="text-align:left;"><?=$data['paidid']?></td>
                            <td style="text-align:left;"><?=$data['paytime']?></td>
                            <td style="text-align:left;"><?=$data['time']?></td>
                            <td style="text-align:left;"><?=$data['comm']?></td>
                        </tr>
                        <?endforeach?>
                        <tr style="border-top: 1px solid #dbd5d7;">
                            <td colspan="6"><b>Итого за месяц:</b> <b><?=$msumm[$key_year][$key_month]?> p.</b></td>
                        </tr>
                        <?endif?>
                        </table>
                    </div>
                </div>

                    <?endforeach?>
                <?endif?>
                <?endforeach?>

                <!--/div-->
            <?endif?>

            <?if($navigation):?><div class="navig"><?=$navigation?></div><?endif?>

        </td>
        <td class="right_back_menu">
            <? temp::include('control.index.right.tpl') ?>
        </td>
    </tr>
</table>

<script>
    $(document).ready(function() {
        $('[name="date"]').datepicker({
            language: 'ru-RU',
            dateFormat: 'dd.mm.yyyy'
        });
    });
    
    function confirmDelete( id ) {
	    if (confirm("Вы действительно хотите удалить транзакцию с ID="+id+"?")) {
	        $.get(
                "/control/paydelete",
                {
                    id: id
                },
                function(data) {
                    if ( data == 'ok' ) {
                        create_notify( "Транзакция с ID" + id + " успешно удалена." );
                        //$("#"+id).remove();
                        location.reload();
                    } else {
                        create_notify( data );
                    }
                }
            );
	    } else {
	        return false;
	    }
	}
</script>