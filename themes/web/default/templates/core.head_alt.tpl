<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">
<head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$title?></title>
    <meta name="keywords" content="<?=$keywords?>"/>
    <meta name="description" content="<?=$description?>"/>

    <? $now_id = 80; ?>

    <link rel="stylesheet" type="text/css" href="<?=$themepath?>/styles/style.css?id=<?=$now_id?>"/>
    <link rel="shortcut icon" href="<?=$themepath?>/images/favicon.ico"/>
    <link rel="apple-touch-icon" href="<?=$themepath?>/images/apple-touch-icon.png"/>

</head>
