<link rel="stylesheet" type="text/css" href="<?=$themepath?>/styles/card.css?id=<?=$now_id?>"/>
<div id="floatTip"></div>
<table>
    <tr>
        <td valign="top">
            <div in-fav="<?=$lotFav ? 'on' : 'off'?>" class="content">
                <div class="conthead">
                    <h2><i class="icon-newspaper"></i> Лот №<?=$data['nomeri']?></h2>
                </div>

                <div class="contbody_forms">
                    <b>Название лота:</b><br/>
                    <?=$data['name']?>
                    <hr style="margin: 7px 0"/>
                    <i class="icon-location"></i><b><?=$data['region']?></b>
                    &nbsp;&nbsp;&nbsp;
                    <span class="icon_to_click_fav">
                        <?if($lotFav):?>
                        <span cmd="fav|<?=$type?>|del|<?=$id?>" class="icon-star-clicked">Удалить лот из избранного</span>
                        <?else:?>
                        <span cmd="fav|<?=$type?>||<?=$id?>" class="icon-star-empty">Добавить лот в избранное</span>
                        <?endif?>
                    </span>
                    &nbsp;&nbsp;&nbsp;
                    <span class="icon_to_click_hide">
                        <?if($lotHide):?>
                        <span cmd="hide|<?=$type?>|del|<?=$id?>" class="icon-forward">Достать лот из мусора</span>
                        <?else:?>
                        <span cmd="hide|<?=$type?>||<?=$id?>" class="icon-delete">Отправить лот в мусор</span>
                        <?endif?>
                    </span>
                    &nbsp;&nbsp;&nbsp;
                </div>

                <? temp::include('block.notes.tpl') ?>

                <div class="contfin_forms_delimiter">

                </div>
                <div class="contbody_forms">
                    <table class="lottable">
                        <tr>
                            <td style="width: 300px;"><b>Тип имущества:</b><br/></td>
                            <td>Приватизация</td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Цена лота:</b><br/></td>
                            <td><i class="icon-rouble"></i><?=$data['price']?></td>
                        </tr>

                        <?if($data['cadastr_price']):?>
                        <tr>
                            <td style="width: 300px;"><b>Кадастровая стоимость:</b><br/></td>
                            <?
                                if(core::$all_rights[core::$rights]['market_price'] == 1){
                                ?>
                            <td><i class="icon-rouble"></i><?=$data['cadastr_price']?></a></td>
                            <?
                               } else { 
                                ?>
                                <td><i class="icon-rouble"></i><i class="fa fa-lock"></i></td>
                                <? } ?>
                        </tr>
                        <?endif?>

                        <?if($data['market_price']):?>
                        <tr>
                            <td style="width: 300px;"><b>Рыночная цена:</b><br/></td>
                            <?
                                if(core::$all_rights[core::$rights]['market_price'] == 1){
                                ?>
                            <td><i class="icon-rouble"></i><?=$data['market_price']?></a></td>
                            <?
                               } else { 
                                ?>
                                <td><i class="icon-rouble"></i><i class="fa fa-lock"></i></td>
                                <? } ?>
                        </tr>
                        <?endif?>

                        <tr>
                            <td style="width: 300px;"><b>Задаток:</b><br/></td>
                            <td><i class="icon-rouble"></i><?=$data['zadatok']?></td>
                        </tr>

                        <?if($data['average']):?>
                        <tr>
                            <td style="width: 300px;"><b>Средняя цена на открытом рынке:</b><br/></td>
                            <td><i class="icon-rouble"></i><a href="<?=$data['average_link']?>"><?=$data['average']?></a></td>
                        </tr>
                        <?endif?>
                        <tr>
                            <td style="width: 300px;"><b>Доход:</b><br/></td>
                            <td><i class="icon-rouble"></i><?=$data['profit_rub']?> (<?=$data['profit_proc']?>%)</td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Шаг аукциона:</b><br/></td>
                            <td><i class="icon-rouble"></i><?=$data['shagauction']?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Шаг понижения:</b><br/></td>
                            <td><i class="icon-rouble"></i><?=$data['shagponijenia']?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Валюта:</b><br/></td>
                            <td><?=$data['valuta']?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Обременение:</b><br/></td>
                            <td><?=$data['obremenenie']?></td>
                        </tr>
                    </table>
                    
                    <hr/>
                    <table class="lottable">
                        <tr>
                            <td style="width: 300px;"><b>Категория:</b><br/></td>
                            <td><?if($category):?><?=$data['tipi']?><?else:?>Прочее<?endif?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Вид собственности:</b><br/></td>
                            <td><?=$data['vidsob']?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Тип торгов:</b><br/></td>
                            <td><?=$data['tradetip']?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Статус торгов:</b><br/></td>
                            <td><?=$data['status']?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Дата начала подачи заявок:</b><br/></td>
                            <td><?=$data['start_time']?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Дата окончания подачи заявок:</b><br/></td>
                            <td><?=$data['end_time']?></td>
                        </tr>
                    </table>
                   
                    <hr/>
                    <table class="lottable">
                        <tr>
                            <td style="width: 300px;"><b>Организатор:</b><br/></td>
                            <td>
                                <i class="icon-globe-table"></i><?=$data['organizer']?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Решение:</b><br/></td>
                            <td>
                                <?=$data['reshenie']?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Адрес:</b><br/></td>
                            <td><?=$data['mesto']?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Ссылка на торги:</b><br/></td>
                            <td>
                            <?if($data['link']):?>
                                <a target="_blank" href="<?= $data['link'] ?>" onmouseover="toolTip('Если ссылка ведет на страницу с ошибкой, значит торги завершены досрочно. Сделайте поиск на площадке по коду торгов.')" onmouseout="toolTip()">
                                    <i class="icon-globe-table"></i>torgi.gov.ru
                                </a>
                            <?else:?>
                                <?=$noacc?>
                            <?endif?>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <table class="lottable">
                        <tr>
                            <td style="width: 300px;"><b>Дополнительная информация:</b><br/></td>
                            <td><?=$data['mestosrok']?></td>
                        </tr>
                    </table>

                    <? if(!empty($similarDataPrice) && $similarDataPrice !== 'access'): ?>
                    <table class="lottable">
                        <tr>
                        <td>
                            <div id="grapch"></div>
                        </td>
                        </tr>
                    </table>
                    <? elseif($similarDataPrice == 'access'): ?>
                    <table class="lottable">
                        <tr>
                        <td>
                            <div id="grapch"><center><strong style="font-size:18px;">
                                <? if($graphType == 1): ?>
                                    Цены на аналогичные товары
                                <? else: ?>
                                    Цена 1м2 аналогичных предложений
                                <? endif; ?>
                            </strong></center></div>
                        </td>
                        </tr>
                        <tr>
                            <td>
                            <center>Информация доступна на тарифном плане VIP</center>
                            </td>
                        </tr>
                    </table>
                    <? endif; ?>
                </div>
            </div>

        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню</div>
                <div class="elmenu"><a href="/privatized">На главную</a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>
<script>
    let engine_formid = <?= core::$formid ?>;
</script>

<? if(isset($similarDataPrice) && $similarDataPrice !== 'access'): ?>
<? temp::include('block.similar_prices.tpl') ?>
<? endif; ?>