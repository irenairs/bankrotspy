<div class="lots_stats">
    <div>Контакты АУ:
        <span> <?= $counts['withPhoneOrMail'] ?> (<?= $counts['withPhoneOrMailPr'] ?>%)</span>
    </div>
    <div>График понижения:
        <span> <?= $counts['withPriceSchedule'] ?> (<?= $counts['withPriceSchedulePr'] ?>%)</span>
    </div>
    <div>Фото:
        <span> <?= $counts['withPhoto'] ?> (<?= $counts['withPhotoPr'] ?>%)</span>
    </div>
    <div>Адреса:
        <span> <?= $counts['withAddress'] ?> (<?= $counts['withAddressPr'] ?>%)</span>
    </div>
    <div>Рыночных цен:
        <span> <?= $counts['mPrice'] ?> (<?= $counts['mPricePr'] ?>%)</span>
    </div>
    <div>Кадастровых цен:
        <span> <?= $counts['cPrice'] ?> (<?= $counts['cPricePr'] ?>%)</span>
    </div>
    <div>Активных лотов:
        <span> <?= $counts['lotAct'] ?></span>
    </div>
    <div>Время МСК:
        <span> <?= date("H:i") ?></span>
    </div>
</div>
