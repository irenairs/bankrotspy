<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-key"></i>Тарифы</h2>
                    <a href="<?= core::$home ?>/accountstate/create" class="button"  style="float: right; margin-top: -25px;" target="_blank">
                        Выписать счет для юрлица
                    </a>
                </div>
                <div class="contbody_forms">
                   <?=$text?>
                </div>

            </div>
            <section class="our-tariffs" id="tariff">
                <div class="wrapper-center">

                    <div class="t-tariffs third-table">
                        <div class="head">Доступ к базе торгов</div>
                        <? 
                        $table_text = false;
                        ?>
                        <div class="table-price">
                            <div class="item">
                                <div class="price"><span><?=$rmenu[1]['price_source']?></span> руб</div>
                                <div class="time">1 месяц</div>
                                <div class="sum">(<?=$rmenu[1]['price_source']?> в месяц)</div>
                                <? if (!empty($rmenu[1]['subtext'])): $table_text = true ?> <?endif?> 
                                <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[1]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[1]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[1]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[1]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[1]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[1]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[1]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[1]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[1]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[1]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[1]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[1]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                            <div class="item">
                                <div class="price"><span><?=$rmenu[2]['price_source']?></span> руб</div>
                                <div class="time">3 месяца</div>
                                <div class="sum">(<?=round($rmenu[2]['price_source']/3)?> в месяц)</div>
                                <? if (!empty($rmenu[2]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[2]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[2]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[2]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[2]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[2]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[2]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[2]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[2]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[2]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[2]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[2]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[2]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                            <div class="item">
                                <div class="price"><span><?=$rmenu[16]['price_source']?></span> руб</div>
                                <div class="time">6 месяцев</div>
                                <div class="sum">(<?=round($rmenu[16]['price_source']/6)?> в месяц)</div>
                                <? if (!empty($rmenu[16]['subtext'])): $table_text = true ?> <?endif?> 
                                <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[16]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[16]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[16]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[16]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[16]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[16]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[16]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[16]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[16]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[16]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[16]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[16]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                            <div class="item">
                                <div class="price"><span><?=$rmenu[3]['price_source']?></span> руб</div>
                                <div class="time">12 месяцев</div>
                                <div class="sum">(<?=round($rmenu[3]['price_source']/12)?> в месяц)</div>
                                <? if (!empty($rmenu[3]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[3]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[3]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[3]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[3]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[3]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[3]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[3]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[3]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[3]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[3]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[3]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[3]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                            <div class="item">
                                <div class="price"><span><?=$rmenu[6]['price_source']?></span> руб</div>
                                <div class="time">1 день</div>
                                <? if (!empty($rmenu[6]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[6]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[6]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[6]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[6]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[6]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[6]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[6]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[6]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[6]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[6]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[6]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[6]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                        </div>
                        <? if($table_text): ?>
                        <div class="column-text">
                            <div class="col">
                                <? if(!empty($rmenu[1]['subtext'])): ?>
                                <p><i></i><?=$rmenu[1]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[2]['subtext'])): ?>
                                <p><i></i><?=$rmenu[2]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[16]['subtext'])): ?>
                                <p><i></i><?=$rmenu[16]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[3]['subtext'])): ?>
                                <p><i></i><?=$rmenu[3]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[6]['subtext'])): ?>
                                <p><i></i><?=$rmenu[6]['subtext']?></p>
                                <? endif ?>
                            </div>
                        </div>
                        <? endif ?>
                        <div class="column-text" style="padding: 30px">
                            <?=$rmenu[1]['spec_descr']?>
                        </div>
                    </div>

                    <div class="t-tariffs third-table">
                        <div class="head">VIP доступ к базе торгов</div>
                        <? 
                            $table_text = false;
                        ?>
                        <div class="table-price">
                            <div class="item">
                                <div class="price"><span><?=$rmenu[4]['price_source']?></span> руб</div>
                                <div class="time">1 месяц</div>
                                <div class="sum">(<?=$rmenu[4]['price_source']?> в месяц)</div>
                                <? if (!empty($rmenu[4]['subtext'])): $table_text = true ?> <?endif?> 
                                <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[4]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[4]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[4]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[4]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[4]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[4]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[4]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[4]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[4]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[4]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[4]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[4]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                            <div class="item">
                                <div class="price"><span><?=$rmenu[17]['price_source']?></span> руб</div>
                                <div class="time">3 месяца</div>
                                <div class="sum">(<?=round($rmenu[17]['price_source']/3)?> в месяц)</div>
                                <? if (!empty($rmenu[17]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[17]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[17]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[17]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[17]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[17]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[17]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[17]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[17]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[17]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[17]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[17]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[17]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                            <div class="item">
                                <div class="price"><span><?=$rmenu[18]['price_source']?></span> руб</div>
                                <div class="time">6 месяцев</div>
                                <div class="sum">(<?=round($rmenu[18]['price_source']/6)?> в месяц)</div>
                                <? if (!empty($rmenu[18]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[18]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[18]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[18]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[18]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[18]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[18]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[18]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[18]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[18]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[18]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[18]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[18]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                            <div class="item">
                                <div class="price"><span><?=$rmenu[8]['price_source']?></span> руб</div>
                                <div class="time">12 месяцев</div>
                                <div class="sum">(<?=round($rmenu[8]['price_source']/12)?> в месяц)</div>
                                <? if (!empty($rmenu[8]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[8]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[8]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[8]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[8]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[8]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[8]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[8]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[8]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[8]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[8]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[8]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[8]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                             <div class="item">
                                <div class="price"><span><?=$rmenu[27]['price_source']?></span> руб</div>
                                <div class="time">1 день</div>
                                <div class="sum">(<?=round($rmenu[27]['price_source'])?> в день)</div>
                                <? if (!empty($rmenu[27]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[27]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[27]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[27]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[27]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[27]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[27]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[27]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[27]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[27]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[27]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[27]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[27]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                        </div>
                        <? if($table_text): ?>
                        <div class="column-text">
                            <div class="col">
                                <? if(!empty($rmenu[4]['subtext'])): ?>
                                <p><i></i><?=$rmenu[4]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[17]['subtext'])): ?>
                                <p><i></i><?=$rmenu[17]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[18]['subtext'])): ?>
                                <p><i></i><?=$rmenu[18]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[8]['subtext'])): ?>
                                <p><i></i><?=$rmenu[8]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[27]['subtext'])): ?>
                                <p><i></i><?=$rmenu[27]['subtext']?></p>
                                <? endif ?>
                            </div>
                        </div> 
                        <? endif ?>
                        <div class="column-text" style="padding: 30px">
                            <?=$rmenu[4]['spec_descr']?>
                        </div>
                    </div>
                    
                    <div class="t-tariffs third-table" id="help">
                        <div class="head">Помощь в торгах, рассылки и реклама</div>
                        <? 
                            $table_text = false;
                        ?>
                        <div class="table-price">
                            <div class="item">
                                <div class="title"><?=$rmenu[19]['name']?></div>
                                <div class="price"><span><?=$rmenu[19]['price_source']?></span> руб</div>
                                <? if (!empty($rmenu[19]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[19]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[19]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[19]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[19]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[19]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[19]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[19]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[19]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[19]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[19]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[19]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[19]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                            <div class="item">
                                <div class="title"><?=$rmenu[15]['name']?></div>
                                <div class="price"><span><?=$rmenu[15]['price_source']?></span> руб</div>
                                <? if (!empty($rmenu[15]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[15]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[15]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[15]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[15]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[15]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[15]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[15]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[15]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[15]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[15]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[15]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[15]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                            <div class="item">
                                <div class="title"><?=$rmenu[10]['name']?></div>
                                <div class="price"><span><?=$rmenu[10]['price_source']?></span> руб</div>
                                <? if (!empty($rmenu[10]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[10]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[10]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[10]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[10]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[10]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[10]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[10]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[10]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[10]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[10]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[10]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[10]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                            <div class="item">
                                <div class="title"><?=$rmenu[21]['name']?></div>
                                <div class="price"><span><?=$rmenu[21]['price_source']?></span> руб</div>
                                <? if (!empty($rmenu[21]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[21]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[21]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[21]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[21]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[21]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[21]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[21]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[21]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[21]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[21]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[21]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[21]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
							<div class="item">
                                <div class="title"><?=$rmenu[11]['name']?></div>
                                <div class="price"><span><?=$rmenu[11]['price_source']?></span> руб</div>
                                <? if (!empty($rmenu[11]['subtext'])): $table_text = true ?> <?endif?> 
                                 <? if( core::$user_id ):?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[11]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[11]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[11]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[11]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[11]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[11]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="PC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-blue-button" type="submit" value="Оплатить: яндекс деньги">
                                </form>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu[11]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu[11]['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu[11]['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu[11]['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu[11]['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu[11]['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <input type="hidden" name="paymentType" value="AC">
                                    <!--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>-->
                                    <input style="width: 230px;" class="g-green-button" type="submit" value="Оплатить: банковская карта">
                                </form>
                                <? else:?>
                                <a class="g-green-button green" style="line-height: normal; bottom: 45px; padding: 5px 15px;" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                                <? endif?>
                            </div>
                        </div>
                        <? if($table_text): ?>
                        <div class="column-text">
                            <div class="col">
                                <? if(!empty($rmenu[19]['subtext'])): ?>
                                <p><i></i><?=$rmenu[19]['subtext']?></p>
                                <? endif ?>
                            </div>
                            
                            <div class="col">
                                <? if(!empty($rmenu[15]['subtext'])): ?>
                                <p><i></i><?=$rmenu[15]['subtext']?></p>
                                <? endif ?>
                                <!--<p><i></i>Помощь в выборе ликвидного имущества (до 3-х вариантов) по заданным критериям</p><br>
                                <p><i></i>Помощь в получении допуска к торгам: оформление заявки + перечисление задатка</p><br>
                                <p><i></i>Помощь в правильной подготовке и подаче заявки на электронных торгах</p><br>-->
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[10]['subtext'])): ?>
                                <p><i></i><?=$rmenu[10]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[21]['subtext'])): ?>
                                <p><i></i><?=$rmenu[21]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[11]['subtext'])): ?>
                                <p><i></i><?=$rmenu[11]['subtext']?></p>
                                <? endif ?>
                            </div>
                         </div>
                        <? endif ?>
                        <div class="column-text" style="padding: 30px">
                            <?=$rmenu[19]['spec_descr']?>
                        </div>
					
                   <!-- <? if($table_text): ?>
                        <div class="column-text">
                            <div class="col">
                                <? if(!empty($rmenu[20]['subtext'])): ?>
                                <p><i></i><?=$rmenu[20]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[22]['subtext'])): ?>
                                <p><i></i><?=$rmenu[22]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[23]['subtext'])): ?>
                                <p><i></i><?=$rmenu[23]['subtext']?></p>
                                <? endif ?>
                            </div>
                            <div class="col">
                                <? if(!empty($rmenu[24]['subtext'])): ?>
                                <p><i></i><?=$rmenu[24]['subtext']?></p>
                                <? endif ?>
                            </div>
                        </div>
                        <? endif ?>
                        <div class="column-text" style="padding: 30px">
                            <?=$rmenu[20]['spec_descr']?>
                        </div>-->
                    </div>
                    


                </div>
            </section>


            <!--<? if($rmenu): ?>
            <? foreach($rmenu as $rmenu): ?>
                <? if(intval($rmenu['price_source']) < 10 && core::$rights < 100): ?>
                    <? continue; ?>
                <? endif; ?>
                <div class="tarbody" id="tariff_<?=$rmenu['id']?>">
                    <table >
                        <tr>
                            <td valign="top" width="100%">
                                <h2 class="tarhead"><i class="icon-briefcase"></i> <?=$rmenu['name']?></h2>
                                <span class="costpt">Стоимость подписки: <?=$rmenu['price_source']?> руб.</span>
                                <span class="undertartext"><?=$rmenu['subtext']?></span>
                            </td>
                            <td valign="top"><img height="40px" src="<?=$themepath?>/images/ym.png"/></td>
                        </tr>
                    </table>
                    
                    <div class="button_div">
                        <hr style="margin-bottom: 13px" />
                        <? if( core::$user_id ):?>
                            <? if( $rmenu['rights'] == 10 && core::$rights == 11 ):?>
                                <p>У вас подписка ВИП.</p>
                            <? else: ?>
                                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                                    <input type="hidden" name="receiver" value="410011048401080">
                                    <input type="hidden" name="formcomment" value="<?=$rmenu['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="short-dest" value="<?=$rmenu['name']?> bankrot-spy.ru">
                                    <input type="hidden" name="label" value="<?=$rmenu['order']?>">
                                    <input type="hidden" name="quickpay-form" value="shop">
                                    <input type="hidden" name="targets" value="<?=$rmenu['name']?>">
                                    <input type="hidden" name="sum2" value="<?=$rmenu['price_source']?>" data-type="number">
                                    <input type="hidden" name="sum" value="<?= $rmenu['price']?>" >
                                    <input type="hidden" name="need-fio" value="true">
                                    <input type="hidden" name="need-email" value="true">
                                    <input type="hidden" name="need-phone" value="false">
                                    <input type="hidden" name="need-address" value="false">
                                    <label><input type="radio" name="paymentType" value="PC">Яндекс.Деньгами</label>
                                    <label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>
                                    <input type="submit" value="Оплатить">
                                </form>
                            <? endif; ?>
                        <? else: ?>
                            <a style="margin-left: 0px;margin-top:7px;padding-left: 15px;padding-right: 15px;" class="urlbutton_index" href="<?=$home?>/user/register" >Зарегистрироваться и оплатить</a>
                        <? endif; ?>
                    </div>
                </div>
            <?endforeach?>

            <?else:?>
                <!--<div class="content">
                    <div class="contbody_forms">Нет ни одного пункта меню.</div>
                </div>-->
            <!--<?endif?>-->

        </td>
    </tr>
</table>
<?if($tt):?>
<table id="tarif_table">
        <tbody><tr class="head">
            <td></td>
            <td class="t_name gray_bg">Бесплатный</td>
            <td></td>
            <td class="t_name green_bg">Простой</td>
            <td></td>
            <td class="t_name blue_bg">VIP-доступ</td>
        </tr>
        <?foreach($tt as $tt): ?>
            <?if($tt['free'] == 0 && $tt['plain'] == 0 && $tt['vip'] == 0):?>
            <!--<tr></tr>-->
            <? else: ?>
            <tr>
                <td>
                    <?=$tt['text']?> 
                    <?if(!empty($tt['link'])):?>
                    <a href="<?=$tt['link']?>"><?=$tt['name_link']?></a>
                    <? endif ?>
                </td>
                <td class="icon">
                <? if($tt['free']==1): ?>    
                    <img src="/images/check.png">
                <? else: ?>
                    <img src="/images/line.png">
                <? endif ?>
                </td>
                <td></td>
                <td class="icon">
                <? if($tt['plain']==1): ?>    
                    <img src="/images/check.png">
                <? else: ?>
                    <img src="/images/line.png">
                <? endif ?>
                </td>
                <td></td>
                <td class="icon">
                <? if($tt['vip']==1): ?>    
                    <img src="/images/check.png">
                <? else: ?>
                    <img src="/images/line.png">
                <? endif ?>
                </td>
            </tr>
            <?  endif ?>
        <? endforeach ?>            
        <tr class="prices no_bg">
            <td></td>
            <td style="border-top: 1px solid #BBBBBB"></td>
            <td></td>
            <td style="border-top: 1px solid #BBBBBB"></td>
            <td></td>
            <td style="border-top: 1px solid #BBBBBB"></td>
        </tr>
    </tbody>
</table>
<? endif ?>


<style>
label {margin-right:10px;}
label input {
    margin-right:5px;
}
</style>
<script>
$(function(){
    $('body').on('change', 'input[name=paymentType]', function(){
        var type = $(this).val();
        var form = $(this).parent().parent();
        var sum = $(form).find('input[name="sum2"]').val();

        if (type == 'PC') {
            sum = sum * '1.006';
            sum = sum.toPrecision(5);
            $(form).find('input[name=sum]').val(sum);
        }
        
        if (type == 'AC') {
            sum = sum * '1.021';
            sum = sum.toPrecision(5);
            $(form).find('input[name=sum]').val(sum);
        }
    });
});
</script>

<script>
    var curr_item = false
    function choiceTariff(num) {
        if (curr_item) {
            curr_item.style.background = "#fff";
        }
        var item = document.getElementById("tariff_"+num);
        item.style.background = "rgba(167, 241, 176, 0.36)";
        curr_item = item;
    };
</script>






<!-- футер -->
            </td>
        </tr>
    </table>
</div>


            <div class="mainundall"></div>
<div class="prebottom">
    <table>
        <tr>
            <td style="width: 100%;padding-top: 10px;padding-left: 13px;">
                <?if($production):?>
                <script async type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                <div style="display: inline-block; margin-right: 5px;" class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,gplus" data-yashareTheme="counter" data-yashareImage="<?=$themepath?>/images/apple-touch-icon.png"></div>
                <?endif?>
                <? temp::include('block.bottom.links.tpl') ?>
            </td>

            <td style="min-width: 210px;padding-top: 10px;text-align: right;padding-right: 17px;">
                <!--<span style="color: #888888"><?=round((microtime(true) - rem::get('microtime'))*1000, 0)?>ms, sql:<?=$sql_count?></span>-->
                <a class="user_onl_bottom" href="<?=$home?>/user/online"><i class="icon-user-male-bottom"></i> <?=$onl_all?></a>
            </td>
        </tr>
    </table>
</div>
<? temp::include('button_go_top.tpl') ?>            
<div class="bottom">
    <table>
        <tr>
            <td> Информация на данном сайте предоставляется "КАК ЕСТЬ" и предназначена только для ознакомительных целей без каких-либо гарантий и передачи прав. Любое использование информации в иных целях запрещено и производится на ваш страх и риск. Все права защищены.
            </td>
            <td>	   
            </td>
        </tr>
    </table>
</div>


<script type="text/javascript">

    //Эти 3 строки ничего страшного не делают
    //Нужно просто для системы защиты
    $(document).ready(function() {
        setInterval('connection_keeper()',30000);
    });

    function set_normal_height()
    {
        if(device.mobile() == false)
        {
          var height = $(window).height();
          var prebottom_h = $('div.prebottom').height();
          var bottom_h = $('div.bottom').height();
          var allhead_h = $('table.allhead').height();
          height = height - bottom_h - prebottom_h - allhead_h - 28;
          $('td.main').height(height);
        }
    }

    set_normal_height();

    $(window).resize(function(event) {
      set_normal_height();
      correct_images();
    });

    //учитываем скроллбар
    if(!get_scroll('Height') && ($('body').width() - $('.all_content').width() > 20)) {
        $('body').css({'margin-right' : scrollWidth()});
    }
</script>

<!-- Google Code for YOUTUBE_BS #1 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 993818846;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "i2zXCOWe_GMQ3vHx2QM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/993818846/?label=i2zXCOWe_GMQ3vHx2QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</div>
</div>
</body>
</html>