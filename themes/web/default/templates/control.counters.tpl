<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-cog-alt"></i><?=lang('counters')?></h2>
                </div>
                
                <div class="contbody_forms">
                    Счетчики пользовательских переходов на основные страницы сайта
                </div>
                <style>
                    b.bold-row {
                        margin-right: 15px;
                        font-size: 13px;
                    }
                </style>
            </div>

            <?if($out):?>
                <?foreach($out as $key_year => $year): ?>

                <?if($year):?>
                <?foreach($year as $key_month => $month): ?>

                <div class="content">
                    <div class="conthead">
                        <h2><?=$marr[$key_month]?> <?=$key_year?></h2>
                    </div>
                    <div class="contbody_forms">
                        
                        <table class="counters_t">
                        
                        <?foreach($month as $id => $data): ?>

                        <tr id="id">
                            <td style="font-weight:bold; pading:0 3px;"><?=$parr[$data['page']][0]?></td>
                            <td style="text-align:left;"><b><a href="<?=$parr[$data['page']][1]?>"><?=$parr[$data['page']][1]?></a></b></td>
                            <td style="text-align:right;"><strong>Всего: </strong><?=$data['cnt']?></td>
                        </tr>
                        <?endforeach?>
                        </table>
                    </div>
                </div>

                <?endforeach?>
                <?endif?>
                <?endforeach?>



            <?endif?>


        </td>
        <td class="right_back_menu">
            <? temp::include('control.index.right.tpl') ?>
        </td>
    </tr>
</table>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">

<style>
    .counters_t td {
        border-bottom:1px solid #eee;
    }

    .counters_t tr:hover{
        background:#f9f8f8;
    }
    
    .counters_t td{
        padding: 10px 0;
    }

    .counters_t tr strong {
        color: #626262;
    }

</style>
