<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead"><h2><i class="icon-list"></i>Тест фильтра</h2></div>
                <div class="contbody_forms"  style="overflow: hidden;">
                  <div style="float: left; margin-right: 30px;">
                    <form  method="get" enctype="multipart/form-data">
                      <label style="display: block; ">Показать</label>
                        <label for="f">Только отличающиеся</label>
                        <input type="checkbox" id="f" <?= (isset($_GET['f']) && $_GET['f']?'checked="checked"':'') ?> name="f" value="1">
                            <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                        <input type="hidden" name="p" value="<?= $off ?>">
                        <input name="submit" type="submit" value="Показать">                  
                    </form> 
                  </div>
                    <div style="float: left; ">
                      <form  method="POST" enctype="multipart/form-data">
                          <label style="display: block;">Обновить лоты</label>
                          <select name="update" style="width:120px"   >
                                  <option value="<?= time()-60*60*24*30 ?>">За 30 дней</option>
                                  <option value="<?= time()-60*60*24*90 ?>">За 90 дней</option>
                                  <option value="1">Все</option>
                                  
                          </select>
                          <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                          <input name="submit" type="submit" value="Обновить лоты">                  
                      </form> 
                    </div>
                    <div style="float: right;">
                        <form  method="get" id="testkey" action="/control/keystest" enctype="multipart/form-data">
                            <label for="key" style="display: block;">Тестировать ключ</label>
                            <input type="text" name="key" style="width:200px" id="test-key" placeholder="Введите ключ">
                            <select name="cat" id="testcat" style="width:120px"   >
                                <?foreach ($cats as $key => $cat): ?>
                                    <option value="<?= $cat['id']?>"><?= $cat['name']?></option>
                                <?endforeach ?>
                                
                            </select>
                            <input type="hidden" name="p" value="<?= $off ?>">
                            <?foreach ($cats as $key => $cat): ?>
                                <input type="hidden" name="old[<?= $cat['id']?>]" value="<?= 
                                implode('_+_',$cat['all']) ?>">
                            <?endforeach ?>
                            
                            <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                            <input name="submit" type="submit" value="Тестировать">                  
                        </form> 
                    </div>
                </div>
                <div class="contbody_forms" id="tested">
                    <div id="testeditems"></div>
                    <div id="savekeydiv" style="text-align:right; display:none">
                        <form id="savekey">
                            <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                            <input type="hidden" name="cat" id="savecat" value="">
                            <input type="hidden" name="key" id="savekeytext" value="">
                            <input id="savekey" name="submit" type="submit" value="Сохранить ключ">
                        </form>
                    </div>
                </div>
                <div class="contbody_forms">
                    <?foreach ($cats as $key => $cat): ?>
                    <?= '<h2><a href="#" data-id="'.$key.'" class="show-cat">'.$cat['name'].' ('.count($cat['items']).')</a></h2><br>' ?>
                        <div id="cat-<?= $key ?>" style="display: none;">
                            <?foreach ($cat['items'] as $key => $item): ?>
                                <?= $revent[$item].'<br><br>' ?>
                            <?endforeach ?>
                        </div>
                    <?endforeach ?>
                </div>
                <div class="contbody_forms">
                    <h2><a href="#" data-id="all" class="show-cat">Нераспределено (<?= count($data) ?>)</a> </h2>
                    <div id="cat-all" style="display: none;">
                        <?= implode('<br><br>', $data) ?>
                    </div>
                </div>
                <div class="contbody_forms" style="text-align: center">
                    <?if ($off): ?>
                    <a href="/control/keysfix?p=0"<?= '&f='.$filtr ?>><< Первая</a>
                    <a href="/control/keysfix?p=<?= $off-1 ?><?= '&f='.$filtr ?>">< Предыдущая</a>
                    <?endif?>
                    <span><b><?= $off*1000+1 ?>-<?= ($off+1)*1000 ?></b></span>
                    <?if ($off !== $pages): ?>
                    <a href="/control/keysfix?p=<?= $off+1 ?><?= '&f='.$filtr ?>">Следущая ></a>
                    <a href="/control/keysfix?p=<?= $pages ?><?= '&f='.$filtr ?>">Последняя >></a>

                    <?endif?>
                </div>
            </div>                           
        </td>
        <td class="right_back_menu">
            <? temp::include('control.categories.right.tpl') ?>
        </td>
    </tr>
</table>

<!-- футер -->
            </td>
        </tr>
    </table>
</div>


            <div class="mainundall"></div>
<div class="prebottom">
    <table>
        <tr>
            <td style="width: 100%;padding-top: 10px;padding-left: 13px;">
                <?if($production):?>
                <script async type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                <div style="display: inline-block; margin-right: 5px;" class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,gplus" data-yashareTheme="counter" data-yashareImage="<?=$themepath?>/images/apple-touch-icon.png"></div>
                <?endif?>
                <? temp::include('block.bottom.links.tpl') ?>
            </td>

            <td style="min-width: 210px;padding-top: 10px;text-align: right;padding-right: 17px;">
                <!--<span style="color: #888888"><?=round((microtime(true) - rem::get('microtime'))*1000, 0)?>ms, sql:<?=$sql_count?></span>-->
                <a class="user_onl_bottom" href="<?=$home?>/user/online"><i class="icon-user-male-bottom"></i> <?=$onl_all?></a>
            </td>
        </tr>
    </table>
</div>
<div class="bottom">
    <table>
        <tr>
            <td> Информация на данном сайте предоставляется "КАК ЕСТЬ" и предназначена только для ознакомительных целей без каких-либо гарантий и передачи прав. Любое использование информации в иных целях запрещено и производится на ваш страх и риск. Все права защищены.
            </td>
            <td>
                <?if($production):?>
                <!-- Yandex.Metrika counter --> <script src="https://mc.yandex.ru/metrika/watch.js" type="text/javascript"></script> <script type="text/javascript" > try { var yaCounter30118844 = new Ya.Metrika({ id:30118844, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } </script> <noscript><div><img src="https://mc.yandex.ru/watch/30118844" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
                <!-- Global site tag (gtag.js) - Google Analytics -->
                <script async src="https://www.googletagmanager.com/gtag/js?id=UA-39694496-4"></script>
                <script>
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', 'UA-39694496-4');
                </script>
                <?endif?>
            </td>
        </tr>
    </table>
</div>


<script type="text/javascript">
    $(document).ready(function() {
       $('body').on('click', '.show-cat', function(event) {
           event.preventDefault();
           id = $(this).attr('data-id');
           $('#cat-'+id).toggle('fast');
           
           /* Act on the event */
           return false;
       });
       $oldj = [];
       $('body').on('submit', '#testkey', function(event) {
           event.preventDefault();
           $('#testeditems').html('');
           if ($('#test-key').val()) {
               
               $.post('/control/keystest', $(this).serialize(), function(data, textStatus, xhr) {
                //console.log(JSON.stringify($oldj));
                $.each(data, function(index, val) {
                    $('#testeditems').prepend(val);
                });
                $('#savekeytext').val($('#test-key').val());
                $('#savecat').val($('#testcat').val());
                $('#savekeydiv').show();
               }, "json");
           } else {
                $('#tested').append('<div style="color:red"><b>Введите ключ</b></div>');
           }
           return false;
       });
       $('body').on('submit', '#savekey', function(event) {
           event.preventDefault();
           $.post('/control/keyssave', $(this).serialize(), function(data, textStatus, xhr) {
                $('#savekeydiv').prepend(data.message);
               console.log(data);
           }, "json");
           
           return false;
       });
    });

</script>
<script type="text/javascript">

    //Эти 3 строки ничего страшного не делают
    //Нужно просто для системы защиты
    $(document).ready(function() {
        setInterval('connection_keeper()',30000);
    });

    function set_normal_height()
    {
        if(device.mobile() == false)
        {
          var height = $(window).height();
          var prebottom_h = $('div.prebottom').height();
          var bottom_h = $('div.bottom').height();
          var allhead_h = $('table.allhead').height();
          height = height - bottom_h - prebottom_h - allhead_h - 28;
          $('td.main').height(height);
        }
    }

    set_normal_height();

    $(window).resize(function(event) {
      set_normal_height();
      correct_images();
    });

    //учитываем скроллбар
    if(!get_scroll('Height') && ($('body').width() - $('.all_content').width() > 20)) {
        $('body').css({'margin-right' : scrollWidth()});
    }
</script>

<!-- Google Code for YOUTUBE_BS #1 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 993818846;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "i2zXCOWe_GMQ3vHx2QM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/993818846/?label=i2zXCOWe_GMQ3vHx2QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</div>
</div>
</body>
</html>