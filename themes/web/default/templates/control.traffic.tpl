<link rel="stylesheet" href="<?=$themepath?>/styles/popline.css" type="text/css">
<style>
    .counters_t td {
        border-bottom:1px solid #eee;
    }

    .counters_t tr:hover{
        background:#f9f8f8;
    }
    
    .counters_t td{
        padding: 10px 0;
    }

    .counters_t tr strong {
        color: #626262;
    }
    
    .listPages{
        position: absolute;
        display: none;
        background: #f9f8f8;
        padding: 10px;
        border: #e4e4e4 solid 1px;
        z-index: 500;
        overflow: auto;
        max-height: 200px; 
        max-width: 250px;
    }
    b.bold-row {
        margin-right: 15px;
        font-size: 13px;
    }
    .modal-window{
        position: fixed;
        /*top: 212px;*/
        width: 350px;
        left: 33%;
       /* margin-left: -100px;*/
        z-index: 10000;
   }

    .backdor{
        position: fixed;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        z-index: 9999;
        display: none;
        background-color: rgba(0,0,0,.5);
    }
                       

</style>
    <div class="backdor" onclick="$('#blocking_warning').fadeOut();$('.backdor').fadeOut();"></div>
    <div class="modal-window" style="display: none" id="blocking_warning">
        <div class="close_modal_img_editlot" onclick="$('#blocking_warning').fadeOut();$('.backdor').fadeOut();"><span id="icon_close_butt"><i class="icon-cancel-circled"></i></span></div>
        <div>
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-cog-alt"></i>Предупреждение</h2>
                </div>
                <div class="contbody_forms forms_blocks">
                    <div class="contbody_forms_blocks">
                        
                        <b>Вы точно хотите заблокировать пользователя
                        <span style="color: #d27600;" id="loginBlockUser"></span>?</b>
                        
                    </div>
                </div>             
                <div class="contfin_forms">
                    <input id="save_blocking" type="button" value="Заблокировать">
                    <input style="float: right;" type="button" onclick="$('#blocking_warning').fadeOut();$('.backdor').fadeOut();" value="Отмена">
                </div>
            </div>             
        </div>
    </div>
<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-cog-alt"></i><?=lang('traffic')?></h2>
                
                </div>
                
                <div class="contbody_forms">
                    Статистика c <?=date('d.m.Y', $dateStart)?> по <?=date('d.m.Y', $dateEnd)?>
                </div>
               
            </div>
            <?foreach($array_for_traffic as $day => $rows): ?>
            <div class="content">
                    <div class="conthead">
                        <h2><?=$day?></h2>
                    </div>
                    <div class="contbody_forms">
                    
            
                <table class="counters_t">
                <? $i=1;?>    
                <?foreach($rows as $key => $trafic): ?>
                    <tr id="<?=$key+1?>">
                            <td style="font-weight:bold; pading:0 3px;"><?=$i?></td>
                            <? if($trafic['user_id'] == 0): ?>
                                <td style="text-align:left;"><b>Гость</b></td>
                            <? else: ?>
                                <td style="text-align:left;"><b><a href='<?=$home?>/user/profile?id=<?=$trafic['user_id']?>'><?=$trafic['login']?></a></b></td>
                            <?endif?>
                            <td style="text-align:left;"><?=$trafic['mail']?></td>
                            <td style="text-align:left;"><?=str_replace('.', '', $trafic['user_ip'])?></td>
                            <td style="text-align:left;"><?=$trafic['total']?></td>
                            <td>
                                <strong style="cursor: pointer" class="pagesListOpen">Страницы </strong>
                                <div class="listPages">
                                    <table>
                                        <?foreach($trafic['pages'] as $page): ?>
                                        <tr>
                                            <td class="tdList" style="border-right:  1px solid #eee; padding-right: 5px">
                                                <?=$page['page']?>
                                            </td> 
                                            <td class="tdList" style="padding-left: 5px; padding-right: 15px"><?=$page['count']?></td>
                                        </tr>
                                        <?endforeach?>
                                    </table>
                                </div>    
                            </td>
                            <td>
                                <? if($trafic['user_id'] != 0 AND $trafic['rights'] != -1): ?>
                                    <a href="javascript:{}" onclick="changeStatus('<?=$trafic['user_id']?>', '<?=$trafic['login']?>')" class="button block<?=$trafic['user_id']?>" style="float: right;" target="_blank">Заблокировать</a>
                                <? endif?>
                                
                                <? if($trafic['user_id'] != 0 AND $trafic['rights'] == -1): ?>
                                    <span style="float: right;">Заблокирован</span>
                                <? endif?> 
                            </td>    
                        </tr>
                        <? $i++;?>
                        <?endforeach?>
                        
                        </table>
                    </div>
                </div>
                <?endforeach?>

            
        </td>
         <td class="right_back_menu">
            <? temp::include('control.index.right.tpl') ?>
        </td>
    </tr>
    
</table> 
<script>
    
    $(".pagesListOpen").mouseover(function () {
        $(this).next().show();
    });
    /*$(".pagesListOpen").mouseover(function () {
        $(this).next().show();
    })*/
    $(".pagesListOpen").mouseleave(function (event) {
        console.log(event.toElement);
        if(event.toElement.className != "listPages" && event.toElement.className != "tdList"){
            $(this).next().hide();
        }    
    });
    $(".listPages").mouseleave(function(){
        $(this).hide();
    });
    
    function changeStatus(id, login ){
        //var userId = $("input[name*='user_id_"+id+"']").val();
        var rightId = -1;
        $('#blocking_warning').fadeIn();
        $('.backdor').fadeIn();
        $('#loginBlockUser').text(login);
        $('#save_blocking').unbind('click');
        //alert(rightId);
        $('#save_blocking').click(function(){
            $.ajax({
                type: 'GET',
                url: '/user/chstatus',
                data: {'user_id': id, 'right_id': rightId},
                success: function(result){
                    //alert(result);
                    create_notify(result);
                    $('.block'+id).parent().append('<span style="float: right;">Заблокирован</span>');
                    $('.block'+id).remove();
                    $('#blocking_warning').fadeOut();
                    $('.backdor').fadeOut();
                }
            });
            return false;
        });    
    }
</script>

<script type="text/javascript" src="<?=$themepath?>/js/jquery.popline.js"></script>
<script type="text/javascript" src="<?=$themepath?>/js/jquery.popline.social.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#lotdescr").popline({mode: 'view'});
    });
</script>