<div class="right_panel_conf">
    <div class="menu_rt">Админпанель:</div>
    <div class="elmenu"><a href="<?=$home?>/control"><?=lang('base_settings')?></a></div>
    <div class="elmenu"><a href="<?=$home?>/control/mail">Рассылка писем</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/menu"><?=lang('menu_settings')?></a></div>
    <div class="elmenu"><a href="<?=$home?>/control/pnews">Новости площадок</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/rights">Группы и доступ</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/pages">Страницы</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/keys">Инструменты для работы с лотами</a></div>
    <div class="elmenu"><a href="<?=$home?>/parserstat">Инструменты для работы с парсерами</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/tset">Настройки тарифов и оплаты</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/pays">Платежи</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/allusers">Пользователи сайта</a></div>
    <div class="elmenu"><a href="<?=$home?>/zayavka?stat">Заявки для Орг. Торг</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/requests">Заявки на ЭЦП/КЦП</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/xls">Экспорт в xls</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/invest">Список активов</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/interview">Опрос для юзеров</a></div>
    <div class="elmenu"><a href="<?=$home?>/accountstate/adminindex">Выписанные счета</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/counters">Счетчики посещений</a></div>
    <div class="elmenu"><a href="<?=$home?>/control/traffic">Трафик пользователей</a></div>
    <div class="elmenu"><a href="/control/refersum">Реферальная программа</a></div>
    <div class="elmenu"><a href="/control/ad">Рекламные кампании</a></div>
    <div class="elmenu"><a href="/control/help">Справка</a></div>
    <div class="down_rmenu"> </div>
</div>