<table>
    <tr>
        <td valign="top">

<div class="content">
    <div class="conthead">
        <h2><i class="icon-cog-alt"></i> Опросы для юзеров</h2>
    </div>

    <?if($rmenu):?>

    <?foreach($rmenu as $rmenu): ?>
    <div class="contbody_forms">
        <table>
            <tr>
              <td style="width: 30px;"><i class="icon-docs"></i></td>
              <td>
                  <?= $rmenu['name'] ?>
              </td>
                <td style="text-align: left">
                    Ссылка: <a target="_blank" href="http://<?= $_SERVER['HTTP_HOST'] ?>/interview?id=<?= $rmenu['id'] ?>">http://<?=  $_SERVER['HTTP_HOST'] ?>/interview?id=<?= $rmenu['id'] ?></a>
                </td>
                <td style="text-align: left">
                    Дата: <?= date("H:i d.m.Y", strtotime($rmenu['data'])); ?>
                </td>
                <td class="cont_act"><a title="Редактировать опрос" href="<?=$home?>/control/interview/edit?id=<?= $rmenu['id'] ?>"><i class="icon-edit"></i></a></td>
                <td class="cont_act"><a title="Удалить опрос" href="<?=$home?>/control/interview/delete?id=<?=$rmenu['id']?>"><i class="icon-delete"></i></a></td>
            </tr>
        </table>
    </div>
    <?endforeach?>

    <?else:?>
        <div class="contbody_forms">Нет ни одного опроса.</div>
    <?endif?>

    <div class="contfin">
        <a class="urlbutton" href="<?=$home?>/control/interview/add">Создать опрос</a>
    </div>
</div>

        </td>
        <td class="right_back_menu">
            <? temp::include('control.index.right.tpl') ?>
        </td>
    </tr>
</table>
