<table>
    <tr>
        <td valign="top">

<div class="content">
    <div class="conthead">
        <h2><i class="icon-cog-alt"></i> Настройки тарифов</h2>
    </div>

    <?if($rmenu):?>

    <?foreach($rmenu as $rmenu): ?>
    <div class="contbody_forms">
        <table>
            <tr>
              <td valign="top" style="width: 30px;padding: 4px 0 4px 0"><i class="icon-list"></i></td>
              <td valign="top" style="text-align: center; width: 50px;">
                  <b>ID</b><br/>
                  <hr style="margin: 3px 6px 3px 8px"/>
                  <?=$rmenu['id']?>
              </td>
              <td>
                  <b><?=$rmenu['name']?></b> (<?=$rmenu['price']?> руб.)<br/>
                  <hr style="margin: 3px 6px 3px 8px"/>
                  Доступ: <?=$rmenu['longtime']?><br/>
                  Период: <?=$rmenu['typetime']?><br/>
                  <span style="font-size: 13px;color: #b1aea8;"><?=$rmenu['subtext']?></span>
              </td>
              <td valign="top" style="text-align: center; width: 150px;">
                  
                  <b>Посадочная страница</b><br/>
                  <hr style="margin: 3px 6px 3px 8px"/>
                  <input onchange="check_page(this, '<?=$rmenu['id']?>');" type="checkbox" name="page_main" <?=($rmenu['page_main'] == 1) ? 'checked="checked"' : ''?>>
                  
              </td>
              <td valign="top" style="text-align: center; width: 150px;">
                  <b>Страница тарифов</b><br/>
                  <hr style="margin: 3px 6px 3px 8px"/>
                  <input type="checkbox" onchange="check_page(this, '<?=$rmenu['id']?>');" name="page_tariff" <?=($rmenu['page_tariff'] == 1) ? 'checked="checked"' : ''?>>
                  
              </td>    
                <td class="cont_act"><a title="Редактировать" href="<?=$home?>/control/tarifedit?id=<?=$rmenu['id']?>"><i class="icon-edit"></i></a></td>
                <td class="cont_act"><a title="Удалить" href="<?=$home?>/control/tarifdel?id=<?=$rmenu['id']?>"><i class="icon-delete"></i></a></td>
            </tr>
        </table>
    </div>
    <?endforeach?>

    <?else:?>
        <div class="contbody_forms">Нет ни одного тарифа.</div>
    <?endif?>

    <div class="contfin">
        <a class="urlbutton" href="<?=$home?>/control/newtarif">Новый тариф</a>
    </div>
</div>
            <style>
                .tabletarif td{
                    text-align: center;
                }
                .tabletarif textarea{
                    margin: 8px;
                }
            </style>
            <?if($tt):?>

    
            <form name="tabletarif" action="<?=$home?>/control/tset" method="post">
                <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                <div style="margin-top: 29px;" class="content">
                    <div class="conthead">
                        <h2><i class="icon-cog-alt"></i> Настройки сравнительной таблицы</h2>
                    </div>
                    <div>
                        <table class="tabletarif">
                            <tr>
                                <th>Текст (описание)</th>
                                <th>Ссылка</th>
                                <th>Имя ссылки</th>
                                <th>Очередь</th>
                                <th>Бесплатный</th>
                                <th>Простой</th>
                                <th>VIP</th>
                            </tr>
                            <?foreach($tt as $tt): ?>
                            <tr>
                                <input name="id[]" type="hidden" value="<?=$tt['id']?>">
                                <td><textarea cols="20" name="text[<?=$tt['id']?>][]"><?=$tt['text']?></textarea></td>
                                <td><input name="link[<?=$tt['id']?>][]" type="text" value="<?=$tt['link']?>"></td>
                                <td><input style="width:150px;" name="name_link[<?=$tt['id']?>][]" type="text" value="<?=$tt['name_link']?>"></td>
                                <td><input style="width:25px;" name="turn[<?=$tt['id']?>][]" type="text" value="<?=$tt['turn']?>"></td>
                                <td><input name="free[<?=$tt['id']?>][]" type="checkbox" value="<?=$tt['free']?>" <?=$tt['free']==1 ? 'checked': '' ?> ></td>
                                <td><input name="plain[<?=$tt['id']?>][]" type="checkbox" value="<?=$tt['plain']?>" <?=$tt['plain']==1 ? 'checked': '' ?>></td>
                                <td><input name="vip[<?=$tt['id']?>][]" type="checkbox" value="<?=$tt['vip']?>" <?=$tt['vip']==1 ? 'checked': '' ?>></td>
                            </tr>
                            <?endforeach?>
                        </table>
                    </div>
                    
                    <div class="contfin_forms">
                        <input name="edittabletarif" type="submit" value="Сохранить" />
                    </div>
                </div>
                
            </form>
            <?endif?>
            
            <form name="newtabletarif" action="<?=$home?>/control/tset" method="post">
                <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                <div style="margin-top: 29px;" class="content">
                    <div class="conthead">
                        <h2><i class="icon-cog-alt"></i> Новый тариф в таблице сравнения</h2>
                    </div>
                    <div>
                        <table class="tabletarif">
                            <tr>
                                <th>Текст (описание)</th>
                                <th>Ссылка</th>
                                <th>Имя ссылки</th>
                                <th>Очередь</th>
                                <th>Бесплатный</th>
                                <th>Простой</th>
                                <th>VIP</th>
                            </tr>
                            <tr>
                                <td><textarea cols="20" name="text"></textarea></td>
                                <td><input name="link" type="text" value=""></td>
                                <td><input style="width:150px;" name="name_link" type="text" value=""></td>
                                <td><input style="width:25px;" name="turn" type="text" value=""></td>
                                <td><input name="free" type="checkbox" value=""></td>
                                <td><input name="plain" type="checkbox" value=""></td>
                                <td><input name="vip" type="checkbox" value=""></td>
                            </tr>
                        </table>
                    </div>
                    
                    <div class="contfin_forms">
                        <input name="newtabletarif" type="submit" value="Сохранить" />
                    </div>
                </div>
                
            </form>

            <form name="mess" action="<?=$home?>/control/tset" method="post">
                <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                <div style="margin-top: 29px;" class="content">
                    <div class="conthead">
                        <h2><i class="icon-cog-alt"></i> Настройки системы оплаты (ПСКБ)</h2>
                    </div>
                    <div class="contbody_forms">
                        <b>Ключ системы оплаты:</b><br/>
                        <input style="width: 460px" type="text" name="merchant_key" value="<?=$merchant_key?>" />
                    </div>
                    <div class="contbody_forms">
                        <b>ID магазина:</b><br/>
                        <input type="text" name="market_id" value="<?=$market_id?>" />
                    </div>
                    <div class="contbody_forms">
                        <b>Префикс:</b> (4 буквы!)<br/>
                        <input type="text" name="prefix" value="<?=$market_prefix?>" />
                    </div>
                    <div class="contbody_forms">
                        <?=func::tagspanel('messarea');?>
                        <div class="texta"><textarea id="messarea" name="mess" rows="4"><?=$text?></textarea></div>
                    </div>
                    <div class="contfin_forms">
                        <input name="submit" type="submit" value="Сохранить" />
                    </div>
                </div>
            </form>

        </td>
        <td class="right_back_menu">
            <? temp::include('control.index.right.tpl') ?>
        </td>
    </tr>
</table>
<script>
    function check_page(input, id){
        var page = $(input).attr('name');
        var check = 0;
        if($(input).prop('checked')){
            check = 1;
        }
        
        $.ajax({
                type: 'GET',
                url: '/control/tset',
                data: {'id': id, 'page_update': page, 'check': check},
                success: function(result){
                   
                    create_notify(result);
                   
                }
            });
    }
</script>    
