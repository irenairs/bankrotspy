<div id="<?=$mapColumns['mode']?>-oper-cont">
    <?foreach($mapColumns['on'] as $group):?>
    <div class="check-group">
        <div class="check-group-name"><?=$group['title']?></div>
        <?foreach($group['items'] as $item):?>
        <div class="column-check-cont">
            <label>
                <input cmd="<?=$mapColumns['mode']?>|check" type="checkbox" name="column" value="<?=$item['name']?>"<?=$item['ch'] ? ' checked' : ''?><?=$item['def'] ? ' def' : ''?>>
                <span><?=$item['title']?></span>
            </label>
        </div>
        <?endforeach?>
    </div>
    <?endforeach?>
    <?foreach($mapColumns['btn'] as $btn):?>
    <div cmd="<?=$btn['cmd']?>" prm="<?=$btn['prm']?>" class="btn urlbutton_index button_no_top_index"><?=$btn['title']?></div>
    <?endforeach?>
</div>
<?if(isset($mapColumns['off']['reg'])):?>
<div class="columns-blocked-cont">
    Колонки
    <?foreach($mapColumns['off']['reg'] as $column):?>
    <div><?=$column?></div>
    <?endforeach?>
    будут доступны после <a href="/login">авторизации</a> или <a href="/user/register">регистрации</a>
</div>
<?endif?>
<?if(isset($mapColumns['off']['pay'])):?>
<div class="columns-blocked-cont">
    Колонки
    <?foreach($mapColumns['off']['pay'] as $column):?>
    <div><?=$column?></div>
    <?endforeach?>
    будут доступны после оплаты <a href="/tariffs">базового доступа</a>
</div>
<?endif?>
<?if(isset($mapColumns['off']['vip'])):?>
<div class="columns-blocked-cont">
    Колонки
    <?foreach($mapColumns['off']['vip'] as $column):?>
    <div><?=$column?></div>
    <?endforeach?>
    будут доступны после оплаты <a href="/tariffs">VIP доступа</a>
</div>
<?endif?>
