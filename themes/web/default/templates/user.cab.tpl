<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-user-male"></i> Личный кабинет</h2>
                </div>
                <div class="contbody_forms">
                    <table>
                        <tr>
                            <td style="width: 60px;">
                                <a href="<?=$home?>/user/profile?id=<?=$user_prof?>"><img class="avatar" src="<?=$avatar?>"/></a>
                            </td>
                            <td>
                                <div class="ank">
                                    <b>Статус: </b> <span class="status"><?=$rights?></span><hr style="margin: 4px 0 6px 0"/>
                                    <b><?=lang('now')?></b>  <?if($online):?><span class="us_on"> <?=lang('lang_on_anc')?></span><?else:?><span class="us_off"> <?=lang('lang_off_anc')?></span><?endif?>

                                </div>
                            </td>
                        </tr>
                    </table>

                </div>
                <div class="contbody_forms">
                    <table class="lottable">
                        <tr>
                            <td style="width: 180px;"><b><span class="user_mark">Тарифный план: </span></b><br/></td>
                            <td><?if($tariff):?><b><?=$tariff?></b><?else:?>нет<?endif?></td>
                        </tr>
                        <tr>
                            <td style="width: 180px;"><b><span class="user_mark">Срок окончания: </span></b><br/></td>
                            <td><?=$desttime ? $desttime : 'нет' ?> </td>
                        </tr>
                        <tr>
                            <td style="width: 180px;"><b><span class="user_mark">E-mail для оповещений: </span></b><br/></td>
                            <td>
                                <?if(core::$user_mail):?>
                                <?=core::$user_mail ?> <span style="font-size: 13px;color: #8c8989;">(hidden)</span>
                                <?else:?>
                                -
                                <?endif?>
                            </td>
                        </tr>
                    </table>

                </div>
                <div class="contfin_forms">
                    <form name="mess" action="<?=$home?>/exit" method="POST">
                        <input type="hidden" name="act" value="do"/>
                        <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                        <input type="submit" value="<?=lang('exit')?>" />
                    </form>
                </div>
            </div>

            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-cog-alt"></i> Платежи</h2>
                </div>

                <div class="contbody_forms" style="padding-left: 0;padding-right: 0;">

                    <?if($out):?>
                    <?foreach($out as $key_year => $year): ?>

                    <?if($year):?>
                    <?foreach($year as $key_month => $month): ?>


                        <div class="conthead">
                            <h2><?=$marr[$key_month]?> <?=$key_year?></h2>
                        </div>
                        <div class="contbody_forms">
                            <style>
                                .pays_t td {
                                    /*border-bottom:1px solid #eee;*/
                                }
                                .pays_t tr:hover{
                                    background:#f9f8f8;
                                }
                            </style>
                            <table class="pays_t">

                                <?if($month):?>
                                <?foreach($month as $id => $data): ?>
                                <tr id="<?=$data['id']?>">
                                    <td width="10" style="text-align:center; pading:0 3px;"><?= $id+1 ?></td>
                                    <td width="10"><a class="icon-cancel" onclick="return confirmDelete(<?=$data['id']?>);" title="Удалить транзакцию."></a></td>
                                    <td width="50" style="text-align:left;"><b><a href="<?=$home?>/user/profile?id=<?=$data['userid']?>"><?=$data['username']?></a></b></td>
                                    <td width="50" style="text-align:left;"><?=$data['summ']?> р.</td>
                                    <td width="160" style="text-align:left;"><?=$data['paidid']?></td>
                                    <td width="100" style="text-align:left;"><?=$data['paytime']?></td>
                                    <td width="100" style="text-align:left;"><?=$data['time']?></td>
                                    <td width="200" style="text-align:left;"><?=$data['comm']?></td>
                                </tr>
                                <?endforeach?>
                                <?endif?>
                            </table>
                        </div>


                    <?endforeach?>
                    <?endif?>
                    <?endforeach?>

                    <!--/div-->
                    <?endif?>


                </div>

            </div>

            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-users"></i> Реферальная программа</h2>
                </div>
                <div class="contbody_forms">
                    Вы можете участвовать в реферальной программе и получать дополнительных доход
                    в размере 25% от выручки привлеченных пользователей.
                    Просто скопируйте и разместите ссылку на ваших сайтах (ресурсах).
                    Выплаты по реферальной программе происходят ежемесячно, не позднее 15 числа следующего месяца,
                    после отчетного. Вопросы по реферальной программе можно задать по эл.почте:
                    <a href="mailto:sales@i-tt.ru">sales@i-tt.ru</a>
                </div>
                <div class="contbody_forms">
                    <b>Ваша ссылка для привлечения пользователей на сайт</b>
                    <div style="float: right"><?=$refer['link']?></div>
                </div>
                <div class="contbody_forms">
                    <b>Количество пользователей, которые зашли на сайт по вашей ссылке</b>
                    <div style="float: right"><?=isset($refer['enter']) ? $refer['enter'] : 0?></div>
                </div>
                <div class="contbody_forms">
                    <b>Количество пользователей, которые зарегистрировались на сайте</b>
                    <div style="float: right"><?=isset($refer['reg']) ? $refer['reg'] : 0?></div>
                </div>
                <div class="contbody_forms">
                    <b>Количество пользователей, которые сделали хотя бы одну оплату на сайте</b>
                    <div style="float: right"><?=isset($refer['pay']) ? $refer['pay'] : 0?></div>
                </div>
                <div class="contbody_forms">
                    <b>Выведено в рамках реферальной программы</b>
                    <div style="float: right"><?=round($refer['summary']['paidOut'], 2)?> руб.</div>
                </div>
                <div class="contbody_forms">
                    <b>Доступно к выводу сейчас</b>
                    <div style="float: right"><?=round($refer['summary']['toPayPrev'], 2)?> руб.</div>
                </div>
                <?if($refer['curWait']):?>
                <div class="contbody_forms">
                    <b>Будет доступно к выводу после <?=$refer['curWait']?></b>
                    <div style="float: right"><?=round($refer['summary']['toPayCur'], 2)?> руб.</div>
                </div>
                <?endif?>
                <div class="contbody_forms">
                    <b>Будет доступно к выводу после <?=$refer['nextWait']?></b>
                    <div style="float: right"><?=round($refer['summary']['toPayNext'], 2)?> руб.</div>
                </div>
            </div>

            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-search"></i> Поисковые профили</h2>
                </div>
                <div class="contbody_forms">
                    <b>Поисковые профили</b> - это инструмент для отслеживания большого количества лотов по заданным поисковым параметрам. Более подробное описание в разделе <a href="<?=$home?>/pages/1">Помощь</a>.
                </div>
                <?if($outprofiles):?>

                <?foreach($outprofiles as $rmenu): ?>
                <div class="contbody_forms">
                    <table>
                        <tr>
                            <td valign="top" style="width: 30px;padding-top: 6px"><i class="icon-list"></i></td>
                            <td>
                                <b><?=$rmenu['name']?></b> (<?=($rmenu['fresh'] ? '+' : '') . $rmenu['fresh']?>)<br/>
                                <a href="<?=$home?>/user/loadrofile?id=<?=$rmenu['id']?>">Загрузить профиль</a>
                            </td>
                            <?if($rmenu['can_edit']):?>
                                <?if($rmenu['subscribe'] == 1):?>
                                    <td class="cont_act"><a class="unsubscribe_button " title="Отписаться от рассылки" href="<?=$home?>/user/unsubscribeprofile?id=<?=$rmenu['id']?>">Отписаться</a></td>
                                <?else:?>
                                    <td class="cont_act"><a class="subscribe_button" title="Подписаться на рассылку" href="<?=$home?>/user/subscribeprofile?id=<?=$rmenu['id']?>">Подписаться</a></td>
                                <?endif?>
                                <td class="cont_act"><a title="Редактировать" href="<?=$home?>/user/renameprofile?id=<?=$rmenu['id']?>"><i class="icon-edit"></i></a></td>
                                <td class="cont_act"><a title="Удалить" href="<?=$home?>/user/deleteprofile?id=<?=$rmenu['id']?>"><i class="icon-delete"></i></a></td>
                            <?endif?>
                        </tr>
                    </table>
                </div>
                <?endforeach?>

                <?else:?>
                <div class="contbody_forms" style="padding-top: 20px;padding-bottom: 20px;">Нет ни одного поискового профиля.</div>
                <?endif?>
                <div class="contfin_forms">
                    <a href="<?=$home?>/user/newprofile" class="urlbutton_index">Создать новый</a>
                </div>
            </div>





        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню:</div>
                <div class="elmenu"><a href="<?=$home?>/user/profile">Мой профиль</a></div>
                <?if($change_nick):?>
                    <div class="elmenu"><a href="<?=$home?>/user/chnick"> Изменить никнейм</a></div>
                <?endif?>
                <?if($change_pass):?>
                    <div class="elmenu"><a href="<?=$home?>/user/chpass"><?=lang('ch_pass')?></a></div>
                <?endif?>
                <div class="elmenu"><a href="<?=$home?>/user/chmail"><?=lang('ch_mail')?></a></div>
				<div class="elmenu"><a href="<?=$home?>/zayavka?dann">Заявка на участие в торгах</a></div>
                <div class="elmenu"><a href="<?=$home?>/accountstate">Выписка счета для юрлица</a></div>
                <div class="elmenu"><a href="<?=$home?>">На главную</a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>
<style>
    .subscribe_button, .unsubscribe_button {
        display: block;
        padding: 2px 10px;
        text-decoration: none;
        color:#fff;
        font-size: 12px;
        font-weight: bold;
        border-radius: 6px;
        margin-right: 7px;
        width: 90px;
        text-align: center;
    }
    .subscribe_button:hover, .unsubscribe_button:hover {
        color:#fff;
        border: none;
        text-decoration: none;
    }
    .subscribe_button {
        background: #319118;
    }
    .unsubscribe_button {
        background: #B00C0C;
    }
    .subscribe_button:hover {
        background: #0E9E0E;
    }
    .unsubscribe_button:hover {
        background: red;
    }
</style>