<table>
    <tr>
        <td valign="top">
            <?if ($error):?>
            <div class="error">
                <?foreach($error as $error): ?>
                <?=$error?><br/>
                <?endforeach?>
            </div>
            <?endif?>

            <div class="content">
                <div class="conthead">
                    <h2><?=lang('changenick')?></h2>
                </div>
                <?if($text):?>
                <div class="contbody_forms">
                    <?=$text?>
                </div>
                <?endif?>
                <form action="?act=save" method="post">
                    <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                    <div class="contbody_forms">
                        <b><?=lang('nick')?></b><br/>
                        <span class="under"><?=lang('nick_und')?></span><br/>
                        <input type="text" name="nick" value="<?=$nick?>" />
                    </div>
                    <div class="contfin_forms">
                        <input type="submit" value="<?=lang('save')?>" />
                </form>
            </div>
        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню:</div>
                <div class="elmenu"><a href="<?=$home?>/user/profile?id=<?=$user_prof?>">Отмена</a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>
