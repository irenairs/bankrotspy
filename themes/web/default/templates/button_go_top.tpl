<style>
#buttonGoTop {
	position: fixed;
	z-index: 10;
    right: 15px;
    bottom: -100px;;
    
    height: 68px;
    width: 68px;
    text-align: center;
    
	
	-webkit-transition: bottom 1s ease-out 0.5s;
    -moz-transition: bottom 1s ease-out 0.5s;
    -o-transition: bottom 1s ease-out 0.5s;
    transition: bottom 1s ease-out 0.5s;
}

#buttonGoTop.active{
	bottom: 50px;
}

#buttonGoTop:hover{
    cursor: pointer;
}
	
</style>  

<div id="buttonGoTop" title="Вверх">
    <img src="/images/up.png" alt="Вверх" width="68px"/>
</div>
<script>
    function getButtonRigth(){
        var WW = $("body").width();
        var BW = $(".all_content").width();
        var diff = (WW - BW) / 2;
        $("#buttonGoTop").css("right", (diff+35) + "px");
    }
    $(document).ready(function(){
        getButtonRigth();
        $(document).scroll(function(){
            
            if($(document).scrollTop() > 200){
                $("#buttonGoTop").addClass("active");
            } else {
                $("#buttonGoTop").removeClass("active");
            }
        });
        $(window).resize(function(){
            getButtonRigth();
        });
        $('#buttonGoTop').click(function() {
            $('html, body').animate({scrollTop: 0},500);
            return false;
          })
    });
</script>