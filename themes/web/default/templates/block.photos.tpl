<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
<div class="contbody_forms photo-cont">
    <?foreach($photoArr as $item):?>
    <a data-fancybox="gallery" href="<?=$item?>"><img src="<?=$item?>"></a>
    <?endforeach?>
</div>
