<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-cog-alt"></i> Редактирование опроса</h2>
                </div>
                <div class="contbody_forms">

                </div>
                <form action="<?=$home?>/control/interview/edit" method="post">
                    <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                    <input type="hidden" name="id" value="<?= $data['id'] ?>" />
                    <div class="contbody_forms">
                        <b>Название опроса</b><br/>
                        <input type="text" style="width: 98%" name="name" required placeholder="Название опроса" value="<?= $data['name'] ?>">
                    </div>
                    <div class="contbody_forms">
                        <b>Дата создания</b><br/>
                        <input type="text" disabled name="data" required placeholder="Дата создания" value="<?= date('H:i d.m.Y', strtotime($data['data'])) ?>">
                    </div>

                    <? $n=1; foreach ($answer as $ans){ ?>
                    <div class="contbody_forms">
                        <b>Ответ №<?= $n ?></b><br/>
                        <input type="text" name="question[]" placeholder="Вариант ответа" value="<?= $ans['answer'] ?>">
                    </div>
                    <? $n++; } ?>
                    <div class="contbody_forms">
                        <a href="javascript:;" data-role="add-row" data-number="<?= $n-1; ?>">Добавить еще ответ</a>
                    </div>

                    <div class="contbody_forms">
                        <b>Состояние</b><br/>
                        <label>
                            <input type="radio" name="status" value="1" <?= ($data['status'] == 1) ? 'checked="checked"' : ''?>>
                            Активный
                        </label>
                        <label>
                            <input type="radio" name="status" value="0" <?= ($data['status'] == 0) ? 'checked="checked"' : ''?>>
                            Не активный
                        </label>
                    </div>

                    <div class="contbody_forms">
                        <input type="submit" value="Сохранить" name="edit">
                    </div>
                </form>
                <div class="conthead">
                    <h2><i class="icon-cog-alt"></i> Результаты</h2>
                </div>
                <? foreach ($answer2 as $ans){ ?>
                <div class="contbody_forms">
                    <label><?= $ans['name'] ?></label><br/>
                    <progress max="<?= $max ?>" value="<?= $ans['count'] ?>"></progress> (<?= $ans['count'] ?>)
                </div>
                <? } ?>
            </div>
        </td>
        <td class="right_back_menu">
            <? temp::include('control.index.right.tpl') ?>
        </td>
    </tr>
</table>
<script>
    $(function(){
        $("a[data-role=add-row]").click(function (e) {
            e.preventDefault();
            var number=Number($(this).attr('data-number'));
            $(this).attr('data-number', number+1);
            $(this).closest('.contbody_forms').before('<div class="contbody_forms"><b>Ответ №'+(number+1)+'</b><br/><input type="text" name="question[]" placeholder="Вариант ответа" value="<?= $mail['subject'] ?>"></div>');
        });
    });
</script>
<style>

</style>