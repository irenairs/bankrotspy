<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-cog-alt"></i> Создание опроса</h2>
                </div>
                <div class="contbody_forms">

                </div>
                <form action="<?=$home?>/control/interview/add" method="post">
                    <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                    <input type="hidden" name="id" value="<?= $mail['id'] ?>" />
                    <div class="contbody_forms">
                        <b>Название опроса</b><br/>
                        <input type="text"  style="width: 98%" name="name" required placeholder="Название опроса" value="">
                    </div>
                    <div class="contbody_forms">
                        <b>Ответ №1</b><br/>
                        <input type="text" name="question[]" placeholder="Вариант ответа" value="">
                    </div>

                    <div class="contbody_forms">
                        <a href="javascript:;" data-role="add-row" data-number="1">Добавить еще ответ</a>
                    </div>

                    <div class="contbody_forms">
                        <b>Состояние</b><br/>
                        <label>
                            <input type="radio" name="status" value="1" <?= ($_SESSION['opr']['name'] == 1) ? 'checked="checked"' : ''?>>
                            Активный
                        </label>
                        <label>
                            <input type="radio" name="status" value="0" <?= ($_SESSION['opr']['name'] == 0) ? 'checked="checked"' : ''?>>
                            Не активный
                        </label>
                    </div>

                    <div class="contbody_forms">
                        <input type="submit" value="Создать" name="add">
                    </div>
                </form>
            </div>
        </td>
        </td>
        <td class="right_back_menu">
            <? temp::include('control.index.right.tpl') ?>
        </td>
    </tr>
</table>
<script>
    $(function(){
        $("a[data-role=add-row]").click(function (e) {
            e.preventDefault();
            var number=Number($(this).attr('data-number'));
            $(this).attr('data-number', number+1);
            $(this).closest('.contbody_forms').before('<div class="contbody_forms"><b>Ответ №'+(number+1)+'</b><br/><input type="text" name="question[]" placeholder="Вариант ответа" value="<?= $mail['subject'] ?>"></div>');
        });
    });
</script>
<style>

</style>