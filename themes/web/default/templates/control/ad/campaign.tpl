<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2 class="icon-cog-alt"> Настройка рекламной компании</h2>
                </div>
                <form name="campaign" action="/control/ad/campaign" method="post">
                    <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                    <input type="hidden" name="id" value="<?=$campaign['id']?>" />
                    <div class="contbody_forms">
                        <b>Расположение</b><br/>
                        <select name="pid" style="width: 400px">
                            <option value="0">Выберите рекламное место</option>
                            <?foreach($places as $key => $value):?>
                            <option value="<?=$key?>"<?if($key==$campaign['pid']):?> selected<?endif?>><?=$value?></option>
                            <?endforeach?>
                        </select>
                    </div>
                    <div class="contbody_forms">
                        <b>Период</b><br/>
                        <input type="datetime-local" name="tm_from" value="<?=strftime('%Y-%m-%dT%H:%M:%S', $campaign['tm_from'])?>">
                        &nbsp;-&nbsp;
                        <input type="datetime-local" name="tm_to" value="<?=strftime('%Y-%m-%dT%H:%M:%S', $campaign['tm_to'])?>">
                    </div>
                    <div class="contbody_forms">
                        <b>Приоритет</b><br/>
                        <input type="number" name="priority" value="<?= $campaign['priority'] ?>">
                    </div>
                    <div class="contbody_forms">
                        <b>Ссылка</b><br/>
                        <input type="text" name="link" value="<?= $campaign['link'] ?>" style="width: 90%;">
                    </div>
                    <div id="banner-cont" class="contbody_forms" <?if($campaign['img']):?>has-image<?endif?>>
                        <b>Баннер</b><br/>
                        <div class="img-cont">
                            <input type="hidden" name="img" value="<?=$campaign['img']?>"/>&nbsp;&nbsp;
                            <img id="banner" src="<?=$campaign['img']?>">
                            <a id="delete" class="button upload">Удалить</a>
                        </div>
                        <div class="upload-cont">
                            <a class="button upload">Добавить
                                <input type="file" name="image" />
                            </a>
                        </div>
                    </div>
                    <div class="contbody_forms">
                        <b>Текст</b><br/>
                        <textarea name="text" rows="3"><?= $campaign['text'] ?></textarea>
                    </div>
                    <div class="contbody_forms">
                        <b>Кнопка</b><br/>
                        <input type="text" name="btn" value="<?= $campaign['btn'] ?>">
                    </div>
                    <div class="contbody_forms">
                        <input type="checkbox" name="active" <?if($campaign['active']=='1'):?>checked="true"<?endif?>>
                        <label>active</label>
                    </div>
                    <div class="contbody_forms">
                        <input type="submit" value="Сохранить" name="submit">
                    </div>
                </form>
            </div>
        </td>
        <? temp::include('control/ad/menu.tpl') ?>
    </tr>
</table>
<script>
$(function(){
    var id = "<?=$campaign['id']?>",
        bannerCont = document.getElementById('banner-cont'),
        banner = document.getElementById('banner'),
        input = document.forms.campaign.img;
    $('input[type=file]').on('change', function(){
        var fileName = $(this)[0].files[0],
            form = new FormData();
        form.append('image', fileName);
        $.ajax({
            url : '/control/ad/files?action=upload&id=' + id,
            type : 'POST',
            dataType: 'json',
            data : form,
            processData: false,
            contentType: false,
            success : function(data) {
                banner.src = data.name;
                input.value = data.name;
                bannerCont.setAttribute('has-image', 'true');
            }
        });
    });
    $('.content').on('click', '#delete',  function(e){
        e.preventDefault();
        $.ajax({
            url : '/control/ad/files?action=delete&file=' + input.value.substr(1) + '&id=' + id,
            type : 'POST',
            dataType: 'json',
            success : function() {
                banner.src = '';
                input.value = '';
                bannerCont.removeAttribute('has-image');
            }
        });
    });
});
</script>
<style>
    .img-cont{
        display: none;
    }
    #banner-cont[has-image] .img-cont{
        display: block;
    }
    #banner-cont[has-image] .upload-cont{
        display: none;
    }
    .upload{
        display:block;
        position: relative;
        overflow: hidden;
        text-align:center;
        max-width:100px;
        margin: 10px 0;
    }

    .upload input[type=file]{
        position: absolute;
        left: 0;
        top: 0;
        transform: scale(20);
        letter-spacing: 10em;     /* IE 9 fix */
        -ms-transform: scale(20); /* IE 9 fix */
        opacity: 0;
        cursor: pointer
    }
</style>