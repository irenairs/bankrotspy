<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2 class="icon-cog-alt"> Активные баннеры для шапки сайта</h2>
                </div>
                <? if(!empty($ad['top'])): ?>
                    <? foreach($ad['top'] as $adItem): ?>
                    <div class="contbody_forms">
                        <div class="prm_line_descr">
                            Module: <span><?=$adItem['module']?></span>
                            Action: <span><?=$adItem['action']?></span>
                            Description: <span><?=$adItem['descr']?></span><br/>
                            Period: <span><?=ds_time($adItem['tm_from'])?> - <?=ds_time($adItem['tm_to'])?></span>
                            Priority: <span><?=$adItem['priority']?></span>
                            Active: <span><?if($adItem['active']=='1'):?>yes<?else:?>no<?endif?></span>
                        </div>
                        <div class="top-banner">
                            <? temp::include('block.top.ad.tpl') ?>
                        </div>
                        <a class="urlbutton_index" href="/control/ad/campaign?action=edit&id=<?=$adItem['id']?>">Редактировать</a>
                    </div>
                    <? endforeach; ?>
                <? else: ?>
                    <div class="contbody_forms">
                        Нет активированных баннеров для шапки сайта на данный момент
                    </div>
                <? endif; ?>
            </div>
            <div class="content">
                <div class="conthead">
                    <h2 class="icon-cog-alt"> Активная текстовая реклама</h2>
                </div>
                <? if(!empty($ad['content'])): ?>
                <? foreach($ad['content'] as $adItem): ?>
                <div class="contbody_forms">
                    <div class="prm_line_descr">
                        Module: <span><?=$adItem['module']?></span>
                        Action: <span><?=$adItem['action']?></span>
                        Description: <span><?=$adItem['descr']?></span><br/>
                        Period: <span><?=ds_time($adItem['tm_from'])?> - <?=ds_time($adItem['tm_to'])?></span>
                        Priority: <span><?=$adItem['priority']?></span>
                        Active: <span><?if($adItem['active']=='1'):?>yes<?else:?>no<?endif?></span>
                    </div>
                    <? temp::include('block.content.ad.tpl') ?>
                    <a class="urlbutton_index" href="/control/ad/campaign?action=edit&id=<?=$adItem['id']?>">Редактировать</a>
                </div>
                <? endforeach; ?>
                <? else: ?>
                <div class="contbody_forms">
                    Нет активированой текстовой рекламы на данный момент
                </div>
                <? endif; ?>
            </div>
        </td>
        <? temp::include('control/ad/menu.tpl') ?>
    </tr>
</table>
