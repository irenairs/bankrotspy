<table>
    <tr>
        <td valign="top">

            <?if ($error):?>
            <div class="error">
                <?foreach($error as $error): ?>
                <?=$error?><br/>
                <?endforeach?>
            </div>
            <?endif?>

            <form name="mess" action="<?=$home?>/control/tarifedit?id=<?=$id?>" method="post">
                <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                <div class="content">
                    <div class="conthead">
                        <h2><i class="icon-cog-alt"></i> Редактирование тарифа</h2>
                    </div>
                    <div class="contbody_forms">
                        <b>Название тарифа:</b><br/>
                        <input name="name" type="text" value="<?=$name?>"/>
                    </div>
                    <div class="contbody_forms">
                        <b>Цена, руб.:</b><br/>
                        <input name="price" type="text" value="<?=$price?>"/>
                    </div>
                    <div class="contbody_forms">
                        <b>Права для тарифа</b><br/>
                        <select name="rights">
                            <?foreach($rights as $rig_id => $rig_name): ?>
                            <?if(in_array($rig_id, array(10, 11, 0))):?>
                            <option value="<?=$rig_id?>" <?if($rig_id==$user_rights):?>selected="selected"<?endif?> ><?=$rig_name?></option>
                            <?endif?>
                            <?endforeach?>
                        </select>
                    </div>
                    <div class="contbody_forms">
                        <b>Продолжительность:</b><br/>
                        <input name="longtime" type="number" min="0" value="<?=$longtime?>"/><br/>
                        <b>Тип периода:</b><br/>
                        <select name="typetime">
                            <option value="0" <? if($typetime == 0) echo 'selected'; ?>>День</option>
                            <option value="1" <? if($typetime == 1) echo 'selected'; ?>>Месяц</option>
                            <option value="2" <? if($typetime == 2) echo 'selected'; ?>>Час</option>
                        </select>
                    </div>
                    <div class="contbody_forms">
                        <b>Посадочная страница:</b>&numsp;&numsp;
                        <input name="page_main" type="checkbox" <?=($page_main == '1') ? 'checked="checked"' : ''?>>
                        <br/><br/>
                        <b>Страница тарифов:</b>&numsp;&numsp;
                        <input name="page_tariff" <?=($page_tariff == '1') ? 'checked="checked"' : ''?> type="checkbox">
                    </div>    
                    <div class="contbody_forms">
                        <b>Краткое описание тарифа:</b><br/>
                        <?=func::tagspanel('messarea');?>
                        <div class="texta"><textarea id="messarea" name="descr" rows="4"><?=$descr?></textarea></div>
                    </div>
                    <div class="contbody_forms">
                        <b>Дополнительное описание:</b><br/>
                        <?=func::tagspanel('messarea_2');?>
                        <div class="texta"><textarea id="messarea_2" name="spec_descr" rows="4"><?=$spec_descr?></textarea></div>
                    </div>
                    <div class="contfin_forms">
                        <input name="submit" type="submit" value="Сохранить" />
                    </div>
                </div>
            </form>

        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt"><?=lang('menu_settings')?>:</div>
                <div class="elmenu"><a href="<?=$home?>/control/tset">Вернуться</a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>