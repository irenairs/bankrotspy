<table>
    <tr>
        <td valign="top">

<div class="content">
    <div class="conthead">
        <h2><i class="icon-cog-alt"></i> Список активов</h2>
    </div>

    <?if($rmenu):?>

    <?foreach($rmenu as $rmenu): ?>
    <div class="contbody_forms">
        <table>
            <tr>
              <td style="width: 30px;"><i class="icon-docs"></i></td>
              <td>
                  <b>№:</b> <?=$rmenu['number']?></a><br/>
                  <b>Актив:</b> <?=$rmenu['active']?></a><br/>
                  <b>Сумма:</b> <?=$rmenu['sum_invest']?> руб.</a><br/>
                  <b>Срок:</b> <?=$rmenu['time_invest']?> мес.</a><br/>
                  <b>Менеджер:</b> <?=$rmenu['manager']?><br/>
              </td>
                <td class="cont_act"><a title="Редактировать актив" href="<?=$home?>/invest/invedit?id=<?=$rmenu['id']?>"><i class="icon-edit"></i></a></td>
                <td class="cont_act"><a title="Удалить актив" href="<?=$home?>/invest/invdel?id=<?=$rmenu['id']?>"><i class="icon-delete"></i></a></td>
            </tr>
        </table>
    </div>
    <?endforeach?>

    <?else:?>
        <div class="contbody_forms">Нет ни одного пункта меню.</div>
    <?endif?>

    <div class="contfin">
        <a class="urlbutton" href="<?=$home?>/invest/add">Создать актив</a>
    </div>
</div>

        </td>
        <td class="right_back_menu">
            <? temp::include('control.index.right.tpl') ?>
        </td>
    </tr>
</table>
