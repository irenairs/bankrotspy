<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-cog-alt"></i> Статистика определения регионов</h2>
                         <a href="/control/regionsfixpreg?save=1" class="button" style="float: right;margin-top: -25px;">Сохранить в базу</a></div>
                 </div>  
				
         <div class="contbody_forms">
		 <div style="border: 1px solid #e1e1e1;padding: 1px 5px 3px 5px;font-size: 1.1em;margin-bottom: 5px;font-weight: bold;background-color: #f9f8f8;">
				 Всего лотов с не определенными регионами: <?=$lot_quanty?>
				</div>
			<div style="border: 1px solid #e1e1e1;padding: 1px 5px 3px 5px;font-size: 1.1em;margin-bottom: 5px;font-weight: bold;background-color: #f9f8f8;">
				 Определенно регионов: <?=$count?>
				</div>
				<?if($succes):?>
					<div style="border: 1px solid #e1e1e1;padding: 1px 5px 3px 5px;font-size: 1.1em;margin-bottom: 5px;font-weight: bold;background-color: #f9f8f8;">
				 <?=$succes?>
				</div>
				<?endif;?>
                <?if($messages):?>
                        <table class="data_table"  style="background-color: white">
							<tr style="text-align: center;">
								<td><b>№ Лота</td>
								<td><b>Присвоенный регион</td>
								<td><b>Содержание поля по которому определен регион</td>
							</tr>
                    <?foreach($messages as $message):?>
                   
                          <tr>
                              <td>
                                    <a href="/card/<?=($message['lot'])?>" target="blank"><?=($message['lot'])?>
                                </td>
                              
                                 <td>
                                    <?=$message['name']?>
                                </td>
                                 <td>
                                    <?=$message['field']?>
                                </td>
                            </tr>
                      
                    <?endforeach?>
                      </table>
                    </div>
                <?else:?>
                    <div class="contbody_forms">Нет ни одного региона.</div>
                <?endif?>
                <div class="contfin_forms">
                    <br/>
                </div>
                
            </div>
        </td>
        <td class="right_back_menu">
            <? temp::include('control.categories.right.tpl') ?>
        </td>
    </tr>
</table>
