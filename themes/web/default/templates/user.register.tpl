<meta name="google-signin-clientid" content="<?=system::$config['key']['google']?>" />
<meta name="facebook-app" content="<?=system::$config['key']['facebook']?>" ver="<?=system::$config['ver']['facebookSDK']?>"/>
<meta name="vk-app" content="<?=system::$config['key']['vk']?>"/>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://vk.com/js/api/openapi.js?154" type="text/javascript"></script>
<script src="<?=$themepath?>/js/user.js?v=<?=filemtime($_SERVER['DOCUMENT_ROOT'].'/themes/web/default/js/user.js')?>"></script>
<table>
    <tr>
        <td valign="top">
            <?if ($error):?>
            <div class="error">
                <?foreach($error as $error): ?>
                <?=$error?><br/>
                <?endforeach?>
            </div>
            <?endif?>

            <div class="content">
                <div class="conthead">
                    <h2><?=lang('registration')?></h2>
                </div>
                <?if($text):?>
                <div class="contbody_forms">
                    <?=$text?>
                </div>
                <?endif?>
                <div class="oper-container" auth>
                    <div id="user_google_reg">Gmail / Google+</div>
                    <div id="user_facebook_reg">Facebook</div>
                    <div id="user_vk_reg">ВКонтакте</div>
                </div>
                <div class="hr-with-title">
                    <span>or</span>
                </div>
                <form action="?act=save" method="post">
                    <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                    <div class="contbody_forms">
                        <b><?=lang('nick')?></b><br/>
                        <span class="under"><?=lang('nick_und')?></span><br/>
                        <input type="text" name="nick" value="<?=$nick?>" />
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('pass')?></b><br/>
                        <span class="under"><?=lang('pass_und')?></span><br/>
                        <span style="padding-right:20px;">
                            <input type="password" name="pass" value="<?=$pass?>" />
                        </span>
                        <span style='color:red;'>ВАЖНО! Пожалуйста, используйте сложный пароль, так как слишком простой пароль может быть подобран злоумышленниками и использован во вред вам и нам.</span>
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('pass_rep')?></b><br/>
                        <input type="password" name="pass_rep" value="<?=$pass_rep?>" />
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('mail')?></b><br/>
                        <span class="under"><?=lang('mail_und')?></span><br/>
                        <input type="text" name="mail" value="<?=$mail?>" />
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('capcha')?></b><br/>
                        <?=$capcha?><br/>
                        <input type="text" name="vcode" size="4" value="" />
                    </div>
                    <div class="contbody_forms">
                        Внимание! Продолжая регистрацию вы соглашаетесь с <a target="_blank" href="https://docs.google.com/document/d/19ILUoZ82iZPtZmthXvdMj2UQQVn80t2PGxMCZYNKECQ/edit?usp=sharing">Публичным договором оферты</a>.
                    </div>
                    <div class="contfin_forms">
                        <input type="submit" value="<?=lang('save')?>" />
                    </div>
                </form>
            </div>
        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню:</div>
                <div class="elmenu"><a href="<?=$home?>/login"><?=lang('autorization')?></a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>
