<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-users"></i> Реферальная программа
                    <?if($paySummary['login']):?> » <?=$paySummary['login']?><?endif?>
                    </h2>
                </div>
                <div class="contbody_forms">
                    Расчет настроен на выплату <b>25%</b> от выручки со сроком выплаты после <b>5-го</b> числа
                    месяца, следующего за месяцем поступления выручки.
                </div>
                <?if($paySummary):?>
                <link rel="stylesheet" href="<?=$themepath?>/styles/refer.css" type="text/css" />
                <div class="contbody_forms">
                    <b>Выплачено</b>
                    <div style="float: right" class="rub"><?=round($paySummary['paidOut'], 2)?></div>
                </div>
                <div class="contbody_forms">
                    <b>К оплате сейчас</b>
                    <div style="float: right" class="rub"><?=round($paySummary['toPayPrev'], 2)?></div>
                </div>
                <?if($curWait):?>
                <div class="contbody_forms">
                    <b>К оплате после <?=$curWait?></b>
                    <div style="float: right" class="rub"><?=round($paySummary['toPayCur'], 2)?></div>
                </div>
                <?endif?>
                <div class="contbody_forms">
                    <b>К оплате после <?=$nextWait?></b>
                    <div style="float: right" class="rub"><?=round($paySummary['toPayNext'], 2)?></div>
                </div>
                <div class="contbody_forms" >
                    <form  action="/control/refer?key=<?=$referKey?>" method="post">
                        <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                        <label><b>Выплата: </b></label>
                        <input type="text" placeholder="Сумма" name="sum" style="height: 15px; width:110px;;">
                        <input type="text" placeholder="Дата" name="date" autocomplete="off" style="height: 15px; width:110px;;">
                        <input type="hidden" name="action" value="addPay">
                        <input class="button_no_top_index" type="submit" value="Добавить">
                    </form>
                </div>
                <?if($pays):?>
                <div class="contbody_forms">
                    <table class="refer-table">
                        <thead>
                        <tr>
                            <th>Логин</th>
                            <th>Платеж</th>
                            <th>К выплате (25%)</th>
                            <th>Выплачено</th>
                            <th>Дата</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?foreach($pays as $details): ?>
                        <tr>
                            <?if($details['rf_oper'] == 3):?>
                                <td class="user"><b><a href="/user/profile?id=<?=$details['rf_uid']?>"><?=$details['login']?></a></b></td>
                                <td class="rub"><?=round($details['rf_sum'], 2)?></td>
                                <td class="rub"><?=round($details['rf_sum'] * .25, 2)?></td>
                                <td></td>
                            <?else:?>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="rub"><?=round($details['rf_sum'], 2)?></td>
                            <?endif?>
                            <td class="dt"><?=date('d-m-Y H:i', $details['rf_dt'])?></td>
                        </tr>
                        <?endforeach?>
                        </tbody>
                    </table>
                </div>
                <?endif?>
                <?endif?>
            </div>
        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню:</div>
                <div class="elmenu"><a href="/control/refersum">Вернуться к списку</a></div>
                <div class="elmenu"><a href="/control/pays">Платежи</a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>
<script>
    $(document).ready(function() {
        $('[name="date"]').datepicker({
            language: 'ru-RU',
            dateFormat: 'dd.mm.yyyy'
        });
    });
</script>