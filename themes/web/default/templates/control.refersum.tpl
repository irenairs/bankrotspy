<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-users"></i> Реферальная программа</h2>
                </div>
                <div class="contbody_forms">
                    Расчет настроен на выплату <b>25%</b> от выручки со сроком выплаты после <b>5-го</b> числа
                    месяца, следующего за месяцем поступления выручки.
                </div>
                <?if($data):?>
                <link rel="stylesheet" href="<?=$themepath?>/styles/refer.css" type="text/css" />
                <div class="contbody_forms">
                    <table class="refer-table">
                        <thead>
                        <tr>
                            <th rowspan="2">Логин</th>
                            <th rowspan="2">Выплачено</th>
                            <th colspan="<?if($curWait):?>3<?else:?>2<?endif?>">К оплате</th>
                        </tr>
                        <tr>
                            <th>сейчас</th>
                            <?if($curWait):?>
                            <th>после <?=$curWait?></th>
                            <?endif?>
                            <th>после <?=$nextWait?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?foreach($data as $key => $details): ?>
                        <tr>
                            <td class="user"><b><a href="/control/refer?key=<?=$key?>"><?=$details['login']?></a></b></td>
                            <td class="rub"><?=round($details['paidOut'], 2)?></td>
                            <td class="rub"><?=round($details['toPayPrev'], 2)?></td>
                            <?if($curWait):?>
                            <td class="rub"><?=round($details['toPayCur'], 2)?></td>
                            <?endif?>
                            <td class="rub"><?=round($details['toPayNext'], 2)?></td>
                        </tr>
                        <?endforeach?>
                        </tbody>
                    </table>
                </div>
                <?endif?>
            </div>
        </td>
        <td class="right_back_menu">
            <? temp::include('control.index.right.tpl') ?>
        </td>
    </tr>
</table>
