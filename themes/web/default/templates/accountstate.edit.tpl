<table>
    <tr>
        <td valign="top">
            <div class="content">
                    <div class="conthead">
                        <h2><i class="icon-key"></i>Выписка счёта на оплату</h2>
                    </div>



        <script type="text/JavaScript">
            //--------------------------------------------------
//            function filter_input(e,regexp)
//            {
//                e=e || window.event;
//                var target=e.target || e.srcElement;
//                var isIE=document.all;
//
//                if (target.tagName.toUpperCase()=='INPUT')
//                {
//                    var code=isIE ? e.keyCode : e.which;
//                    if (code!=34 || e.ctrlKey || e.altKey) return true;
//                    if (code==34)  return false;
//                    if (code==39)  return false;
//                    var char=String.fromCharCode(code);
//                    //  if (!regexp.test(char)) return false;
//                }
//                return true;
//            }


            //--------------------------------------------------
        </script>

        

        <div style="width: 100%; margin: 20px 50px;">Для выписки счёта на оплату, внимательно заполните расположенную ниже форму. Поля формы отмеченные знаком <font color="Red">*</font> - обязательны <br>для заполнения.
            <br>
            <font color="Red"> При вводе запрещено использовать двойные и одинарные кавычки .
                <br> Они будут удалятся автоматически.
            </font>
        </div>
        <script type="text/javascript">
            function flt()
            {
                var a = document.getElementById("firmname").value;
                var ns;
                var cur='';
                var n=0;
                var not1=String.fromCharCode(34);
                var not2=String.fromCharCode(39);
                ns="";
                for (n = 0; n <= a.length; n++)
                {
                    cur=a[n];
                    if (a[n]!=not1&&a[n]!=not2)
                    {
                        if (cur!=undefined)	{ns=ns+cur;	}
                                }
                    }
                    document.getElementById("firmname").value=ns;
                }
        </script>
        <style>

          #accstate  table{

              margin: 0 auto;
            }

          #accstate   table tr{
                
            }

          #accstate  table tr.header td{
              text-align: center; 
              /*font-weight: bold;*/
              background: #ebebeb;
              padding: 20px;
              border: 1px solid #d1d1d1;

          }
          #accstate   table tr td {
              padding: 10px;
              text-align: center;
              border: 1px solid #d1d1d1;
          }
          #accstate   table tr td.fcol {
              padding:  10px 10px 10px 30px;
              text-align: left;
              border: 1px solid #d1d1d1;
              /*margin-left: 20px;*/
          }
            #accstate div{

                margin: 0px 50px 0px 50px;
            }

          #accstate   table tr td input[type=radio] {
              margin-right: 5px;
              margin-top: 2px;;

          }

          #accstate textarea{
              margin-left: 0px;
          }
          .padtop_10{
              padding-top: 10px;
          }
          .padtop_30{
                padding-top: 30px;
          }
            .padbot_20{
                padding-bottom: 20px;
            }
          .padbot_10{
              padding-bottom: 10px;
          }
        </style>

        <form method="post" id="accstate" action="<?=$home?>/accountstate/update">
            <? temp::formid() ?>
            <div class="padtop_10">Покупатель (Наименование юридического лица)<font color="Red">*</font>:</div>
            <div>
                <input data-validation-error-msg="Это поле обязательное для заполнения" data-validation="required"  name="name" size="40" maxsize="250" value="<?= $data['compname'] ?>" id="firmname" onchange="flt();" type="text">
            </div>
            <div class="padtop_10" >ИНН<font color="Red"></font>:</div>
            <div>

                <!--    <input data-validation-error-msg="" name="inn" size="20" maxsize="20" value="" type="text" id="inn" data-validation="number length" data-validation-length="10">-->
                <input  name="inn" size="20" maxsize="20" value="<?= $data['inn'] ?>" type="text" id="inn" >
            </div>
            <input name="docid" value="<?= $data['id'] ?>" id="pr26" type="hidden">
            <!--  <div class="padtop_10">КПП<font color="Red">*</font>:</div>
              <div><input name="kpp" size="20" maxsize="20" value="" type="text"></div>
              <div class="padtop_10">Адрес<font color="Red">*</font>:</div>
              <div><textarea name="adr" rows="4" cols="40" style="width: 30%"></textarea></div>  -->
            
            
            
            <div class="padtop_30 padbot_10">Выберите тариф<font color="Red">*</font>:</div>
            <table style="width: 90%; border-color: #dfdfdf;" cellspacing="0" cellpadding="3" border="1" align="center">
                <tbody>
                <tr class="header">
                    <td>Продукт</td>
                    <td>Описание</td>
                    <td>Срок</td>
                    <td>Цена,</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T1" checked="" type="radio">
                        <strong>Тариф - 1 день</strong>
                    </td>
                    <td >Доступ на 1 день к материалам сайта</td>
                    <td >1 день </td>
                    <td ><input name="pr26[]" value="90" id="t1" type="hidden">90</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T2" checked="" type="radio">
                        <strong>Тариф - 1 месяц</strong></td>
                    <td>Доступ на 1 месяц к материалам сайта</td>
                    <td>1 месяц</td>
                    <td>
                        <input name="pr26[]" value="550" id="pr26" type="hidden">550</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T3" checked="" type="radio">
                        <strong>Анализ дебиторки 1</strong></td>
                    <td>Анализ дебиторки </td>
                    <td>1 день</td>
                    <td>
                        <input name="pr26[]" value="600" id="pr26" type="hidden">600</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T4" checked="" type="radio">
                        <strong>Анализ дебиторки 2</strong></td>
                    <td>Доступ на 1 месяц к материалам сайта</td>
                    <td>1 день</td>
                    <td>
                        <input name="pr26[]" value="900" id="pr26" type="hidden">900</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T5" checked="" type="radio">
                        <strong>Доступ на 3 месяца</strong></td>
                    <td>Доступ на 3 месяца к материалам сайта</td>
                    <td>3 месяца</td>
                    <td>
                        <input name="pr26[]" value="1200" id="pr26" type="hidden">1200</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T6" checked="" type="radio">
                        <strong>VIP-доступ на 1 месяц</strong></td>
                    <td>VIP-доступ на 1 месяц к аналитическим материалам сайта</td>
                    <td>1 месяц</td>
                    <td>
                        <input name="pr26[]" value="1500" id="pr26" type="hidden">1500</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T7" checked="" type="radio">
                        <strong>Коммерсант_1</strong></td>
                    <td>--</td>
                    <td>10 дней</td>
                    <td>
                        <input name="pr26[]" value="1500" id="pr26" type="hidden">1500</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T8" checked="" type="radio">
                        <strong>Поиск дебиторки 2</strong></td>
                    <td>--</td>
                    <td>1 день</td>
                    <td>
                        <input name="pr26[]" value="1500" id="pr26" type="hidden">1500</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T9" checked="" type="radio">
                        <strong>Доступ на 6 месяцев к материалам сайта</strong></td>
                    <td>Доступ на 6 месяцев к материалам сайта</td>
                    <td>6 месяцев</td>
                    <td>
                        <input name="pr26[]" value="2200" id="pr26" type="hidden">2200</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T10" checked="" type="radio">
                        <strong>Доступ на 12 месяцев к материалам сайта</strong></td>
                    <td>--</td>
                    <td>12 месяцев</td>
                    <td>
                        <input name="pr26[]" value="3500" id="pr26" type="hidden">3500</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T11" checked="" type="radio">
                        <strong>VIP-доступ на 3 месяца</strong></td>
                    <td>VIP-доступ на 3 месяца к аналитическим материалам сайта</td>
                    <td>3 месяца</td>
                    <td>
                        <input name="pr26[]" value="3500" id="pr26" type="hidden">3500</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T12" checked="" type="radio">
                        <strong>Обучение "Специалист"</strong></td>
                    <td>--</td>
                    <td>1 месяц</td>
                    <td>
                        <input name="pr26[]" value="4000" id="pr26" type="hidden">4000</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T13" checked="" type="radio">
                        <strong>VIP-доступ на 6 месяцев</strong></td>
                    <td>VIP-доступ на 6 месяцев к аналитическим материалам сайта</td>
                    <td>6 месяцев</td>
                    <td>
                        <input name="pr26[]" value="5000" id="pr26" type="hidden">5000</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T14" checked="" type="radio">
                        <strong>Поиск дебиторки 1</strong></td>
                    <td>--</td>
                    <td>1 день</td>
                    <td>
                        <input name="pr26[]" value="5000" id="pr26" type="hidden">5000</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T15" checked="" type="radio">
                        <strong>VIP-доступ на 12 месяцев</strong></td>
                    <td>VIP-доступ на 12 месяцев к аналитическим материалам сайта</td>
                    <td>12 месяцев</td>
                    <td>
                        <input name="pr26[]" value="6900" id="pr26" type="hidden">6900</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T16" checked="" type="radio">
                        <strong>Коммерсант_2</strong></td>
                    <td>Услуга: помощь в аккредитации на 1-й площадке и помощь в выборе и получении ЭЦП.</td>
                    <td>10 дней</td>
                    <td>
                        <input name="pr26[]" value="8000" id="pr26" type="hidden">8000</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T17" checked="" type="radio">
                        <strong>Коммерсант_3</strong></td>
                    <td>Услуга: помощь в выборе ликвидного имущества (до 3-х вариантов) по заданным критериям.</td>
                    <td>10 дней</td>
                    <td>
                        <input name="pr26[]" value="9500" id="pr26" type="hidden">9500</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T18" checked="" type="radio">
                        <strong> 	Поиск дебиторки 3</strong></td>
                    <td>--</td>
                    <td>1 день</td>
                    <td>
                        <input name="pr26[]" value="10000" id="pr26" type="hidden">10000</td>
                </tr>
                <tr>
                    <td class="fcol">
                        <input name="paymentType" value="T19" checked="" type="radio">
                        <strong>Обучение "Эксперт"</strong></td>
                    <td>Услуга: помощь в аккредитации на 1-й площадке, помощь в выборе и получении ЭЦП (цена/качество), помощь в выборе ликвидного имущества (до 3-х вариантов) по заданным критериям, помощь в получении допуска к тогам: оформление зявки + перечисления задатка, помощь в правильной подготовке и подаче заявки на электронных торгах.</td>
                    <td>3 месяца</td>
                    <td>
                        <input name="pr26[]" value="19000" id="pr26" type="hidden">19000</td>
                </tr>
                </tbody>
            </table>
            <div class="padtop_30">
                <input id="gen" name="gen" value="Изменить выписку счета" type="submit">
            </div>
        </form>
        </div>
</td>
<td class="right_back_menu">
    <div class="right_panel_conf">
        <div class="menu_rt">Меню:</div>
        <div class="elmenu"><a href="<?= core::$home ?>/accountstate">Выписанные счета</a></div>
        <div class="elmenu"><a href="<?= core::$home ?>/accountstate/create">Новая выписка счета</a></div>

        <div class="down_rmenu"> </div>
    </div>
</td>
</tr>
</table>
<span id='pres-max-length' style="display:none">10</span>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?=$themepath?>/js/inputmask.min.js"></script>
<script type="text/javascript">
    var inn = document.getElementById("inn");
    var im = new Inputmask("9", { repeat: 10 });
    im.mask(inn);
    $.validate({
        form : '#accstate',
        lang: 'ru'
    });
//$('#inn').restrictLength( $('#pres-max-length'));
$('input:radio[name="paymentType"][value="<?= $data['prodcode'] ?>"]').attr('checked',true);
</script>