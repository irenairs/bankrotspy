<table>
    <tr>
        <td valign="top">

            <?if ($error):?>
            <div class="error">
                <?foreach($error as $error): ?>
                <?=$error?><br/>
                <?endforeach?>
            </div>
            <?endif?>

            <form name="mess" action="<?=$home?>/invest/add" method="post" enctype="multipart/form-data">
                <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                <div class="content">
                    <div class="conthead"><h2><i class="icon-docs"></i> Новый актив</h2></div>
                    <table cellspacing = "0"><tr>
                  <td>
                    <div class="contbody_forms">
                        <b>Номер актива</b><br/>
                        <input type="text" name="number" value="<?=$number?>" />
                   </div>
                    <div class="contbody_forms">
                        <b>Актив</b><br/>
                        <input type="text" name="active" value="<?=$active?>" />
                   </div>
                     </td>
                    </tr></table>
                    <div class="contbody_forms">
                        <b>Сумма инвестиций, руб.</b><br/>
                        <input type="text" name="sum_invest" value="<?=$sum_invest?>" style="width:98%;"/>
                    </div>
                    <div class="contbody_forms">
                        <b>Срок инвестиций, мес.</b><br/>
                        <input type="text" name="time_invest" value="<?=$time_invest?>" style="width:98%;"/>
                    </div>
                    <div class="contbody_forms">
                        <b>Ориентир по доходности, %</b><br/>
                        <input type="text" name="profit_proc" value="<?=$profit_proc?>" style="width:98%;"/>
                    </div>
                    <div class="contbody_forms">
                        <b>Ответственный менеджер</b><br/>
                        <?=func::tagspanel('messarea');?>
                        <div class="texta"><textarea id="messarea" name="manager" rows="15"><?=$manager?></textarea></div>
                    </div>
                    <div class="contbody_forms">
                        <b>Ссылка на презентацию</b><br/>
                        <input type="text" name="present" value="<?=$present?>" style="width:98%;"/>
                    </div>
                    <div class="contfin_forms">
                        <input name="submit" type="submit" value="<?=lang('save')?>" />
                    </div>
                </div>

            </form>

        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню</div>
                <div class="elmenu"><a href="<?=$home?>/control/pages">Отмена</a></div>
                <div class="elmenu"><a href="<?=$home?>">На главную</a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("input[name = 'submit']").click(function(){
        
        $.post("http://bankrot-spy.ru/verifyvalue.php",{entered_value_alias_page:$("input[name = 'alias']").val()},function(data)
            {
                            
                if(data=="1") 
                {   
                        
                    $("input[name = 'alias']").val("");
                    EasyMessage.show("name","alias",messagetext);
                    return false;
                }
            });
        
        });
            
        $("input[name = 'alias']").on('focusout',function()
        {var messagetext ="Данный псевдоним уже<br/> используется! <br/>Выберите другой псевдоним.";
        if($(this).val()!="")
        {
            var this_p = $(this);
            $.post("http://bankrot-spy.ru/verifyvalue.php",{entered_value_alias_page:this_p.val()},function(data)
            {
                            
                if(data=="1") 
                {   
                        
                    this_p.val("");
                    EasyMessage.show("name","alias",messagetext);
                    
                }
            });
                    
        }
            
        }).on('focus',function()
        {
            var this_p = $(this);
            EasyMessage.hide();
            
            
        });
    });

</script>