<table>
    <tr>
        <td valign="top">
            <div class="content">
                <div class="conthead"><h2><i class="fa fa-list-alt"></i> <?= $title ?></h2></div>
                <? if(!empty($textData)): ?>
                <div class="contbody_forms">
                    <?= $textData ?>
                </div>
                <? endif; ?>
                <div class="contbody_forms">
                    <div class="results">
                        Показаны результаты: <?= $start ?>-<?= $end ?> из <?= $total ?>
                    </div>
                    <table class="table">
                        <tr>
                            <th>№</th>
                            <th>
                                Актив
                            </th>
                            <th>
                                Сумма инвестиций, руб.
                            </th>
                            <th>Срок инвестиций, мес.
                            </th>
                            <th>Ориентир по доходности, %
                            </th>
                            <th>Партнер / Ответственный менеджер</th>
                            <th>Презентация</th>
                        </tr>
                        <? foreach($data as $item): ?>
                        <tr>
                            <td align="center" width="40">
                                <?= $item['number'] ?>
                            </td>
                            <td align="center" width="300">
                                <?= $item['active'] ?>
                            </td>
                            <td align="center" width="190">
                                <?= $item['sum_invest'] ?>
                            </td>
                            <td align="center" width="120">
                            <?= $item['time_invest'] ?>
                            </td>
                            <td align="center" width="200">
                                <?= $item['profit_proc'] ?>
                            </td>
                            <td align="center" width="350">
                                <?= $item['manager'] ?>
                            </td>
                            <td align="center" width="150"><?= $item['present'] ?></td>
                        </tr>
                        <? endforeach; ?>
                    </table>
                    <!-- <?if($navigation):?><div class="navig"><?=$navigation?></div><?endif?> -->
                </div>
            </div>
        </td>
    </tr>
</table>

<style>
.fa-list-alt {
    color:#838488;
}

.table th {
    background-color: #ebebeb;
    border: 1px solid #d1d1d1;
    color: #676767;
    font-size: 14px;
    font-weight: normal;
    padding: 4px 16px 4px 3px;
}

.table td{
    border:1px solid #e1e1e1;
    padding:5px 10px;
}
.table tr:hover {
    background:#f8f8f8;
}


</style>