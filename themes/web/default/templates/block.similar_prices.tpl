<style>
    #grapch {
        width		: 100%;
        height		: 500px;
        font-size	: 11px;
        max-width       : 980px; 
    }

    .amcharts-graph-graph2 .amcharts-graph-stroke {
        stroke-dasharray: 4px;
        stroke-linejoin: round;
        stroke-linecap: round;
        -webkit-animation: am-moving-dashes 1s linear infinite;
        animation: am-moving-dashes 1s linear infinite;
    }

    @-webkit-keyframes am-moving-dashes {
        100% {
            stroke-dashoffset: -28px;
        }
    }
    @keyframes am-moving-dashes {
        100% {
            stroke-dashoffset: -28px;
        }
    }

</style>
<script src="//www.amcharts.com/lib/3/amcharts.js"></script>
<script src="//www.amcharts.com/lib/3/serial.js"></script>
<script src="//www.amcharts.com/lib/3/themes/light.js"></script>
<script type="text/javascript" src="//www.amcharts.com/lib/3/lang/ru.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        var chart = AmCharts.makeChart("grapch", {
            "type": "serial",
            "titles": [{
            <? if($graphType == 1): ?>
        "text": "Цены на аналогичные товары",
        <? else: ?>
        "text": "Цена 1м2 аналогичных предложений",
        <? endif; ?>
        "size": 15
        }],

        "theme": "light",


            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,
            "balloon": {
            "adjustBorderColor": false,
                "horizontalPadding": 10,
                "verticalPadding": 8,
                "color": "#ffffff"
        },

        "dataProvider": <?= $similarDataPrice ?>,
        "valueAxes": [{
            "axisAlpha": 0,
            "position": "left"
        }],
            "startDuration": 1,
            "graphs": [{
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:12px;'>[[title]]:<br>[[value]] руб.</span>",
            "fillAlphas": 1,
            "title": "Цена",
            "type": "column",
            "valueField": "price",
            "dashLengthField": "dashLengthColumn"
        }, {
            "id": "graph2",
            "balloonText": "<span style='font-size:12px;'>[[title]]: [[value]] руб.</span>",
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "Средняя цена",
            "valueField": "average"
        }],
            "categoryField": "price",
            "categoryAxis": {
            "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
        }
        });
    });
</script>
