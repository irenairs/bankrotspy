<table>
    <tr>
        <td valign="top">
            <div class="content">
            <div class="conthead"><h2><i class="icon-newspaper"></i> Новости сайта</h2></div>
            <?if($narr):?>
            <?foreach($narr as $narr):?>

                <div class="contbody_forms">
                    <div class="news_cont">
                        <div class='newsdown' style=" margin-top: 5px;font-size:13px;color:#a2a3a2;float:right;"> <?=$narr['date']?></div>
                        <div style="float: left; margin-top: 8px;">
                            <a href="<?=$home?>/user/profile?id=<?=$narr['user_id']?>"><img class="avatar" src="<?=$avatar?>"/></a>
                        </div>
                        <div style="float: left; margin-left: 20px;">
                            <a href="<?=$home?>/news/view?id=<?=$narr['id']?>"><h2><?=$narr['name']?></h2></a>
                            <?if($narr['new_news']):?><sup style="color:red;font-weight:bold;">[new]</sup><?endif?>
                            <a href="<?=$home?>/news/view?id=<?=$narr['id']?><?if($narr['page_to_go']):?>&amp;page=<?=$narr['page_to_go']?>#ncm<?else:?>#comms<?endif?>">
                                <i class="icon-comment-1"></i><?=$narr['comm_count']?>  
                                <?if($narr['new_comm_count']):?><span class="newred">+ <?=$narr['new_comm_count']?></span><?endif?>
                            </a>
                            <br />
                            <span style="color: #d27600; font-style: italic;"><?=$narr['keywords']?></span>
                        </div>
                        <div style="clear: both;"></div>
                        <hr/>
                        <?=$narr['text']?> ...
                        <hr/>
                        <a class="urlbutton_index button_no_top_index" href="<?=$home?>/news/view?id=<?=$narr['id']?>">Далее</a>
                    </div>
                </div>

                <div class="contfin_forms_delimiter">

                </div>

            <?endforeach?>
            <?else:?>
                <div class="contbody_forms">Нет ни одного пункта меню.</div>
            <?endif?>
            </div>

            <?if($navigation):?><div class="navig"><?=$navigation?></div><?endif?>
        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню</div>
                <?if($can_cr_news):?>
                <div class="elmenu"><a href="<?=$home?>/news/create">Добавить новость</a></div>
                <?endif?>
                <div class="elmenu"><a href="<?=$home?>">На главную</a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>








<!-- футер -->
            </td>
        </tr>
    </table>
</div>


            <div class="mainundall"></div>
<div class="prebottom">
    <table>
        <tr>
            <td style="width: 100%;padding-top: 10px;padding-left: 13px;">
                <?if($production):?>
                <script async type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                <div style="display: inline-block; margin-right: 5px;" class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,gplus" data-yashareTheme="counter" data-yashareImage="<?=$themepath?>/images/apple-touch-icon.png"></div>
                <?endif?>
                <? temp::include('block.bottom.links.tpl') ?>
            </td>

            <td style="min-width: 210px;padding-top: 10px;text-align: right;padding-right: 17px;">
                <!--<span style="color: #888888"><?=round((microtime(true) - rem::get('microtime'))*1000, 0)?>ms, sql:<?=$sql_count?></span>-->
                <a class="user_onl_bottom" href="<?=$home?>/user/online"><i class="icon-user-male-bottom"></i> <?=$onl_all?></a>
            </td>
        </tr>
    </table>
</div>
<? temp::include('button_go_top.tpl') ?>  
<div class="bottom">
    <table>
        <tr>
            <td> Информация на данном сайте предоставляется "КАК ЕСТЬ" и предназначена только для ознакомительных целей без каких-либо гарантий и передачи прав. Любое использование информации в иных целях запрещено и производится на ваш страх и риск. Все права защищены.
            </td>
            <td>
         
		 
		 
            </td>
        </tr>
    </table>
</div>


<script type="text/javascript">

    //Эти 3 строки ничего страшного не делают
    //Нужно просто для системы защиты
    $(document).ready(function() {
        setInterval('connection_keeper()',30000);
    });

    function set_normal_height()
    {
        if(device.mobile() == false)
        {
          var height = $(window).height();
          var prebottom_h = $('div.prebottom').height();
          var bottom_h = $('div.bottom').height();
          var allhead_h = $('table.allhead').height();
          height = height - bottom_h - prebottom_h - allhead_h - 28;
          $('td.main').height(height);
        }
    }

    set_normal_height();

    $(window).resize(function(event) {
      set_normal_height();
      correct_images();
    });

    //учитываем скроллбар
    if(!get_scroll('Height') && ($('body').width() - $('.all_content').width() > 20)) {
        $('body').css({'margin-right' : scrollWidth()});
    }
</script>

<!-- Google Code for YOUTUBE_BS #1 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 993818846;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "i2zXCOWe_GMQ3vHx2QM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/993818846/?label=i2zXCOWe_GMQ3vHx2QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</div>
</div>
</body>
</html>