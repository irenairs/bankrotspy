<table>
    <tbody><tr>
        <td valign="top">
            <div class="content">
                <div class="conthead">
                    <h2><i class="icon-key"></i>Выписанные счета</h2>
                </div>
                <div class="contbody_forms">

                    <table class="data_table" style="padding: 0px;">
                        <thead class="tableFloatingHeaderOriginal" style="position: static; margin-top: 0px; left: 27px; z-index: 3; width: 1304px; top: 0px;">
                        <tr>
                            <th colattr="" style="max-width: 200px; min-width: 0px;">Счет</th>
                           <!-- <th colattr="" style="max-width: 200px; min-width: 0px;">Время</th>-->
                            <th colattr="" style="max-width: 200px; min-width: 0px;">

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach($data as $item): ?>
                        <tr>
                            <td style="text-align:center;">
                                <a style="padding:0px 20px 0px 0px;" target="_blank" href="<?= core::$home ?>/accountstate/load/<?= $item['id'] ?>/<?= $item['filename'] ?>">
                                    <?= $item['filename'] ?>
                                </a>
                            </td>
                           <!-- <td style="text-align:center;"><?= $item['datecreate'] ?>
                            </td>-->
                            <td style="text-align:center;">

                                <!--      <a style="padding:0px 20px 0px 0px;" target="_blank" href="<?= core::$home ?>/accountstate/load">Скачать</a>
-->                                <?if(!$admin):?>
                                         <span id="maildocid<?= $item['id'] ?>" style="cursor:pointer; display:inline-block; padding:3px 5px 3px 45px;" title="Отправить на почту"  data-id = "<?= $item['id'] ?>"><i class="fa fa-envelope-o"></i></span>
                                <?endif?>
                                <a target="_blank" style="display:inline-block; padding:3px 0px 3px 5px;" title="Посмотреть" href="<?= core::$home ?>/accountstate/view?id=<?= $item['id'] ?>"><i class="icon-doc-inv"></i></a>
                                
                                   <a style="display:inline-block; padding:3px 0px 3px 5px;" title="Редактировать" href="<?= core::$home ?>/accountstate/edit/<?= $item['id'] ?>"><i class="icon-edit"></i></a>


                                   <a style="display:inline-block; padding:3px 0px 3px 5px;" title="Удалить" href="<?= core::$home ?>/accountstate/delete/<?= $item['id'] ?>"><i class="icon-delete"></i></a>

                               </td>
                           </tr>
                           <? endforeach; ?>
                           </tbody>
                       </table>
                    <?if($navigation):?><div class="navig"><?=$navigation?></div><?endif?>
                   </div>
               </div>
           </td>
           
           <td class="right_back_menu">
               <?if(!$admin):?>
               <div class="right_panel_conf">
                   <div class="menu_rt">Меню:</div>
                   <div class="elmenu"><a href="<?= core::$home ?>/accountstate">Выписанные счета</a></div>
                   <div class="elmenu"><a href="<?= core::$home ?>/accountstate/create">Новая выписка счета</a></div>

                   <div class="down_rmenu"> </div>
               </div>
               <?endif?>
           </td>
       </tr>
       </tbody></table>
<script>
    $(document).ready(function(){

        $("span[id*='maildocid']").click(function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            //var data =
//            alert(id);
            $.ajax({
                type: 'POST',
                url: '/accountstate/mail?id='+id,
               data: $(this).serialize(),
                dataType: 'json',
                success: function(data){
                    if(data.status == 0 || data.status == 1 || data.status == 3) {
                        create_notify(data.message);
                 }
// else if (data.status == 2) {
//                        window.location = data.file;
//                    } else {
//                        create_notify('Произошла ошибка!');
//                    }

//                    load_table();
                }
            });
        });
    });
</script>