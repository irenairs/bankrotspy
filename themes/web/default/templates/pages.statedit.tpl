<table>
    <tr>
        <td valign="top">

            <?if ($error):?>
            <div class="error">
                <?foreach($error as $error): ?>
                <?=$error?><br/>
                <?endforeach?>
            </div>
            <?endif?>

            <form name="mess" action="<?=$home?>/pages/statedit?id=<?=$id?>" method="post" enctype="multipart/form-data">
                <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>

                <div class="content">
                    <div class="conthead"><h2><i class="icon-docs"></i> Редактирование страницы</h2></div>
                    <table cellspacing = "0"><tr>
				  <td>
				  <div class="contbody_forms">
                        <b><?=lang('inst_name')?></b><br/>
                        <input type="text" name="art_name" value="<?=$name?>" />
                   </div>
				     </td>
				  <td  align="right">
				  <div class="contbody_forms" >
					  <table>
						<tr><td style = "width:25%"></td><td align = "left" style = "width:75%"><b><?=lang('inst_alias')?></b></td></tr> 
						<tr><td style = "width:25%"></td><td align = "left" style = "width:75%"><input type="text" class="input_class_in" name="page_alias" value="<?=$alias?>" style=""/></td></tr>
					  </table>
					</div>
					</td>
					</tr></table>
                    <div class="contbody_forms">
                        <b><?=lang('keywords')?></b><br/>
                        <input type="text" name="keywords" value="<?=$keywords?>" style="width:98%;"/>
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('description')?></b><br/>
                        <input type="text" name="description" value="<?=$description?>" style="width:98%;"/>
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('data_opis')?></b><br/>
                        <?=func::tagspanel('messarea');?>
                        <div class="texta"><textarea id="messarea" name="art_text" rows="15"><?=$text?></textarea></div>
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('pr_file')?></b><br/>
                        <input type="file" name="file" />
                        <input type="submit" name="add_attachment" value="<?=lang('do_file')?>"/>
                        <hr/>
                        <?if($att_true):?>
                        <br/>
                        <b><?=lang('pr_files_n')?></b><hr/>
                        <?foreach($out as $data):?>
                        <i class="icon-doc-inv"></i> <input type="text" value="[<?=$data['type']?>=<?=$data['filename']?>]<?=$data['name']?>[/<?=$data['type']?>]"/> <b><a href="<?=$home?>/article/file<?=$data['id']?>/<?=$data['nameraw']?>"><?=$data['name']?></a></b>

                        <input type="submit" name="del_attachment[<?=$data['id']?>]" value="<?=lang('del_th')?>"/>
                        <hr/>
                        <?endforeach?>
                        <?endif?>
                    </div>

                    <div class="contfin_forms">
                        <input name="submit" type="submit" value="<?=lang('save')?>" />
                    </div>
                </div>
            </form>

        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню</div>
                <div class="elmenu"><a href="<?=$home?>/pages/<?=$alias?>/">Отмена</a></div>
                <div class="elmenu"><a href="<?=$home?>">На главную</a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>
<script type="text/javascript">
	var messagetext ="Данный псевдоним уже<br/> используется! <br/>Выберите другой псевдоним.";
	$(document).ready(function()
	{
		
		$("form[name='mess']").submit(
		
		function()
		{
		var res=0;
			var this_p = $(this);			
			$.ajax({
			url:"http://bankrot-spy.ru/verifyvalue.php",
			type:"POST",
			data:"entered_value_alias_old_page="+$("input[name = 'page_alias']").val()+"&id=<?=$id?>",
			async:false,
			success:function(data)
			{
				res=data;				
			}});
			if(res=="1") 
				{
					$("input[name = 'page_alias']").val("");
					EasyMessage.show("name","page_alias",messagetext);
					return false;
				}
			
		});	
			
			
		
		
		$("input[name = 'page_alias']").on('focusout',function()
		{var this_p = $(this);
		if(this_p.val()!="")
		{
			$.post("http://bankrot-spy.ru/verifyvalue.php",{entered_value_alias_old_page:this_p.val(),id:<?=$id?>},function(data)
			{
				if(data=="1") 
				{
					this_p.val("");
					EasyMessage.show("name","page_alias",messagetext);
				}
			});
		}
			
		}).on('focus',function()
		{
			
			EasyMessage.hide();
			
			
		});
	});

</script>