<meta name="google-signin-clientid" content="<?=system::$config['key']['google']?>" />
<meta name="facebook-app" content="<?=system::$config['key']['facebook']?>" ver="<?=system::$config['ver']['facebookSDK']?>"/>
<meta name="vk-app" content="<?=system::$config['key']['vk']?>"/>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://vk.com/js/api/openapi.js?154" type="text/javascript"></script>
<script src="<?=$themepath?>/js/user.js?v=<?=filemtime($_SERVER['DOCUMENT_ROOT'].'/themes/web/default/js/user.js')?>"></script>
<table>
    <tr>
        <td valign="top">

            <?if ($error):?>
            <div class="error">
                <?foreach($error as $error): ?>
                <?=$error?><br/>
                <?endforeach?>
            </div>
            <?endif?>

            <div class="content">
                <div class="conthead">
                    <h2><?=lang('autorization')?></h2>
                </div>
                <div class="oper-container" auth>
                    <div id="user_google_auth">Gmail / Google+</div>
                    <div id="user_facebook_auth">Facebook</div>
                    <div id="user_vk_auth">ВКонтакте</div>
                </div>
                <div class="hr-with-title">
                    <span>or</span>
                </div>
                <form action="?act=save" method="post">
                    <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                    <div class="contbody_forms">
                        <b><?=lang('nick')?></b><br/>
                        <input type="text" name="nick" value="<?=$nick?>" />
                    </div>
                    <div class="contbody_forms">
                        <b><?=lang('pass')?></b><br/>
                        <input type="password" name="pass" value="" /><br/>

                        <input type="checkbox" id="memch" name="mem"/>
                        <label for="memch"><?=lang('remember')?></label>
                    </div>
                    <?if($capcha):?>
                    <div class="contbody_forms">
                        <b><?=lang('capcha')?></b><br/>
                        <?=$capcha?><br/>
                        <input type="text" name="vcode" size="4" value="" />
                    </div>
                    <?endif?>
                    <div class="contfin_forms">
                        <input type="submit" value="<?=lang('save')?>" />
                    </div>
                </form>
            </div>

        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню:</div>
                <div class="elmenu"><a href="<?=$home?>/user/register"><?=lang('register')?></a></div>
                <div class="elmenu"><a href="<?=$home?>/user/recpassword"><?=lang('forg_pass')?></a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>