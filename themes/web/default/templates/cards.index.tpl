<link rel="stylesheet" type="text/css" href="<?=$themepath?>/styles/card.css?id=<?=$now_id?>"/>
<div id="floatTip"></div>
<table>
    <tr>
        <td valign="top">
            <div in-fav="<?=$lotFav ? 'on' : 'off'?>" class="content">
                <div class="conthead">
                    <h2><i class="icon-newspaper"></i>
                        <? if((int)$id_user!==0 ){ ?>
                            Лот №<?=$lotnumber?>
                        <? }else{ ?>
                        <i class="fa fa-lock"></i>
                        <? } ?>
                     </h2>
                    <?if($isBn):?>
                    <a href="<?= core::$home ?>/zayavka?dann&id=<?= $id ?>" class="button" style="float: right; margin-top: -25px; " target="_blank">Оформить заявку на участие в торгах</a>
                    <?endif?>
                    <?if(!empty($organizer['email'])):?>
                    <button  class="button cog-alt" style="float: right;margin-top: -26px;height: 30px; margin-right: 5px; " target="_blank" onclick="$('#sendMessage').fadeIn();$('.backdor').fadeIn();$('.modal-window#sendMessage').css({top: '25%'})">
                        <span class="icon-cog-alt-img"></span>  Запросить документы у Орг.Торг
                    </button>
                    <?endif?>
                    <!-- <a href="<?= core::$home ?>/assistance" class="button"  style="float: right; margin-top: -25px;margin-right: 10px;" target="_blank">
                         Запросить документы у Орг.Торг
                     </a>-->

                    <a href="<?= core::$home ?>/request?lotid=<?= $id ?>" class="button" style="display:none; float: right; margin-top: -25px; margin-right: 10px;" target="_blank">
                        Заказать ЭЦП/КЦП
                    </a>
                </div>

                <?if($photoArr):?>
                <? temp::include('block.photos.tpl') ?>
                <?endif?>

                <div class="contbody_forms" id="lotdescr">
                    <b>Название лота:</b><span style="float: right"><a href="/news/view?id=68" target="_blank">Дополнительный поиск: avito, google, росреестр, яндекс... / Как искать?</a></span><br/>
                    <?=$lotdescr?>
                    <hr style="margin: 7px 0"/>
                    <i class="icon-location"></i><b><?=$lotregion?></b>
                    &nbsp;&nbsp;&nbsp;
                    <span class="icon_to_click_fav">
                        <?if($lotFav):?>
                            <span cmd="fav|<?=$src?>|del|<?=$id?>" class="icon-star-clicked">Удалить лот из избранного</span>
                        <?else:?>
                            <span cmd="fav|<?=$src?>||<?=$id?>" class="icon-star-empty">Добавить лот в избранное</span>
                        <?endif?>
                    </span>
                    &nbsp;&nbsp;&nbsp;
                    <span class="icon_to_click_hide">
                        <?if($lotHide):?>
                            <span cmd="hide|<?=$src?>|del|<?=$id?>" class="icon-forward">Достать лот из мусора</span>
                        <?else:?>
                            <span cmd="hide|<?=$src?>||<?=$id?>" class="icon-delete">Отправить лот в мусор</span>
                        <?endif?>
                    </span>
                    &nbsp;&nbsp;&nbsp;
                    <?if(CAN('lot_complain')):?>
                    <span class="icon_compl"><a class="link_compl" href="<?=$home?>/support/newticket?lot_id=<?=$id?>">Пожаловаться на лот</a></span>
                    <?endif?>
                    &nbsp;&nbsp;&nbsp;
                    <!--span class="icon_to_click_complaint" complaint_attr="<?=$id?>">
                        <?if($lotcomplaint):?>
                            <i class="icon-redstar-clicked"></i><span id="complaint_info">Снять жалобу с лота</span>
                        <?else:?>
                            <i class="icon-star-empty"></i><span id="complaint_info">Пожаловаться на лот</span>
                        <?endif?>
                    </span-->

                    <? if(core::$rights == 100||core::$rights==70): ?>
                        <style>
                            .modal-window{
                                position: fixed;
                                top: 212px;
                                width: 600px;
                                left: 50%;
                                margin-left: -100px;
                                z-index: 10000;
                            }

                            .backdor{
                                position: fixed;
                                top: 0;
                                left: 0;
                                bottom: 0;
                                right: 0;
                                z-index: 9999;
                                display: none;
                                background-color: rgba(0,0,0,.5);
                            }
                        </style>
                        <script>
                            $(function () {
                                $("#toggle-name").click(function () {
                                    if ($(this).attr('data-open')=='0'){
                                        $(this).html('скрыть').attr('data-open', 1);
                                        $("#name-box").slideDown();
                                    } else {
                                        $(this).html('показать').attr('data-open', 0);
                                        $("#name-box").slideUp();
                                    }
                                })

                                $("#toggle-address").click(function () {
                                    if ($(this).attr('data-open')=='0'){
                                        $(this).html('скрыть').attr('data-open', 1);
                                        $("#desc-box").slideDown();
                                    } else {
                                        $(this).html('показать').attr('data-open', 0);
                                        $("#desc-box").slideUp();
                                    }
                                })


                                $("#toggle-phoneot").click(function () {
                                                                        if ($(this).attr('data-open')=='0'){
                                                                                $(this).html('скрыть').attr('data-open', 1);
                                                                               $("#phoneot-box").slideDown();
                                                                            } else {
                                                                                $(this).html('показать').attr('data-open', 0);
                                                                                $("#phoneot-box").slideUp();
                                                                            }
                                                                    })

                                                                $("#toggle-emailot").click(function () {
                                                                        if ($(this).attr('data-open')=='0'){
                                                                                $(this).html('скрыть').attr('data-open', 1);
                                                                                $("#emailot-box").slideDown();
                                                                            } else {
                                                                                $(this).html('показать').attr('data-open', 0);
                                                                                $("#emailot-box").slideUp();
                                                                            }
                                                                    })


                            });
                        </script>
                        <a href="javascript:;" onclick="$('#edit-lot').fadeIn();$('.backdor').fadeIn();" class="edit aright">Изменить</a>
                        <div class="backdor" onclick="$('#edit-lot').fadeOut();$('.backdor').fadeOut();$('#sendMessage').fadeOut();"></div>
                        <div class="modal-window" style="display: none" id="edit-lot">
                            <div class="close_modal_img_editlot" onclick="$('#edit-lot').fadeOut();$('.backdor').fadeOut();"><span id="icon_close_butt"><i class="icon-cancel-circled"></i></span></div>
                            <form name="mess" action="" method="post">
                                <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                                <div class="content">
                                    <div class="conthead">
                                        <h2><i class="icon-cog-alt"></i>Редактирование лота</h2>
                                        <div class="contfin_forms">
                                            <input name="save_edit_admin" type="submit" value="Сохранить">
                                        </div>
                                    </div>


                                    <div class="contbody_forms forms_blocks">
                                        <div class="contbody_forms_blocks">
                                            <div class="tel_ot_left">
                                                <b>Название лота:</b>&nbsp;&nbsp;<a href="javascript:;" data-open="0"
                                                                                    id="toggle-name">показать</a> <br>
                                                <div class="texta" style="display: none;padding-left: 8px;"
                                                     id="name-box"><textarea name="name"
                                                                             rows="4"><?=$origin_name?></textarea></div>
                                            </div>
                                            <div class="tel_ot">
                                                <b>Телефон OT:</b>&nbsp;&nbsp;<a href="javascript:;" data-open="0"
                                                                                 id="toggle-phoneot">показать</a> <br>
                                                <div class="texta" style="display: none;padding-left: 8px;"
                                                     id="phoneot-box"><textarea name="phoneot"
                                                                                rows="4"><?=$origin_phoneot?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contbody_forms forms_blocks">
                                        <div class="contbody_forms_blocks">
                                            <div class="tel_ot_left">
                                                <b>Адрес лота:</b>&nbsp;&nbsp;<a href="javascript:;" data-open="0"
                                                                                 id="toggle-address">показать</a> <br>
                                                <div class="texta" style="display: none;padding-left: 8px;"
                                                     id="desc-box"><textarea name="address"
                                                                             rows="4"><?=$origin_address?></textarea>
                                                </div>
                                            </div>
                                            <div class="tel_ot">
                                                <b>Email ОТ:</b>&nbsp;&nbsp;<a href="javascript:;" data-open="0"
                                                                               id="toggle-emailot">показать</a> <br>
                                                <div class="texta" style="display: none;padding-left: 8px;"
                                                     id="emailot-box"><textarea name="emailot"
                                                                                rows="4"><?=$origin_emailot?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="contbody_forms">
                                        <b>Текущая цена:</b><br>
                                        <div class="texta">
                                            <input type="text" name="now_price" value="<?=$now_clear_price ?>">
                                        </div>
                                    </div>
                                    <div class="contbody_forms">
                                        <b>Регион:</b><br>
                                        &nbsp;&nbsp;<select name="place">
                                            <? $selected = '';?>
                                            <? foreach($regions as $region): ?>
                                            <? if($region['name'] == $lotregion): ?>
                                            <? $selected = 'selected'; ?>
                                            <? else: ?>
                                            <? $selected = ''; ?>
                                            <? endif; ?>
                                            <option value="<?= $region['number'] ?>" <?= $selected ?>><?= $region['mini'] ?></option>
                                            <? endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="contbody_forms">
                                        <b>Категория:</b><br>
                                        &nbsp;&nbsp;<select name="category">
                                            <? $selected = '';?>
                                            <? foreach($categories_select as $category_select): ?>
                                            <? if($category_select['name'] == $category): ?>
                                            <? $selected = 'selected'; ?>
                                            <? else: ?>
                                            <? $selected = ''; ?>
                                            <? endif; ?>
                                            <option value="<?= $category_select['id'] ?>" <?= $selected ?>><?= $category_select['name'] ?></option>
                                            <? endforeach; ?>
                                        </select>
                                    </div>

                                </div>
                            </form>
                        </div>
                    <? endif; ?>
                </div>


                <? if($request_doc){ ?>

                        <div class="modal-window" style="display: none" id="sendMessage">
                            <div class="close_modal_img_editlot" onclick="$('#sendMessage').fadeOut();$('.backdor').fadeOut();"><span id="icon_close_butt"><i class="icon-cancel-circled"></i></span></div>
                            <div class="content">
                                <div class="conthead">
                                    <h2><i class="icon-cog-alt"></i>Запрос организатору торгов</h2>
                                </div>

                                <form action="" method="post" name="form_requeest" id="send_request">
                                    <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
                                    <div class="contbody_forms forms_blocks">
                                        <div class="contbody_forms_blocks_blocks">
                                            <label>Шаблон запроса
                                                <select onchange="getShab()" id="shab_request">
                                                    <option>автомобиль</option>
                                                    <option>квартира</option>
                                                    <option>земля</option>
                                                    <option>дебиторка</option>
                                                    <option>уставной капитал</option>
                                                </select>
                                            </label><br>


                                            <label style="">Кому: <input type="text" name="request_to" value="<?=!empty($organizer['email'])?$organizer['email']:''?>" class="send_mess"></label>

                                            <label>Ваше имя: <input type="text" onchange="getShab()" name="request_name" value="" class="send_mess"></label><br>

                                            <label>Тема: <input type="text" name="request_title" value="Запрос документов по торгам" class="send_mess"></label>
                                            <label>Ваш email: <input type="text" onchange="getShab()" name="request_email" value="" class="send_mess"></label><br>

                                            <textarea name="request_text" id="request_text_editor"></textarea>


                                            <button type="submit" name="ruquestSubmit" value="1" id="sendRequest_button">Отправить</button>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                <? }else{ ?>
                <div class="modal-window" style="display: none" id="sendMessage">
                    <div class="close_modal_img_editlot" onclick="$('#sendMessage').fadeOut();$('.backdor').fadeOut();"><span id="icon_close_butt"><i class="icon-cancel-circled"></i></span></div>
                    <div class="content">
                        <div class="conthead">
                            <h2><i class="icon-cog-alt"></i>Запрос организатору торгов</h2>
                            <p>Сервис доступен при подписке "ВИП тариф"</p>
                            <div class="contfintext">
                                <center><a class="urlbutton" href="http://bsd.bankrot-spy.ru/tariffs">Перейти к оплате</a></center>
                            </div>
                        </div>

                </div>

                </div>
                <? } ?>

                <? temp::include('block.notes.tpl') ?>

                <div class="contfin_forms_delimiter">

                </div>
                <div class="contbody_forms bs_index_table">
                    <? if(CAN('phone_email_ot')): ?>
                    <div class="contact_wrapper">
                        <?if($organizer['phone']):?>
                        <div class="button_wrapper phone_button">
                            <div class="ico_phone"></div>
                            <a href="skype:<?=$organizer['phone']?>?call"><?=$organizer['phone']?></a>
                        </div>
                        <?endif?>
                        <?if($organizer['email']):?>
                        <div class="button_wrapper email_button">
                            <div class="ico_email"></div>
                            <a href="mailto:<?=$organizer['email']?>"><?=$organizer['email']?></a>
                        </div>
                        <?endif?>
                    </div>
                    <? endif; ?>
                    <table class="lottable">
                        <tr>
                            <td style="width: 300px;"><b>Тип имущества:</b><br/></td>
                            <td><?=$srcType?></td>
                        </tr>
                    </table>
                    <hr/>
                    <table class="lottable">
                        <?if(!$priceSchedule || !CAN('price_reduction_schedule')):?>
                        <tr>
                            <td style="width: 300px;"><b>Начальная цена:</b><br/></td>
                            <td class="icon-rouble"> <?=$lotprice?></td>
                        </tr>
                        <?if($isBn):?>
                        <tr>
                            <td><b>Текущая цена:</b><br/></td>
                            <td>
                                <?if($nowprice):?>
                                <i class="icon-rouble"></i> <?=$nowprice?>
                                <?endif?>
                            </td>
                        </tr>
                        <?endif?>
                        <?endif?>
                        <?if($priceSchedule):?>
                        <?if($organizer['phone'] || $organizer['email']):?>
                        <tr><td><div style="min-height: 18px;"></div></td><td></td></tr>
                        <?endif?>
                        <tr>
                            <td style="width: 300px;"><b>График понижения цены:</b><br></td>
                            <?if(CAN('price_reduction_schedule')):?>
                            <td id="card-price-schedule" actual class="price-schedule">
                                <?if($priceSchedule[0]['st'] == 'ps-prev'):?>
                                <div onclick="psFull();" class="ps-oper">показать весь график</div>
                                <script>
                                    function psFull(){
                                        if($('#card-price-schedule').attr('actual') !== undefined){
                                            document.getElementById('card-price-schedule').removeAttribute('actual');
                                            $('.ps-oper').text("скрыть график");
                                        } else {
                                            document.getElementById('card-price-schedule').setAttribute('actual', '');
                                            $('.ps-oper').text("показать весь график");
                                        }
                                    }
                                </script>
                                <?endif?>
                                <table>
                                    <?foreach($priceSchedule as $row):?>
                                    <tr class="<?=$row['st']?>">
                                        <td class="icon-rouble"> <?=$row['pr']?></td>
                                        <td><?=$row['title']?></td>
                                    </tr>
                                    <?endforeach?>
                                </table>
                            </td>
                            <?else:?>
                            <td><i class="fa fa-lock"></i></td>
                            <?endif?>
                        </tr>
                        <?elseif($priceScheduleOrigin):?>
                        <tr>
                            <td style="width: 300px;"><b>График понижения цены:</b><br></td>
                            <?if(CAN('price_reduction_schedule')):?>
                            <td><?=$priceScheduleOrigin?></td>
                            <?else:?>
                            <td><i class="fa fa-lock"></i></td>
                            <?endif?>
                        </tr>
                        <?endif?>
                    </table>
                    <?if($priceExtra):?>
                    <table class="lottable">
                        <tr data-lotid="<?= $id ?>">
                            <td style="width: 300px;"><b>Рыночная цена:</b><br/></td>
                            <?if(CAN('market_price')):?>
                            <td>
                                <?if($realPriceIsNumeric):?>
                                <i class="icon-rouble"></i> <?=$realprice?>
                                <?if($market_price_hint):?>
                                <?if(CAN('cost_meter')):?>
                                <a href="<?=$market_price_link?>" target="_blank"><?=$market_price_hint?></a>
                                <?else:?>
                                <div onmouseout="tdMouseOut()" onmouseover="tdMouseOver('Данная функция доступна на тарифном плане VIP', this)" style="display: inline-block">
                                    (<?=$market_price_hint?>)
                                </div>
                                <?endif?>
                                <?endif?>
                                <?else:?>
                                <a class="get_lot_price"><span>Узнать</span></a>
                                <?endif?>
                            </td>
                            <?else:?>
                            <td class="icon-rouble"><i class="fa fa-lock"></i></td>
                            <?endif;?>
                        </tr>
                        <?if($isBn):?>
                        <tr>
                            <td><b>Шаг цены:</b><br/></td>
                            <td>5%</td>
                        </tr>
                        <?endif?>
                        <?if(isset($stepUp)):?>
                        <tr>
                            <td><b>Шаг аукциона:</b><br/></td>
                            <td class="icon-rouble"> <?=$stepUp?></td>
                        </tr>
                        <?endif?>
                        <?if(isset($stepDown)):?>
                        <tr>
                            <td><b>Шаг понижения:</b><br/></td>
                            <td class="icon-rouble"> <?=$stepDown?></td>
                        </tr>
                        <?endif?>
                        <tr>
                            <td><b> Задаток:</b><br/></td>
                            <?if(isset($deposit)):?>
                            <td class="icon-rouble"> <?=$deposit?></td>
                            <?else:?>
                            <td>10%</td>
                            <?endif?>
                        </tr>
                        <?if(isset($currency)):?>
                        <tr>
                            <td><b>Валюта:</b><br/></td>
                            <td><?=$currency?></td>
                        </tr>
                        <?endif?>
                        <?if(isset($cadastr_price)):?>
                        <tr>
                            <td><b>Кадастровая стоимость:</b><br/></td>
                            <?if(CAN('market_price')):?>
                            <td class="icon-rouble"> <?=$cadastr_price?></td>
                            <?else:?>
                            <td class="icon-rouble"><i class="fa fa-lock"></i></td>
                            <?endif?>
                        </tr>
                        <?endif?>
                        <tr>
                            <td style="width: 300px;"><b>Доход:</b><br/></td>
                            <?if(CAN('profit_rub')):?>
                            <td class="icon-rouble"> <?=$profitrub?>
                            <?if($profitproc AND $profitproc != '-'):?>
                              (<?=$profitproc?>%)
                            <?endif?>
                            </td>
                            <?else:?>
                            <td class="icon-rouble"><i class="fa fa-lock"></i></td>
                            <?endif?>
                        </tr>
                        <?if(isset($depend)):?>
                        <tr>
                            <td><b>Обременение:</b><br/></td>
                            <td><?=$depend?></td>
                        </tr>
                        <?endif?>
                    </table>
                    <?endif?>
                    <?if($isBn && $debExtra):?>
                    <table class="lottable">
                        <tr>
                            <td style="width: 300px;"><b>Баллы:</b><br/></td>
                            <td <?=$customclassdeb?> <?=$additionhtmldeb?> > <?=$debpoints?></td>
                        </tr>
                    </table>
                    <?endif?>
                    <hr/>
                    <table class="lottable">
                        <tr>
                            <td style="width: 300px;"><b>Категория:</b><br/></td>
                            <td><?if($category):?><?=$category?><?else:?>Прочее<?endif?></td>
                        </tr>
                        <?if(isset($subject)):?>
                        <tr>
                            <td><b>Предмет:</b><br/></td>
                            <td><?=$subject?></td>
                        </tr>
                        <?endif?>
                        <?if(isset($property)):?>
                        <tr>
                            <td><b>Вид собственности:</b><br/></td>
                            <td><?=$property?></td>
                        </tr>
                        <?endif?>
                        <tr>
                            <td style="width: 300px;"><b>Адрес:</b><br/></td>
                            <td>
                                <?if($origin_address):?>
                                <?=$origin_address?>
                                <?if($address_by):?>
                                <span style="color: red;">[<?=$address_by?>]</span>
                                <?endif?>
                                <?else:?>нет<?endif?>
                            </td>
                        </tr
                        <tr>
                            <td style="width: 300px;"><b>Тип торгов:</b><br/></td>
                            <td><?=$lottype?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Статус торгов:</b><br/></td>
                            <td><?=$lotstatus?></td>
                        </tr>
                        <?if(!isset($lotIsLiveAuction)):?>
                        <tr>
                            <td style="width: 300px;"><b>Дата начала подачи заявок:</b><br/></td>
                            <td><?=$lotstarttime?></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Дата окончания подачи заявок:</b><br/></td>
                            <td><?=$lotendtime?></td>
                        </tr>
                        <?endif?>
                    </table>
                    <hr/>
                    <?if(CAN('lot_info')):?>
                    <?if($isBn):?>
                    <table class="lottable">
                        <tr>
                            <td style="width: 300px;"><b>Банкрот:</b><br/></td>
                            <td>
                                <?if($debtor):?>
                                    <?if($debtor_profile):?>
                                        <a target="_blank" href="<?=$debtor_profile?>"><i class="icon-globe-table"></i>
                                            <?=$debtor?>
                                        </a>
                                    <?else:?>
                                        <?=$debtor?>
                                    <?endif?>
                                <?else:?>
                                    <span style="color:#95968d">нет</span>
                                <?endif?>
                            </td>
                        </tr>
                        
                        <? if ( strlen($case_number) > 5 ) : ?>
                        <tr>
                            <td style="width: 300px;"><b>Дело №:</b><br/></td>
                            <?
                            if(core::$all_rights[core::$rights]['delo_number']== 1){
                            ?>
                            <td><?=$case_number?></td>
                            <? } else { ?>
                            <td><i class="fa fa-lock"></i></td>
                            <? } ?>
                        </tr>
                        <? endif ?>
                        
                        <tr>
                            <td style="width: 300px;"><b>ИНН банкрота:</b><br/></td>
                            <?  
                                if(core::$all_rights[core::$rights]['inn_bancrot']== 1){
                            ?>
                            <td>
                                <? if((int)$id_user!==0 ){ ?>
                                    <?if($debtor_inn):?><?=$debtor_inn?><?else:?><span style="color:#95968d">нет</span><?endif?>
                                    <? }else{ ?>
                                        <i class="fa fa-lock"></i>
                                    <? } ?>
                            </td>
                            <? } else { ?>
                            <td><i class="fa fa-lock"></i></td>
                            <? } ?>
                        </tr>
                        <?if(isset($debtorOtherLots)):?>
                        <tr>
                            <td><b>Другие лоты:</b></td>
                            <td><?=$debtorOtherLots?></td>
                        </tr>
                        <?endif?>
                        <tr>
                            <td><b>Сообщения:</b></td>
                            <?if(isset($debtorMsgs) && $debtorMsgs):?>
                            <td class="msg-cont">
                                <table>
                                <?foreach($debtorMsgs as $item):?>
                                <tr class="<?=$item[pr]?>">
                                    <td><?=$item['date']?></td>
                                    <td><a href="<?=$item['link']?>" target="_blank"><?=$item['title']?></a></td>
                                </tr>
                                <?endforeach?>
                                </table>
                            </td>
                            <?else:?>
                            <td>нет</td>
                            <?endif?>
                        </tr>
                    </table>
                    <hr/>
                    <?endif?>
                    <table class="lottable">
                        <tr>
                            <td style="width: 300px;"><b>Организатор торгов<?if($organizer_profile):?> (ссылка на карточку на федресурсе)<?endif?>:</b><br/></td>
                            <td>
                                <?if($organizer['name']):?>
                                    <?if($organizer_profile):?>
                                        <a target="_blank" href="<?=$organizer_profile?>"><i class="icon-globe-table"></i>
                                        <?=$organizer['name']?>
                                        </a>
                                    <?else:?>
                                        <?=$organizer['name']?>
                                    <?endif?>
                                <?else:?>
                                    <span style="color:#95968d">нет</span>
                                <?endif?>
                            </td>
                        </tr>
                        <?if(isset($decision)):?>
                        <tr>
                            <td><b>Решение:</b><br/></td>
                            <td><?=$decision?></td>
                        </tr>
                        <?endif?>
                        <?if($isBn):?>
                        <tr>
                            <td style="width: 300px;"><b>Контактное лицо организатора торгов:</b><br/></td>
                            <td>
                                <?if($organizer['person']):?>
                                <?=$organizer['person']?>
                                <?else:?>
                                <span style="color:#95968d">нет</span>
                                <?endif?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td><b>Телефон:</b><br/></td>
                            <td>
                                <?if(CAN('phone_email_ot')):?>
                                    <?if($organizer['phone']):?>
                                    <?=$organizer['phone']?>
                                    <?else:?>
                                    <span style="color:#95968d">нет</span>
                                    <?endif?>
                                <?else:?>
                                    <i class="fa fa-lock"></i>
                                <?endif?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>Email:</b><br/></td>
                            <td>
                                <?if(CAN('phone_email_ot')):?>
                                    <?if($organizer['email']):?>
                                    <?=$organizer['email']?>
                                    <?else:?>
                                    <span style="color:#95968d">нет</span>
                                    <?endif?>
                                <?else:?>
                                    <i class="fa fa-lock"></i>
                                <?endif?>
                            </td>
                        </tr>
                       
                        <tr>
                            <td style="width: 300px; background:#eee;"><b>Арбитражный управляющий:</b><br/></td>
                            <td style="background:#eee;">
                                <? if($organizer['manager']): ?>
                                    <a target="_blank" href="/amc/<?= $oid ?>"><i class="icon-globe-table"></i>
                                    <?=$organizer['manager']?>
                                    </a>
                                <? else: ?>
                                  <span style="color:#95968d">нет</span>
                                <? endif ?>
                                &nbsp;&nbsp;&nbsp;Рейтинг:&nbsp;&nbsp;
                                <a <?=($rating > 5)?'plus':'minus'?> href="/amc/<?=$oid?>" target="_blank"><?= $rating ?></a>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 300px;"><b>ИНН организатора:</b><br/></td>
                            <td>
                                <? if((int)$id_user!==0 ){ ?>
                                <?if($inn_orgname):?><?=$inn_orgname?><?else:?><span style="color:#95968d">нет</span><?endif?>
                                <? }else{ ?>
                                    <i class="fa fa-lock"></i>
                                <? } ?>
                            </td>
                        </tr>
                        <?endif?>
                        <?if(isset($final)):?>
                        <tr>
                            <td style="width: 300px;"><b>Место и срок подведения итогов:</b><br/></td>
                            <td><?=$final?></td>
                        </tr>
                        <?endif?>
                    </table>
                    <hr/>
                    <table class="lottable">
                        <?if($isBn):?>
                        <tr>
                            <td style="width: 300px;"><b>Номер торгов:</b><br/></td>
                            <td>
                                <? if($code_torg !== '-1'): ?>
                                    <?=$code_torg?>
                                <? else: ?>
                                    <i class="fa fa-lock"></i>
                                <? endif; ?>
                            </td>
                        </tr>
                        <?endif?>
                        <tr>
                            <td style="width: 300px;"><b>Торги на площадке:</b><br/></td>
                            <td>
							<?  		if ( strlen($auct_link)<3) { $auct_link="http://".$platform_url; }   
							?>
                                <? if($auct_link !== '-1'): ?>
                                <a target="_blank" href="<?=$auct_link?>"  onmouseover="toolTip('Если ссылка ведет на страницу с ошибкой, значит торги завершены досрочно. Сделайте поиск на площадке по коду торгов.')" onmouseout="toolTip()">
                                    <i class="icon-globe-table"></i><?=$platform_url?>
                                </a>
                                <? else: ?>
                                <i class="fa fa-lock"></i>
                                <? endif; ?>
                            </td>
                        </tr>
                        <?if($isBn):?>
                        <tr>
                            <td style="width: 300px;"><b>Лот на федресурсе:</b><br/></td>
                            <td>
                                <? if($fedlink !== '-1'): ?>
                                    <? if($fedlink): ?>
                                    <a target="_blank" href="<?= $fedlink ?>" onmouseover="toolTip('Если ссылка ведет на страницу с ошибкой, значит торги завершены досрочно. Сделайте поиск на площадке по коду торгов.')" onmouseout="toolTip()">
                                        <i class="icon-globe-table"></i>fedresurs.ru
                                    </a>
                                    <? else: ?>
                                    нет
                                    <? endif; ?>
                                <? else: ?>
                                    <i class="fa fa-lock"></i>
                                <? endif; ?>
                            </td>
                        </tr>
                        <? if (strlen($reportLink) > 5) : ?>
                            <tr>
                                <td style="width: 300px;"><b>Отчет оценщика:</b></td>
                                <td><i class="icon-globe-table"></i> <a href="<?=$reportLink?>" target="_blank">Отчет</a></td><td><span style="color:red;">ВАЖНО! Если Вы не нашли данный лот в отчете оценщика, тогда ищите его в документах на федресурсе (ссылка выше).</span></td>
                            </tr>
                        <? endif; ?>
                        <?endif?>
                    </table>
                    <? if(isset($similarDataPrice)): ?>
                    <? if($similarDataPrice == 'access'): ?>
                    <table style="text-align: center;" class="lottable">
                        <tr>
                            <td>
                                <div id="grapch">
                                    <strong style="font-size:18px;">
                                        <? if($graphType == 1): ?>
                                        Цены на аналогичные товары
                                        <? else: ?>
                                        Цена 1м2 аналогичных предложений
                                        <? endif; ?>
                                    </strong>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Информация доступна на тарифном плане VIP</td>
                        </tr>
                    </table>
                    <? else: ?>
                    <table class="lottable">
                        <tr>
                            <td>
                                <div id="grapch"></div>
                            </td>
                        </tr>
                    </table>
                    <?endif;?>
                    <?endif;?>
                    <?else:?>
                    <table class="lottable">
                        <tr>
                            <td>
                                <div>Полная информация по лоту доступна после оформления <a href="/tariffs">подписки</a></div>
                            </td>
                        </tr>
                    </table>
                    <?endif?>
                </div>
            </div>

        </td>
        <td class="right_back_menu">
            <div class="right_panel_conf">
                <div class="menu_rt">Меню</div>
                <div class="elmenu"><a href="<?=$home?>">На главную</a></div>
                <div class="down_rmenu"> </div>
            </div>
        </td>
    </tr>
</table>

<style>
    #cke_15,#cke_22,#cke_30,#cke_35,#cke_67,#cke_71,#cke_81,#cke_84,#cke_87{
        display: none!important;
    }
</style>

<? if(isset($similarDataPrice) && $similarDataPrice !== 'access'): ?>
<? temp::include('block.similar_prices.tpl') ?>
<? endif; ?>

<script type="text/javascript">
    var engine_formid = <?=core::$formid?>;

    function action_complaint_cards(lot, action, item) {
        if(action == 1) {
            //Снимаем жалобу
            $.post(
                    "/tabledata/complaint",
                    {
                        itemid: lot,
                        actionid: action,
                        formid: engine_formid
                    },
                    function(data) {
                        if(data == 'ok')
                            create_notify('Жалоба на лот снята!');
                        else {
                            create_notify('Ошибка! Только для зарегистрированных пользователей!');
                            $(item).find('i').attr('class', 'icon-graystar-clicked');
                            $('#hide_info').text('Снять жалобу с лота.');
                        }
                    }
            );
        } else {
            //пожаловаться на лот
            $.post(
                    "/tabledata/complaint",
                    {
                        itemid: lot,
                        actionid: action,
                        formid: engine_formid
                    },
                    function(data) {
                        if(data == 'ok')
                            create_notify('На лот подана жалоба!');
                        else
                        {
                            create_notify('Ошибка! Только для зарегистрированных пользователей!');
                            $(item).find('i').attr('class', 'icon-star-empty');
                            $('#hide_info').text('На лот подана жалоба');
                        }
                    }
            );
        }
    }

    function listen_to_complaint_cards(item) {
        var item_id = $(item).attr('complaint_attr');
        var now_class = $(item).find('i').attr('class');

        if( now_class == 'icon-redstar-clicked' ) {
            //Кнопка уже нажата
            $(item).find('i').attr('class', 'icon-star-empty');
            $('#complaint_info').text('Пожаловаться на лот');
            action_complaint_cards(item_id, 1, item);
        } else {
            //Кнопка не нажата
            $(item).find('i').attr('class', 'icon-redstar-clicked');
            $('#complaint_info').text('Снять жалобу с лота');
            action_complaint_cards(item_id, 0, item);
        }
    }

    $(document).on('click', '.icon_to_click_complaint', function(){
        listen_to_complaint_cards(this);
    });
</script>
<link rel="stylesheet" href="<?=$themepath?>/styles/popline.css" type="text/css">
<script type="text/javascript" src="<?=$themepath?>/js/jquery.popline.js"></script>
<script type="text/javascript" src="<?=$themepath?>/js/jquery.popline.social.js"></script>
<script type="text/javascript" src="/dscore/libs/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/dscore/libs/ckeditor/adapters/jquery.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#lotdescr").popline({mode: 'view'});
        getShab();

    });

    var ckeditor1 = CKEDITOR.replace( 'request_text_editor' );



    function getShab() {
        var shab_current = $('#shab_request').val();
        var request_name = $('input[name="request_name"]').val();
        var request_email = $('input[name="request_email"]').val();
        var textshab = '';

        textshab = '<p style="text-align: right;">Организатору торгов  <?=$organizer['name']?> от '+request_name+'</p>';
        textshab += '<p style="text-align: left;"><?=date("d-m-y")?></p>';
        textshab += '<h1 style="text-align: center">Уважаемый организатор торгов!</h1>';
        textshab += '<p style="text-align: left">Ознакомившись с сообщением о проведении электронных торгов по продаже имущества <?=$debtor?>:</p>';

        textshab += '<p style="text-align: left;"><b>Торги <?=$code_torg?> Лот №<?=$lotnumber?></b> <br><?=$lotdescr?></p>';

        textshab += '<p style="text-align: left;">Для решения об участии в торгах, прошу Вас предоставить мне следующую информацию:</p>';
        

        if(shab_current!=='undefined'){
            switch(shab_current) {
                case 'автомобиль':
                    textshab += '<h2 style="text-align: left">АВТОМОБИЛЬ</h2>';
                    textshab += '<ol>';
                    textshab += '<li>Документы, подтверждающие факт покупки автомобиля (договор, справка-счет);</li>';
                    textshab += '<li>Копию паспорта технического Средства (ПТС);</li>';
                    textshab += '<li>Копию свидетельства о регистрации ТС;</li>';
                    textshab += '<li>Копию талона техосмотра или диагностической карты;</li>';
                    textshab += '<li>Сертификат производителя;</li>';
                    textshab += '<li>Отчет оценщика;</li>';
                    textshab += '<li>Сервисную книжку автомобиля;</li>';
                    textshab += '<li>Акт дефектовки с описанием технического состояния транспортного средства;</li>';
                    textshab += '<li>Действующую страховку на транспортное средство;</li>';
                    textshab += '<li>Фотографии, отражающие состояние автомобиля на данный момент;</li>';
                    textshab += '<li>Положение о порядке и сроках реализации имущества должника;</li>';
                    textshab += '</ol>';
                    break;
                case 'квартира':
                    textshab += '<h2 style="text-align: left">КВАРТИРА</h2>';
                    textshab += '<ol>';

                    textshab += '<li>Свидетельство о праве собственности;</li>';
                    textshab += '<li>Правоустанавливающие документы (договора);</li>';
                    textshab += '<li>Кадастровый паспорт;</li>';
                    textshab += '<li>Экспликация БТИ;</li>';
                    textshab += '<li>Выписка из ЕГРП (если заказывали);</li>';
                    textshab += '<li>Информация по прописанным лицам (выписка из домовой книги);</li>';
                    textshab += '<li>Информация об обременениях или арестах;</li>';
                    textshab += '<li>Документы, на основании которых у кредитора возникло право на заложенное имущество;</li>';
                    textshab += '<li>Фотографии, отражающие состояние квартиры на данный момент;</li>';
                    textshab += '<li>Положение о порядке и сроках реализации имущества должника;</li>';
                    textshab += '<li>Оценку рыночной стоимости квартиры;</li>';

                    textshab += '</ol>';
                    break;
                case 'земля':

                    textshab += '<h2 style="text-align: left">ЗЕМЛЯ</h2>';
                    textshab += '<ol>';

                    textshab += '<li>Свидетельство о праве собственности;</li>';
                    textshab += '<li>Правоустанавливающие документы (договора);</li>';
                    textshab += '<li>Кадастровый паспорт участка;</li>';
                    textshab += '<li>Экспликация БТИ;</li>';
                    textshab += '<li>Выписка из ЕГРП (если заказывали);</li>';
                    textshab += '<li>Информация об обременениях или арестах;</li>';
                    textshab += '<li>Документы, на основании которых у кредитора возникло право на заложенное имущество;</li>';
                    textshab += '<li>Положение о порядке и сроках реализации имущества должника;</li>';
                    textshab += '<li>Оценку рыночной стоимости участка;</li>';

                    textshab += '</ol>';

                    break;
                case 'дебиторка':
                    textshab += '<h2 style="text-align: left">ДЕБИТОРКА</h2>';
                    textshab += '<ol>';

                    textshab += '<li>Договор-основание возникновения задолженности;</li>';
                    textshab += '<li>Платежные и иные документы, подтверждающие данную дебиторскую задолженность;</li>';
                    textshab += '<li>Акты сверки взаиморасчетов по поставкам, акты приемки-передачи, акты выполненных работ (выбрать нужное);</li>';
                    textshab += '<li>Информацию по дебитору (адреса, телефоны) и его платежах в погашение данной задолженности;</li>';
                    textshab += '<li>Постановление суда по иску к заемщику;</li>';
                    textshab += '<li>Исполнительный лист;</li>';
                    textshab += '<li>Заявление о возбуждении исполнительного производства;</li>';
                    textshab += '<li>Постановление о возбуждении исполнительного производства;</li>';
                    textshab += '<li>Переписку со службой судебных приставов;</li>';
                    textshab += '<li>Отчет оценщика по данной задолженности;</li>';
                    textshab += '<li>Положение о порядке и сроках реализации имущества должника;</li>';

                    textshab += '</ol>';
                    break;
                case 'уставной капитал':
                    textshab += '<h2 style="text-align: left">УСТАВНОЙ КАПИТАЛ</h2>';
                    textshab += '<ol>';

                    textshab += '<li>Учредительный договор и/или устав</li>';
                    textshab += '<li>Реестр акционеров/участвников</li>';
                    textshab += '<li>Платежные документы, подтверждающие оплату доли</li>';
                    textshab += '<li>Иные документы, подтверждающие право на долю</li>';
                    textshab += '<li>Балансы организации за три последних года</li>';
                    textshab += '<li>Отчет оценщика</li>';
                    textshab += '<li>Положение о порядке и сроках реализации имущества должника;</li>';

                    textshab += '</ol>';
                    break;

            }


            textshab += '<p style="text-align: left;">Для возможности проведения анализа и принятия решения, информацию и документы прошу вас направить на адрес электронной почты: '+request_email+'</p>';

            textshab += '<p style="text-align: left;">Заранее благодарен!<br>С уважением, '+request_name+'!</p>';

            CKEDITOR.instances['request_text_editor'].setData(textshab)
        }
    }


</script>