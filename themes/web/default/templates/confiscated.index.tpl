<form id="search_form">
    <input type="hidden" name="search_form_extend" id="search_form_extend" value="<?= GET('search_form_extend') ?>">

    <? if ( $main_adv_text && !$user_id ) : ?>
        <div class="adv-text-wrap">
            <div class="adv-text" style="margin-bottom: 10px; text-align: justify;">
                <div style="margin: 0px 20px; padding: 10px 0px; text-align: justify;"><?=$main_adv_text?></div>
            </div>
        </div>
    <? endif ?>
        <div class="lots_choice">
          <a href="/torgi">Банкротство</a>
          <span>Конфискат</span>
          <a href="/privatized">Приватизация</a>
        </div>
        <? temp::include('block.top.stats.tpl') ?>
    <div class="clear-fix"></div>

    <div class="container">
        <div class="row">
            <span id="on_load_new_page"></span>

            <div id="search-form-wrapper" class="column">
                <div class="panel-box">
                    <div id="search-form-header" class="panel-box-header">
                        <div class="row">
                            <div class="left column">
                                Настройки поиска:
                                <div class="select-form-type">
                                    <a href="javascript:void(0);" class="active" onclick="simple();" id="set-simple">Простой</a>
                                    /
                                    <a href="javascript:void(0);" onclick="extend();" id="set-extend">Расширенный</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="search-form-body" class="panel-box-body">
                        <div class="search-field">
                            <div class="row">
                                <div class="column left">
                                    <label for="svalname"><i class="icon-search"></i> Поиск по ключевым словам:</label> <a href="http://bankrot-spy.ru/articles/post35">Как искать?</a>
                                </div>
                                <div class="column right">
                                </div>
                            </div>
                            <input value="<?= $search_name ?>" type="search" name="search_name" id="svalname" placeholder="Название лота, город, организатор и т.п."/>
                            <p class="examples">Примеры: "Квартира Москва", "Toyota Camry", "Трактор Краснодар"</p>
                            <p class="help">Не нашли нужный лот? Воспользуйтесь <a href="javascript:void(0);" onclick="extend();">расширенным поиском</a>.</p>
                            <label class="all-keywords-checkbox" onmouseover="toolTip('Отображаются только лоты, которые<br />содержат все ключевые слова в совокупности')" onmouseout="toolTip()">
                                <input type="checkbox" name="all_keywords" <?=!empty($all_keywords) ? 'checked' : '' ?>/>
                                <span>Содержит все ключевые слова (жесткий поиск)</span>
                            </label>
                            <div id="search_advice_wrapper"></div>
                        </div>
                        <div id="extend-fields" class="row confiscated">
                            <div class="filed-group column column_1 first">
                                <i class="icon-search"></i> Категория и регион:<br/>
                                <select id="cat_select" name="cat_select" class="search_type_style">
                                    <option value="any">Все категории</option>
                                    <? foreach($data_cats as $item): ?>
                                    <option value="<?=$item?>" <?if($cat_select == $item):?>selected<?endif?>><?=$item?></option>
                                    <? endforeach; ?>
                                </select>
                                <select id="reg_select" name="reg_select" class="search_type_style">
                                    <option value="any">Все регионы</option>
                                    <? foreach($data_regions as $item): ?>
                                    <option value="<?=$item?>" <?if($reg_select == $item):?>selected<?endif?>><?=$item?></option>
                                    <? endforeach; ?>
                                </select>
                                <hr/>
                                <i class="icon-hammer-set"></i> Тип торгов:<br/>
                                <label><input style="width:20px;" type="checkbox" <?if($status_auct_1):?>checked<?endif?> class="cb_search" name="status_auct_1"/>Аукцион</label><br/>
                                <label><input style="width:20px;" type="checkbox" <?if($status_auct_2):?>checked<?endif?> class="cb_search" name="status_auct_2"/>Публичное предложение</label>
                            </div>
                            <div class="filed-group column column_4">
                                <i class="icon-rouble"></i> Цена лота:<br/>
                                <table class="nomarginnews">
                                    <tr>
                                        <td>С: </td><td><input type="text" id="price_start_forid" onkeyup="number_format(this.id);" name="price_start" value="<?=$price_start?>"/></td>
                                    </tr>
                                    <tr>
                                        <td>По: </td><td><input type="text" id="price_end_forid" onkeyup="number_format(this.id);" name="price_end" value="<?=$price_end?>"/></td>
                                    </tr>
                                </table>
                                <hr/>
                                <div class="column">
                                    <i class="icon-clock"></i> Статус:<br/>
                                    <label><input type="checkbox" name="status_auct_4" value="1" <?=($status_auct_4==1)?'checked':''?> />Объявленные</label><br/>
                                    <label><input type="checkbox" name="status_auct_5" value="1" <?=($status_auct_5==1)?'checked':''?>/>Окончены</label><br/>
                                </div>
                            </div>
                            <div class="filed-group column column_2">
                                <i class="icon-calendar"></i>
                                Дата <a class="by_start date_toggle_link active" href="javascript:void(0);" onclick="setDateFilerByStart();">начала</a>
                                / <a class="by_end date_toggle_link" href="javascript:void(0);" onclick="setDateFilerByEnd();">окончания</a>:<br/>

                                <input type="hidden" name="data_filter_type" value="<?=$data_filter_type?>"/>

                                <div class="data-fields">
                                    <div class="data-field">
                                        <label>С:</label> <input class="date-input" type="text" name="begin_set_date" value="<?=$begin_set_date?>"/>
                                    </div>
                                    <div class="data-field">
                                        <label>По:</label> <input class="date-input" type="text" name="end_set_date" value="<?=$end_set_date?>"/>
                                    </div>
                                </div>

                                <hr class="custom-margin-bottom"/>

                                <div>
                                    <input class="days" onmouseover="toolTip('Число или интервал,<br/> например: 2-5<hr/>Нельзя одновременно использовать Дату начала/окончания и эту функцию.')" onmouseout="toolTip()" type="text" name="altintconf" value="<?=$altintconf;?>"/>
                                    <span style="font-size: 13px">Дней до <span class="data_filter_label_text">начала</span></span>
                                </div>
                            </div>
                            <div class="filed-group column column_3">
                                <i class="icon-rouble"></i> Задаток:<br/>
                                <div class="data-fields">
                                    <div class="data-field">
                                        <label>С:</label> <input class="date-input" type="text" onkeyup="number_format(this.id)" name="begin_set_deposit" id="begin_set_deposit" value="<?=$begin_set_deposit?>"/>
                                    </div>
                                    <div class="data-field">
                                        <label>По:</label> <input class="date-input" type="text" onkeyup="number_format(this.id)" name="end_set_deposit" id="end_set_deposit" value="<?=$end_set_deposit?>"/>
                                    </div>
                                </div>
                                <hr/>
                                <label><input type="checkbox" name="more" value="1" <?=($more==1)?'checked':''; ?> style="margin-left: 6px;"/> <span>Подробнее</span></label><br/>
                                <label><input type="checkbox" name="fav" value="1" <?=($fav==1)?'checked':''; ?> style="margin-left: 6px;" class="active_tab"/> <span>Избранное</span></label>
                            </div>
                        </div>
                    </div>
                    <div id="search-form-footer" class="panel-box-footer">
                        <div class="row">
                            <div class="right column">
                                <div class="set_button_cont">
                                    &nbsp;
                                    <span id="clear_set_table" class="btn urlbutton_index button_no_top_index">Очистить</span>
                                    &nbsp;
                                    <input id="search_in_table" class="btn urlbutton_index button_no_top_index" type="submit" value="Искать"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="bs_news_container" class="column">
                <div class="panel-box">
                    <div class="panel-box-header">
                        Новости площадок
                    </div>
                    <div class="panel-box-body">
                        <div class="news_text" id="bs_news_text">
                            <?if($outnews):?>
                            <?foreach($outnews as $otn): ?>
                            <table class="newsitem_table">
                                <tr>
                                    <td style="width: 29px;">
                                        <i class="icon-newspaper"></i>
                                    </td>
                                    <td>
                                        <?=$otn['text']?><br/>
                                        <span class="undtexttime">Время: <b><?=$otn['time']?></b>, <?=$otn['data']?></span>
                                    </td>
                                </tr>
                            </table>
                            <hr class="hrnews"/>
                            <?endforeach?>
                            <?else:?>
                            <div style="text-align: center;margin-top: 10px;color:#b8baba;">Нет новостей</div>
                            <?endif?>
                        </div>
                    </div>
                    <div class="grback"></div>
                    <div class="panel-box-footer">
                        <a href="<?=$home?>/pnews" class="btn urlbutton_index button_no_top_index">Показать все</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        var extend_fields = $('#extend-fields');
        var news_body = $('#bs_news_container .panel-box .panel-box-body');
        var simpleToggle = $('.select-form-type #set-simple');
        var extendToggle = $('.select-form-type #set-extend');
        var search_form_extend_input = $('#search_form_extend');
        var extendToggleHelp = $('#search-form-body .help');
        var allKeywordsCheckbox = $('#search-form-body .all-keywords-checkbox');

        function new_block_resize()
        {
            var search_form_body_height = $('#search-form-wrapper .panel-box-body').height();
            news_body.css({height: search_form_body_height});
        }

        function simple(){
            search_form_extend_input.val(0);
            extendToggleHelp.show('fast');
            allKeywordsCheckbox.hide('fast');
            extend_fields.hide("fast", function() {
                extendToggle.removeClass('active');
                simpleToggle.addClass('active');
                new_block_resize()
            });

        }

        function extend(){
            search_form_extend_input.val(1);
            extendToggleHelp.hide('fast');
            allKeywordsCheckbox.show('fast');
            extend_fields.show("fast", function() {
                simpleToggle.removeClass('active');
                extendToggle.addClass('active');
                extend_fields.css('display', 'flex');
                new_block_resize()
            });
        }

        $(document).ready(function(){

            new_block_resize();

            // Какую форму показывать, расширенную или сокращенную
            var sfe = $('#search_form_extend').val();
            if ( sfe == 1 ) {
                extend();
            } else {
                simple();
            }

            $("#clear_set_table").click(function () {
                $('#search_form input[type="search"]').val('');
                $('#search_form input[type="text"]').val('');
                //$("input[type=checkbox]").prop('checked', false);
                $("#cat_select").prop('selectedIndex',0);
                $("#reg_select").prop('selectedIndex',0);
                $('#search_form').submit();
            });
        });

    </script>
<!--</form>-->
    <div id="floatTip" onmouseover="hintMouseOver()" onmouseout="hintMouseOut()"></div>

<div class="loadmess" id="loadmess"><i class="icon-spin5"></i> Загрузка</div>

<div class="content bs_index_table">
    <div class="contbody" style="padding: 0; border:0;">
        <div class="total_lots">
            <div class="info" style="width: 100%">
                <div>Показаны результаты: <?= $start ?>-<?= $end ?> из <?= $total ?></div>
                <img src="<?=$themepath?>/images/Excel_1.png" alt="Export" style="position: absolute;right: 0;width: 150px;top: -5px;cursor: pointer;" <? if(core::$all_rights[core::$rights]['export_all']==1): ?>id="export-all"<? endif; ?> onmouseover="tdMouseMove('Информация доступна на платной подписке <a href=\'#\'>ВИП тариф</a>',this)" onmouseout="tdMouseOut();">
                <? temp::formid() /* ЭТА ФУНКЦИЯ ОБЯЗАТЕЛЬНА ДЛЯ ВСЕХ ФОРМ!!! */?>
            </div>
        </div>
        <style>
            .total_lots{
                margin: 10px 20px;
                font-weight: bold;
                font-size: 12px;
                color: #b1b1b1;
            }
        </style>

        <script>
            $(function () {
                $('#export-all').click(function(e) {

                    $("#loadmess").css({'left':'50%', 'bottom':'50%'}).html('<i class="icon-spin5"></i> Загрузка').show();

                    var action = 'download';

                    var pst={};

                    var req=((window.location.search).replace('?', '')).split('&');
                    for (i=0;i<req.length;i++){
                        var tmp=req[i].split('=');
                        pst[tmp[0]]=tmp[1];
                    }

                    $.ajax({
                        type: 'POST',
                        url: '/tabledata/exportother?action='+action,
                        data: {formid: $(this).siblings('input[name=formid]').val(), pst: pst},
                        dataType: 'json',
                        success: function(data){
                            $("#loadmess").hide();
                            if(data.status == 0 || data.status == 1 || data.status == 3) {
                                create_notify(data.message);
                            } else if (data.status == 2) {
                                window.location = data.file;
                            } else {
                                create_notify('Произошла ошибка!');
                            }

                            load_table();
                        }
                    });
                });
            })
        </script>



        <table id="confiscated_tb" class="alt_lot_table table">
		<thead>
            <tr>
                <th>
                    Лот
                    <?if($sortField=='name'):?>
                        <?if($sortOrder == 'DESC'):?>
                            <a class="js_href" onclick="setSortValues('name', 'ASC')">
                                <img src="/themes/web/default/images/table/desc.png">
                            </a>
                        <?else:?>
                            <a class="js_href" onclick="setSortValues('name', 'DESC')">
                                <img src="/themes/web/default/images/table/asc.png">
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="js_href" onclick="setSortValues('name', 'ASC')">
                            <img src="/themes/web/default/images/table/bg.png">
                        </a>
                    <?endif;?>
                </th>
                <th>
                    Тип
                    <?if($sortField=='tradetip'):?>
                        <?if($sortOrder == 'DESC'):?>
                            <a class="js_href" onclick="setSortValues('tradetip', 'ASC')">
                                <img src="/themes/web/default/images/table/desc.png">
                            </a>
                        <?else:?>
                            <a class="js_href" onclick="setSortValues('tradetip', 'DESC')">
                                <img src="/themes/web/default/images/table/asc.png">
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="js_href" onclick="setSortValues('tradetip', 'ASC')">
                            <img src="/themes/web/default/images/table/bg.png">
                        </a>
                    <?endif;?>
                </th>
                <th>Регион</th>
                <th>Дата начала
                    <?if($sortField=='start_time'):?>
                        <?if($sortOrder == 'DESC'):?>
                            <a class="js_href" onclick="setSortValues('start_time', 'ASC')">
                                <img src="/themes/web/default/images/table/desc.png">
                            </a>
                        <?else:?>
                            <a class="js_href" onclick="setSortValues('start_time', 'DESC')">
                                <img src="/themes/web/default/images/table/asc.png">
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="js_href" onclick="setSortValues('start_time', 'ASC')">
                            <img src="/themes/web/default/images/table/bg.png">
                        </a>
                    <?endif;?>
                </th>
                <th>Дата окончания
                    <?if($sortField=='end_time'):?>
                        <?if($sortOrder == 'DESC'):?>
                            <a class="js_href" onclick="setSortValues('end_time', 'ASC')">
                                <img src="/themes/web/default/images/table/desc.png">
                            </a>
                        <?else:?>
                            <a class="js_href" onclick="setSortValues('end_time', 'DESC')">
                                <img src="/themes/web/default/images/table/asc.png">
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="js_href" onclick="setSortValues('end_time', 'ASC')">
                            <img src="/themes/web/default/images/table/bg.png">
                        </a>
                    <?endif;?>
                </th>
                <th>Статус</th>
                <th>Цена, руб
                    <?if($sortField=='price'):?>
                        <?if($sortOrder == 'DESC'):?>
                            <a class="js_href" onclick="setSortValues('price', 'ASC')">
                                <img src="/themes/web/default/images/table/desc.png">
                            </a>
                        <?else:?>
                            <a class="js_href" onclick="setSortValues('price', 'DESC')">
                                <img src="/themes/web/default/images/table/asc.png">
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="js_href" onclick="setSortValues('price', 'ASC')">
                            <img src="/themes/web/default/images/table/bg.png">
                        </a>
                    <?endif;?>
                </th>
                <th onmouseover="toolTip('Рыночная цена лота на рынке')" onmouseout="toolTip()">Рыночная цена, руб
                    <?if($sortField=='market_price'):?>
                        <?if($sortOrder == 'DESC'):?>
                            <a class="js_href" onclick="setSortValues('market_price', 'ASC')">
                                <img src="/themes/web/default/images/table/desc.png">
                            </a>
                        <?else:?>
                            <a class="js_href" onclick="setSortValues('market_price', 'DESC')">
                                <img src="/themes/web/default/images/table/asc.png">
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="js_href" onclick="setSortValues('market_price', 'ASC')">
                            <img src="/themes/web/default/images/table/bg.png">
                        </a>
                    <?endif;?>
                </th>
                <th onmouseover="toolTip('Разница между ценой на рынке и на торгах')" onmouseout="toolTip()">Доход, руб
                    <?if($sortField=='profit_rub'):?>
                        <?if($sortOrder == 'DESC'):?>
                            <a class="js_href" onclick="setSortValues('profit_rub', 'ASC')">
                                <img src="/themes/web/default/images/table/desc.png">
                            </a>
                        <?else:?>
                            <a class="js_href" onclick="setSortValues('profit_rub', 'DESC')">
                                <img src="/themes/web/default/images/table/asc.png">
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="js_href" onclick="setSortValues('profit_rub', 'ASC')">
                            <img src="/themes/web/default/images/table/bg.png">
                        </a>
                    <?endif;?>
                </th>
                <th onmouseover="toolTip('Вероятная доходность операции по приобретению лота на торгах и продаже его на рынке')" onmouseout="toolTip()">Доходность %
                    <?if($sortField=='profit_proc'):?>
                        <?if($sortOrder == 'DESC'):?>
                            <a class="js_href" onclick="setSortValues('profit_proc', 'ASC')">
                                <img src="/themes/web/default/images/table/desc.png">
                            </a>
                        <?else:?>
                            <a class="js_href" onclick="setSortValues('profit_proc', 'DESC')">
                                <img src="/themes/web/default/images/table/asc.png">
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="js_href" onclick="setSortValues('profit_proc', 'ASC')">
                            <img src="/themes/web/default/images/table/bg.png">
                        </a>
                    <?endif;?>
                </th>
                <th>Задаток
                    <?if($sortField=='zadatok'):?>
                        <?if($sortOrder == 'DESC'):?>
                            <a class="js_href" onclick="setSortValues('zadatok', 'ASC')">
                                <img src="/themes/web/default/images/table/desc.png">
                            </a>
                        <?else:?>
                            <a class="js_href" onclick="setSortValues('zadatok', 'DESC')">
                                <img src="/themes/web/default/images/table/asc.png">
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="js_href" onclick="setSortValues('zadatok', 'ASC')">
                            <img src="/themes/web/default/images/table/bg.png">
                        </a>
                    <?endif;?>
                </th>
                <th>Обременение
                    <?if($sortField=='obremenenie'):?>
                        <?if($sortOrder == 'DESC'):?>
                            <a class="js_href" onclick="setSortValues('obremenenie', 'ASC')">
                                <img src="/themes/web/default/images/table/desc.png">
                            </a>
                        <?else:?>
                            <a class="js_href" onclick="setSortValues('obremenenie', 'DESC')">
                                <img src="/themes/web/default/images/table/asc.png">
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="js_href" onclick="setSortValues('obremenenie', 'ASC')">
                            <img src="/themes/web/default/images/table/bg.png">
                        </a>
                    <?endif;?>
                </th>
                <th>Площадка</th>
                <th>
                    <?if($favAll):?>
                    <i cmd="fav|cn|del" title="Удалить все лоты из избранного" class="icon-star-clicked"></i>
                    <?else:?>
                    <i cmd="fav|cn|" title="Добавить все лоты в избранное" class="icon-star-empty"></i>
                    <?endif?>
                </th>
            </tr>
			</thead>
            <input id="sortField" name="sortField" type="text" value="<?= $sortField?>">
            <input id="sortOrder" name="sortOrder" type="text" value="<?= $sortOrder?>">
            <style>
                #sortField, #sortOrder, #page{
                    display: none;
                }
                .js_href{
                    cursor: pointer;
                    display: block;
                }
            </style>
            <script>
                function setSortValues(field, order) {
                    document.getElementById('sortField').value = field;
                    document.getElementById('sortOrder').value = order;
                    var form = document.getElementById("search_form");
                    form.submit();
                }
            </script>
            <? foreach($data as $item): ?>
            <tr data-lotid="<?= $item['lotid'] ?>">
                <td width="200"><a class="namelink link_<?=  $item['lotid'] ?>" href="/confiscated/<?=  $item['lotid'] ?>" target="_blank"><?= $item['name'] ?></a><? if($more==0){ ?><span class='show_or_hide show_or_hide_<?=$item["lotid"]; ?>'onclick="toggleShow(<?=$item['lotid']; ?>)">Показать</span><? } ?></td>
                <td align="center">
                    <?= $item['tradetip'] ?>
                </td>
                <td align="center">
                    <?= $item['region'] ?>
                </td>
                <td align="center">
                    <?= $item['start_time'] ?>
                </td>
                <td align="center">
                    <?= $item['end_time'] ?>
                </td>
                <td align="center">
                    <?= $item['status'] ?>
                </td>
                <td align="center">
                    <?= $item['price'] ?>
                </td>
                <td align="center">
                    <?= $item['market_price'] ?>
                </td>
                <td align="center">
                    <?= $item['profit_rub'] ?>
                </td>
                <td align="center">
                    <?= $item['profit_proc'] ?>
                </td>
                <td align="center">
                    <?= $item['zadatok'] ?>
                </td>
                <td align="center">
                    <?= $item['obremenenie'] ?>
                </td>
                <td align="center">
                    <?if($item['link']):?>
                        <a target="_blank" href="<?= $item['link'] ?>" onmouseover="toolTip('Если ссылка ведет на страницу с ошибкой, значит торги завершены досрочно. Сделайте поиск на площадке по коду торгов.')" onmouseout="toolTip()">
                            <i class="icon-globe-table"></i>torgi.gov.ru
                        </a>
                    <?else:?>
                        <?=$noacc?>
                    <?endif?>
                </td>
                <td align="center"><?= $item['fh'] ?></td>
            </tr>
            <? endforeach; ?>
        </table>
<script>
	$("#confiscated_tb").stickyTableHeaders();
</script>		
        <style>
            .alt_lot_table td, .alt_lot_table tr th{
                font-weight: normal;
                font-size: 13px; 
                padding: 4px 10px 4px 10px;
            }
        </style>
    </div>
</div>
<?if($navigation):?>
<div class="navig" style="margin-right:0px">
<?=$navigation?>

    <span style="margin-left:100px;">
    <select name="nav_select" onchange="setNavAmount()" id="nav_select" style="width:70px;">
        <option value="20" <?if($nav_select == 20):?>selected<?endif?>>20</option>
        <option value="50" <?if($nav_select == 50):?>selected<?endif?>>50</option>
        <option value="100" <?if($nav_select == 100):?>selected<?endif?>>100</option>
        <option value="200" <?if($nav_select == 200):?>selected<?endif?>>200</option>
    </select>

    <input id="page" name="page" type="text" value="<?= $page?>">

    </span>
    <script>
        function setNavAmount(){
            var form = document.getElementById("search_form");
            form.submit();
        }
    </script>
    <script>
        $('select.navselect').change(function(e){
            e.preventDefault();
            document.getElementById('page').value = $(this).val();
            var form = document.getElementById("search_form");
            form.submit();
        });
        function setPage(link){
          link.PreventDefault;
          var full = link.href;
          var page = full.split('?page=')[1];
          if (!page) {
            page = 1;
          }
          document.getElementById('page').value = page;
          var form = document.getElementById("search_form");
          form.submit();
        }


        var links = document.getElementsByClassName("navpg");
        for (var i = 0; i < links.length; i++) {
          links[i].setAttribute("onclick", "setPage(this)");
          links[i].addEventListener("click", function(e){e.preventDefault()}, false);
        }
    </script>
</div>
<?endif?>
</form>


<div style="text-align: center; cursor:pointer;">
</div>

<div class="content" style="margin:8px 0 8px 16px;">
    <div class="conthead" style="border-bottom: 1px dotted #E4E4E4;background:#fff; padding: 10px 17px;">
        <table>
            <tr>
                <td>
                    <b><?=$title?></b>
                </td>
                <td class="timetd">
                   
                </td>
            </tr>
        </table>
    </div>
    <div class="contbodytext"><div class="image_resizer"><?=$textData?></div></div>
    <div class="contfintext" style="padding:15px; text-decoration:underline;">
        
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('[name="begin_set_date"]').jdPicker();
        $('[name="end_set_date"]').jdPicker();
    });
</script>
<style>
.fa-list-alt {
    color:#838488;
}

.table th {
    background-color: #ebebeb;
    border: 1px solid #d1d1d1;
    color: #676767;
    font-size: 14px;
    font-weight: normal;
    padding: 4px 16px 4px 3px;
}

.table td{
    border:1px solid #e1e1e1;
    padding:5px 10px;
}
.table tr:hover {
    background:#f8f8f8;
}

.text_all{
    display: none;
}
</style>

<!-- футер -->
            </td>
        </tr>
    </table>
</div>


            <div class="mainundall"></div>
<div class="prebottom">
    <table>
        <tr>
            <td style="width: 100%;padding-top: 10px;padding-left: 13px;">
                <?if($production):?>
                <script async type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                <div style="display: inline-block; margin-right: 5px;" class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,gplus" data-yashareTheme="counter" data-yashareImage="<?=$themepath?>/images/apple-touch-icon.png"></div>
                <?endif?>
                <? temp::include('block.bottom.links.tpl') ?>
            </td>

            <td style="min-width: 210px;padding-top: 10px;text-align: right;padding-right: 17px;">
                <!--<span style="color: #888888"><?=round((microtime(true) - rem::get('microtime'))*1000, 0)?>ms, sql:<?=$sql_count?></span>-->
                <a class="user_onl_bottom" href="<?=$home?>/user/online"><i class="icon-user-male-bottom"></i> <?=$onl_all?></a>
            </td>
        </tr>
    </table>
</div>
<div class="bottom">
    <table>
        <tr>
            <td> Информация на данном сайте предоставляется "КАК ЕСТЬ" и предназначена только для ознакомительных целей без каких-либо гарантий и передачи прав. Любое использование информации в иных целях запрещено и производится на ваш страх и риск. Все права защищены.
            </td>
            <td>

            </td>
        </tr>
    </table>
</div>


<script type="text/javascript">

    var engine_formid = <?=core::$formid?>;
    //Эти 3 строки ничего страшного не делают
    //Нужно просто для системы защиты
    $(document).ready(function() {
        setInterval('connection_keeper()',30000);
    });

    function set_normal_height()
    {
        if(device.mobile() == false)
        {
          var height = $(window).height();
          var prebottom_h = $('div.prebottom').height();
          var bottom_h = $('div.bottom').height();
          var allhead_h = $('table.allhead').height();
          height = height - bottom_h - prebottom_h - allhead_h - 28;
          $('td.main').height(height);
        }
    }

    set_normal_height();

    $(window).resize(function(event) {
      set_normal_height();
      correct_images();
    });

    //учитываем скроллбар
    if(!get_scroll('Height') && ($('body').width() - $('.all_content').width() > 20)) {
        $('body').css({'margin-right' : scrollWidth()});
    }
</script>

<script>

    function toggleShow(a) {
        $('a.namelink.link_'+a).css({opacity:'0'});
        $('.allattr_'+a).slideToggle(function() {
            $('.show_or_hide_'+a).html($(this).is(':visible') ? 'Скрыть' : 'Показать');
            $('.moreattr_'+a).css({display:$(this).is(':visible') ? 'none' : 'inline'});
        });
        setTimeout(function() {$('a.namelink.link_' + a).css({'opacity':'1'}) }, 500);
    }

</script>

<!-- Google Code for YOUTUBE_BS #1 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 993818846;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "i2zXCOWe_GMQ3vHx2QM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/993818846/?label=i2zXCOWe_GMQ3vHx2QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script type="text/javascript">
    var extend_fields = $('#extend-fields');
    var news_body = $('#bs_news_container .panel-box .panel-box-body');
    var simpleToggle = $('.select-form-type #set-simple');
    var extendToggle = $('.select-form-type #set-extend');
    var search_form_extend_input = $('#search_form_extend');
    var extendToggleHelp = $('#search-form-body .help');
    var allKeywordsCheckbox = $('#search-form-body .all-keywords-checkbox');

    var toggleLink_start = $('#search-form-body .date_toggle_link.by_start');
    var toggleLink_end = $('#search-form-body .date_toggle_link.by_end');
    var dateFilterToggle = $('input[name="data_filter_type"]');
    var dateFilterToggleText = $('.data_filter_label_text');

    function new_block_resize()
    {
        var search_form_body_height = $('#search-form-wrapper .panel-box-body').height();
        news_body.css({height: search_form_body_height});
    }

    function simple(){
        search_form_extend_input.val(0);
        extendToggleHelp.show('fast');
        allKeywordsCheckbox.hide('fast');
        extend_fields.hide("fast", function() {
            extendToggle.removeClass('active');
            simpleToggle.addClass('active');
            new_block_resize()
        });

    }

    function extend(){
        search_form_extend_input.val(1);
        extendToggleHelp.hide('fast');
        allKeywordsCheckbox.show('fast');
        extend_fields.show("fast", function() {
            simpleToggle.removeClass('active');
            extendToggle.addClass('active');
            extend_fields.css('display', 'flex');
            new_block_resize()
        });
    }

    //Переключение фильтра по датам
    function setDateFilerByStart() {
        toggleLink_start.addClass('active');
        toggleLink_end.removeClass('active');
        dateFilterToggle.val('by_start');
        dateFilterToggleText.html('начала');
    }

    function setDateFilerByEnd() {
        toggleLink_start.removeClass('active');
        toggleLink_end.addClass('active');
        dateFilterToggle.val('by_end');
        dateFilterToggleText.html('окончания');
    }


    $(document).ready(function(){
        new_block_resize();

        //переключение режима поиска по дате при загрузке страницы
        if (dateFilterToggle.val() == 'by_end') {
            setDateFilerByEnd();
        } else {
            setDateFilerByStart();
        }
    });

</script>
</div>
</div>
</body>
</html>