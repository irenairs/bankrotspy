<body style="background: #fff;">
<header class="wrapper-center">
    <a href="<?=core::$home?>"><img src="/themes/web/default/images/logo.png" alt="" class="logo" style="padding: 0"></a>
    <a href="<?=core::$home?>/login" class="link-tariff">Войти</a>
    <a href="#tariff" class="link-tariff">Перейти к тарифам</a>
</header>

<section class="b-top-screen">
    <div class="wrapper-center">
        <h1>Банкротный шпион</h1>
        <h3>Банкротный шпион – сервис, помогающий искать имущество<br> банкротов и конфискованное имущество, а также объекты<br> приватизации на электронных торговых площадках – ЭТП.</h3>
        <div class="center">
            <a href="<?=core::$home?>/user/register" class="g-blue-button">Бесплатный доступ</a>
        </div>
    </div>
</section>

<section class="b-capabilities">
    <div class="wrapper-center center">
        <div class="title">Возможности сервиса</div>
        <div class="subtitle">Cервис помогающий искать имущество банкротов, конфискат на электронных торговых площадках</div>

        <div class="item-list">
            <div class="item">
                <a href="<?=core::$home?>/torgi"><img src="/themes/web/default/images/cap8.jpg" alt=""></a>
                <div class="text">
                    Большая база имущества:<br>
                    банкротство, конфискат,<br>
                    приватизация
                </div>
            </div>
            <div class="item">
                <a href="<?=core::$home?>/news/view?id=48"><img src="/themes/web/default/images/cap1.jpg" alt=""></a>
                <div class="text">
                    Удобный и расширенный <br>
                    поиск, с возможностью <br>
                    сохранять свои настройки
                </div>
            </div>
            <div class="item">
                <a href="<?=core::$home?>/news/view?id=37"><img src="/themes/web/default/images/cap5.jpg" alt=""></a>
                <div class="text">
                    Сравнительная оценка <br> стоимости имущества на торгах <br> и на открытом рынке
                    
                </div>
            </div>
            <div class="item">
                <a href="/assistance"><img src="/themes/web/default/images/cap7.jpg" alt=""></a>
                <div class="text">
                    Помощь в торгах<br>
                    и заказ ЭЦП
                </div>
            </div>
            <div class="item">
                <a href="<?=core::$home?>/articles/post17"><img src="/themes/web/default/images/cap6.jpg" alt=""></a>
                <div class="text">
                    Оценка качества дебиторской задолженности <br> Оформление заявки на участие <br> в торгах у Организатора торгов
                    
                </div>
            </div>
            <div class="item">
                <a href="<?=core::$home?>/news/view?id=58"><img src="/themes/web/default/images/cap2.jpg" alt=""></a>
                <div class="text">
                    Звонок <br>организатору <br> торгов с сайта
                </div>
            </div>
            <div class="item">
                <a href="<?=core::$home?>/news/view?id=62"><img src="/themes/web/default/images/cap4.jpg" alt=""></a>
                <div class="text">
                    Запрос документов на имущество <br> у Организатора торгов <br> Рассылка новых лотов на Email

                </div>
            </div>

            <div class="item">
                <a href="<?=core::$home?>/news/view?id=56"><img src="/themes/web/default/images/cap3.jpg" alt=""></a>
                <div class="text">
                    Аналитика, статистика и справочная
                    информация: организаторы торгов,
                    победители торгов, должники,
                    торговые площадки, имущества
                </div>
            </div>
        </div>
        <div class="center tariff">
            <a href="#tariff" class="g-blue-button">Перейти к тарифам</a>
        </div>
    </div>
</section>
<section class="b-popular">
    <div class="wrapper-center center">
        <div class="title">Самое популярное имущество</div>

        <div class="item-list-popular">
            <div class="item">
                <img src="/themes/web/default/images/pop1.jpg" alt="">
                <div class="text">Автомобили</div>
            </div>
            <div class="item">
                <img src="/themes/web/default/images/pop2.jpg" alt="">
                <div class="text">Спецтехника</div>
            </div>
            <div class="item">
                <img src="/themes/web/default/images/pop3.jpg" alt="">
                <div class="text">Оборудование, <br>
                    инструменты, <br>
                    материалы</div>
            </div>
            <div class="item">
                <img src="/themes/web/default/images/pop4.jpg" alt="">
                <div class="text">Недвижимость
                    жилая</div>
            </div>
            <div class="item">
                <img src="/themes/web/default/images/pop5.jpg" alt="">
                <div class="text">Недвижимость
                    коммерческая</div>
            </div>
            <div class="item">
                <img src="/themes/web/default/images/pop6.jpg" alt="">
                <div class="text">Дебиторская
                    задолженность</div>
            </div>
            <div class="item">
                <img src="/themes/web/default/images/pop7.jpg" alt="">
                <div class="text">Земельные
                    участки</div>
            </div>
            <div class="item">
                <img src="/themes/web/default/images/pop8.jpg" alt="">
                <div class="text">Сельхоз
                    имущество</div>
            </div>
            <div class="item">
                <img src="/themes/web/default/images/pop9.jpg" alt="">
                <div class="text">Избранное
                    имущество</div>
            </div>
        </div>

        <div class="desc center">50% имущества можно купить за 50% рыночной стоимости</div>
    </div>
</section>
<section class="b-video">
    <div class="wrapper-center center">
        <div class="title">Видео презентация</div>

        <iframe width="1000" height="560" src="https://www.youtube.com/embed/3fOllsKl-x0" frameborder="0" allowfullscreen></iframe>

        <a href="/pages/manual-bankrot-spy" class="g-blue-button">Смотреть руководство пользователя</a>
    </div>
</section>



<section class="our-tariffs" id="tariff">
    <div class="wrapper-center">
        <div class="title center">Наши тарифы</div>
        <div class="t-tariffs second-table">
            <div class="head">Простой доступ к базе торгов</div>
            <div class="table-price">
                <div class="item">
                    <div class="price"><span><?=$rmenu[1]['price_source']?></span> руб</div>
                    <div class="time">1 месяц</div>
                    <div class="sum">(<?=$rmenu[1]['price_source']?> в месяц)</div>
                    <a href="/tariffs#tariff_<?=$rmenu[1]['id']?>" class="g-blue-button">Выбрать тариф</a>
                </div>
                <div class="item">
                    <div class="price"><span><?=$rmenu[2]['price_source']?></span> руб</div>
                    <div class="time">3 месяца</div>
                    <div class="sum">(<?=round($rmenu[2]['price_source']/3)?> в месяц)</div>
                    <a href="/tariffs#tariff_<?=$rmenu[2]['id']?>" class="g-blue-button">Выбрать тариф</a>
                </div>
                <div class="item">
                    <div class="price"><span><?=$rmenu[16]['price_source']?></span> руб</div>
                    <div class="time">6 месяцев</div>
                    <div class="sum">(<?=round($rmenu[16]['price_source']/6)?> в месяц)</div>
                    <a href="/tariffs#tariff_<?=$rmenu[16]['id']?>" class="g-blue-button">Выбрать тариф</a>
                </div>
                <div class="item">
                    <div class="price"><span><?=$rmenu[3]['price_source']?></span> руб</div>
                    <div class="time">12 месяцев</div>
                    <div class="sum">(<?=round($rmenu[3]['price_source']/12)?> в месяц)</div>
                    <a href="/tariffs#tariff_<?=$rmenu[3]['id']?>" class="g-blue-button">Выбрать тариф</a>
                </div>
                <div class="item">
                    <div class="price"><span><?=$rmenu[6]['price_source']?></span> руб</div>
                    <div class="time">1 день</div>
                    <a href="/tariffs#tariff_<?=$rmenu[6]['id']?>" class="g-blue-button">Выбрать тариф</a>
                </div>
            </div>
            <div class="head">Возможности простого и ВИП-доступа к базе торгов</div>
			<div class="column-text">
                <div class="col">
                    <p><i></i>Расширенный поиск лота</p>
                    <p><i></i>Рыночная цена лота, руб</p>
                    <p><i></i>Доход, руб - разница между ценой на рынке и на торгах</p>
                    <p><i></i>Доходность % - вероятная доходность по приобретению лота на торгах и продаже его на рынке</p>
                    <p><i></i>Рассылка новых лотов на Email</p>
					<p><i></i>Добавление лота в избранное</p>
                    <p><i></i>Ссылка на площадку и федресурс</p>
                    <p><i></i>Поиск рыночной цены лота</p>
                </div>
                <div class="col">
                    <p><i></i>Комментарий к избранным лотам</p>
                    <p><i></i>Экспорт выбранных лотов в Excel</p>
                    <p><i></i>Рейтинг управляющих(АУ) или организаторов торгов: жалобы и претензии к АУ по документам судов и ФАС (10-бальная система), а так же их контакты данные</p>
					<p><i></i>Оценка качества и ликвидности дебиторской задолженности (3-х бальная система)</p>
                </div>
                <div class="col">
                    <p><i></i>Гистограммы стоимости 1кв. метра и средняя стоимость аналогичных предложений на открытом рынке по выборке: автомобили и спецтехника, коммерческая и жилая недвижимость</p>
                    <p><i></i>Запрос документов на имущество у Организатора торгов (ОТ) прямо с сайта</p>
					<p><i></i>Оформление заявки для организатора торгов (ОТ) для участия в торгах</p>
                </div>
            </div>
        </div>

           
        <div class="t-tariffs third-table" id="debt">
            <div class="head">Помощь в торгах, анализ, поиск и выкуп дебиторки</div>
                <div class="table-price">
                    <div class="item">
                                <div style="font-size: 21px;" class="title"><?=$rmenu[19]['name']?></div>
                                <div class="price"><span><?=$rmenu[19]['price_source']?></span> руб</div>
                                <a href="/tariffs#tariff_<?=$rmenu[19]['id']?>" class="g-blue-button">Выбрать тариф</a>
                            </div>
                            <div class="item">
                                <div style="font-size: 21px;" class="title"><?=$rmenu[20]['name']?></div>
                                <div class="price"><span><?=$rmenu[20]['price_source']?></span> руб</div>
                                <a href="/tariffs#tariff_<?=$rmenu[20]['id']?>" class="g-blue-button">Выбрать тариф</a>
                            </div>
                            <div class="item">
                                <div style="font-size: 21px;" class="title"><?=$rmenu[23]['name']?></div>
                                <div class="price"><span><?=$rmenu[23]['price_source']?></span> руб</div>
                                <a href="/tariffs#tariff_<?=$rmenu[23]['id']?>" class="g-blue-button">Выбрать тариф</a>
                            </div>
                            <div class="item">
                                <div style="font-size: 21px;" class="title"><?=$rmenu[24]['name']?></div>
                                <div class="price"><span><?=$rmenu[24]['price_source']?></span> руб</div>
                                <a href="/tariffs#tariff_<?=$rmenu[24]['id']?>" class="g-blue-button">Выбрать тариф</a>
                            </div>
							<div class="item">
                                <div style="font-size: 21px;" class="title"><?=$rmenu[11]['name']?></div>
                                <div class="price"><span><?=$rmenu[11]['price_source']?></span> руб</div>
                                <a href="/tariffs#tariff_<?=$rmenu[11]['id']?>" class="g-blue-button">Выбрать тариф</a>
                            </div>
						</div>
                        <div class="column-text">
                            <div class="col">
                                <p><i></i>Одна консультация на темы: банкротство, эл. торги, дебиторская задолженность и пр.</p>
                            </div>
                            <div class="col">
                                <p><i></i>Анализ одного юридического лица</p>
                            </div>
                            <div class="col">
                                <p><i></i>Подборка непросуженной качественной дебиторки для инвестирования</p><br>
                            </div>
                            <div class="col">
                                <p><i></i>Подбор качественной дебиторской задолженности и её приобретение на торгах для инвестирования и/или гашения задолженности по кредитам</p>
                            </div>
							<div class="col">
                                <p><i></i>Размещение ракламы и рекламного баннера на сайте Банкротный шпион</p>
                            </div>
                        </div>
                       
                    </div>
    </div>
</section>
<section class="big-text wrapper-center center">
    <div>
        <a href="<?=core::$home?>/user/register">Зарегистрируйтесь и получите <br> бесплатный доступ к материалам сайта!</a>
    </div>
</section>

<section class="our-tariffs">
    <div class="wrapper-center">
        <div class="t-tariffs">
            <div class="head"><?=$article['name']?></div>
            <div class="spec-text column-text" style="padding: 30px">
                <?=$article['text']?>
            </div>
        </div>
        
    </div>
</section>






<footer>
    <style>
        #buttonGoTop 
        {
            right: 35px !important;
        }
    </style>
    <script type = "text/javascript" src="<?=$themepath?>/js/jquery.min.js"></script>
    <div class="wrapper-center">
        <div style="float: left;"><img src="/themes/web/default/images/logo.png" alt="" class="logo"></div>
        <div style="margin: 10px; overflow: auto; float: left;"><?=core::$set['counters']?></div>
        <a href="#tariff" class="link-tariff">Перейти к тарифам</a>
        <a href="<?=core::$home?>/user/register" class="link-tariff">Получить бесплатный доступ</a>
    </div>
<? temp::include('button_go_top.tpl') ?>  
</footer>

</body>
</html>