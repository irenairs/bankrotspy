    let Dict = {
        actionNote: function(type, id, text, pop){
            let data = {
                type: type,
                id: id,
                text: text,
                formid: engine_formid
            }, src;
            $.post('/cmd/note', data, function(data) {
                if(pop){
                    noteW.style.display = 'none';
                    src = document.querySelector('[cmd="' + ['note_pop', type, id].join('|') + '"]');
                    src.className = 'fa fa-sticky-note' + (text.length ? '' : '-o');
                    src.setAttribute('data-note', text);
                }
                create_notify( data.message );
            });
        },
        actionFH: function(cmd, type, action, items){
            let data = {
                type: type,
                items: items,
                formid: engine_formid
            }, el, prm, sel, hide, tab, fl;
            action && (data.action = action);
            $.post('/cmd/' + cmd, data, function(data) {
                if(!data.error){
                    hide = (tab = document.querySelector('.active_tab'))
                        && ((cmd === 'fav') ? (action && (tab.checked || (tab.getAttribute('attr') === '-1'))) : !action);
                    prm = Dict[cmd][action ? 'off' : 'on'];
                    for(let i = 0; i < items.length; i++){
                        if(hide){
                            sel = '[data-lotid="' + items[i] + '"]';
                            el = document.querySelector(sel);
                            el.parentNode.removeChild(el);
                        } else {
                            sel = '[cmd="' + [cmd, type, action, items[i]].join('|') + '"]';
                            el = document.querySelector(sel);
                            el.setAttribute('cmd', [cmd, type, prm.action, items[i]].join('|'));
                            el.setAttribute('class', prm.class);
                            if(el.tagName.toUpperCase() === 'I'){
                                el.setAttribute('title', prm.title);
                            } else {
                                el.innerText = prm.title;
                            }
                        }
                        fl = document.querySelector('[in-' + cmd + ']');
                        fl && fl.setAttribute('in-' + cmd, action ? 'off' : 'on');
                    }
                    if(items.length > 1){
                        el = document.querySelector('[cmd="' + [cmd, type, action].join('|') + '"]');
                        el.setAttribute('cmd', [cmd, type, prm.action].join('|'));
                        el.setAttribute('class', prm.class);
                        el.setAttribute('title', prm.titleAll);
                    }
                }
                create_notify( data.message );
            });
        },
        fav: {
            on: {
                action: 'del',
                class: 'icon-star-clicked',
                title: 'Удалить лот из избранного',
                titleAll: 'Удалить все лоты из избранного'
            },
            off: {
                action: '',
                class: 'icon-star-empty',
                title: 'Добавить лот в избранное',
                titleAll: 'Добавить все лоты в избранное'
            }
        },
        hide: {
            on: {
                action: 'del',
                class: 'icon-forward',
                title: 'Достать лот из мусора',
                titleAll: 'Достать все лоты из мусора'
            },
            off: {
                action: '',
                class: 'icon-delete',
                title: 'Отправить лот в мусор',
                titleAll: 'Отправить все лоты в мусор'
            }
        }
    },
    noteW, noteW_text, noteW_btnSave, noteW_btnDel, noteW_btnClose;

    if (document.readyState === "complete" || document.readyState === "loaded" || document.readyState === "interactive") {
        onDOMContentLoaded();
    } else {
        document.addEventListener("DOMContentLoaded", onDOMContentLoaded);
    }

    function onDOMContentLoaded(){
        noteW = document.getElementById('note_window');
        noteW_text = noteW.querySelector('textarea');
        noteW_btnSave = noteW.querySelector('.fa-floppy-o');
        noteW_btnDel = noteW.querySelector('.fa-trash');
        noteW_btnClose = noteW.querySelector('.fa-times');
        noteW_btnDel.addEventListener('click', function () {
            noteW_text.value = '';
            noteW_btnSave.click();
        });
        noteW_btnClose.addEventListener('click', function () {
            noteW.style.display = 'none';
        });
    }

    document.addEventListener('click', function (e) {
        let el = e.target, cmd = el.getAttribute('cmd');
        if(!cmd) return;
        cmd = cmd.split('|');
        let items = [], type = cmd[1], act = cmd[2] || '', targets, i;
        switch (cmd[0]) {
            case 'fav':
            case 'hide':
                cmd[3] && items.push(cmd[3]);
                if(!items.length){
                    targets = document.querySelectorAll('[cmd^="' + [cmd[0], type, act, ''].join('|') + '"]');
                    for(i = 0; i < targets.length; i++){
                        items.push(targets[i].getAttribute('cmd').split('|')[3]);
                    }
                }
                Dict.actionFH(cmd[0], type, act, items);
                break;
            case 'note':
                let cl, input;
                if(type === 'show'){
                    cl = 'note-save-active';
                } else {
                    if((input = document.querySelector('[cmd="note|show"]'))){
                        cl = 'note-save-hidden';
                        Dict.actionNote(type, act, input.value);
                    } else {
                        Dict.actionNote(type, act, noteW_text.value, true);
                    }
                }
                cl && (e.target.parentNode.parentNode.className = cl);
                break;
            case 'close':
                e.target.parentNode.parentNode.className = 'note-save-hidden';
                break;
            case 'note_pop':
                let style = $(el).offset();
                noteW_text.innerHTML = el.getAttribute('data-note');
                noteW_btnSave.setAttribute('cmd', ['note', type, act, cmd[3]].join('|'));
                noteW.style.top = (style.top - 90) + 'px';
                noteW.style.left = style.left + 'px';
                noteW.style.display = 'block';
                break;
        }
    });
