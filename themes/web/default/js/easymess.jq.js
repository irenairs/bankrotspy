var EasyMessage = {
			cond:0,
			show : function (type_parent_info,parentinfo,text)
			{ this.cond = 1;
				switch(type_parent_info)
				{
				case "id": var docelement = $("#"+parentinfo); break;
				case "class": var docelement = $("."+parentinfo); break;
				case "name": var docelement =  $("[name = "+parentinfo+"]"); break;
				case "value": var docelement =  $("[value = "+parentinfo+"]"); break;
				}
				
				var messbox = document.createElement("div");
				messbox.className = "message";
				messbox.id = "easymess";
				messbox.width = '200';	
				messbox.style.left = docelement.offset().left+docelement.width()+"px";
				messbox.style.top = docelement.offset().top+"px";				
				var Text = document.createTextNode(text);
				messbox.innerHTML = "<div class = 'messtext'>"+text+"</div>";
				var body = document.getElementsByTagName("body")[0];
				body.appendChild(messbox);
				$('#easymess').animate({opacity:'1',maxHeight:'150px'},600);
			},
			hide : function (){if(this.cond==1){var elem = document.getElementById("easymess"); document.body.removeChild(elem);this.cond=0;}}
		}