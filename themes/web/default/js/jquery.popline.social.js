/*
  jquery.popline.social.js 1.0.0

  Version: 1.0.0
  Updated: Sep 10th, 2014

  (c) 2014 by kenshin54
*/
;(function($) {

  var openWindow = function(url, openWithWindow){
    if (openWithWindow) {
      var width = 600, height = 480;
      var left = (screen.width - width)  / 2;
      var top = (screen.height - height) / 2;
      window.open(url, null, "width=" + width + ", height=" + height + ", top=" + top + ", left=" + left);
    }else {
      window.open(url);
    }
  }

  $.popline.addButton({

    yandex: {
      iconClass: "pop pop_yadnex",
      title: "Поиск Яндекс",
      mode: "view",
      action: function(event) {
        var url = "http://yandex.ru/yandsearch?text=" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },
    googleimages: {
      iconClass: "pop pop_google_images",
      title: "Картинки google",
      mode: "view",
      action: function(event) {
        var url = "https://www.google.com/search?hl=ru&tbm=isch&q=" + encodeURIComponent($.popline.utils.selection().text());
        openWindow(url, false);
      }
    },
    yadneximages: {
      iconClass: "pop pop_yandex_images",
      title: "Яндекс картинки",
      mode: "view",
      action: function(event) {
        var url = "https://yandex.ru/images/search?text=" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },
    //google: {
    //  iconClass: "pop pop_google",
    //  mode: "view",
    //  action: function(event) {
    //    var url = "https://www.google.com/search?q=" + encodeURIComponent($.popline.utils.selection().text());
    //    openWindow(url, false);
    //  }
    //},
    //mail: {
    //  iconClass: "pop pop_mail",
    //  mode: "view",
    //  action: function(event) {
    //    var url = "http://go.mail.ru/search?q=" + encodeURIComponent($.popline.utils.selection().text());
    //    openWindow(url, false);
    //  }
    //},
    yandexmaps: {
      iconClass: "pop pop_yandex_maps",
      title: "Яндекс карты",
      mode: "view",
      action: function(event) {
        var url = "https://maps.yandex.ru/?text=" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },
    googlemaps: {
      iconClass: "pop pop_google_maps",
      title: "Google карты",
      mode: "view",
      action: function(event) {
        var url = "https://www.google.ru/maps/place/" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },
    wikimapia: {
      iconClass: "pop pop_wikimapia",
      title: "Wikimapia",
      mode: "view",
      action: function(event) {
        var url = "http://www.wikimapia.org/#lang=ru&search=" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },
    gis: {
      iconClass: "pop pop_gis",
      title: "2ГИС",
      mode: "view",
      action: function(event) {
        var url = "http://2gis.ru/search/" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },

    avito: {
      iconClass: "pop pop_avito",
      title: "Поиск на Avito",
      mode: "view",
      action: function(event) {
        var url = "http://www.avito.ru/rossiya?q=" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },
    drom: {
      iconClass: "pop pop_drom",
      title: "Поиск на drom.ru",
      mode: "view",
      action: function(event) {
        var url = "http://www.drom.ru/search/?q=" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },
    //googlemaps: {
    //  iconClass: "pop pop_google_maps",
    //  mode: "view",
    //  action: function(event) {
    //    var url = "https://www.google.ru/maps/place/" + encodeURIComponent($.popline.utils.selection().text());
    //    openWindow(url, false);
    //  }
    //},
    //wikimapia: {
    //  iconClass: "pop pop_wikimapia",
    //  mode: "view",
    //  action: function(event) {
    //    var url = "http://www.wikimapia.org/#lang=ru&search=" + encodeURIComponent($.popline.utils.selection().text());
    //    openWindow(url, false);
    //  }
    //},
    //gis: {
    //  iconClass: "pop pop_gis",
    //  mode: "view",
    //  action: function(event) {
    //    var url = "http://2gis.ru/search/" + encodeURIComponent($.popline.utils.selection().text().trim());
    //    openWindow(url, false);
    //  }
    //},
    vinauto: {
      iconClass: "pop pop_vin",
      title: "Проверка VIN-кода",
      mode: "view",
      action: function(event) {
        var url = "http://vin.auto.ru/check.html?a9e8cdd2b6ae849b2768cd174bdf2bf0=1&bank=1&decode=1&vin=" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },
    gibdd: {
      iconClass: "pop pop_gibdd",
      title: "Проверка VIN-кода ГИБДД",
      mode: "view",
      action: function(event) {
        var url = "https://xn--90adear.xn--p1ai/check/auto#" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },
    rosreestr: {
      iconClass: "pop pop_rosreestr",
      title: "Росреестр",
      mode: "view",
      action: function(event) {
        var url = "http://pkk5.rosreestr.ru/#text="+$.popline.utils.selection().text().trim()+"&type=1&app=search&opened=1";
        openWindow(url, false);
      }
    },
    kadaster: {
        iconClass: "pop pop_kadaster",
        title: "Публичная кадастровая карта",
        mode: "view",
        action: function(event) {
          var url = "http://xn-----6kcbaababou8b2age7axh3agnwid7h4jla.xn--p1ai";
          openWindow(url, false);
        }
    },
    egrp365: {
        iconClass: "pop pop_egrp365",
        title: "ЕГРП 365",
        mode: "view",
        action: function(event) {
          var url = "https://egrp365.ru/map/?kadnum="+$.popline.utils.selection().text().trim();
          openWindow(url, false);
        }
    },
    kadastrmap: {
        iconClass: "pop pop_kadastrmap",
        title: "Публичная кадастровая карта РФ",
        mode: "view",
        action: function(event) {
          var url = "https://kadastrmap.ru/?kad_no="+$.popline.utils.selection().text().trim();
          openWindow(url, false);
        }
    },
    //yadneximages: {
    //  iconClass: "pop pop_yandex_images",
    //  mode: "view",
    //  action: function(event) {
    //    var url = "https://yandex.ru/images/search?text=" + encodeURIComponent($.popline.utils.selection().text().trim());
    //    openWindow(url, false);
    //  }
    //},
    yandexmarket: {
      iconClass: "pop pop_yandex_market",
      title: "Яндекс Маркет",
      mode: "view",
      action: function(event) {
        var url = "https://market.yandex.ru/search.xml?text=" + encodeURIComponent($.popline.utils.selection().text().trim());
        openWindow(url, false);
      }
    },
    //mailtovary: {
    //  iconClass: "pop pop_mail_tovary",
    //  mode: "view",
    //  action: function(event) {
    //    var url = "http://torg.mail.ru/search/?q=" + encodeURIComponent($.popline.utils.selection().text());
    //    openWindow(url, false);
    //  }
    //},


  });
})(jQuery);
