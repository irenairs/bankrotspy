    function create_notify(mess) {
        if(!mess) return;
        var el = $("#alltopmess");

        $(el).clearQueue();
        $(el).stop();
        $(el).html(mess);

        var window_height = $(window).height();
        var window_width = $(window).width();
        var el_width = $(el).width();
        var el_height = $(el).height();

        var left = (window_width / 2) - (el_width / 2);
        var top = (window_height / 2) - (el_height / 2);

        $(el).css('left', left);
        $(el).css('bottom', top);

        $(el).fadeIn(100);
        $(el).delay(Math.max(4000, mess.length * 30)).fadeOut(700);

    }

    function connection_keeper() {
        var data = {
            formid: engine_formid
        }
        $.post("/connectionkeeper", data, function(data){//ничего не делаем, только сохраняем подключение
        });
    }

    function listen_namepop(item){
       let lotid = $(item).attr('attr'),
           text_cont = $(item).text();
       if(text_cont === 'Показать'){
         $(item).text('Скрыть');
         $('#min_name_' + lotid).fadeOut(0);
         $('#max_name_' + lotid).fadeIn(700);
       } else {
         $(item).text('Показать');
         $('#max_name_' + lotid).fadeOut(0);
         $('#min_name_' + lotid).fadeIn(700);
       }
    }

    $(document).ready(function(){
        $('.bs_index_table').on('click', '.get_lot_price', function(e){
            e.preventDefault();
            var _this = $(this);
            var param_id = $(this).parent().parent().attr('data-lotid');
            $.ajax({
                url: '/cards/getprice',
                method: 'POST',
                dataType: 'json',
                data: {
                    formid:engine_formid,
                    id: param_id
                },
                success: function(response){
                    if(response.access == "1"){
                       $(_this).find('span').removeClass('loading').html(response.price);
                    }
                },
                beforeSend: function( xhr ) {
                    $(_this).find('span').html('').addClass('loading');
                }
            });
        });

        $(document).on('click', '.show_span', function(){
            listen_namepop(this);
        });

        $(document).on('click', '#icon_close_butt', function(){
            $('.popup_overlay').fadeOut(200);
            $('#popup_overlay_keyw, .popup_table_add').fadeOut(200);
            $('#popup_overlay_keyw, .popup_table_del').fadeOut(200);
            $('#popup_overlay_keyw, .popup_table_change').fadeOut(200);
        });

        $('.popup_overlay').click(function(event) {
            if ((event || window.event).target == this) {
                $('.popup_overlay').fadeOut(200);
                $('#popup_overlay_keyw, .popup_table_add').fadeOut(200);
                $('#popup_overlay_keyw, .popup_table_del').fadeOut(200);
                $('#popup_overlay_keyw, .popup_table_change').fadeOut(200);
            }
        });

    });