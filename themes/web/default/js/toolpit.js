
			function tdMouseMove(text,obj)
			{
				if(text!=undefined&&obj!=undefined)
				{
					$(obj).mousemove(function(event)
					{						
						var X = event.pageX;
						var Y = event.pageY;
						$("#floatTip").html(text);
						var Left = X-$("#floatTip").width()/2; 
						var Top = Y-$("#floatTip").height()-50;
						$("#floatTip").offset({left:Left,top:Top});
						$("#floatTip").show();
						
					});
				}
			}
			
			function tdMouseOver(text,obj)
			{	
				var x0 = $(obj).offset().left;
				var y0 = $(obj).offset().top;
				var kh=0;
				$("#floatTip").html(text);
				if($(obj).height()>=2*$("#floatTip").height()) kh = $(obj).height()/2-$("#floatTip").height()/2;
				if($(obj).height()<=$("#floatTip").height()) kh = $(obj).height()/4-$("#floatTip").height();
				
				$("#floatTip").show();
				$("#floatTip").offset({left:x0-$("#floatTip").width()/3,
				top:y0+kh});
			}
			function tdMouseOut()
			{
				if($("#floatTip").css('display')=='block'){$("#floatTip").hide();}
			}
			
			function hintMouseOver()
			{	
				$("#floatTip").show();				
			}
			function hintMouseOut()
			{				
				$("#floatTip").hide();
			}
		