    function gSign(authResult, reg) {
        var formid = $('[name=formid]').attr('value');
        if (authResult['status'] && authResult['status']['signed_in']) {
            gapi.client
                .load('plus', 'v1')
                .then(function() {
                    gapi.client.plus.people.get({
                        'userId': 'me'
                    }).then(function(resp) {
                        $.ajax({
                            url: (reg ? '/user/register' : '/login') + '?act=save',
                            data: {
                                'formid': formid,
                                'user': resp.result,
                                'type': 'google'
                            },
                            method: 'post',
                            success: function(data) {
                                if(data.url){
                                    window.location = data.url;
                                } else if(data.cmd = 'auth'){
                                    $.ajax({
                                        url: '/login?act=save',
                                        data: {
                                            'formid': formid,
                                            'user': resp.result,
                                            'type': 'google'
                                        },
                                        method: 'post',
                                        success: function(data) {
                                            if(data.url){
                                                window.location = data.url;
                                            }
                                        },
                                        error: function() {
                                            alert('Some error. Try again later.');
                                        }
                                    });
                                }
                            },
                            error: function() {
                                alert('Some error. Try again later.');
                            }
                        });
                    }, function(reason) {
                        alert('Error:' + reason.result.error.message);
                    });
                })
        } else if (authResult['error']) {

        }
    }
    function gRegCb(authResult) {
        gSign(authResult, true);
    }

    function gAuthCb(authResult) {
        gSign(authResult)
    }

    function FB_login(reg){
        var formid = $('[name=formid]').attr('value');
        FB.login(function () {
            FB.getLoginStatus(function(resp) {
                if (resp.status === 'connected') {
                    resp.formid = formid;
                    resp.type = 'facebook';
                    $.ajax({
                        url: (reg ? '/user/register' : '/login') + '?act=save',
                        data: resp,
                        method: 'post',
                        success: function(data) {
                            if(data.url){
                                window.location = data.url;
                            } else if(data.cmd = 'auth'){
                                $.ajax({
                                    url: '/login?act=save',
                                    data: resp,
                                    method: 'post',
                                    success: function(data) {
                                        if(data.url){
                                            window.location = data.url;
                                        }
                                    },
                                    error: function() {
                                        alert('Some error. Try again later.');
                                    }
                                });
                            }
                        },
                        error: function() {
                            alert('Some error. Try again later.');
                        }
                    });
                }  else {

                }
            });
        });
    }

    function VK_login(reg) {
        var formid = $('[name=formid]').attr('value');
        VK.Auth.login(function (resp) {
            if (resp.status === 'connected') {
                resp.formid = formid;
                resp.type = 'vk';
                $.ajax({
                    url: (reg ? '/user/register' : '/login') + '?act=save',
                    data: resp,
                    method: 'post',
                    success: function(data) {
                        if(data.url){
                            window.location = data.url;
                        } else if(data.cmd = 'auth'){
                            $.ajax({
                                url: '/login?act=save',
                                data: resp,
                                method: 'post',
                                success: function(data) {
                                    if(data.url){
                                        window.location = data.url;
                                    }
                                },
                                error: function() {
                                    alert('Some error. Try again later.');
                                }
                            });
                        }
                    },
                    error: function() {
                        alert('Some error. Try again later.');
                    }
                });
            }  else {

            }
        })
    }

    $(document).ready(function(){
        // google
        var gAuth = document.getElementById('user_google_auth'),
            gReg = document.getElementById('user_google_reg');
        gAuth && gAuth.addEventListener('click', function () {
            gapi.auth.signIn({
                'cookiepolicy' : 'single_host_origin',
                'callback' : 'gAuthCb',
                'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email',
                'requestvisibleactions': 'http://schema.org/AddAction'
            });
        });
        gReg && gReg.addEventListener('click', function () {
            gapi.auth.signIn({
                'cookiepolicy' : 'single_host_origin',
                'callback' : 'gRegCb',
                'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email',
                'requestvisibleactions': 'http://schema.org/AddAction'
            });
        });
        (function() {
            var scr = document.createElement('script');
            scr.type = 'text/javascript';
            scr.src = 'https://apis.google.com/js/client:platform.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(scr, s);
        })();

        // facebook
        var $fbApp = $('meta[name=facebook-app]'),
            fbAppId = $fbApp.attr('content'),
            fbAppVer = $fbApp.attr('ver'),
            fbAuth = document.getElementById('user_facebook_auth'),
            fbReg = document.getElementById('user_facebook_reg');
        fbAuth && fbAuth.addEventListener('click', function () {
            FB_login();
        });
        fbReg && fbReg.addEventListener('click', function () {
            FB_login(true);
        });
        window.fbAsyncInit = function() {
            FB.init({
                appId: fbAppId,
                cookie: true,
                xfbml: true,
                version: fbAppVer
            });
            FB.AppEvents.logPageView();
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        // vk
        var vkAppId = $('meta[name=vk-app]').attr('content'),
            vkAuth = document.getElementById('user_vk_auth'),
            vkReg = document.getElementById('user_vk_reg');
        vkAuth && vkAuth.addEventListener('click', function () {
            VK_login();
        });
        vkReg && vkReg.addEventListener('click', function () {
            VK_login(true);
        });
        VK.init({
            apiId: vkAppId
        });
    });
