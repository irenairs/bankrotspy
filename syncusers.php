
<?php
// Скрипт синхронизации пользователей с чатом
set_time_limit(100);
defined('DS_ENGINE') or die('web_demon laughs');

new nav; //Постраничная навигация
$total = core::$db->query('SELECT COUNT(*) FROM `ds_users` WHERE `lastvisit` > "' . (time() - core::$set['onlinetime']) . '";')->count();
$res = core::$db->query('SELECT * FROM `ds_users` WHERE `lastvisit` > "' . (time() - core::$set['onlinetime']) . '" ORDER BY `id` DESC LIMIT '.nav::$start.', '.nav::$kmess.';');

$i = 0;
$total = 0;
$where = "1";
$eng_right = user::get_rights();
$arr = array();

// Выбираем пользователей по запросу
$cntQuery = 'SELECT COUNT(*) FROM `ds_users` WHERE ' . $where;
$query = 'SELECT * FROM `ds_users` WHERE ' . $where;
//var_dump($query);
$total = core::$db->query( $cntQuery )->count();
$res = core::$db->query( $query);
// Создаем экземпояр класса для работы с API
$xenAPI = new xenapi(core::$home.'/forum/api.php', core::$hash);

$error = array();

if(POST('submit')) {
    //Цикл обхода всех существующих пользователей в основной базе
    while ($data = $res->fetch_assoc()) {

        $out = array();
        $out['i'] = $i;
        $out['id'] = $data['id'];
        $out['login'] = $data['login'];
        $login = $data['login'];
        $out['password'] = $data['password'];
        $password = $data['password'];
        $out['mail'] = $data['mail'];
        $mail = $data['mail'];
        $out['rights'] = $eng_right[$data['rights']];
        $out['sex'] = $data['sex'];
        $out['online'] = user::is_online($data['lastvisit']);
        $out['avatar'] = user::get_avatar($data['id'], $data['avtime'], 1);
        $out['registered'] = date('d.m.Y', $data['time']);
        $response = $xenAPI->testUser($login, $password, $mail); //Метод для передачи пользователя через API на форум.
        $arr[] = $out;

        //var_dump($response);
        //var_dump($out);

        $i++;
    }
}

engine_head(lang('u_online'));
// Если нажата кнопка отмены
if(POST('cancel'))  {
    header("Location:../control/allusers");
}
// Если нажата кнопка выполнить
if (POST('submit')) {
    temp::assign('total_in', $total);
    //temp::HTMassign('out', $arr);
    //temp::HTMassign('navigation', nav::display($total, core::$home.'/user/syncusers?'));
} else {
// При переходе на вкладку синхронизации
    temp::assign('total_in', '0');
    //temp::HTMassign('out', $arr);
}

temp::display('user.syncusers');
engine_fin();
