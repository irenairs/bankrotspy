<?php
    require_once($root . 'system/system.class.php');
    new system();

    require_once(system::realPath('system/cache.class.php'));
    require_once(system::realPath('system/dbAdapter.class.php'));
    require_once(system::realPath('system/dbConnector.class.php'));
    require_once(system::realPath('system/dbQuery.class.php'));
    require_once(system::realPath('system/view.class.php'));
    require_once(system::realPath('system/model.class.php'));
