<?php
return [
    'url' => 'https://bankrot-spy.ru',
    'packs' => [
        [
            'name' => 'main',
            'type' => 'alone',
            'pages' => [
                [
                    'priority' => '1.0',
                    'type' => 'list',
                    'list' => [
                        '/',
                        '/torgi',
                        '/pages/manual-bankrot-spy',
                        '/news',
                        '/tariffs',
                        '/pages/services',
                        '/pages/help-trade',
                        '/amc',
                        '/articles'
                    ]
                ], [
                    'priority' => '0.8',
                    'type' => 'article'
                ], [
                    'priority' => '0.8',
                    'type' => 'news'
                ], [
                    'priority' => '0.8',
                    'type' => 'amc'
                ]
            ]
        ], [
            'name' => 'cards',
            'type' => 'list',
            'priority' => '0.8',
            'step' => 50000
        ]
    ]
];
