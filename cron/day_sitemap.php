<?php
    // запускать каждый день, время любое
    //exit();
    define('DS_ENGINE', 1);
    ini_set('display_errors', 1);
    date_default_timezone_set('UTC');
    mb_internal_encoding('UTF-8');

    $root = implode('/', array_slice(explode('/', str_replace('\\', '/', dirname(__FILE__))), 0, -1)) . '/';
    /** @noinspection PhpIncludeInspection */
    require_once($root . 'config/lite.php');

    $sm_config = require_once(system::realPath('config/sitemap.php'));
    $tab = "\t";
    if($F = fopen(system::realPath('sitemap.xml'), 'w')){
        fwrite($F, '<?xml version="1.0" encoding="UTF-8"?>'. PHP_EOL);
        fwrite($F, '<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'. PHP_EOL);
        $list = [];
        $url = $sm_config['url'];
        $tm = date('c', time());
        foreach ($sm_config['packs'] as $pack){
            if($pack['type'] == 'alone'){
                $fn = 'sitemap_' . $pack['name'] . '.xml';
                if($f = fopen(system::realPath($fn), 'w')){
                    $list[] = $fn;
                    fwrite($f, '<?xml version="1.0" encoding="UTF-8"?>'. PHP_EOL);
                    fwrite($f, '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'. PHP_EOL);
                    foreach ($pack['pages'] as $block){
                        switch ($block['type']){
                            case 'list':
                                foreach ($block['list'] as $item){
                                    fwrite($f, '<url>' . PHP_EOL);
                                    fwrite($f, $tab . '<loc>' . $url . $item . '</loc>' . PHP_EOL);
                                    fwrite($f, $tab . '<lastmod>' . $tm . '</lastmod>' . PHP_EOL);
                                    fwrite($f, $tab . '<priority>' . $block['priority'] . '</priority>' . PHP_EOL);
                                    fwrite($f, '</url>' . PHP_EOL);
                                }
                                break;
                            case 'article':
                                $ds_article = new view(null, 'ds_article');
                                $items = $ds_article->select(['lastmod', 'art_alias'])->fetchAll();
                                foreach ($items as $item){
                                    fwrite($f, '<url>' . PHP_EOL);
                                    fwrite($f, $tab . '<loc>' . $url . '/articles/' . $item['art_alias'] . '</loc>' . PHP_EOL);
                                    fwrite($f, $tab . '<lastmod>' . date('c', $item['lastmod']) . '</lastmod>' . PHP_EOL);
                                    fwrite($f, $tab . '<priority>' . $block['priority'] . '</priority>' . PHP_EOL);
                                    fwrite($f, '</url>' . PHP_EOL);
                                }
                                break;
                            case 'news':
                                $ds_news = new view(null, 'ds_news');
                                $items = $ds_news->select(['id', 'time'])->fetchAll();
                                foreach ($items as $item){
                                    fwrite($f, '<url>' . PHP_EOL);
                                    fwrite($f, $tab . '<loc>' . $url . '/news/view?id=' . $item['id'] . '</loc>' . PHP_EOL);
                                    fwrite($f, $tab . '<lastmod>' . date('c', $item['time']) . '</lastmod>' . PHP_EOL);
                                    fwrite($f, $tab . '<priority>' . $block['priority'] . '</priority>' . PHP_EOL);
                                    fwrite($f, '</url>' . PHP_EOL);
                                }
                                break;
                            case 'amc':
                                $ds_maindata_organizers = new view(null, 'ds_maindata_organizers');
                                $items = $ds_maindata_organizers->select('id')->fetchAll(null, 'id');
                                foreach ($items as $item){
                                    fwrite($f, '<url>' . PHP_EOL);
                                    fwrite($f, $tab . '<loc>' . $url . '/amc/' . $item . '</loc>' . PHP_EOL);
                                    fwrite($f, $tab . '<lastmod>' . $tm . '</lastmod>' . PHP_EOL);
                                    fwrite($f, $tab . '<priority>' . $block['priority'] . '</priority>' . PHP_EOL);
                                    fwrite($f, '</url>' . PHP_EOL);
                                }
                                break;
                        }
                    }
                    fwrite($f, '</urlset>'. PHP_EOL);
                    fclose($f);
                }
            } elseif ($pack['type'] == 'list'){
                switch ($pack['name']){
                    case 'cards':
                        $ds_maindata = new view(null, 'ds_maindata');
                        $maxId = $ds_maindata->select('MAX(id) max')->fetch('max');
                        $ind = 0;
                        $start = 0;
                        $ds_maindata->select('id, GREATEST(`loadtime`, `last_update`, `mprice_update`) up');
                        while ($start < $maxId){
                            $fn = 'sitemap_cards_' . $ind . '.xml';
                            $fp = system::realPath($fn);
                            $where = ['id' => ['begin' => $start, 'end' => $start + $pack['step'] - 1]];
                            if($items = $ds_maindata->where($where)->fetchAll('id', 'up')){
                                if($f = fopen($fp, 'w')){
                                    $list[] = $fn;
                                    fwrite($f, '<?xml version="1.0" encoding="UTF-8"?>'. PHP_EOL);
                                    fwrite($f, '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'. PHP_EOL);
                                    foreach ($items as $id => $up){
                                        fwrite($f, '<url>' . PHP_EOL);
                                        fwrite($f, $tab . '<loc>' . $url . '/card/' . $id . '</loc>' . PHP_EOL);
                                        fwrite($f, $tab . '<lastmod>' . date('c', $up) . '</lastmod>' . PHP_EOL);
                                        fwrite($f, $tab . '<priority>' . $pack['priority'] . '</priority>' . PHP_EOL);
                                        fwrite($f, '</url>' . PHP_EOL);
                                    }
                                    fwrite($f, '</urlset>'. PHP_EOL);
                                    fclose($f);
                                }
                            } elseif(file_exists($fp)){
                                unlink($fp);
                            }
                            $start += $pack['step'];
                            $ind++;
                        }
                        break;
                }
            }
        }
        foreach ($list as $fn){
            fwrite($F, $tab . '<sitemap>' . PHP_EOL);
            fwrite($F, $tab . $tab . '<loc>' . $url . '/' . $fn . '</loc>' . PHP_EOL);
            fwrite($F, $tab . $tab . '<lastmod>' . $tm . '</lastmod>' . PHP_EOL);
            fwrite($F, $tab . '</sitemap>' . PHP_EOL);
        }
        fwrite($F, '</sitemapindex>'. PHP_EOL);
        fclose($F);
    }
