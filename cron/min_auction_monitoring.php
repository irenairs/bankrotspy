<?php
    // запускать каждую минуту
    //exit();
    define('DS_ENGINE', 1);
    ini_set('display_errors', 1);
    ini_set('memory_limit', '8192M');
    date_default_timezone_set('UTC');
    mb_internal_encoding('UTF-8');
    set_time_limit(0);

    $root = implode('/', array_slice(explode('/', str_replace('\\', '/', dirname(__FILE__))), 0, -1)) . '/';
    /** @noinspection PhpIncludeInspection */
    require_once($root . 'config/lite.php');

    $timeStepSec = 86400; // время в секундах, на которое отодвигается завершение аукциона
    $timeDeltaSec = 3600; // время в секундах после завершения аукциона, на котором еще будет срабатывать продление

    require_once(system::realPath('system/lot.class.php'));
    require_once(system::realPath('system/bankruptcy.class.php'));
    (new bankruptcy())->upAuctionStatus($timeStepSec, $timeDeltaSec);
