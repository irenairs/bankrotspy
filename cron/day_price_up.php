<?php
    // запускать вместо parse-2.php по его расписанию
    // обновление цен согласно графиков снижения цены

    define('DS_ENGINE', 1);
    ini_set('display_errors', 1);
    date_default_timezone_set('Europe/Moscow');
    mb_internal_encoding('UTF-8');
    set_time_limit(0);

    $root = implode('/', array_slice(explode('/', str_replace('\\', '/', dirname(__FILE__))), 0, -1)) . '/';
    /** @noinspection PhpIncludeInspection */
    require_once($root . 'config/lite.php');

    require_once(system::realPath('system/priceSchedule.class.php'));
    require_once(system::realPath('system/lot.class.php'));
    require_once(system::realPath('system/bankruptcy.class.php'));
    (new bankruptcy())->upPrices(['now_price', 'min_price']);
