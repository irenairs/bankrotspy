<?php
    // запускать каждый день в 19:45
    // рассылка информации о новых лотах по подписке

    define('DS_ENGINE', 1);
    ini_set('display_errors', 1);
    date_default_timezone_set('Europe/Moscow');
    mb_internal_encoding('UTF-8');

    $root = implode('/', array_slice(explode('/', str_replace('\\', '/', dirname(__FILE__))), 0, -1)) . '/';
    /** @noinspection PhpIncludeInspection */
    require_once($root . 'config/lite.php');

    $dbQuery = new dbQuery();
    $sql = 'SELECT'
            .' ds_search_profiles.id,'
            .' ds_search_profiles.userid,'
            .' ds_search_profiles.profile,'
            .' ds_search_profiles.pname,'
            .' ds_users.mail'
            .' FROM'
            .' ds_search_profiles,'
            .' ds_users'
        .' WHERE'
            .' ds_search_profiles.subscribe = 1'
            .' AND ds_search_profiles.userid =  ds_users.id';

    $dbQuery->exec($sql);
    if($subscrArr = $dbQuery->fetchAll()){

        require_once system::realPath('dscore/classes/defaultset.php');
        require_once system::realPath('vendor/autoload.php');
        require_once system::realPath('system/searchSphinx.class.php');
        $searchSphinx = new searchSphinx();

        require_once system::realPath('dscore/classes/mailer.php');
        $mail_temp = '<p align="center"><a href="http://bankrot-spy.ru/"><img src="http://bankrot-spy.ru/themes/web/default/images/logo.png" width="392" height="70" /></a></p>'
            . '<p align="center"><span style="color: #696969;"><span style="font-size: x-large;"><strong>Новости по новым лотам</strong></span></span></p><br>'
            . '<table style="border-color: #ссс;" border="3">'
            . '<tbody>'
            . '<tr><th><strong>Название</strong></th><th><strong>Цена, руб</strong></th><th><strong>Добавлен</strong></th></tr>';

        foreach ($subscrArr as $profileData){
            $res = $searchSphinx->getFreshTop(json_decode($profileData['profile'], 1), $profileData['userid']);
            if($res[0]) {
                $body = $mail_temp;
                foreach ($res[0] as $item){
                    $name = (mb_strlen($item['name']) > 70) ? (mb_substr($item['name'], 0, 70) . "...") : $item['name'];
                    $body .= '<tr>';
                    $body .= '<td style="padding: 10px;"><strong>&nbsp;<span style="color: #ff9900;"><a style="text-decoration:none; color: #C77216;" href="http://bankrot-spy.ru/card/' . $item['id'] . '">' . $name . '</a></span></strong></td>';
                    $body .= '<td style="padding: 10px; text-align: center;"><strong>' . (int)$item['price'] . '</strong></td>';
                    $body .= '<td style="padding: 10px; text-align: center;"><strong>' . date("d.m.Y H:i:s",$item['loadtime']) . '</strong></td>';
                    $body .= '</tr>';
                }
                $body .= '</tbody></table>';

                $mail = new Mailer();
                $mail->addAddress($profileData['mail']);
                $mail->Subject = 'Новости по лотам (' . $profileData['pname'] . ')';
                $mail->Body = $body;
                $mail->send();
            }
        }
    }
