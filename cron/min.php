<?php
    // запускать каждую минуту
    //exit();
    define('DS_ENGINE', 1);
    ini_set('display_errors', 1);
    date_default_timezone_set('Europe/Moscow');
    mb_internal_encoding('UTF-8');

    $root = implode('/', array_slice(explode('/', str_replace('\\', '/', dirname(__FILE__))), 0, -1)) . '/';
    /** @noinspection PhpIncludeInspection */
    require_once($root . 'config/lite.php');

    // статистика по лотам
    $src = [];
    require_once(system::realPath('system/lot.class.php'));
    require_once(system::realPath('system/bankruptcy.class.php'));
    $src[] = (new bankruptcy())->getStats();

    function getSummary($name, $src, $details = false, $pr = false){
        $res[$name] = 0;
        foreach ($src as $item){
            $res[$name] += $details ? (isset($item['details'][$name]) ? $item['details'][$name] : 0) : $item[$name];
        }
        if($pr){
            $res[$name . 'Pr'] = number_format($res[$name] / $pr * 100, 2);
        } else {
            $res['pr'] = $res[$name];
        }
        $res[$name] = number_format($res[$name], 0, '.', ' ');
        return $res;
    }

    $details = ['mPrice', 'cPrice', 'withAddress', 'withPhoneOrMail', 'withPhoto', 'withPriceSchedule'];
    $res = [];
    $res = array_merge($res, getSummary('lotAct', $src));
    foreach ($details as $item){
        $res = array_merge($res, getSummary($item, $src, true, $res['pr']));
    }
    unset($res['pr']);

    cache::create('counts_torg', $res);

    // типизация новостей площадок для раздельного вывода
    $reArr = [
        2 => '/Высокая доходность у лот/'
    ];
    $ds_platform_news = new model(null, 'ds_platform_news');
    $items = $ds_platform_news->select(['id', 'type', 'text'])->where([
        'type' => [0]
    ])->fetchAll();
    foreach ($items as $item){
        $index = 1;
        foreach ($reArr as $ind => $re){
            if(preg_match($re, $item['text']) == 1){
                $index = $ind;
                break;
            }
        }
        $ds_platform_news->updateByField('id' , $item['id'], [
            'type' => $index
        ]);
    }

    // конвертация новых графиков цен в табличный формат
    require_once(system::realPath('system/priceSchedule.class.php'));
    (new priceSchedule(true))->parsePriceSchedules();

    // конвертация новых лотов конфиската и приватизации
    $convertLimit = 100;
    require_once(system::realPath('system/convert.class.php'));
    require_once(system::realPath('system/convertCn.class.php'));
    $c = new convertCn();
    $i = $convertLimit;
    while($i && $c->step()){$i--;}
    require_once(system::realPath('system/convertPr.class.php'));
    $c = new convertPr();
    $i += $convertLimit;
    while($i && $c->step()){$i--;}
