<?php
    // запускать каждый день
    //exit();
    define('DS_ENGINE', 1);
    ini_set('display_errors', 1);
    date_default_timezone_set('UTC');
    mb_internal_encoding('UTF-8');

    $root = implode('/', array_slice(explode('/', str_replace('\\', '/', dirname(__FILE__))), 0, -1)) . '/';
    /** @noinspection PhpIncludeInspection */
    require_once($root . 'config/lite.php');

    $res = [];
    $def = [];
    $t = time();

    // Platforms
    $tmpGr = [];
    $grNow = 'A';
    $tmpDef = [];
    $ds_maindata_platforms = new view(null, 'ds_maindata_platforms');
    $reestr_pl = new view(null, 'reestr_pl');
    $items = $ds_maindata_platforms->order('`platform_url` ASC')->fetchAll();
    foreach ($items as $data){
        $id = $data['id'];
        $url = $data['platform_url'];
        $tmp = [
            'id' => $id,
            'name' => $url,
            'title' => $reestr_pl->select('nazva')->where('`link` like \'%' . $url . '%\'')->limit(1)->fetch('nazva'),
        ];
        $grCur = mb_strtoupper(mb_substr($url, 0, 1));
        if($grCur == $grNow){
            $tmpGr[] = $tmp;
        } else {
            if($tmpGr){
                $res['platform'][] = [
                    'title' => $grNow,
                    'list' => $tmpGr
                ];
            }
            $grNow = $grCur;
            $tmpGr = [$tmp];
        }
        $tmpDef[$id] = 1;
    }
    if($tmpGr){
        $res['platform'][] = [
            'title' => $grNow,
            'list' => $tmpGr
        ];
    }
    $def['platform'] = $tmpDef;

    // Regions
    $tmpPlace = [0];
    $tmpReg = [];
    $tmpForce = [77, 78, 50, 47, 0];
    $tmpArr = [];
    $res['region'] = [['list' => []]];

    $ds_maindata_regions = new view(null, 'ds_maindata_regions');
    $items = $ds_maindata_regions->order('`mini` ASC')->fetchAll();
    foreach ($items as $data){
        $num = (int)$data['number'];
        if($data['parent_id'] !== '0' && $num != -1) {
            $tmpPlace[$num] = 1;
        }
        $tmpReg[] = $data;
    }

    foreach($tmpReg as $region) {
        $id = $region['id'];
        if (in_array($region['number'], $tmpForce)) {
            $tmpArr[$region['number']] = $region['mini'];
        } elseif ($region['parent_id'] == '0') {
            $tmp = [
                'title' => $region['mini'],
                'group' => $id,
                'list' => []
            ];
            foreach($tmpReg as $sub) {
                if($sub['parent_id'] == $id && !in_array($sub['number'], $tmpForce)) {
                    $tmp['list'][] = [
                        'id' => $sub['number'],
                        'name' => $sub['mini']
                    ];
                }
            }
            $res['region'][] = $tmp;
        }
    }
    foreach ($tmpForce as $key) {
        $res['region'][0]['list'][] = [
            'id' => $key,
            'name' => $tmpArr[$key]
        ];
    }
    $def['place'] =  $tmpPlace;

    $res['category'] = [['list' => []]];
    $def['categories'] =  [];
    $ds_maindata_category = new view(null, 'ds_maindata_category');
    $items = $ds_maindata_category->order('`usort` ASC')->fetchAll();
    foreach ($items as $data) {
        $res['category'][0]['list'][] = [
            'id' => $data['id'],
            'name' => $data['name']
        ];
        $def['categories'][$data['id']] = 1;
    };

    $def['type'] = [1 => 1, 2 => 1];
    $def['src'] = ['bn' => 1, 'cn' => 1, 'pr' => 1];
    $def['user'] = ['all' => 1, 'fav' => 1, 'trash' => 0];

    cache::create('map_torg', $res);
    cache::create('settings_default', $def);

    // попытка определить host по ip для тех, у кого раньше выдало ошибку
    $ds_guest_bun = new model(null, 'ds_guest_bun');
    $items = $ds_guest_bun->where('`host` = \'\' OR `host` = \'getHostByAddrError\'')->fetchAll();
    foreach ($items as $item){
        system::checkHost($item['ip']);
    }

    // очистка таблицы сохраненных запросов от статистического мусора
    require_once(system::realPath('vendor/autoload.php'));
    require_once(system::realPath('system/searchQuery.class.php'));
    $searchQuery = new searchQuery();
    $searchQuery->clear();
