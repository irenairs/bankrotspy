<?php
    // экспорт данных `bankrotspy`.`ds_maindata` -> `forexp`.`bankruptcy`
    define('DS_ENGINE', 1);
    ini_set('display_errors', 1);
    ini_set('memory_limit', '8192M');
    date_default_timezone_set('UTC');
    mb_internal_encoding('UTF-8');
    set_time_limit(0);

    $root = implode('/', array_slice(explode('/', str_replace('\\', '/', dirname(__FILE__))), 0, -1)) . '/';
    /** @noinspection PhpIncludeInspection */
    require_once($root . 'config/lite.php');

    $start = microtime(true);  //Запуск счетчика
    $t = time();
    $tDate = date('Y-m-d H:i:s', $t);

    $dbQuery = new dbQuery(null, [
        'db' => 'forexp',
        'config' => system::$config['db']['external']['forexp']
    ]);
    $bankruptcy = new model($dbQuery->db, 'bankruptcy');

    $where = [
        'end_time' => ['begin' => $t],
        'status' => [1, 2, 6, 7, 8, 9, 10]
    ];

    $sql = "
      SELECT 
        `ds_maindata`.`id`, 
        `ds_maindata`.`price`, 
        `ds_maindata`.`status`,
        `ds_maindata`.`description`,
        `ds_maindata`.`start_time`, 
        `ds_maindata`.`end_time`,
        `ds_maindata`.`now_price`,
        `ds_maindata`.`code`,
        `ds_maindata`.`auct_link`, 
        `ds_maindata`.`cat_id`, 
        `ds_maindata`.`market_price`, 
        `ds_maindata`.`profit_rub`, 
        `ds_maindata`.`profit_proc`, 
        `ds_maindata`.`fedlink`, 
        `ds_maindata`.`address`, 
        `ds_maindata`.`cadastr_price`, 
        `ds_maindata_type`.`type_name` 'tName',
        `ds_maindata_status`.`status_name` 'stName',
        `ds_maindata_regions`.`name` 'rName',
        `ds_maindata_category`.`name` 'cName',
        `ds_maindata_debtors`.`dept_name` 'debtName',
        `ds_maindata_debtors`.`inn` 'debtInn',
        `ds_maindata_debtors`.`debt_profile` 'debtProfile',
        `ds_maindata_organizers`.`org_name` 'orgName',
        `ds_maindata_organizers`.`contact_person` 'orgPerson',
        `ds_maindata_organizers`.`org_profile` 'orgProfile',
        `ds_maindata_organizers`.`phone` 'orgPhone',
        `ds_maindata_organizers`.`mail` 'orgMail',
        `ds_maindata_organizers`.`inn` 'orgInn',
        `bankruptcy`.`id` 'targetId' 
      FROM `bankrotspy`.`ds_maindata` 
      LEFT JOIN `bankrotspy`.`ds_maindata_type` ON `ds_maindata`.`type` = `ds_maindata_type`.`id`
      LEFT JOIN `bankrotspy`.`ds_maindata_status` ON `ds_maindata`.`status` = `ds_maindata_status`.`id`
      LEFT JOIN `bankrotspy`.`ds_maindata_regions` ON `ds_maindata`.`place` = `ds_maindata_regions`.`number`
      LEFT JOIN `bankrotspy`.`ds_maindata_category` ON `ds_maindata`.`cat_id` = `ds_maindata_category`.`id`
      LEFT JOIN `bankrotspy`.`ds_maindata_debtors` ON `ds_maindata`.`debtor` = `ds_maindata_debtors`.`id`
      LEFT JOIN `bankrotspy`.`ds_maindata_organizers` ON `ds_maindata`.`organizer` = `ds_maindata_organizers`.`id`
      LEFT JOIN `forexp`.`bankruptcy` ON `ds_maindata`.`id` = `bankruptcy`.`lot_id` 
      WHERE 
        `end_time` >= " . $t . " 
        AND `status` IN ('" . join("','", $where['status']) . "')";

    $dbQuery->exec($sql);
    $items = $dbQuery->fetchAll();
    foreach ($items as $item){
        $cdPrice = $item['cadastr_price'];
        if(in_array($item['cat_id'], ['3', '5', '6'])){
            if($cdPrice > 0){
                if($item['market_price'] == 0) {
                    $item['market_price'] = $cdPrice;
                }
            } else {
                $cdPrice = $item['market_price'];
            }
        } else {
            $cdPrice = 0;
        }
        $prm = [
            'price_start' => $item['price'],
            'price_actual' => $item['now_price'],
            'price_market' => $item['market_price'],
            'price_step' => '5%',
            'deposit' => '10%',
            'cadastral_value' => $cdPrice,
            'profit_rub' => $item['profit_rub'],
            'profit_proc' => $item['profit_proc'],
            'category' => $item['cName'],
            'address' => $item['address'],
            'auction_type' => $item['tName'],
            'auction_status' => getStatus($item['start_time'], $item['end_time'], $item['stName'], (int)$item['status']),
            'application_start' => $item['start_time'],
            'application_deadline' => $item['end_time'],
            'trading_number' => $item['code'],
            'lot_online' => $item['auct_link'],
            'fedresurs' => $item['fedlink'],
            'regionname' => $item['rName'],
            'date_update' => $tDate,
            'lot_number' => $item['code'],
            'description' => $item['description']
        ];
        if($item['debtName']){
            $prm['bankrupt'] = $item['debtName'];
            $prm['inn_bankruptcy'] = $item['debtInn'];
            if ($item['debtProfile'] && $item['debtProfile'] != 'www1'){
                $prm['bankrupt_href'] = $item['debtProfile'];
            }
        }
        if($item['orgName']){
            $org_name_new = [];
            $tmp = explode(' ', $item['orgName']);
            foreach ($tmp as $tm) {
                if (!is_numeric($tm)) {
                    $org_name_new[] = $tm;
                }
            }
            $prm['contact_person'] = $item['orgPerson'];
            $prm['organizer'] = join(' ', $org_name_new);
            $prm['organizer_link'] = $item['orgProfile'];
            $prm['phone'] = $item['orgPhone'];
            $prm['email'] = $item['orgMail'];
            $prm['inn_organizer'] = $item['orgInn'];
        }
        if($item['targetId']){
            $bankruptcy->updateByField('id', $item['targetId'], $prm);
        } else {
            $prm['lot_id'] = $item['id'];
            $bankruptcy->insert($prm);
        }
    }

    $whereUp = [
        '`date_update` <  \'' . $tDate . '\'',
        '`auction_status` LIKE \'%заявок\''
    ];
    $bankruptcy->updateByField(join(' AND ', $whereUp), false, [
        'auction_status' => 'Торги окончены'
    ]);

    printf("Done for %.2f seconds", microtime(true) - $start);

    function getStatus($startTime = '', $endTime = '', $status = '', $intStatus = 0){
        $startTime = (new DateTime())->setTimestamp($startTime);
        $days = (new DateTime())->diff($startTime)->days;
        if (in_array($intStatus, [3, 4, 5, 6])) { //Если пришел один из статусов окончания
            $res = $status;
        } elseif ($days <= 0) {
            $res = $endTime >= time() ? ($status == 'Оконченный' ? 'Торги окончены' : 'Приём заявок') : 'Торги окончены';
        } else {
            $res = $days . ' ' . getNumDecl($days, [1 => 'день', 'дня', 5 => 'дней']).' до подачи заявок';
        }
        return  $res;
    }

    function getNumDecl($n, $dict){
        $p1 = $n % 10;
        if((($n - $p1) % 100) / 10 == 1)
            $p1 = 5;
        return $dict[($p1 == 1) ? 1 : (($p1 > 1 && $p1 < 5) ? 2 : 5)];
    }
