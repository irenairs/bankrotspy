<?php
    // запускать раз в день, время любое
    // очистка от старых лотов

    define('DS_ENGINE', 1);
    ini_set('display_errors', 1);
    date_default_timezone_set('Europe/Moscow');
    mb_internal_encoding('UTF-8');
    set_time_limit(0);

    $root = implode('/', array_slice(explode('/', str_replace('\\', '/', dirname(__FILE__))), 0, -1)) . '/';
    /** @noinspection PhpIncludeInspection */
    require_once($root . 'config/lite.php');

    $days = 365; // количество дней, в течении которых храним информацию о завершенных лотах
    $tm = time() - $days * 86400;

    require_once(system::realPath('system/lot.class.php'));
    require_once(system::realPath('system/bankruptcy.class.php'));
    $target = new bankruptcy();
    $target->cleaner();
    $target->cleaner($tm);
