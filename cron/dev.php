<?php
    define('DS_ENGINE', 1);
    ini_set('display_errors', 1);
    date_default_timezone_set('UTC');
    mb_internal_encoding('UTF-8');

    $root = implode('/', array_slice(explode('/', str_replace('\\', '/', dirname(__FILE__))), 0, -1)) . '/';
    /** @noinspection PhpIncludeInspection */
    require_once($root . 'config/lite.php');

//    require_once(system::realPath('system/priceSchedule.class.php'));
//    $parser = new priceSchedule();
//    $str = '<div class="node_offer_reduction"><h2 class="label-above">Порядок снижения цены:&nbsp;</h2><p>с 26.09.18 по 02.10.18-1800000 руб.(Период снижения по московскому времени с 00:00 до 23:59)<br>с 03.10.18 по 09.10.18-1710000 руб.(Период снижения по московскому времени с 00:00 до 23:59)<br>с 10.10.18 по 16.10.18-1620000 руб.(Период снижения по московскому времени с 00:00 до 23:59)<br>с 17.10.18 по 23.10.18-1530000 руб.(Период снижения по московскому времени с 00:00 до 23:59)<br>с 24.10.18 по 30.10.18-1440000 руб.(Период снижения по московскому времени с 00:00 до 23:59)<br>с 31.10.18 по 06.11.18-1350000 руб.(Период снижения по московскому времени с 00:00 до 23:59)<br>с 07.11.18 по 13.11.18-1260000 руб.(Период снижения по московскому времени с 00:00 до 23:59)<br>с 14.11.18 по 20.11.18-1170000 руб.(Период снижения по московскому времени с 00:00 до 23:59)<br>с 21.11.18 по 27.11.18-1080000 руб.(Период снижения по московскому времени с 00:00 до 23:59)<br>с 28.11.18 по 04.12.18-990000 руб.(Период снижения по московскому времени с 00:00 до 23:59)<br>с 05.12.18 по 11.12.18-900000 руб.(Период снижения по московскому времени с 00:00 до 23:59) </p></div>';
//    echo json_encode($parser->parseCurrent($str));

//    $t = time();
//    $target = new model(null, 'ds_maindata');
//    $where = [
//        '`end_time` >= ' . $t,
//        '`status` IN ("' . join('","', [1, 2, 6, 7, 8, 9, 10]) . '")',
//        '`cat_id` IN ("' . join('","', [3, 5, 6]) . '")',
//        '`cadastr_num` = \'\''
//    ];
//    $res = $target->select(['id', 'description'])->where(join(' AND ', $where))->limit(10)->fetchAll();
//    foreach ($res as &$item){
//        preg_match_all('/[^\d](\d*:\d*:\d*:\d*?)[^\d]/', $item['description'], $m);
//        $nums = array_unique($m[1]);
//        $c = count($item['m']);
//        $target->updateByField('id', $item['id'], [
//            'cadastr_num' => ($c == 1)
//                ? (checkCN($nums[0]) ? $nums[0] : 'zero')
//                : ($c ? 'multi' : 'no')
//        ]);
//    }//
//    echo json_encode($res);
//

//    $m = [];
//    $str = 'Симферопольская ул., 2а.Здание.Назначение – нежилоеКадастровый номер:42:24:0101022:625 Общей площадью 335,6 кв.м';
//    preg_match_all('/[^\d](\d+:\d+:\d+:\d+)/', $str, $m);
//    echo checkCN($m[1][0]);
//
//    function checkCN($num){
//        $groups = explode(':', $num);
//        foreach ($groups as $group){
//            if(!(int)$group) return false;
//        }
//        return true;
//    }

//    require_once(system::realPath('system/parserAddress.class.php'));
//    $parser = new parserAddress();
//    $t = time();
//    $ds_maindata = new model(null, 'ds_maindata');
//    $where = [
//        '`status` IN ("' . join('","', [1, 2, 6, 7, 8, 9, 10]) . '")',
//        '`cat_id` IN ("' . join('","', [5, 6]) . '")',
//        '`address` = \'\''
//    ];
//    if(system::$config['mode']['production']){
//        $where[] = '`end_time` >= ' . $t;
//    }
//    $items = $ds_maindata->select(['id', 'description'])->where(join(' AND ', $where))->fetchAll();
//    $out = [];
//    foreach ($items as $item){
//        if($address = $parser->parseCurrent($item['description'])){
//            $out[] = $address;
//        } else {
//            //$out[] = $item['description'];
//        }
//    }
//    echo json_encode($out);
