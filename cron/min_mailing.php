<?php
    // запускать каждую минуту
    //exit();
    define('DS_ENGINE', 1);
    ini_set('display_errors', 1);
    date_default_timezone_set('UTC');
    mb_internal_encoding('UTF-8');

    $root = implode('/', array_slice(explode('/', str_replace('\\', '/', dirname(__FILE__))), 0, -1)) . '/';
    /** @noinspection PhpIncludeInspection */
    require_once($root . 'config/lite.php');

    $sleep = 2; // пауза между отправками
    $pack = 20; // писем в минуту
    $host = 'https://' . system::$config['mode']['host'];

    $mail_mailing = new model(null, 'mail_mailing');
    if($mail = $mail_mailing->where('status', 1)->limit(1)->fetch()){
        $mailID = $mail['id'];
        $c = [
            'pointer' => (int)$mail['user_pointer'],
            'ok' => 0,
            'total' => 0
        ];
        $where = [
            'id' => ['begin' => $c['pointer']],
            'subscribe' => 1
        ];
        if($mail['groups'] !== '1') {
            $where['rights'] = $mail['groups'];
        }
        $ds_users = new view(null, 'ds_users');
        $users = $ds_users->where($where)->order('`id` ASC')->limit($pack)->fetchAll();
        if($users){
            require_once (system::realPath('dscore/classes/mailer.php'));
            $mail_mailing_log = new model(null, 'mail_mailing_log');
            $mail_mailing_errors = new model(null, 'mail_mailing_errors');
            $mail_mailing_files = new view(null, 'mail_mailing_files');
            $files = [];
            if($attachments = $mail_mailing_files->where([
                'mail_id' => $mailID,
                'type' => 'attachments'
            ])->fetchAll()){
                foreach ($attachments as $item){
                    $files[] = [
                        'path' => system::realPath('data/mailing/' . $mailID . '/' . $item['name']),
                        'name' => $item['name']
                    ];
                }
            }
            foreach ($users as $user){
                sleep($sleep);
                $body = [
                    'host'  => $host,
                    'text'  => $mail['text_compiled'],
                    'hash'  => system::hash($user['id'])
                ];
                $mailer = mailer::factory(system::realPath('engine/templates') . '/');
                $mailer->setSubject($mail['subject']);
                $mailer->setBody('mailing', $body);
                $mailer->addAddress($user['mail']);
                foreach ($files as $file) {
                    $mailer->addAttachment($file['path'], $file['name']);
                }
                if($mailer->send()) {
                    $mail_mailing_log->insert([
                        'mail_id' => $mailID,
                        'user_id' => $user['id'],
                        'created' => time()
                    ]);
                    $c['ok']++;
                } else {
                    $mail_mailing_errors->insert([
                        'mail_id' => $mailID,
                        'error' => $mailer->debug(),
                        'user_id' => $user['id'],
                        'created' => time()
                    ]);
                }
                $c['pointer'] = $user['id'];
                $c['total']++;
            }
            $upDate = [
                'user_pointer' => $c['pointer'] + 1,
                'user_ok' => (int)$mail['user_ok'] + $c['ok'],
                'user_total' => (int)$mail['user_total'] + $c['total']
            ];
        }
        if(!$users || ($c['total'] < $pack)){
            $upDate['status'] = 3;
            $upDate['end_time'] = time();
        }
        $mail_mailing->updateByField('id', $mailID, $upDate);
    }
