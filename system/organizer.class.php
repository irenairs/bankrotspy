<?php
class organizer extends model{
    public $table = 'ds_maindata_organizers';
    public $id_field = 'id';

    public function parse($prm){
        $res = ['phone' => ''];
        if(preg_match('/[^\d](\+7|8)((\)?\s?\-?\(?\d){10})[^\d]/i', $prm['contact_person'], $m) == 1){
            $res['phone'] = '+7' . str_replace([' ', '(', ')', '-'], '', $m[2]);
            $res['person'] = trim(str_replace($m[0], ' ', $prm['contact_person']));
        } else {
            $res['person'] = trim($this->parseStr($prm['contact_person']));
        }
        $res['phone'] = $prm['phone'] ? $prm['phone'] : $res['phone'];
        $res['email'] = $prm['mail'];
        $res['name'] = $this->parseStr($prm['org_name']);
        $res['manager'] = $this->parseStr($prm['manager']);
        return $res;
    }

    private function parseStr($str){
        $tmp = [];
        foreach (explode(' ', $str) as $item) {
            if (!is_numeric($item)) {
                $tmp[] = $item;
            }
        }
        return join(' ', $tmp);
    }
}
