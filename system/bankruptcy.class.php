<?php
class bankruptcy extends lot{
    public $table = 'ds_maindata';

    public $details = [
        'index' => ['tbl' => 'ds_maindata_index', 'field' => 'lotID'],
        'favorite' => ['tbl' => 'ds_maindata_favorive', 'field' => 'item', 'user' => 'user_id'],
        'hide' => ['tbl' => 'ds_maindata_hide', 'field' => 'item', 'user' => 'user_id'],
        'hint' => ['tbl' => 'ds_maindata_hint', 'field' => 'id'],
        'source' => ['tbl' => 'ds_maindata_source', 'field' => 'lotid'],
        'notes' => ['tbl' => 'lot_notes', 'field' => 'lot_id', 'user' => 'user_id'],
        'prices' => ['tbl' => 'lot_prices', 'field' => 'id'],
        'views' => ['tbl' => 'lot_views', 'field' => 'lot_id'],
    ];
    public $pref = 'bankruptcy_';
    public $searchAddressExtraFields = ['description'];
    public $searchCadastrNumExtraFields = ['place', 'description'];
    public $whereAddressFilter = '`cat_id` IN ("5", "6")';
    public $whereCadastrNumFilter = '`cat_id` IN ("3", "5", "6")';
    public $whereLotActive = '`status` IN ("1", "2", "6", "7", "8", "9", "10")';
    public $whereWithPhoto = '`havefoto` > 0';
    public $whereWithPriceSchedule = '`grafik1` != \'\'';

    public function clearExtra($ind, $range){
        // photo
        $mInd = $this->pref . 'photo';
        if(!isset($this->models[$mInd])){
            $this->models[$mInd] = new model(null, 'lot_fotos');
        }
        $items = $this->models[$mInd]->where(['lotid' => $range ? ['end' => $ind] : $ind])->fetchAll();
        if($items){
            if($production = system::$config['mode']['production']){
                $photoPath = system::realPath('../foto.bankrot-spy.ru');
            }
            foreach ($items as &$item){
                if($production){
                    unlink($photoPath . $item['link']);
                }
                $this->models[$mInd]->deleteByField('id', $item['id']);
            }
        }
    }

    public function getLotsByDebtor($dId, $exclude = []){
        $where = [
            $this->whereLotActual(),
            $this->whereLotActive,
            '`debtor` = ' . $dId
        ];
        if($exclude){
            $where[] = '`id` NOT IN (' . join(',', $exclude) . ')';
        }
        return $this->select(['id', 'profit_proc'])->where(join(' AND ', $where))
            ->order('`profit_proc` DESC, `id` DESC')->fetchAll('id', 'profit_proc');
    }

    public function getStatsExtra(){
        $res = [];
        $where = [
            $this->whereLotActual(),
            $this->whereLotActive
        ];
        $where[2] = $this->whereWithPhoto;
        $res['withPhoto'] = $this->where(join(' AND ', $where))->count();
        $where[2] = $this->whereWithPriceSchedule;
        $res['withPriceSchedule'] = $this->where(join(' AND ', $where))->count();
        return $res;
    }

    public function upPrices($prm = []){
        if(!$prm) return;
        $prm[] = 'id';
        $parser = new priceSchedule();
        $where = [
            $this->whereLotActual(),
            $this->whereLotActive,
            '`grafik_state` > 0'
        ];
        if($items = $this->select($prm)->where(join(' AND ', $where))->fetchAll()){
            foreach ($items as $item){
                $upPrm = [];
                if(in_array('now_price', $prm)){
                    $resPrice = $parser->getActualPrice($item['id']);
                    if(($resPrice > 0) && ($resPrice != $item['now_price'])){
                        $upPrm['now_price'] = $resPrice;
                    }
                }
                if(in_array('min_price', $prm)){
                    $resPrice = $parser->getLastPrice($item['id']);
                    if(($resPrice > 0) && ($resPrice != $item['min_price'])){
                        $upPrm['min_price'] = $resPrice;
                    }
                }
                if($upPrm){
                    $upPrm['last_update'] = time();
                    $this->updateByField('id', $item['id'], $upPrm);
                }
            }
        }

    }

    public function upAuctionStatus($step, $delta){
        $tm = time();
        $items = $this->select(['id', 'status', 'end_time'])->where([
            'type' => 1,
            'status' => [6, 8, 9, 10],
            'end_time' => ['begin' => $tm - $delta, 'end' => $tm]
        ])->fetchAll();
        foreach ($items as $item){
            $this->updateByField('id', $item['id'], [
                'end_time' => $item['end_time'] + $step
            ]);
        }
    }
}
