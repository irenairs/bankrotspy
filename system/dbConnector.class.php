<?php
class dbConnector{

    protected static $adapter = [];

    public static function getConnection($prm = ['db' => 'def', 'config' => null]){
        if (isset(self::$adapter[$prm['db']])){
            return self::$adapter[$prm['db']];
        } else {
            $adapter = new dbAdapter($prm['config']);
            if (!$adapter->connect()){
                return false;
            }
            self::$adapter[$prm['db']] = $adapter;
            return $adapter;
        }
    }
}
