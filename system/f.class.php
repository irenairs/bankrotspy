<?php
class f {

    public static function prF($str, $fixTo = 0, $no = '-'){
        return (float)$str ? number_format($str, $fixTo, ' .', '&nbsp;') : $no;
    }

    public static function w125($n, $dict){
        $p1 = $n % 10;
        if((($n - $p1) % 100) / 10 == 1)
            $p1 = 5;
        return $dict[($p1 == 1) ? 1 : (($p1 > 1 && $p1 < 5) ? 2 : 5)];
    }
}