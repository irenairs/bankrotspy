<?php
class view extends dbQuery{

    public $table = '';
    public $id_field = 'id';
    public $fields = array();
    protected $what = '*';
    protected $where = '';
    protected $order = '';
    protected $group = '';
    protected $limit = false;

    public function __construct($db = false, $table = false){
        parent::__construct($db);
        if ($table){
            $this->table = $table;
        }
        $this->fields = $this->db->scheme($this->table);
        $this->select();
    }

    public function getSql(){
        $sql = 'SELECT ' . $this->what . ' FROM ' . $this->table . $this->where;
        if ($this->order){
            $sql .= ' ORDER BY ' . $this->order;
        }
        if ($this->group){
            $sql .= ' GROUP BY ' . $this->group;
        }
        if ($this->limit){
            $sql .= ' LIMIT ' . $this->limit[0] . ', ' . $this->limit[1];
        }
        return $sql;
    }

    public function adaptValueToField($field, $value){
        return (isset($this->fields[$field])) ? $this->db->adaptValue($value, $this->fields[$field]['type'], $this->fields[$field]['null']) : false;
    }

    public function reset(){
        return $this->select()->where()->order()->group()->limit();
    }

    public function select($fields = '*'){
        if ($fields == '*'){
            $this->what = '`' . implode('`, `', array_keys($this->fields)) . '`';
        } else {
            if (is_array($fields)){
                $this->what = '`' . implode('`, `', $fields) . '`';
            } else {
                $this->what = $fields;
            }
        }
        $this->exec = false;
        return $this;
    }

    public function where($param1 = false, $param2 = false){
        $where = ' WHERE ';
        if (!$param1 && !$param2){
            $where = '';
        } elseif ($param1 && is_string($param1) && !$param2){
            if(!is_null($param2)) {
                $where .= $param1;
            } else {
                $where .= $param1 . ' IS NULL';
            }
        } elseif (is_array($param1) && !$param2) {
            foreach ($param1 as $k => $v) {
                if (is_array($v)) {
                    if (isset($v['begin']) || isset($v['end'])) {
                        if (isset($v['begin']) && isset($v['end'])) {
                            $where .= '(`' . $k . '` BETWEEN ' .
                                $this->adaptValueToField($k, @$v['begin']) . ' AND ' .
                                $this->adaptValueToField($k, @$v['end']) . ') AND ';
                        } elseif (isset($v['begin'])) {
                            $where .= '(`' . $k . '` >= ' .
                                $this->adaptValueToField($k, @$v['begin']) . ') AND ';
                        } elseif (isset($v['end'])) {
                            $where .= '(`' . $k . '` <= ' .
                                $this->adaptValueToField($k, @$v['end']) . ') AND ';
                        }
                    } else {
                        $where .= '`' . $k . '` IN(';
                        if ($v) {
                            foreach ($v as $v2) {
                                $where .= $this->adaptValueToField($k, $v2) . ',';
                            }
                        } else {
                            $where .= '0,';
                        }
                        $where = substr($where, 0, -1) . ') AND ';
                    }
                } else {
                    if(is_null($v)) {
                        $where .= '`' . $k . '` IS NULL AND ';
                    } else {
                        $where .= '`' . $k . '`=' . $this->adaptValueToField($k, $v) . ' AND ';
                    }
                }
            }
            $where = substr($where, 0, -5);
        } elseif (is_array($param2)) {
            if (isset($param2['begin']) || isset($param2['end'])) {
                if (@$param2['begin'] && @$param2['end']) {
                    $where .= '(`' . $param1 . '` BETWEEN ' .
                        $this->adaptValueToField($param1, $param2['begin']) . ' AND ' .
                        $this->adaptValueToField($param1, $param2['end']) . ')';
                } elseif (@$param2['begin']) {
                    $where .= '(`' . $param1 . '` >= ' .
                        $this->adaptValueToField($param1, $param2['begin']) . ')';
                } elseif (@$param2['end']) {
                    $where .= '(`' . $param1 . '` <= ' .
                        $this->adaptValueToField($param1, $param2['end']) . ')';
                }
            } else {
                $where .= '`' . $param1 . '` IN(';
                if ($param2){
                    foreach ($param2 as $v) {
                        $where .= $this->adaptValueToField($param1, $v) . ',';
                    }
                } else {
                    $where .= '0,';
                }
                $where = substr($where, 0, -1) . ') ';
            }
        } else {
            $where .= '`' . $param1 . '`=' . $this->adaptValueToField($param1, $param2);
        }
        $this->where = $where;
        $this->exec = false;
        return $this;
    }

    public function order($order = ''){
        $this->order = $order;
        $this->exec = false;
        return $this;
    }

    public function group($group = ''){
        $this->group = $group;
        $this->exec = false;
        return $this;
    }

    public function limit($start = 0, $count = 0) {
        if (!$start && !$count) {
            $this->limit = false;
        } elseif ($start && !$count) {
            $this->limit = array(0, intval($start));
        } else {
            $this->limit = array(intval($start), intval($count));
        }
        $this->exec = false;
        return $this;
    }

    public function getByField($param1 = false, $param2 = false, $all = false) {
        $this->where($param1, $param2);
        return $all ? $this->fetchAll() : $this->fetch();
    }

    public function getById($id) {
        if (!$id) return false;
        $this->where($this->id_field, $id);
        return is_array($id) ? $this->fetchAll() : $this->fetch();
    }

    public function count(){
        $count = false;
        if ($this->exec('SELECT COUNT(*) as `count` FROM ' . $this->table . $this->where)) {
            $count = $this->fetch('count');
        }
        $this->exec = false;
        return $count;
    }

    public function countByField($param1, $param2 = false){
        $this->where($param1, $param2);
        return $this->count();
    }

    public function getItemsCount(){
        return $this->reset()->count();
    }
}
