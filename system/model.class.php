<?php
class model extends view{

    public function updateByField($param1, $param2 = false, $values, $getSql = false){
        $this->where($param1, $param2);
        if (!is_array($values)) return false;
        $str = '';
        foreach ($values as $k => $v) {
            $str .= '`' . $k . '`=' . $this->adaptValueToField($k, $v) . ', ';
        }
        $str = substr($str, 0, -2);
        $query = 'UPDATE ' . $this->table . ' SET ' . $str . ' ' . $this->where;
        if($getSql){
            $res = $query;
        } else {
            $res = $this->exec($query);
            $this->exec = false;
        }
        return $res;
    }

    public function updateById($id, $values) {
        if (!$id) return false;
        return $this->updateByField($this->id_field, $id, $values);
    }

    public function deleteByField($param1, $param2 = false){
        $this->where($param1, $param2);
        $res = $this->exec('DELETE FROM ' . $this->table . $this->where);
        $this->exec = false;
        return $res;
    }

    public function deleteById($id){
        if (!$id) return false;
        return $this->deleteByField($this->id_field, $id);
    }

    public function insert($values, $replace = false, $getSql = false){
        if (!is_array($values)) return $this;
        if (is_array(current($values))) {
            $values_array = $values;
        } else {
            $values_array[0] = $values;
        }
        $fields = array_keys(current($values_array));
        $fieldsCount = count($fields);
        $fields = '`' . implode('`, `', $fields) . '`';
        $values = '';
        foreach ($values_array as $array) {
            if (count($array) != $fieldsCount) continue;
            $values .= '(';
            $temp = '';
            foreach ($array as $k=> $v) {
                $temp .= $this->adaptValueToField($k, $v) . ',';
            }
            $values .= substr($temp, 0, -1) . '), ';
        }
        $values = substr($values, 0, -2);
        $query = ($replace ? 'REPLACE' : 'INSERT') . ' IGNORE INTO ' . $this->table . ' (' . $fields . ') VALUES ' . $values;
        return $getSql ? $query : $this->exec($query);
    }

    public function replace($values){
        return $this->insert($values, true);
    }
}
