<?php

class parserCadastrNum{

    private static $reCadastrNum = '/[^\d](\d+:\d+:\d+:\d+)/';

    public function parseCurrent($str){
        preg_match_all(self::$reCadastrNum, $str, $m);
        $nums = array_unique($m[1]);
        if($c = count($nums)){
            return [
                'reg' => explode(':', $nums[0])[0],
                'num' => ($c == 1)
                    ? ($this->checkCN($nums[0]) ? $nums[0] : 'zero')
                    : $this->commonCN($nums)
            ];
        } else {
            return [
                'reg' => 0,
                'num' => 'no'
            ];
        }
    }

    private function checkCN($num){
        $groups = explode(':', $num);
        foreach ($groups as $group){
            if(!(int)$group) return false;
        }
        return true;
    }

    private function commonCN($items){
        $map = [];
        $ind = 0;
        $res = '';
        foreach ($items as $item){
            $groups = explode(':', $item);
            foreach ($groups as $key => $value){
                $map[$key][] = $value;
            }
        }
        foreach ($map as $key => $values){
            if(count(array_unique($values)) == 1){
                $ind = $key + 1;
            }
        }
        if($ind){
            $res .= join(':', array_slice(explode(':', $items[0]), 0, $ind)) . ':';
        }
        return $res . 'multi';
    }

}