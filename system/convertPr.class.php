<?php
class convertPr extends convert {

    public $table = 'privatizacia';

    public $mode = 'pr';

    public $logTablesExtra = [
        'log' => 'log_pr'
    ];

    public $srcTablesExtra = [
        'index' => 'privatizacia_index'
    ];

    public $targetTablesExtra = [
        'details' => 'ds_maindata_pr'
    ];

    public $directExtra = ['min_price'];

    public $detailsExtra = [
        'reshenie' => 'decision',
        'valuta' => 'currency',
        'strana' => 'country',
        'shagauction' => 'stepUp',
        'shagponijenia' => 'stepDown',
    ];

    public function _type($value){
        return ['type' => preg_match('/аукцион/iu', $value) ? 1 : 2];
    }
}
