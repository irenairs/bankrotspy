<?php
class convert extends model{

    public $id_field = 'id';

    public $models = [];
    public $log = [];
    public $src = [];
    public $target = [];
    public $mode;
    public $errLimit = 500;

    public $targetMain;

    public $logTables = [
        'err' => 'log_err_converter',
    ];
    public $logTablesExtra = [];

    public $srcTables = [];
    public $srcTablesExtra = [];

    public $targetTables = [
        'fav' => 'ds_maindata_favorive',
        'hide' => 'ds_maindata_hide',
        'hint' => 'ds_maindata_hint',
        'index' => 'ds_maindata_index',
        'notes' => 'lot_notes',
        'org' => 'ds_maindata_organizers',
        'prices' => 'lot_prices',
    ];
    public $targetTablesExtra = [];

    public $drop = ['id', 'kolvo', 'check_version', 'check_stamp'];
    public $dropExtra = [];

    public $modify = [
        'nomeri' => ['_redirect', ['code']],
        'mesto' => '',
        'region' => ['_region', null],
        'organizer' => ['_organizer', null],
        'tipi' => ['_category', null],
        'obremenenie' => ['_depend', null],
        'tradetip' => ['_type', null],
        'price' => ['_price', ['price', 'now_price']],
        'status' => ['_status', null],
        'link' => ['_redirect', ['auct_link']],
        'address' => '',
        'address_by' => '',
        'item' => ['_redirect', ['item_key']]
    ];
    public $modifyExtra = [];
    public $modifyKeys = [];

    public $direct = ['name', 'start_time', 'end_time', 'market_price',
        'profit_rub', 'profit_proc', 'market_min', 'market_max',
        'mprice_update', 'cadastr_price', 'cadastr_num', 'state'
    ];
    public $directExtra = [];

    public $details = [
        'predmet' => 'subject',
        'mestosrok' => 'final',
        'vidsob' => 'property',
        'zadatok' => 'deposit'
    ];
    public $detailsExtra = [];
    public $detailsKeys = [];

    public $mapType = [];

    public $mapStatus = [
        'Объявлен' => 1
    ];

    public $mapRegions = [];

    public $mapCategory = [
        ['жилой дом', 5],
        ['квартира', 5],
        ['садовый дом', 5],
        ['дача', 5],
        ['коммерческая недвижимость', 6],
        ['гараж', 6],
        ['здани', 6],
        ['строени', 6],
        ['помещени', 6],
        ['предприяти', 6],
        ['сооружени', 6],
        ['строени', 6],
        ['земельный участок', 3],
        ['земли', 3],
        ['автобус', 7],
        ['судно', 7],
        ['грузов', 7],
        ['сельскохозяйственная техника', 7],
        ['с\/х техника', 7],
        ['суда', 7],
        ['трактор', 7],
        ['автотранспорт', 1],
        ['движимое', 1],
        ['автомобил', 1],
        ['транспорт', 1],
        ['дебиторск', 2],
        ['с\/х продукция', 4],
        ['домашний скот', 4],
        ['оборудование', 8],
        ['инструмент', 8],
        ['товар', 8],
        ['станки', 8],
        ['мебель', 8],
        ['техника', 8],
        ['лекарств', 8],
        ['автозапчасти', 8],
        ['одежда', 8],
        ['обувь', 8],
        ['оружие', 8],
        ['акции', 9],
        ['доля', 9],
        ['иное', 0],
        ['другое', 0],
        ['прочие категории', 0],
        ['высвобождаемое военное имущество', 0],
        ['космический объект', 0],

    ];

    public function __construct(){
        parent::__construct();
        $this->targetMain = new model(null, 'ds_maindata');
        $this->logTables = array_merge($this->logTables, $this->logTablesExtra);
        $this->srcTables = array_merge($this->srcTables, $this->srcTablesExtra);
        $this->targetTables = array_merge($this->targetTables, $this->targetTablesExtra);
        $this->drop = array_merge($this->drop, $this->dropExtra);
        $this->modify = array_merge($this->modify, $this->modifyExtra);
        $this->modifyKeys = array_keys($this->modify);
        $this->direct = array_merge($this->direct, $this->directExtra);
        $this->details = array_merge($this->details, $this->detailsExtra);
        $this->detailsKeys = array_keys($this->details);
        $regions = new view(null, 'ds_maindata_regions');
        $this->mapRegions = $regions->where(['number' => ['begin' => 1]])->fetchAll('number', 'short');
        $this->mapRegions[0] = 'no';
    }

    public function step(){
        $this->checkTable('log', 'err');
        $errArr = $this->log['err']->where('src', $this->mode)->fetchAll(null, 'lotID');
        if(($count = count($errArr)) > $this->errLimit) return false;
        $where = $count ? ('`id` NOT IN(' . join(',', $errArr) . ')') : '';
        if(!($src = $this->order($this->id_field)->where($where)->limit(1)->fetch())){
            return false;
        }
        $id = $src['id'];
        $resTarget = [
            'src' => $this->mode,
            'platform_id' => 57,
            'debicheck' => -1
        ];
        $resDetails = [
            'src' => $id
        ];
        $resModify = [];
        foreach ($src as $key => $value){
            if(in_array($key, $this->drop)){
                continue;
            } elseif(in_array($key, $this->modifyKeys)){
                if($prm = $this->modify[$key]){
                    $method = $prm[0];
                    $resTarget = array_merge($resTarget, $this->$method($value, $prm[1]));
                } else {
                    $resModify[$key] = $value;
                }
            } elseif(in_array($key, $this->direct)){
                $resTarget[$key] = $value;
            } elseif(in_array($key, $this->detailsKeys)){
                $resDetails[$this->details[$key]] = $value;
            } else {
                $resTarget['error'] = 'Необработанная колонка ' . $key;
            }
            if(isset($resTarget['error'])) break;
        }
        if(isset($resTarget['error'])){
            $this->log['err']->insert([
                'src' => $this->mode,
                'lotID' => $id,
                'error' => $resTarget['error']
            ]);
            return true;
        }
        $resTarget = array_merge($resTarget, $this->_address($resModify));
        $resTarget['loadtime'] = min(time(), $resTarget['start_time']);
        // копируем данные в основную таблицу банкротства
        $this->targetMain->insert($resTarget);
        if(!($targetID = $this->targetMain->insertId())){
            $this->log['err']->insert([
                'src' => $this->mode,
                'lotID' => $id,
                'error' => 'Ошибка записи в таблицу ds_maindata'
            ]);
            return true;
        }
        // копируем данные во вспомогательную таблицу
        $this->checkTable('target', 'details');
        $resDetails['id'] = $targetID;
        $this->target['details']->insert($resDetails);
        // переносим индекс
        $this->checkTable('src', 'index');
        $this->checkTable('target', 'index');
        if($pack = $this->src['index']->where('lotID', $id)->fetch()){
            $pack['lotID'] = $targetID;
            $this->target['index']->insert($pack);
        $this->src['index']->deleteByField('lotID', $id);
        }
        // делаем копию
        $this->checkTable('log', 'log');
        $src['target'] = $targetID;
        $this->log['log']->insert($src);
        // удаляем запись
        $this->deleteById($id);
        $this->reset();
        return true;
    }

    public function dropDuplicate(){
        $this->checkTable('log', 'log');
        if($duplItemID = $this->log['log']->select('item, COUNT(*) AS c')->group('item HAVING c > 1')->limit(1)->fetch('item')){
            $this->log['log']->reset()->select(['id', 'target']);
            $duplPrm = $this->log['log']->where('item', $duplItemID)->order('target DESC')->limit(1)->fetch();
            $duplTargetID = $duplPrm['target'];
            $this->targetMain->deleteByField('id', $duplTargetID);
            $this->checkTable('target', 'details');
            $this->target['details']->deleteByField('id', $duplTargetID);
            $this->checkTable('target', 'index');
            $this->target['index']->deleteByField('lotID', $duplTargetID);
            $this->checkTable('target', 'fav');
            $this->target['fav']->deleteByField('item', $duplTargetID);
            $this->checkTable('target', 'notes');
            $this->target['notes']->deleteByField('lot_id', $duplTargetID);
            $this->checkTable('target', 'hide');
            $this->target['hide']->deleteByField('item', $duplTargetID);
            $this->checkTable('target', 'hint');
            $this->target['hint']->deleteByField('id', $duplTargetID);
            $this->checkTable('target', 'prices');
            $this->target['prices']->deleteByField('id', $duplTargetID);
            $this->log['log']->deleteByField('id', $duplPrm['id']);
            $this->log['log']->reset();
            return true;
        } else {
            return false;
        }
    }

    public function _address($prm){
        $res = [
            'address' => $prm['address'],
            'address_by' => $prm['address_by']
        ];
        if(!$res['address']){
            $res['address'] = $prm['mesto'];
            $res['address_by'] = 'INIT';
        }
        return $res;
    }

    public function _category($value){
        foreach ($this->mapCategory as $prm){
            if(preg_match('/' . $prm[0] . '/iu', $value)){
                return ['cat_id' => $prm[1]];
            }
        }
        return ['error' => 'Категория не распознана -> ' . $value];
    }

    public function _depend($value){
        if(in_array($value, ['', 'Да', 'Нет'])){
            return ['depend' => $value];
        } else {
            return ['error' => 'Обременение не распознано'];
        }
    }

    public function _organizer($value){
        if(!isset($this->models['org'])){
            $this->models['org'] = new model(null, 'ds_maindata_organizers');
        }
        $this->checkTable('target', 'org');
        $prm = ['org_name' => $value];
        if($id = $this->target['org']->where($prm)->fetch('id')){
            return ['organizer' => $id];
        } else {
            $prm['src'] = $this->mode;
            $this->target['org']->insert($prm);
            if($id = $this->target['org']->insertId()){
                return ['organizer' => $id];
            } else {
                return ['error' => 'Ошибка обработки организатора'];
            }
        }

    }

    public function _price($value, $prm){
        $res = [];
        foreach ($prm as $field){
            $res[$field] = $value;
        }
        return $res;
    }

    public function _redirect($value, $prm){
        return [$prm[0] => $value];
    }

    public function _region($value){
        $res = [];
        foreach ($this->mapRegions as $key => $val){
            if(($value == $val) || preg_match('/' . $val . '/iu', $value)){
                $res[] = $key;
            }
        }
        if(count($res) == 1){
            return ['place' => $res[0]];
        } elseif($value == 'Алтайский край') {
            return ['place' => 22];
        } elseif($value == 'Костромская обл') {
            return ['place' => 44];
        } elseif($value == 'Томская обл') {
            return ['place' => 70];
        } else {
            return ['error' => 'Регион не распознан -> ' . $value];
        }
    }

    public function _status($value){
        if(isset($this->mapStatus[$value])){
            return ['status' => $this->mapStatus[$value]];
        } else {
            return ['error' => 'Статус торгов не распознан -> ' . $value];
        }
    }

    public function _type($value){
        if(isset($this->mapType[$value])){
            return ['type' => $this->mapType[$value]];
        } else {
            return ['error' => 'Тип торгов не распознан -> ' . $value];
        }
    }

    public function checkTable($mode, $name){
        if(!isset($this->{$mode}[$name])){
            $this->{$mode}[$name] = new model(null, $this->{$mode . 'Tables'}[$name]);
        }
    }

}
