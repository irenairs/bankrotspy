<?php

class parserAddress{

    private static $method = [
        'AD.01.01' => 'address_kv',
        'AD.02.01' => 'address_dom',
        'PL.01.01' => 'place_kv',
        'PL.02.01' => 'place_dom'
    ];

    private static $temp = [
        'address_pre' => '[А|а]дрес[^\:]*\:\s*',
        'address_body' => '[^\:]{0,200}?',
        'place_pre' => 'есто\s*положени[^\:]*\:\s*',
        'kv_post' => '[\s,]+кв[^\d\:]{0,8}\d+',
        'dom_post' => '[\s,]д(ом)?\.?\s*\d+'
    ];

    public function parseCurrent($str){
        foreach (self::$method as $type => $method){
            if($res = $this->$method($str)){
                return [
                    'str' => $str,
                    'type' => $type,
                    'res' => $res
                ];
            }
        }
        return false;
    }

    private function address_kv($str){
        $res = false;
        $arrExc = ['Москва'];
        $arrRepl = ['Мос##а'];
        $re = '/' . self::$temp['address_pre'] . '(' . self::$temp['address_body'] . self::$temp['kv_post'] . ')/i';
        if(preg_match($re, str_replace($arrExc, $arrRepl, $str), $m) == 1){
            $res = str_replace($arrRepl, $arrExc, $m[1]);
        }
        return $res;
    }

    private function address_dom($str){
        $res = false;
        $arrExc = [];
        $arrRepl = [];
        $re = '/' . self::$temp['address_pre'] . '(' . self::$temp['address_body'] . self::$temp['dom_post'] . ')/i';
        if(preg_match($re, str_replace($arrExc, $arrRepl, $str), $m) == 1){
            $res = str_replace($arrRepl, $arrExc, $m[1]);
        }
        return $res;
    }

    private function place_kv($str){
        $res = false;
        $arrExc = ['Москва'];
        $arrRepl = ['Мос##а'];
        $re = '/' . self::$temp['place_pre'] . '(' . self::$temp['address_body'] . self::$temp['kv_post'] . ')/i';
        if(preg_match($re, str_replace($arrExc, $arrRepl, $str), $m) == 1){
            $res = str_replace($arrRepl, $arrExc, $m[1]);
        }
        return $res;
    }

    private function place_dom($str){
        $res = false;
        $arrExc = [];
        $arrRepl = [];
        $re = '/' . self::$temp['place_pre'] . '(' . self::$temp['address_body'] . self::$temp['dom_post'] . ')/i';
        if(preg_match($re, str_replace($arrExc, $arrRepl, $str), $m) == 1){
            $res = str_replace($arrRepl, $arrExc, $m[1]);
        }
        return $res;
    }

}