<?php
class searchQuery extends model{
    public $table = 'search_queries';
    public $id_field = 'id';

    private $mapCleaner = [
        ['total' => 1, 'days' => 90],
        ['total' => 5, 'days' => 180],
        ['total' => 10, 'days' => 360]
    ];

    public function clear(){
        $sphinx_queries = new view(null, 'sphinx_queries');
        $tm = time();
        foreach ($this->mapCleaner as $kew => $prm){
            $where = [
                'total' => ['end' => $prm['total']],
                'dt' => ['end' => $tm - $prm['days'] * 86400]
            ];
            if($idArr = $sphinx_queries->select('`index`')->where($where)->fetchAll(null, 'index')){
                $this->deleteById($idArr);
            }
        }
    }

    public function fix($q, $mode = 'any'){
        $tm = time();
        if($res = $this->where('query', $q)->fetch()){
            $this->updateById($res[$this->id_field], [
                $mode => $res[$mode] + 1,
                'dt' => $tm
            ]);
        } else {
            $this->insert([
                'query' => $q,
                $mode => 1,
                'dt' => $tm
            ]);
        }
    }
}
