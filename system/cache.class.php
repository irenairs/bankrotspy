<?php
class cache {

    public static function getHash($name){
        return md5($name);
    }

    public static function create($name, $content){
        if (!$name || !$content) {
            return false;
        }

        $folder = system::realPath('cache');
        if (!is_writable($folder)){
            chmod($folder, 0777);
        }

        $filename = $folder . '/' . self::getHash($name);
        if(file_put_contents($filename, serialize($content), LOCK_EX)){
            if(fileowner($filename) == 0){
                chown($filename, 'bsp1');
            }
            return true;
        } else {
            return false;
        }
    }

    public static function get($name, $expTime = 0, $expTimeFull = false){
        if (!$name){
            return false;
        }
        $name = self::getHash($name);
        $filename = system::realPath('cache/' . $name);
        if (!file_exists($filename)){
            return false;
        }
        if ($expTime === 0){
            return @unserialize(file_get_contents($filename));
        } else {
            $time = filemtime($filename);
            if (!$expTimeFull) {
                $expTime = time() - $expTime;
            }
            if ($expTime <= $time) {
                return @unserialize(file_get_contents($filename));
            } else {
                @unlink($filename);
                return false;
            }
        }
    }

    public static function clear($name){
        if (!$name) {
            return false;
        }
        $name = self::getHash($name);
        $filename = system::realPath('cache/' . $name);
        if (!file_exists($filename)){
            return false;
        }
        @unlink($filename);
        return true;
    }
}
