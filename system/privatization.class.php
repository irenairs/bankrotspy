<?php
class privatization extends lot{
    public $table = 'privatizacia';

    public $details = [
        'index' => ['tbl' => 'privatizacia_index', 'field' => 'lotID'],
        'favorite' => ['tbl' => 'ds_maindata_favorive_pr', 'field' => 'item', 'user' => 'user_id'],
        'hide' => ['tbl' => 'ds_maindata_hide_pr', 'field' => 'item', 'user' => 'user_id'],
        'hint' => ['tbl' => 'ds_maindata_hint_privatizacia', 'field' => 'id'],
        'notes' => ['tbl' => 'lot_notes_pr', 'field' => 'lot_id', 'user' => 'user_id'],
        'prices' => ['tbl' => 'lot_prices_privatizacia', 'field' => 'id'],
    ];
    public $pref = 'privatization_';
    public $whereLotActive = '`status` = \'Объявлен\'';
}
