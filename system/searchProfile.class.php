<?php
class searchProfile extends model{
    public $table = 'ds_search_profiles';

    public $bankruptcy;
    public $dbQuery;
    private static $defName = 'Стандартный профиль';

    public function getProfiles($uid){
        $tm = time() - 60;
        $res = [];
        $data = $this->where('userid', $uid)->fetchAll();
        foreach ($data as $item){
            $tmp = [
                'id' => $item['id'],
                'name' => $item['pname'],
                'subscribe' => $item['subscribe'],
                'fresh' => $item['tm'] < $tm ? $this->upProfileFresh($item['profile'], $uid, $item['id']) : $item['fresh']
            ];
            if($tmp['id'] != core::$user_set['defprofile']){
                $tmp['can_edit'] = 1;
            }
            $res[] = $tmp;
        }
        return $res;
    }

    public function getProfilesExt($uid){
        $res = [
            'def' => defaultset::get(),
            'user' => []
        ];
        if($uid){
            $res['user']['current'] = core::$user_set['tabledata'];
            $res['user']['def'] = core::$user_set['defprofile'];
            $res['user']['list'] = [];
            $profiles = $this->where('userid', $uid)->fetchAll();
            foreach ($profiles as $profile){
                $res['user']['list'][$profile['id']] = [
                    'n' => $profile['pname'],
                    's' => (bool)$profile['subscribe']
                ];
                if($profile['id'] == $res['user']['current']){
                    $res['user']['prm'] = [];
                    $userPrm = json_decode($profile['profile'], 1);
                    foreach($res['def'] as $key => $value){
                        $res['user']['prm'][$key] = isset($userPrm[$key]) ? $userPrm[$key] : $value;
                    }
                }
            }
        } else {
            $res['user']['current'] = 0;
        }
        $res['columns'] = $this->columns();
        $res['fields'] = $this->fields();
        return $res;
    }

    private function upUserSettings($uid){
        $ds_users = new model(null, 'ds_users');
        $ds_users->updateByField('id', $uid, [
            'settings' => serialize(core::$user_set)
        ]);
    }

    public function createProfile($data, $name = false){
        if(!$uid = core::$user_id) return;
        $this->insert([
            'userid' => $uid,
            'profile' => json_encode($data),
            'pname' => $name ? $name : self::$defName
        ]);
        $id = $this->insertId();
        core::$user_set['tabledata'] = $id;
        if(!$name){
            core::$user_set['defprofile'] = $id;
        }
        $this->upUserSettings($uid);
        $ds_counts = new model(null, 'ds_counts');
        $ds_counts->insert([
            'page' => 17,
            'counttime' => time(),
            'day' => date('j'),
            'monthyear' => date('ny') . 17
        ]);
    }

    public function fixProfile($prm){
        $def = defaultset::get();
        $save = [];
        foreach($def as $key => $value){
            if(isset($prm->$key)){
                if(is_object($prm->$key) && is_array($value)){
                    $tmp = [];
                    foreach((array)$prm->$key as $k => $v){
                        $tmp[$k] = $v ?  1 : 0;
                    }
                    $save[$key] = $tmp;
                } elseif(!is_object($prm->$key) && !is_array($value)) {
                    $save[$key] = $prm->$key;
                }
            } else {
                $save[$key] = $value;
            }
        }
        (isset(core::$user_set['tabledata']) && core::$user_set['tabledata'])
            ? $this->upProfile(core::$user_set['tabledata'], $save)
            : $this->createProfile($save);
    }

    public function goProfile($ind, $uid){
        if($this->where(['id' => $ind, 'userid' => $uid])->fetch()){
            core::$user_set['tabledata'] = $ind;
            $this->upUserSettings($uid);
        }
    }

    public function addProfile($name){
        $this->createProfile(defaultset::get(), htmlspecialchars(trim($name)));
    }

    public function rnProfile($ind, $name, $uid){
        $where = ['id' => $ind, 'userid' => $uid];
        if($this->where($where)->fetch()){
            $this->updateByField($where, false, ['pname' => $name]);
        }
    }
    public function rmProfile($ind, $uid){
        $where = ['id' => $ind, 'userid' => $uid];
        if($this->where($where)->fetch()){
            $this->deleteByField($where);
            core::$user_set['tabledata'] = core::$user_set['defprofile'];
            $this->upUserSettings($uid);
        }
    }

    public function upProfile($id, $data){
        if($this->where('id', $id)->count()){
            $this->updateByField('id', $id, [
                'profile' => json_encode($data)
            ]);
        } else {
            $this->createProfile($data);
        }
    }

    public function upProfileFresh($prm, $uid, $id){
        if(!isset($this->searchSphinx)){
            $this->searchSphinx = new searchSphinx();
        }
        $count = $this->searchSphinx->getFreshCount(json_decode($prm, true), $uid);
        $this->updateByField('id', $id, [
            'fresh' => $count,
            'tm' => time()
        ]);
        return $count;
    }

    public function syncProfile($id){
        $def = defaultset::get();
        if($prm = $this->where('id', $id)->fetch('profile')){
            $prm = json_decode($prm, 1);
            $save = [];
            foreach($def as $key => $value){
                if(isset($prm[$key])){
                    if(is_array($prm[$key]) && is_array($value)){
                        $tmp = [];
                        foreach($prm[$key] as $k => $v){
                            $tmp[$k] = $v ?  1 : 0;
                        }
                        $save[$key] = $tmp;
                    } elseif(!is_array($prm[$key]) && !is_array($value)) {
                        $save[$key] = $prm[$key];
                    }
                } else {
                    $save[$key] = $value;
                }
            }
            $this->updateById($id, ['profile' => json_encode($save)]);
            return true;
        } else {
            return false;
        }
    }

    public function columns(){
        $uid = core::$user_id;
        $uset = core::$user_set;
        $viewColumns = mapColumns::get('view');
        $defUser = (isset($uset['columns']) && isset($uset['columns']['bankruptcy']))
            ? explode('|', $uset['columns']['bankruptcy']) : [];
        $resColumns = [
            'def' => $defUser,
            'on' => [],
            'off' => []
        ];
        foreach($viewColumns as $group){
            $tmpGroup = [
                'title' => $group['title'],
                'items' => []
            ];
            foreach ($group['items'] as $item){
                if((!isset($item['can']) || CAN($item['can'])) && (!isset($item['groups']) || in_array(core::$rights, $item['groups']))){
                    if(!$uid && !$item['def']){
                        $resColumns['off']['reg'][] = $item['title'];
                    } else {
                        $tmp = [
                            'name' => $item['name'],
                            'title' => $item['title'],
                            'ch' => in_array($item['name'], $defUser) ? !$item['def'] : $item['def'],
                            'def' => $item['def']
                        ];
                        $tmpGroup['items'][] = $tmp;
                    }
                } else {
                    $resColumns['off'][$item['bl']][] = $item['title'];
                }
            }
            if($tmpGroup['items']){
                $resColumns['on'][] = $tmpGroup;
            }
        }
        return $resColumns;
    }

    public function fields(){
        $uset = core::$user_set;
        $exportColumns = mapColumns::get('export');
        $defUser = (isset($uset['columns']) && isset($uset['columns']['export']))
            ? explode('|', $uset['columns']['export']) : [];
        $resColumns = [
            'on' => []
        ];
        foreach($exportColumns as $group){
            $tmpGroup = [
                'title' => $group['title'],
                'items' => []
            ];
            foreach ($group['items'] as $item){
                $tmp = [
                    'name' => $item['name'],
                    'title' => $item['title'],
                    'ch' => !in_array($item['name'], $defUser),
                    'def' => true
                ];
                $tmpGroup['items'][] = $tmp;
            }
            if($tmpGroup['items']){
                $resColumns['on'][] = $tmpGroup;
            }
        }
        return $resColumns;
    }
}
