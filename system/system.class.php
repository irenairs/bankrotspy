<?php
class system {

    public static $config;
    public static $levels = [
        'admin' => 100,
        'premium' => 50,
        'register' => 20
    ];
    public static $level;
    public static $rights = [];
    public static $mask;
    private static $salt = 'ca04d10700a2a2283e58a192f4d90755';

    function __construct() {
        self::$config = require_once(self::realPath('config/prm.php'));
    }

    public static function setRights($level){
        self::$level = $level;
    }

    public static function checkHost($ip, $first = false){
        $check = false;
        if(($host = gethostbyaddr($ip)) && ($host != $ip)){
            $host = join('.', array_slice(explode('.', $host), -2));
            $check = in_array($host, self::$config['host']['allow']);
        } else {
            $host = 'getHostByAddrError';
        }
        $ds_guest_bun = new model(null, 'ds_guest_bun');
        if($check){
            $ds_ban_exceptions = new model(null, 'ds_ban_exceptions');
            $ds_ban_exceptions->insert(['ip' => $ip, 'comment' => $host]);
            $ds_guest_bun->deleteByField('ip', $ip);
        } else {
            if($first){
                $ds_guest_bun->insert(['ip' => $ip, 'host' => $host]);
            } else {
                $ds_guest_bun->updateByField('ip', $ip, ['host' => $host]);
            }
        }
    }

    public static function hash($str = 'b69c76548e38fb859447070d40f7061f'){
        return md5(self::$salt . $str);
    }

    public static function realPath($path){
        static $realpath = '';
        if (!$realpath){
            $realpath = str_replace('\\', '/', realpath(dirname(__FILE__) . '/..')) . '/';
        }
        $path = str_replace('\\', '/', trim($path, '\//'));
        return $realpath . $path;
    }

}