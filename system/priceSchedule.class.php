<?php
include_once  system::realPath('parse/simple_html_dom.php');

class priceSchedule extends model{

    public $table = 'ds_maindata_prices';

    private static $src;
    private static $src_limit = 1000;

    private $str;
    private $prm;
    private $dom;

    private static $method = [
        5 => 'dt_dt_pr',
        6 => 'td_td_pr',
        7 => 'd_d_pr'
    ];

    private static $parse_method = [
        5 => 'dt_dt_pr',
        6 => 'td_td_pr',
        7 => 'd_d_pr',
        8 => 'dt_pr',
        9 => 'srPublicOfferReductionPeriod'
    ];

    private static $parse_error = [
        'empty_node_offer_reduction'
    ];
    
    private static $where_rules = [
        'withSchedule' => '`grafik1` != \'\'',
        'withoutSchedule' => '`grafik1` = \'\'',
        'stateError' => '`grafik_state` = -2',
        'stateDisabled' => '`grafik_state` = -1',
        'stateUndefined' => '`grafik_state` = 0'

    ];

    public function __construct($src = false){
        parent::__construct();
        if($src){
            self::$src = new model(null, 'ds_maindata');
        }
    }
    
    public function getActualPrice($id){
        return ($res = $this->where([
            'lot_id' => $id,
            'dt_from' => ['end' => time()]
        ])->order('`dt_from` DESC')->limit(1)->fetch('price')) ? (float)$res : 0;
    }

    public function getAllPrices($id){
        return $this->where([
            'lot_id' => $id
        ])->order('`dt_from` ASC')->fetchAll();
    }

    public function getPricesList($id, $format = false){
        $items = $this->getAllPrices($id);
        switch ($format){
            case 'card':
                $tm = time();
                $out = [];
                $cur = false;
                foreach ($items as $key => $item){
                    $to = date('d.m.Y H:i', $item['dt_to']);
                    $tmp = [
                        'title' => 'с ' . date('d.m.Y H:i', $item['dt_from']) . ' по ' . $to,
                        'pr' => f::prF($item['price'], 2),
                    ];
                    if($item['dt_to'] < $tm){
                        $tmp['st'] = 'ps-prev';
                    }
                    if(
                        (($item['dt_from'] <= $tm) && ($item['dt_to'] >= $tm))
                        || (!$cur && isset($items[$key + 1]) && ($items[$key + 1]['dt_from'] > $tm))
                    ){
                        $cur = true;
                        $tmp['st'] = 'ps-cur';
                        $tmp['title'] = 'текущая цена до ' . $to;
                    }
                    if($item['dt_from'] > $tm){
                        $tmp['st'] = 'ps-next';
                    }
                    $out[] = $tmp;
                }
                return $out;
                break;
            default:
                return $items;
        }
    }

    public function getLastPrice($id){
        return ($res = $this->where([
            'lot_id' => $id
        ])->order('`dt_from` DESC')->limit(1)->fetch('price')) ? (float)$res : 0;
    }

    public function parsePriceSchedules(){
        self::$src->updateByField(
            join(' AND ', [self::$where_rules['withoutSchedule'], self::$where_rules['stateUndefined']]),
            false, ['grafik_state' => -1]);
        self::$src->updateByField(
            join(' AND ', [self::$where_rules['withSchedule'], self::$where_rules['stateDisabled']]),
            false, ['grafik_state' => 0]);
        $items = self::$src->select(['id', 'grafik1', 'end_time'])
            ->where(self::$where_rules['stateUndefined'])->limit(self::$src_limit)->fetchAll();
        foreach ($items as $item){
            $upSrc = [];
            if($res = $this->parseSchedule($item['grafik1'], ['end' => $item['end_time']])){
                $this->deleteByField(['lot_id' => $item['id']]);
                if($res['type'] == -1){
                    $upSrc['grafik1'] = '';
                } else {
                    foreach ($res['details'] as $rec){
                        $this->insert([
                            'lot_id' => $item['id'],
                            'dt_from' => $rec['fr'],
                            'dt_to' => $rec['to'],
                            'price' => $rec['pr']
                        ]);
                    }
                }
            }
            $upSrc['grafik_state'] = $res ? $res['type'] : -2;
            self::$src->updateByField('id', $item['id'], $upSrc);
        }
    }
    
    public function parseCurrent($str){
        $this->str = $str;
        $this->dom = str_get_html($str);
        foreach (self::$method as $type => $method){
            if($res = $this->$method()){
                return [
                    'type' => $type,
                    'price' => $this->getCurrent($res)
                ];
            }
        }
        return false;
    }

    public function parseSchedule($str, $prm = []){
        $this->str = $str;
        $this->prm = $prm;
        $this->dom = str_get_html($str);
        foreach (self::$parse_error as $method){
            if($res = $this->$method()){
                return [
                    'type' => -1
                ];
            }
        }
        foreach (self::$parse_method as $type => $method){
            if($res = $this->$method()){
                return [
                    'type' => $type,
                    'details' => $res
                ];
            }
        }
        return false;
    }

    private function dt_dt_pr(){
        $details = [];
        if($this->dom->find('div.node_offer_reduction')){
            preg_match_all('/(\d{2}\.\d{2}\.\d{4}\s\d{1,2}:\d{2}(:\d{2})?)[^\d]*(\d{2}\.\d{2}\.\d{4}\s\d{1,2}:\d{2}(:\d{2})?).*?(\d[\d\s,]*)/', $this->str, $m);
            if(count($m)){
                for($i = 0; $i < count($m[0]); $i++){
                    $details[] = [
                        'fr' => strtotime($m[1][$i]),
                        'to' => strtotime($m[3][$i]),
                        'pr' => (float)str_replace([' ', ','], ['', '.'], $m[5][$i])
                    ];
                }
            }
        }
        return $details;
    }

    private function td_td_pr(){
        $details = [];
        if($this->dom->find('div.node_offer_reduction')){
            preg_match_all('/(\d{2}:\d{2}(:\d{2})?\s\d{2}\.\d{2}\.\d{4})[^\d]*(\d{2}:\d{2}(:\d{2})?\s\d{2}\.\d{2}\.\d{4}).*?(\d[\d\s,]*)/', $this->str, $m);
            if(count($m)){
                for($i = 0; $i < count($m[0]); $i++){
                    $details[] = [
                        'fr' => strtotime($m[1][$i]),
                        'to' => strtotime($m[3][$i]),
                        'pr' => (float)str_replace([' ', ','], ['', '.'], $m[5][$i])
                    ];
                }
            }
        }
        return $details;
    }

    private function d_d_pr(){
        $details = [];
        if($this->dom->find('div.node_offer_reduction')){
            preg_match_all('/(\d{2}\.\d{2}\.\d{2,4})[^\d]*(\d{2}\.\d{2}\.\d{2,4}).*?(\d[\d\s,]*)/', $this->str, $m);
            if(count($m)){
                for($i = 0; $i < count($m[0]); $i++){
                    $details[] = [
                        'fr' => strtotime(preg_replace('/^(.*)\.(\d{2})$/', '${1}.20${2}', $m[1][$i])),
                        'to' => strtotime(preg_replace('/^(.*)\.(\d{2})$/', '${1}.20${2}', $m[2][$i])),
                        'pr' => (float)str_replace([' ', ','], ['', '.'], $m[3][$i])
                    ];
                }
            }
        }
        return $details;
    }

    private function dt_pr(){
        $details = [];
        if($this->dom->find('div.node_offer_reduction')){
            preg_match_all('/(\d{2}\.\d{2}\.\d{4}\s\d{1,2}:\d{2}(:\d{2})?).*?(\d[\d\s,]*)/', $this->str, $m);
            if(count($m)){
                for($i = 0; $i < count($m[0]); $i++){
                    $details[] = [
                        'fr' => strtotime($m[1][$i]),
                        'pr' => (float)str_replace([' ', ','], ['', '.'], $m[3][$i])
                    ];
                }
                for($i = 0; $i < count($details); $i++){
                    $details[$i]['to'] = isset($details[$i + 1])
                        ? ($details[$i + 1]['fr'] - 1)
                        : (int)$this->prm['end'];
                }
            }
        }
        return $details;
    }

    private function srPublicOfferReductionPeriod(){
        $details = [];
        if($items = $this->dom->find('table[id*=srPublicOfferReductionPeriod]>tr.gridRow')){
            $col = count($items[0]->children);
            if(in_array($col, [7, 8])){
                foreach ($items as $item){
                    $details[] = [
                        'fr' => strtotime($item->children[0]->plaintext),
                        'to' => strtotime($item->children[3]->plaintext),
                        'pr' => (float)str_replace([' ', ',', '&nbsp;'], ['', '.', ''], $item->children[$col - 2]->plaintext)
                    ];
                }
            }
        }
        return $details;
    }

    private function empty_node_offer_reduction(){
        if($items = $this->dom->find('div.node_offer_reduction')){
            return !count($items[0]->children);
        }
        return false;
    }

    private function getCurrent($data){
        $now = time();
        $res = null;
        foreach ($data as $prm){
            if($prm['fr'] > $now){
                return $res ? $res : $prm['pr'];
            } else {
                $res = $prm['pr'];
            }
        }
        return $res;
    }

}