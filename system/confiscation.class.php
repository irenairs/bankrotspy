<?php
class confiscation extends lot{
    public $table = 'konfiscat2';

    public $details = [
        'index' => ['tbl' => 'konfiscat2_index', 'field' => 'lotID'],
        'favorite' => ['tbl' => 'ds_maindata_favorive_cn', 'field' => 'item', 'user' => 'user_id'],
        'hide' => ['tbl' => 'ds_maindata_hide_cn', 'field' => 'item', 'user' => 'user_id'],
        'hint' => ['tbl' => 'ds_maindata_hint_konfiscat2', 'field' => 'id'],
        'notes' => ['tbl' => 'lot_notes_cn', 'field' => 'lot_id', 'user' => 'user_id'],
        'prices' => ['tbl' => 'lot_prices_konfiscat2', 'field' => 'id'],
    ];
    public $pref = 'confiscation_';
    public $whereLotActive = '`status` = \'Объявлен\'';
}
