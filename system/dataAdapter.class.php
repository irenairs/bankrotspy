<?php
class dataAdapter {

    public static $url;
    public static $round = 2;

    public function __construct($url = ''){
        self::$url = $url;
    }

    public static function curl($prm = [], $url = false, $mode = 'json') {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $prm = http_build_query($prm, '', '&');

        $url = $url ? $url : self::$url;
        curl_setopt($ch, CURLOPT_URL, $url . '?' . $prm);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $res = curl_exec($ch);

        if ($res === false) {
            echo 'Curl error: [' . $url . '] ' . curl_error($ch);
            return false;
        } elseif($mode !== 'json'){
            return $res;
        } elseif (!($dec = json_decode($res, true))){
            return false;
        }else{
            return $dec;
        }
    }

    public static function nf($val, $round){
        return number_format($val, $round, '.', '');
    }
}