<?php
use Foolz\SphinxQL\Connection;
use Foolz\SphinxQL\SphinxQL;

class searchSphinx{
    
    private static $removeSymbols = [',', '~', '>', '<', '?', ')', '(', '}', '{', '&', '^', '$', '#', '|', '_', '/', '\\'];

    private static $mapOrderType = [1 => 'ASC', 'DESC'];

    private static $mapColumnsOrder = [
        'addition' => 'id',
        'beforedate' => 'start_time',
        'begindate' => 'start_time',
        'beginprice' => 'price',
        'closedate' => 'end_time',
        'debpoints' => 'debpoints',
        'marketprice' => 'marketPriceFilter ASC, market_price',
        'minprice' => 'minPriceFilter ASC, min_price',
        'name' => 'loadtime',
        'nowprice' => 'nowPriceFilter ASC, now_price',
        'place' => 'place',
        'platform' => 'platform_id',
        'pricediff' => 'price_diff',
        'profitproc' => 'profitProcFilter ASC, profit_proc',
        'profitrub' => 'profitRubFilter ASC, profit_rub',
        'type' => 'type'
    ];

    private static $mapColumnsSelect = [
        'marketprice' => 'IF(market_price = 0,1,0) as marketPriceFilter',
        'minprice' => 'IF(min_price = 0,1,0) as minPriceFilter',
        'nowprice' => 'IF(now_price = 0,1,0) as nowPriceFilter',
        'profitproc' => 'IF(profit_proc = 0,1,0) as profitProcFilter',
        'profitrub' => 'IF(profit_rub = 0,1,0) as profitRubFilter'
    ];

    public function getLots($prm, $uid = false, $start = 0, $size = 20, $onlyCount = false){
        $tm = time();
        $select = ['*'];
        $and = [];

        if($uid){
            $all = in_array('all', $prm['user']);
            if($all != in_array('fav', $prm['user'])){
                $ds_maindata_favorive = new view(null, 'ds_maindata_favorive');
                if($uLots = $ds_maindata_favorive->where('user_id', $uid)->fetchAll(null, 'item')){
                    $and[] = 'id ' . ($all ? 'NOT' : '') . ' IN(' . join(',', $uLots) . ')';
                }
            }
            if($all != in_array('trash', $prm['user'])){
                $ds_maindata_hide = new view(null, 'ds_maindata_hide');
                if($uLots = $ds_maindata_hide->where('user_id', $uid)->fetchAll(null, 'item')){
                    $and[] = 'id ' . ($all ? 'NOT' : '') . ' IN(' . join(',', $uLots) . ')';
                }
            }
        }

        $searchStr = trim(preg_replace('/\s{2,}/', ' ', str_replace(self::$removeSymbols, ' ', $prm['svalue'])));
        if(mb_strlen($searchStr) > 2){
            if(isset($prm['bio']) && $prm['bio']){
                (new searchQuery())->fix($searchStr, $prm['search_mode']);
            }
            $tmp = [
                'src' => explode(' ', $searchStr),
                'on' => [],
                'off' => []
            ];
            foreach ($tmp['src'] as $item){
                if(in_array(mb_substr($item, 0, 1), ['-', '!'])){
                    $tmp['off'][] = mb_substr($item, 1);
                } else {
                    $tmp['on'][] = $item;
                }
            }
            switch ($prm['search_mode']){
                case 'all':
                    if(preg_match('/^(\d+:){1,3}$/', $searchStr)){
                        $and[] = '(MATCH(\'(@cadastr_num ^' .  $searchStr . ')\'))';
                    } else {
                        $tmp['on'] = $tmp['on'] ? join(' ', $tmp['on']) : '';
                        $tmp['off'] = $tmp['off'] ? (' !(' . join(' | ', $tmp['off']) . ')') : '';
                        $and[] = '(MATCH(\'(@content "' .  $tmp['on'] . '"~100 ' . $tmp['off'] . ')\'))';
                    }
                    break;
                case 'any':
                    $tmp['on'] = $tmp['on'] ? ('(' . join(' | ', $tmp['on']) . ')') : '';
                    $tmp['off'] = $tmp['off'] ? (' !(' . join(' | ', $tmp['off']) . ')') : '';
                    $and[] = '(MATCH(\'(@content ' . $tmp['on'] . $tmp['off'] . ')\'))';
                    break;
                case 'target':
                    $tmp['on'] = $tmp['on'] ? join(' ', $tmp['on']) : '';
                    $tmp['off'] = $tmp['off'] ? (' !(' . join(' | ', $tmp['off']) . ')') : '';
                    $and[] = '(MATCH(\'(@content "' .  $tmp['on'] . '" ' . $tmp['off'] . ')\'))';
                    break;
            }
        }

        if((int)$prm['hasPhoto']){ //Наличие фото
            $and[] = 'havefoto > 0';
        }

        if($categories = $this->checkData('categories', $prm['categories'])){ //Фильтрация по категориям
            $and[] = 'cat_id IN (' . implode(', ', $categories) . ')';
        }

        if($types = $this->checkData('type', $prm['types'])){ //Фильтрация по типам
            $and[] = 'type IN (' . implode(', ', $types) . ')';
        }

        if($src = $this->checkData('src', $prm['src'])){ //Фильтрация по источникам
            $and[] = 'src IN (\'' . implode('\', \'', $src) . '\')';
        }

        if($places = $this->checkData('place', $prm['places'])){ // Фильтрация по регионам
            $and[] = 'place IN (' . implode(', ', $places) . ')';
        }

        if($platforms = $this->checkData('platform', $prm['platforms'])){ // Фильтрация по платформам
            $and[] = 'platform_id IN (' . implode(', ', $platforms) . ') ';
        }

        // Фильтрация по времени
        $dateFilterBy = (isset($prm['date_range']) && ($prm['date_range'] == 'end')) ? 'end_time' : 'start_time';
        if($prm['date_mode'] == 'wait'){
            if($daysInt = $prm['altint']){
                $daysInt = explode('-', $daysInt);
                $daysInt[0] = isset($daysInt[0]) ? abs(intval($daysInt[0])) : 0 ;
                $daysInt[1] = isset($daysInt[1]) ? abs(intval($daysInt[1])) : 0 ;
            } else {
                $daysInt = [0, 0];
            }
            if($daysInt[0]){
                $and[] = $dateFilterBy . ' > ' . ($tm + ($daysInt[0] * 86400));
                $and[] = $dateFilterBy . ' < ' . (($tm + ((($daysInt[1] ? $daysInt[1] : $daysInt[0]) + 1) * 86400)) - 1);
            }
        } else {
            $tmFrom = abs(intval($prm['begin_date']));
            $tmTo = abs(intval($prm['end_date']));
            if($tmTo && $tmFrom && ($tmTo < $tmFrom)){
                $tmTo = 0;
                $tmFrom = 0;
            }
            if($tmTo){
                $tmTo += 86400 - 1;
            }
            if($tmFrom) {
                $and[] = $dateFilterBy . ' >= ' . $tmFrom;
            }
            if($tmTo){
                $and[] = $dateFilterBy . ' <= ' . $tmTo;
            }
        }

        // Фильтрация по статусам
        $statusMap = $prm['status'];
        if(count($statusMap) < 3) {
            $statusClose = implode(', ', [3, 4, 5, 6]);
            $or = [];
            if(in_array(1, $statusMap))
                $or[] = '(start_time > ' . $tm . ' AND NOT IN (status, ' . $statusClose . '))';
            if(in_array(2, $statusMap))
                $or[] = '(NOT IN (status, ' . $statusClose . ') AND (start_time <= ' . $tm . ' AND end_time >= ' . $tm . '))';
            if(in_array(3, $statusMap))
                $or[] = '(IN(status, ' . $statusClose . ') OR end_time < ' . $tm . ')';
            $select[] = 'IF(' . join(' OR ', $or) . ',1,0) AS statusFilter';
            $and[] = 'statusFilter = 1';
        }

        // Фильтрация по ценам
        $priceTypeMap = [1 => 'now_price', 'min_price', 'market_price'];
        $type_price = abs(intval($prm['type_price']));
        $price_search = $priceTypeMap[$type_price];
        $price_start = abs(intval($prm['price_start']));
        $price_end = abs(intval($prm['price_end']));
        if ($price_start && $price_end && ($price_start > $price_end)) {
            $price_start = 0;
            $price_end  = 0;
        }
        if($price_start) {
            $and[] = $price_search . ' >= ' . $price_start;
        }
        if($price_end) {
            $and[] = $price_search . ' <= ' . $price_end;
        }
        if(($price_start || $price_end) && ($type_price == 3)) {
            $and[] = $price_search . ' > 0';
        }

        if($prm['new_lots']) { // Новые лоты (72 часа)
            $and[] = 'loadtime > ' . ($tm - 86400 * (isset($prm['fresh_days']) ? $prm['fresh_days'] : 3));
        }

        $sortC = $prm['sortcolumn'];
        $sortT = $prm['sorttype'];
        if($prm['new_lots']){
            $order = 'loadtime DESC';
        } elseif(!$onlyCount && $sortC && in_array($sortT, [1, 2])){
            if(isset(self::$mapColumnsOrder[$sortC])){
                $order = self::$mapColumnsOrder[$sortC] . ' ' . self::$mapOrderType[$sortT];
                if(isset(self::$mapColumnsSelect[$sortC])){
                    $select[] = self::$mapColumnsSelect[$sortC];
                }
            } else {
                $order = 'loadtime DESC';
            }
        } elseif(!$categories) { // Если категория Все, то сортируем по дате
            $order = 'loadtime DESC';
        } elseif( count($categories) == count(array_intersect($categories, [1, 3, 5, 6, 7]))) { // Если категория Авто, Спецтехника, Недвижимость, Зем. участки, то  сортируем по "Доходность, %" от большого к меньшему
            $select[] = 'IF(platform_manual_price = 1 AND type = 2, 1, 0) AS priceFilter';
            $select[] = 'IF(profit_proc = 0, 1, 0) AS profitFilter';
            $order = 'priceFilter ASC, profitFilter ASC, profit_proc DESC';
        } else { // Сортировка по Падению цены в %
            $select[] = 'IF(platform_manual_price = 1 AND type = 2 AND grafik_state < 1, 1, 0) AS priceFilter';
            $select[] = 'IF(price_diff = 0, 1, 0) AS diffFilter';
            $order = 'priceFilter ASC, diffFilter ASC, price_diff DESC';
        }

        $select = join(',', $select);
        $where = $and ? ('WHERE ' . join(' AND ', $and)) : '';
        $max = $start + $size;
        $sql = 'SELECT ' . $select . ' FROM `main_lots` '
            . $where . ' ORDER BY ' . $order. ' LIMIT ' . $start . ', ' . $size . ' OPTION max_matches = ' . $max;
        $conn = new Connection();
        $conn->silenceConnectionWarning();
        $query = SphinxQL::create($conn);
        $query = $query->query($sql);
        $query = $query->enqueue(SphinxQL::create($conn)->query('SHOW META'))->executeBatch();
        return $onlyCount ? $query[1][1]['Value'] : $query;
    }

    public function getFreshCount($prm, $uid){
        $prm['begin_date'] = strtotime($prm['begin_date']);
        $prm['end_date'] = strtotime($prm['end_date']);
        if(!isset($prm['src'])){
            $prm['src'] = defaultset::get()['src'];
        }
        $prm['src'] = array_keys($prm['src']);
        $prm['status'] = array_keys($prm['status']);
        $prm['types'] = array_keys($prm['types']);
        $prm['places'] = array_keys($prm['places']);
        $prm['platforms'] = array_keys($prm['platforms']);
        $prm['new_lots'] = 1;
        return $this->getLots($prm, $uid, 0, 1, true);
    }

    public function getFreshTop($prm, $uid){
        $prm['begin_date'] = strtotime($prm['begin_date']);
        $prm['end_date'] = strtotime($prm['end_date']);
        if(!isset($prm['src'])){
            $prm['src'] = defaultset::get()['src'];
        }
        $prm['src'] = array_keys($prm['src']);
        $prm['status'] = array_keys($prm['status']);
        $prm['types'] = array_keys($prm['types']);
        $prm['places'] = array_keys($prm['places']);
        $prm['platforms'] = array_keys($prm['platforms']);
        $prm['new_lots'] = 1;
        $prm['fresh_days'] = 1;
        return $this->getLots($prm, $uid, 0, 20);
    }

    public function getQueries($str, $mode = 'any'){
        $res = [];
        $searchStr = trim(preg_replace('/\s{2,}/', ' ', str_replace(self::$removeSymbols, ' ', $str)));
        if(mb_strlen($searchStr) > 2){
            $where = true;
            switch ($mode){
                case 'all':
                    $where = '(MATCH(\'(@content "' .  $searchStr . '"~100 ' . ')\'))';
                    break;
                case 'any':
                    $where = '(MATCH(\'(@content ' . str_replace(' ', ' | ', $searchStr) . ')\'))';
                    break;
                case 'target':
                    $where = '(MATCH(\'(@content "' .  $searchStr . '" )\'))';
                    break;
            }
            $sql = 'SELECT query FROM `main_queries` WHERE ' . $where
                . ' ORDER BY ' . $mode . ' DESC, total DESC, dt DESC LIMIT 0, 10 OPTION max_matches = 10';
            $conn = new Connection();
            $conn->silenceConnectionWarning();
            $query = SphinxQL::create($conn);
            $query = $query->query($sql)->executeBatch();
            foreach ($query[0] as $item){
                $res[] = $item['query'];
            }
        }
        return $res;
    }

    private function checkData($type, $data){
        $def = cache::get('settings_default')[$type];
        $out = [];
        foreach($data as $id) {
            if(isset($def[$id]))
                $out[] = $id;
        }
        return (!$out || (count($out) == count($def))) ? false : $out;
    }
}
