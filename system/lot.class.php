<?php
class lot extends model{

    public static $state = 1;

    public $details = [];
    public $models = [];
    public $pref = '';
    public $searchAddressExtraFields = [];
    public $searchCadastrNumExtraFields = [];
    public $whereAddressFilter;
    public $whereCadastrNumFilter;
    public $whereCadastrNumSet = '`cadastr_num` = \'\'';
    public $whereLotActive;
    public $whereWithAddress = '`address` != \'\'';
    public $whereWithMarketPrice = '`market_price` > 0';
    public $whereWithCadastrPrice = '`cadastr_price` > 0';
    public $whereWithPhoneOrMail = '(`phone` != \'\' or `mail` != \'\')';
    public $whereWithOutAddress = '`address` = \'\'';
    public $whereWithOutMarketPrice = '`market_price` = 0';

    public function checkLots($items){
        $count = $this->where([
            $this->id_field => $items
        ])->count();
        return $count == count($items);
    }

    public function getDetailsAllArray($detail, $uid){
        $mInd = $this->pref . $detail;
        if(!isset($this->models[$mInd])){
            $this->models[$mInd] = new model(null, $this->details[$detail]['tbl']);
        }
        $fieldLot = $this->details[$detail]['field'];
        $fieldUser = $this->details[$detail]['user'];
        return $this->models[$mInd]->select($fieldLot)->where([
            $fieldUser => $uid
        ])->fetchAll(null, $fieldLot);
    }

    public function getDetailsArray($detail, $items, $uid){
        $mInd = $this->pref . $detail;
        if(!isset($this->models[$mInd])){
            $this->models[$mInd] = new model(null, $this->details[$detail]['tbl']);
        }
        $fieldLot = $this->details[$detail]['field'];
        $fieldUser = $this->details[$detail]['user'];
        return $this->models[$mInd]->select($fieldLot)->where([
            $fieldLot => $items,
            $fieldUser => $uid
        ])->fetchAll(null, $fieldLot);
    }

    public function setDetailsArray($detail, $items, $uid, $prm = []){
        $mInd = $this->pref . $detail;
        if(!isset($this->models[$mInd])){
            $this->models[$mInd] = new model(null, $this->details[$detail]['tbl']);
        }
        $fieldLot = $this->details[$detail]['field'];
        $fieldUser = $this->details[$detail]['user'];
        $prm[$fieldUser] = $uid;
        foreach ($items as $item){
            $data = $prm;
            $data[$fieldLot] = $item;
            $this->models[$mInd]->insert($data);
        }
    }

    public function upDetailsArray($detail, $items, $uid, $prm = []){
        $mInd = $this->pref . $detail;
        if(!isset($this->models[$mInd])){
            $this->models[$mInd] = new model(null, $this->details[$detail]['tbl']);
        }
        $fieldLot = $this->details[$detail]['field'];
        $fieldUser = $this->details[$detail]['user'];
        $where[$fieldUser] = $uid;
        foreach ($items as $item){
            $where[$fieldLot] = $item;
            $this->models[$mInd]->updateByField($where, false, $prm);
        }
    }

    public function delDetailsArray($detail, $items, $uid){
        $mInd = $this->pref . $detail;
        if(!isset($this->models[$mInd])){
            $this->models[$mInd] = new model(null, $this->details[$detail]['tbl']);
        }
        $fieldLot = $this->details[$detail]['field'];
        $fieldUser = $this->details[$detail]['user'];
        $this->models[$mInd]->deleteByField([
            $fieldLot => $items,
            $fieldUser => $uid
        ]);
    }

    public function cleaner($tm = 0){
        if(!$tm){
            $min = $this->select('MIN(`' . $this->id_field . '`) \'min\'')->fetch('min');
            $this->clearDetails($min - 1, true);
        } else {
            $items = $this->select($this->id_field)
                ->where(['end_time' => ['end' => $tm - 1]])
                ->order('`' . $this->id_field . '` ASC')
                ->fetchAll(null, $this->id_field);
            foreach ($items as $item){
                $this->clearDetails($item);
                $this->deleteByField($this->id_field, $item);
            }
        }
    }

    public function clearDetails($ind, $range = false){
        if(!$ind) return;
        foreach ($this->details as $key => $prm){
            $this->clearDetail($ind, $range, $key, $prm);
        }
        $this->clearExtra($ind, $range);
    }

    public function clearDetail($ind, $range, $detail, $prm){
        $mInd = $this->pref . $detail;
        if(!isset($this->models[$mInd])){
            $this->models[$mInd] = new model(null, $prm['tbl']);
        }
        $this->models[$mInd]->deleteByField($prm['field'], $range ? ['end' => $ind] : $ind);
    }

    public function clearExtra($ind, $range){

    }

    public function setAddress(){
        require_once(system::realPath('system/parserAddress.class.php'));
        $parser = new parserAddress();
        $step = 1e3;
        $select = array_merge(['id', 'name'], $this->searchAddressExtraFields);
        $where = [
            $this->whereLotActual(),
            $this->whereLotActive,
            $this->whereWithOutAddress
        ];
        if(isset($this->whereAddressFilter)){
            $where[] = $this->whereAddressFilter;
        }
        $whereStr = join(' AND ', $where);
        $count = $this->select($select)->where($whereStr)->count();
        $steps = ceil($count / $step);
        for($i = 0; $i < $steps; $i++){
            $items = $this->where($whereStr)->limit($i * $step, $step)->fetchAll();
            foreach ($items as $item){
                $address = false;
                foreach ($this->searchAddressExtraFields as $field){
                    if($address = $parser->parseCurrent($item[$field])){
                        break;
                    }
                }
                if(!$address){
                    $address = $parser->parseCurrent($item['name']);
                }
                if($address){
                    $this->updateByField('id', $item['id'], [
                        'address' => $address['res'],
                        'address_by' => $address['type']
                    ]);
                }
            }
        }
    }

    public function setCadastrNum(){
        require_once(system::realPath('system/parserCadastrNum.class.php'));
        $parser = new parserCadastrNum();
        $select = array_merge(['id', 'name'], $this->searchCadastrNumExtraFields);
        $where = [
            $this->whereLotActual(),
            $this->whereLotActive,
            $this->whereStateActual($this->whereCadastrNumSet)
        ];
        if(isset($this->whereCadastrNumFilter)){
            $where[] = $this->whereCadastrNumFilter;
        }
        $items = $this->select($select)->where(join(' AND ', $where))->fetchAll();
        foreach ($items as $item){
            $reg = 0;
            foreach ($this->searchCadastrNumExtraFields as $field){
                if(!is_numeric($item[$field])){
                    $res = $parser->parseCurrent($item[$field]);
                    if($reg = $res['reg']) break;
                }
            }
            if(!$reg){
                $res = $parser->parseCurrent($item['name']);
                $reg = $res['reg'];
            }
            $whereUp = [
                'cadastr_num' => $res['num'],
                'state' => self::$state
            ];
            if(isset($item['place']) && $reg && ($reg != $item['place'])){
                if(!isset($this->models['log_update'])){
                    $this->models['log_update'] = new model(null, 'log_update');
                }
                $this->models['log_update']->insert([
                    'lot_id' => $item['id'],
                    'type' => 'changeRegionByCN',
                    'table' => $this->table,
                    'field' => 'place',
                    'val_from' => $item['place'],
                    'val_to' => $reg,
                    'dt' => time()
                ]);
                $whereUp['place'] = $reg;
            }
            $this->updateByField('id', $item['id'], $whereUp);
        }
    }

    public function getStats(){
        $res = [];
        $where = [
            $this->whereLotActual(),
            $this->whereLotActive
        ];
        $res['lotAct'] = $this->where(join(' AND ', $where))->count();
        $where[] = $this->whereWithMarketPrice;
        $res['details']['mPrice'] = $this->where(join(' AND ', $where))->count();
        $where[2] = $this->whereWithOutMarketPrice;
        $where[] = $this->whereWithCadastrPrice;
        $res['details']['mPrice'] += $this->where(join(' AND ', $where))->count();
        array_splice($where, 2, 1);
        $res['details']['cPrice'] = $this->where(join(' AND ', $where))->count();
        $where[2] = $this->whereWithAddress;
        $res['details']['withAddress'] = $this->where(join(' AND ', $where))->count();
        $where[2] = $this->whereWithPhoneOrMail;
        $dbQuery = new dbQuery();
        $sql = 'SELECT COUNT(*) `count` FROM `' . $this->table . '` LEFT JOIN `ds_maindata_organizers` ON `' . $this->table . '`.`organizer`=`ds_maindata_organizers`.`id` WHERE ' . join(' AND ', $where);
        $dbQuery->exec($sql);
        $res['details']['withPhoneOrMail'] = (int)$dbQuery->fetch('count');
        $res['details'] = array_merge($res['details'], $this->getStatsExtra());
        return $res;
    }

    public function getStatsExtra(){
        return [];
    }

    public function getNews(){
        $res = [];
        $ds_platform_news = new view(null, 'ds_platform_news');
        $items = $ds_platform_news->where(['type' => [2]])->order('`time` DESC')->limit(10)->fetchAll();
        foreach ($items as $data){
            $tmp = [];
            $tmp['id'] = $data['id'];
            $tmp['time'] = ds_time($data['time'], '%H:%M');
            $tmp['data'] = ds_time($data['time'], '%d %B2 %Y');
            text::add_cache($data['cache']);
            $tmp['text'] = text::out($data['text'], 0);
            $res[] = $tmp;
        }
        return $res;
    }

    public function updateProfit(){
        $tm = time();
        $items = $this
            ->select(['id', 'price', 'market_price', 'cadastr_price'])
            ->where('(`price` > 0) AND (`market_price` > 0 OR `cadastr_price` > 0) AND (`mprice_update` >= `check_stamp`)')
            ->fetchAll();
        foreach ($items as $item){
            $mPrice = ($item['market_price'] > 0) ? $item['market_price'] : $item['cadastr_price'];
            $profit_rub = $mPrice - $item['price'];
            $this->updateByField('id' , $item['id'], [
                'profit_rub' => $profit_rub,
                'profit_proc' => ceil($profit_rub / $item['price'] * 100),
                'check_stamp' => $tm
            ]);
        }
    }

    public function whereLotActual(){
        return '`end_time` >=  ' . time();
    }

    public function whereStateActual($r){
        return '((' . $r . ') OR (`state` <  ' . self::$state . '))';
    }
}
