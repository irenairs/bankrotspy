<?php
class dbQuery{

    public $db = null;
    public $result = false;
    public $exec = false;
    protected $rows = -1;

    public function __construct($db = null, $prm = null){
        $this->db = $db ? $db : ($prm ? dbConnector::getConnection($prm) : dbConnector::getConnection());
    }

    public function getSql(){
        return false;
    }

    public function escape($string){
        return $this->db->escape($string);
    }

    public function execMultiQuery($queries){
        if(!is_array($queries)){
            $queries = array_filter(explode(';', $queries));
        }
        if ($queries) {
            foreach ($queries as $q) {
                $q = trim($q);
                if (!$q) continue;
                if (!$this->exec($q)) return false;
            }
        }
        return true;
    }

    public function exec($sql = '') {
        $this->exec = true;
        $this->rows = -1;

        if (!$sql) {
            $sql = $this->getSql();
        }
        if (!$sql) {
            throw new Exception('Отсутствует SQL запрос!');
        }
        if (!$this->db) {
            throw new Exception('Ошибка соединения с БД!');
        }
        if ($this->result = $this->db->query($sql)) {
            return true;
        } else {
            throw new Exception('Ошибка при запросе к БД!<br /><br />SQL: ' . htmlspecialchars($sql) . '<br /><br />Ошибка: ' . $this->db->error());
            return false;
        }
    }

    public function fetch($field = ''){
        if (!$this->exec) {
            $this->exec();
        }
        if ($this->result) {
            if ($ar = $this->db->fetch($this->result))
                return $field ? $ar[$field] : $ar;
        }
        return false;
    }

    public function fetchAll($key_field = '', $value_field = '', $array = false){
        if (!$this->exec) {
            $this->exec();
        }
        if ($this->result) {
            $result = array();
            if (!$key_field && !$value_field) {
                while ($ar = $this->db->fetch($this->result)){
                    $result[] = $ar;
                }
            } elseif ($key_field && $value_field && $array) {
                while ($ar = $this->db->fetch($this->result)) {
                    $result[$ar[$key_field]][] = $ar[$value_field];
                }
            } elseif ($key_field && $value_field && !$array) {
                while ($ar = $this->db->fetch($this->result)) {
                    $result[$ar[$key_field]] = $ar[$value_field];
                }
            } elseif ($key_field && !$value_field && $array) {
                while ($ar = $this->db->fetch($this->result)) {
                    $result[$ar[$key_field]][] = $ar;
                }
            } elseif ($key_field && !$value_field && !$array) {
                while ($ar = $this->db->fetch($this->result)) {
                    $result[$ar[$key_field]] = $ar;
                }
            } elseif (!$key_field && $value_field) {
                while ($ar = $this->db->fetch($this->result)) {
                    $result[] = $ar[$value_field];
                }
            }
            return $result;
        } else {
            return false;
        }
    }

    public function rows() {
        if (!$this->exec) {
            $this->exec();
        }

        if ($this->rows < 0) {
            $this->rows = $this->db->rows($this->result);
        }
        return $this->rows;
    }

    public function insertId() {
        return $this->db->insertId();
    }

    public function affectedRows() {
        return $this->db->affectedRows();
    }

    public function createTable($name, $fields = [], $primary = '', $keys = [], $settings = '') {
        $sql = 'CREATE TABLE IF NOT EXISTS `' . $name . '` ';
        $tmp = [];
        foreach ($fields as $name => $value){
            $tmp[] = '`' . $name . '` ' . $value;
        }
        if($primary){
            $tmp[] = 'PRIMARY KEY (`' . $primary . '`)';
        }
        foreach ($keys as $name => $value){
            $tmp[] = 'KEY `' . $name . '` (`' . $value . '`)';
        }
        if(!empty($tmp)){
            $sql .= '(' . implode(', ', $tmp) . ') ';
        }
        $sql .= $settings;
        $this->exec($sql);
    }
}
