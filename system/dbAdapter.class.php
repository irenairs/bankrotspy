<?php
class dbAdapter{

    private static $DB_HOST;
    private static  $DB_NAME;
    private static $DB_USER;
    private static $DB_PASS;

    private $CID;
    private $DB_ERR = true;

    public function __construct($config = null){
        $db = $config ? $config : system::$config['db']['config'];
        self::$DB_HOST = $db['host'];
        self::$DB_NAME = $db['name'];
        self::$DB_USER = $db['user'];
        self::$DB_PASS = $db['pass'];
        $this->CID = 0;
    }

    public function connect($names = 'utf8'){
        if ($this->CID) {
            $this->close();
        }

        $this->CID = mysqli_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, self::$DB_NAME);

        if (!($this->CID) && $this->DB_ERR) {
            if ($this->DB_ERR) {
                throw new Exception('Ошибка соединения с базой данных!!!');
            }
            return false;
        }

        if (!($this->query('SET NAMES ' . $names))) {
            if ($this->DB_ERR) {
                throw new Exception('Ошибка установки NAMES для базы данных!!!');
            }
            return false;
        }

        if (!($this->query('SET `time_zone`=\'+00:00\''))) {
            if ($this->DB_ERR) {
                throw new Exception('Ошибка установки time_zone для базы данных!!!');
            }
            return false;
        }

        return true;
    }

    public function close(){
        if (!mysqli_close($this->CID)) {
            throw new Exception('Ошибка при попытке закрыть соединение с базой данных!!!');
        }
        $this->CID = null;
    }

    public function error(){
        return mysqli_error($this->CID);
    }

    public function query($sql){
        return @mysqli_query($this->CID, $sql);
    }

    public function free($result){
        mysqli_free_result($result);
    }

    public function rows($result){
        return mysqli_num_rows($result);
    }

    public function fetch($result, $num = 0){
        if ($num) {
            mysqli_data_seek($result, $num);
        }
        return mysqli_fetch_assoc($result);
    }

    public function escape($string){
        if (get_magic_quotes_gpc()) {
            $string = stripcslashes($string);
        }
        return mysqli_real_escape_string($this->CID, trim($string));
    }

    public function adaptValue($value, $type, $is_null = false){
        if($is_null && $value === NULL) return 'NULL';
        switch ($type){
            case 'bool':
            case 'bit':
            case 'tinyint':
            case 'smallint':
            case 'mediumint':
            case 'int':
                return (int)$value;
            case 'double':
            case 'float':
                return str_replace(',', '.', (double)$value);
            case 'date':
                if(!$value) return $is_null ? 'NULL' : "'0000-00-00'";
            case 'datetime':
                if(!$value) return $is_null ? 'NULL' : "'0000-00-00 00:00:00'";
            case 'varchar':
            case 'text':
            default:
                return "'" . $this->escape($value) . "'";
        }
    }

    public function insertId(){
        return mysqli_insert_id($this->CID);
    }

    public function affectedRows(){
        return mysqli_affected_rows($this->CID);
    }

    public function scheme($table){
        if ($cache = cache::get(self::$DB_NAME . '_dbScheme', 3600)){
            if (isset($cache[$table])){
                return $cache[$table];
            }
        }

        $res = mysqli_query($this->CID, "DESCRIBE " . $table);
        if (!$res) {
            print_r($this->error());
            throw new Exception($this->error());
        }
        $result = array();
        while ($row = mysqli_fetch_assoc($res)){
            $field = array();
            $field['attributes'] = FALSE;
            $i = strpos($row['Type'], '(');
            if ($i === FALSE){
                $field['type'] = $row['Type'];
                $field['length'] = null;
            } else {
                $field['type'] = substr($row['Type'], 0, $i);
                $j = strpos($row['Type'], ')');
                if($j === (strlen($row['Type'])-1)) {
                    $field['length'] = substr($row['Type'], $i + 1, -1);
                } else {
                    $field['length'] = substr($row['Type'], $i + 1, $j-$i-1);
                    $field['attributes'] = strtolower(substr($row['Type'], $j + 2));
                }
            }
            $field['null'] = $row['Null'] == 'YES' ? TRUE : FALSE;
            $field['default'] = $row['Default'] === 'NULL' ? ($field['null'] ? null : $this->adaptValue('', $field['type'])) : $row['Default'];
            $field['extra'] = $row['Extra'];
            $field['key'] =  strtolower($row['Key']);
            $result[$row['Field']] = $field;
        }

        $res = mysqli_query($this->CID, 'SELECT * FROM `information_schema`.`KEY_COLUMN_USAGE` WHERE `TABLE_NAME` = "' . $table . '" AND `TABLE_SCHEMA` = "' . self::$DB_NAME . '" AND `REFERENCED_TABLE_NAME` IS NOT NULL');
        while ($row = mysqli_fetch_assoc($res)){
            $col = $row['COLUMN_NAME'];
            if(isset($result[$col]) && $row['CONSTRAINT_NAME'] !== 'PRIMARY')
                $result[$col]['ref'] = array(
                    'db' => $row['REFERENCED_TABLE_SCHEMA'],
                    'table' => $row['REFERENCED_TABLE_NAME'],
                    'column' => $row['REFERENCED_COLUMN_NAME'],
                );
        }

        $cache = cache::get(self::$DB_NAME . '_dbScheme', 3600);
        $cache[$table] = $result;
        cache::create(self::$DB_NAME . '_dbScheme', $cache);

        return $result;
    }
}
