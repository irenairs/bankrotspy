<?php
class convertCn extends convert {

    public $table = 'konfiscat2';

    public $mode = 'cn';

    public $logTablesExtra = [
        'log' => 'log_cn'
    ];

    public $srcTablesExtra = [
        'index' => 'konfiscat2_index'
    ];

    public $targetTablesExtra = [
        'details' => 'ds_maindata_cn'
    ];

    public $directExtra = ['last_update'];

    public $mapType = [
        'открытый аукцион' => 1
    ];
}
