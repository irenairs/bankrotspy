<?php
class ad extends model{

    public $table = 'ad_view';
    public static $companies;
    public static $places;

    public function getAdAll(){
        $res = [];
        $items = $this->order('`id` DESC')->fetchAll();
        foreach ($items as $item){
            $res[$item['type']][] = $item;
        }
        return $res;
    }

    public function getAdLive(){
        $res = [];
        $tm = time();
        $items = $this->where([
            'tm_from' => ['end' => $tm],
            'tm_to' => ['begin' => $tm],
            'active' => 1
        ])->order('`priority` DESC')->fetchAll();
        foreach ($items as $item){
            $res[$item['type']][] = $item;
        }
        return $res;
    }

    public function getAdTarget(){
        $res = [];
        $tm = time();
        $items = $this->where([
            'module' => ['any', core::$module],
            'action' => ['any', core::$action],
            'tm_from' => ['end' => $tm],
            'tm_to' => ['begin' => $tm],
            'active' => 1
        ])->order('`priority` DESC')->fetchAll();
        foreach ($items as $item){
            if(($item['type'] == 'top') && !isset($res['top'])){
                $res['top'] = $item;
            } elseif(($item['type'] == 'content') && !isset($res[$item['pid']])){
                $res[$item['pid']] = $item;
            }
        }
        return $res;
    }
}
