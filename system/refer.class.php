<?php
class refer extends model{

    public $table = 'refer';
    public $id_field = 'rf_id';

    private static $ds_users;
    private static $cookie_name = 'bs_refer';
    private static $cookie_live = 365;

    private static $d = 5;
    private static $pr = .25;

    function __construct() {
        parent::__construct();
        self::$ds_users = new model(null, 'ds_users');
    }

    public function enter(){
        if(!isset($_GET['key']) || isset($_COOKIE[self::$cookie_name])) return;
        $key = htmlspecialchars($_GET['key']);
        if($this->checkKey($key) != 1) return;
        $tm = time();
        if($this->insert([
            'rf_key' => $key,
            'rf_oper' => 1,
            'rf_dt' => $tm
        ])){
            setcookie(self::$cookie_name, join('|', [$key, $this->insertId()]), time() + self::$cookie_live * 86400);
        }
    }

    public function reg($uid){
        if(!(int)$uid) return;
        $upDate = [
            'refer_key' => $this->genKey($uid * 10)
        ];
        if(isset($_COOKIE[self::$cookie_name])){
            $cookie = explode('|', $_COOKIE[self::$cookie_name]);
            if($this->checkKey($cookie[0]) == 1){
                $this->updateById($cookie[1], [
                    'rf_uid' => $uid
                ]);
                $this->insert([
                    'rf_key' => $cookie[0],
                    'rf_uid' => $uid,
                    'rf_oper' => 2,
                    'rf_dt' => time()
                ]);
                $upDate['refer_by'] = $cookie[0];
            }
            setcookie(self::$cookie_name, '', 0, '/');
        }
        self::$ds_users->updateByField('id', $uid, $upDate);
    }

    public function pay($uid, $sum){
        if(!(int)$uid || !(int)$sum) return;
        if($refer_by = self::$ds_users->where('id', $uid)->limit(1)->fetch('refer_by')){
            $this->insert([
                'rf_key' => $refer_by,
                'rf_uid' => $uid,
                'rf_oper' => 3,
                'rf_sum' => $sum,
                'rf_dt' => time()
            ]);
        }
    }

    public function payOut($key, $sum, $dt){
        if(!(int)$sum) return;
        $this->insert([
            'rf_key' => $key,
            'rf_uid' => 0,
            'rf_oper' => 4,
            'rf_sum' => $sum,
            'rf_dt' => $dt
        ]);
    }

    public function getPayments($key){
        $dbQuery = new dbQuery();
        $sql = 'SELECT `refer`.*, `login` FROM refer 
          LEFT JOIN ds_users ON `refer`.`rf_uid` = `ds_users`.`id` 
          WHERE `rf_oper` IN (3, 4) AND `rf_key` = "' . $key . '"
          ORDER BY `rf_dt` DESC';
        $dbQuery->exec($sql);
        return $dbQuery->fetchAll();
    }

    public function getDetails($key){
        $select = [
            'sum(case WHEN `rf_oper`="1" then 1 else 0 end) enter',
            'sum(case WHEN `rf_oper`="2" then 1 else 0 end) reg',
            'count(DISTINCT case WHEN `rf_oper`="3" then rf_uid end) pay'
        ];
        $res = $this->select(join(', ', $select))->where('rf_key', $key)->fetch();
        $res['link'] = 'https://' . system::$config['mode']['host'] . '/torgi?key=' . $key;
        $summary = $this->getSummary($key);
        $res['summary'] = isset($summary['data'][$key]) ? $summary['data'][$key] : [];
        $res['curWait'] = $summary['wait']['curWait'];
        $res['nextWait'] = $summary['wait']['nextWait'];
        return $res;
    }

    private function checkKey($key){
        return self::$ds_users->where('refer_key', $key)->count();
    }

    public function genKey($counter){
        $key = system::hash($counter);
        while($this->checkKey($key)){
            $key = system::hash(++$counter);
        }
        return $key;
    }

    public function month($dt){
        return date('y', $dt) * 12 + date('n', $dt);
    }

    public function wait(){
        $curD = date('j');
        return [
            'curWait' => date('j') > self::$d ? false : (self::$d . date('.m.Y')),
            'nextWait' => self::$d . date('.m.Y', strtotime('+' . (date('t') - $curD + 5) . ' day'))
        ];
    }

    public function getSummary($key = false){
        $dbQuery = new dbQuery();
        $sql = 'SELECT `refer`.*, `login` FROM refer 
          LEFT JOIN ds_users ON `refer`.`rf_key` = `ds_users`.`refer_key` 
          WHERE `rf_oper` IN (3, 4)';
        if($key) {
            $sql .= ' AND `rf_key` = "' . $key . '"';
        }
        $dbQuery->exec($sql);
        $data = $dbQuery->fetchAll();

        $out = [];
        $curMonth = $this->month(time());
        foreach ($data as $item){
            $key = $item['rf_key'];
            if(!isset($out[$key])){
                $out[$key] = [
                    'login' => $item['login'],
                    'paidOut' => 0,
                    'toPayPrev' => 0,
                    'toPayCur' => 0,
                    'toPayNext' => 0
                ];
            }
            switch ($item['rf_oper']){
                case '3':
                    $itemMonth = $this->month($item['rf_dt']);
                    $k = self::$pr;
                    if($itemMonth == $curMonth){
                        $paidType = 'toPayNext';
                    } elseif($itemMonth == $curMonth - 1){
                        $paidType = 'toPayCur';
                    } else {
                        $paidType = 'toPayPrev';
                    }
                    break;
                case '4':
                    $k = 1;
                    $paidType = 'paidOut';
                    break;
            }
            $out[$key][$paidType] += $item['rf_sum'] * $k;
        }

        $wait = $this->wait();
        foreach ($out as $key => $details){
            if(!$wait['curWait']){
                $details['toPayPrev'] += $details['toPayCur'];
            }
            if($details['paidOut'] > 0){
                $rest = $details['paidOut'];
                if($rest > $details['toPayPrev']){
                    $rest -= $details['toPayPrev'];
                    $out[$key]['toPayPrev'] = 0;
                    if($wait['curWait']){
                        if($rest > $details['toPayCur']) {
                            $rest -= $details['toPayCur'];
                            $out[$key]['toPayCur'] = 0;
                            $out[$key]['toPayNext'] -= $rest;
                        } else {
                            $out[$key]['toPayCur'] -= $rest;
                        }
                    } else {
                        $out[$key]['toPayNext'] -= $rest;
                    }
                } else {
                    $out[$key]['toPayPrev'] = $details['toPayPrev'] - $rest;
                }
            } else {
                $out[$key]['toPayPrev'] = $details['toPayPrev'];
            }
        }
        return [
            'wait' => $wait,
            'data' => $out
        ];
    }

}