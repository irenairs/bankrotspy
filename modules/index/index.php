<?php
defined('DS_ENGINE') or die('web_demon laughs');

if (!core::$user_id) {
    engine_head_alt();


    // ДЛЯ ПЕСОЧНИЦЫ
    // $res = core::$db->query('SELECT * FROM `ds_tariffs` WHERE id IN(4,13,12,8,19, 1,2,10,3,11, 14,15,16,17,18);');

    $res = core::$db->query('SELECT * FROM `ds_tariffs` WHERE page_main = "1"');
    $rmenu = array();

    while ($data = $res->fetch_array()) {
        $id = intval($data['id']);
        $rmenu[$id]['id'] = $data['id'];
        $rmenu[$id]['name'] =  $data['name'];
        $rmenu[$id]['subtext'] = text::out($data['descr'], 0, $data['id']);
        // $rmenu[$id]['spec_descr'] = $data['spec_descr'];
        $rmenu[$id]['spec_descr'] =  text::out($data['spec_descr'], 0, $data['id']);

        $rmenu[$id]['longtime'] = $data['longtime'];
        $rmenu[$id]['price_source'] = $data['price'];

        $rmenu[$id]['price'] = $data['price'] * 1.021;

        //$rmenu[$id]['order'] = core::$set['market_prefix'].';'.core::$user_id.';'.$data['id'].';'.$ordertime;

        $rmenu[$id]['rights'] = $data['rights'];

    }

    // ДЛЯ ПЕСОЧНИЦЫ
    // $rmenu = array(

    //     $rmenu[1],
    //     $rmenu[2],
    //     $rmenu[10],
    //     $rmenu[3],
    //     $rmenu[11],

    //     $rmenu[4],
    //     $rmenu[13],
    //     $rmenu[12],
    //     $rmenu[8],
    //     $rmenu[19],

    //     $rmenu[14],
    //     $rmenu[15],
    //     $rmenu[16],
    //     $rmenu[17],
    //     $rmenu[18],
    // );

    // ДЛЯ ОСНОВЫ
   /* $rmenu = array(
    // простой доступ
        $rmenu[1],
        $rmenu[2],
        $rmenu[16],
        $rmenu[3],
        $rmenu[6],
    // вип
        $rmenu[4],
        $rmenu[17],
        $rmenu[18],
        $rmenu[8],
        //$rmenu[13],
    // коммерсант
        $rmenu[19],
        $rmenu[15],
        $rmenu[10],
        $rmenu[11],
        //$rmenu[12],

        $rmenu[20],
        $rmenu[21],
        $rmenu[22],
        $rmenu[23],
        $rmenu[24],
    );*/

    core::$db->query("INSERT INTO ds_counts(page, counttime, day, monthyear) VALUES (24,".time().",".date('j').", ".date('n').date('y')."24)");


    // костыль вывода статьи на лавной
    $articleData = core::$db->query('SELECT `ds_article`.*, `ds_users`.`lastvisit`, `ds_users`.`avtime`, `ds_users`.`sex`, `ds_users`.`rights` FROM `ds_article` LEFT JOIN `ds_users` ON `ds_article`.`userid` = `ds_users`.`id` WHERE `ds_article`.`id` = "8" ;');
    $articleData = $articleData->fetch_assoc();

    $article['name'] = htmlentities($articleData['name'], ENT_QUOTES, 'UTF-8');
    $article['text'] = text::out($articleData['text'], 0, $articleData['id']);
    $article['text'] = fload::replace_files($article['text'], $articleData['id'], core::$module);
    $article['time'] = ds_time($articleData['time']);

    temp::HTMassign('article', $article);
    temp::HTMassign('rmenu',$rmenu);

    temp::display('index.index');
    exit();
}

header('Location: ' . core::$home . '/torgi');
