<?php
/**
 * bankrot-spy.sitecreate54.ru
 * Author: Vladimir S. <guyasyou@gmail.com>
 * www.SiteCreate54.ru
 * © 2016
 */
defined('DS_ENGINE') or die('web_demon laughs');

// Если это не админ, выходим из скрипта
if (core::$rights < 100) {
    header( "refresh:5;url=".core::$home );
    die('Только администраторы могут запускать индексацию! Вы будете направлены на главную страницу через несколько секунд');
}

//что индексируем
$index_type = GET('index_type');
if (!$index_type) {
    die('Ошибка! Не указан тип индекса');
}
//отступ выборки
$offset = abs(intval(GET('offset'))); //Отступ не указан, начинаем сначала



$index = data_index::get($index_type);

//запрашиваем количество лотов
$lots_number = $index->getLotsCount();
if ($lots_number == 0) {
    die('Ошибка! Нет лотов для индексирования');
}

$limit = $index->rowsPerStep();
if ($index) {
    //Индексируем пул лотов
    $index->fullReindex($offset, $limit);

} else {
    die('Ошибка! Класс индекса не загружен');
}

if ($offset < $lots_number) {
    //Сдвигаем отступ
    $offset = $offset + $limit;
    if ($offset > $lots_number) {
        $offset = $lots_number;
    }
    //Редиректим на себя с новым отступом
    header( "refresh:0.5;url=/full_reindex_lots/".$index_type."/".$offset."/".$lots_number."" );
} else {
    $finished = true;
}

?>
<h1 style="margin-top: 80px; text-align: center;">Полная индексация лотов</h1>
<p style="text-align: center;">Индексация типа "<?=$index_type?>"</p>
<div style="width: 400px; margin: 0 auto; padding: 20px; background-color: #efefef; border: 1px solid #d6d6d6;">
    <h2 style="text-align: center;">Обработано <?=round($offset*100/$lots_number, 2)?>%</h2>
    <p style="text-align: center;"><?=$offset?> лотов из <?=$lots_number?></p>
    <?php
    if ($finished) {?>
        <h2 style="color: #06ba00; text-align: center">Индексация завершена!</h2>
        <p style="text-align: center;"><a href="/control/keys">Назад в админку</a></p>
    <?php
    }
    ?>
</div>