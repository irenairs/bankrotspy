<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    if(!core::$user_id){
        $error = [];
        if(GET('act')){
            $ds_users = new model(null, 'ds_users');
            $ds_guests = new model(null, 'ds_guests');
            $host = system::$config['mode']['host'];
            if(!isset($_POST['type'])) {
                $_POST['type'] = 'email';
            }
            if(($_POST['type'] == 'google') && isset($_POST['user'])) {
                $auth = $_POST['user'];
                $sid = 'g' . $auth['id'];
                if($user = $ds_users->where(['sid' => $sid, 'type' => 'google'])->fetch()) {
                    $_SESSION['user_id'] = $user['id'];
                    $_SESSION['user_pass'] = $user['password'];
                    setcookie('cuser_id', base64_encode($user['id']), time() + 86400 * 365, '/', $host);
                    setcookie('cuser_pass', md5(md5($sid)), time() + 86400 * 365, '/', $host);
                    $ds_guests->deleteByField('primid', md5(core::$ipl . core::$ua));
                    $res = ['url' => core::$home];
                } else {
                    $res = [];
                }
                header('Content-type: application/json; charset: utf-8', true);
                echo json_encode($res);
                exit();
            }elseif(($_POST['type'] == 'facebook') && isset($_POST['authResponse'])) {
                $auth = $_POST['authResponse'];
                $sid = 'fb' . $auth['userID'];
                if($user = $ds_users->where(['sid' => $sid, 'type' => 'facebook'])->fetch()) {
                    $_SESSION['user_id'] = $user['id'];
                    $_SESSION['user_pass'] = $user['password'];
                    setcookie('cuser_id', base64_encode($user['id']), time() + 86400 * 365, '/', $host);
                    setcookie('cuser_pass', md5(md5($sid)), time() + 86400 * 365, '/', $host);
                    $ds_guests->deleteByField('primid', md5(core::$ipl . core::$ua));
                    $res = ['url' => core::$home];
                } else {
                    $res = [];
                }
                header('Content-type: application/json; charset: utf-8', true);
                echo json_encode($res);
                exit();
            }elseif(($_POST['type'] == 'vk') && isset($_POST['session'])) {
                $auth = $_POST['session']['user'];
                $sid = 'vk' . $auth['id'];
                if($user = $ds_users->where(['sid' => $sid, 'type' => 'vk'])->fetch()) {
                    $_SESSION['user_id'] = $user['id'];
                    $_SESSION['user_pass'] = $user['password'];
                    setcookie('cuser_id', base64_encode($user['id']), time() + 86400 * 365, '/', $host);
                    setcookie('cuser_pass', md5(md5($sid)), time() + 86400 * 365, '/', $host);
                    $ds_guests->deleteByField('primid', md5(core::$ipl . core::$ua));
                    $res = ['url' => core::$home];
                } else {
                    $res = [];
                }
                header('Content-type: application/json; charset: utf-8', true);
                echo json_encode($res);
                exit();
            }

            $login = htmlspecialchars(POST('nick'));
            if(!$login)
                $error[] = lang('empty_nick');
            elseif(!user::len_nick($login))
                $error[] = lang('nick_wrong_len');
            elseif(!user::valid_nick($login))
                $error[] = lang('nick_wrong');

            $pass = htmlspecialchars(POST('pass'));
            if(!$pass)
                $error[] = lang('empty_pass');
            elseif(mb_strlen($pass) < 4 or mb_strlen($pass) > 15)
                $error[] = lang('pass_wrong_len');

            if(!$error){
                if($user = $ds_users->where([
                    'login' => $login,
                    'password' => md5(md5($pass)),
                    'type' => 'email'
                ])->fetch()) {
                    $_SESSION['user_id'] = $user['id'];
                    $_SESSION['user_pass'] = md5(md5($pass));
                    $_SESSION['lefts'] = $user['rights'];
                    if(POST('mem')){
                        setcookie('cuser_id', base64_encode($user['id']), time() + 3600 * 24 * 365, '/', $host);
                        setcookie('cuser_pass', md5($pass), time() + 3600 * 24 * 365, '/', $host);
                    }
                    $ds_guests->deleteByField('primid', md5(core::$ipl . core::$ua));
                    header('Location: ' . core::$home);
                    exit();
                } else {
                    $error[] = lang('auth_err');
                }
                //Получение настроек для бана при неверной авторизации
                $attempts = isset($_COOKIE['cuser_attempts']) ? (int)$_COOKIE['cuser_attempts'] : 0;
                $attempts++;
                if($attempts >= (int)core::$set['number_attempts'] && !isset($_COOKIE['cuser_autorisete'])){
                    setcookie('cuser_autorisete', time() + 3600 * (int)core::$set['bans_period'], time() + 3600 * (int)core::$set['bans_period']);
                } else if(!isset($_COOKIE['cuser_autorisete'])) {
                    setcookie('cuser_attempts', $attempts, time() + 60 * (int)core::$set['time_attempts']);
                }
            }
        }

        engine_head(lang('autorization'));
        if($error)
            temp::HTMassign('error', $error);
        if(isset($login))
            temp::assign('nick', $login);
        temp::display('login.index');
        engine_fin();
    } else {
        header('Location:'.core::$home);
        exit();
    }