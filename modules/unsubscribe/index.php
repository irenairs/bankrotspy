<?php
    defined('DS_ENGINE') or die('access denied');

    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $userID = 0;
        $ds_users = new model(null, 'ds_users');
        $maxID = $ds_users->select('MAX(`id`) as m')->fetch('m');
        for ($i = 1; $i <= $maxID; $i++) {
            if ($id === system::hash($i)) {
                $userID = $i;
                break;
            }
        }
        if($userID){
            if(!empty($_POST)){
                $ds_users->updateByField('id', $userID, ['subscribe' => 0]);
                func::notify('Рассылка', 'Вы отписались от рассылки', core::$home);
            } else {
                engine_head('Отписка от рассылки');
                temp::assign('id', $id);
                temp::assign('email', $ds_users->select('mail')->where('id', $userID)->limit(1)->fetch('mail'));
                temp::display('mail/unsubscribe');
                engine_fin();
            }

        } else {
            func::notify('Рассылка', 'Произошла ошибка', core::$home);
        }
    } else {
        func::notify('Рассылка', 'Произошла ошибка', core::$home);
    }
