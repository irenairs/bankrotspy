<?php
    if ($_SERVER['REMOTE_ADDR'] != system::$config['mode']['ip']) {
        die('Access denied!');
    }

    if(isset($_GET['email'])){
        $ds_users = new model(null, 'ds_users');
        $ds_users->updateByField('mail', $_GET['email'], ['subscribe' => 0]);
    }
