<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    if (!($id = abs(intval(GET('id'))))) denied();
    $uid = core::$user_id;

    $dbQuery = new dbQuery();
    $sql = 'SELECT
       `ds_maindata`.*,
       `ds_maindata_regions`.`name` AS `regionname`,
       `ds_maindata_type`.`type_name`,
       `ds_maindata_status`.`status_name`,
       `ds_maindata_platforms`.`platform_url`,
       `ds_maindata_category`.`name` AS `catname`,
       `ds_maindata_hint`.`link` AS `link`,
       `ds_maindata_hint`.`price` AS `price_average`,
       `ds_maindata_hint`.`text` AS `hint`,
       `lot_notes`.`text` AS `note`
        FROM `ds_maindata`
        LEFT JOIN `lot_notes` ON `lot_notes`.`lot_id` = `ds_maindata`.`id` AND `lot_notes`.`user_id` = "' . $uid . '"
        LEFT JOIN `ds_maindata_regions` ON `ds_maindata`.`place` = `ds_maindata_regions`.`number`
        LEFT JOIN `ds_maindata_type` ON `ds_maindata`.`type` = `ds_maindata_type`.`id`
        LEFT JOIN `ds_maindata_status` ON `ds_maindata`.`status` = `ds_maindata_status`.`id`
        LEFT JOIN `ds_maindata_platforms` ON `ds_maindata`.`platform_id` = `ds_maindata_platforms`.`id`
        LEFT JOIN `ds_maindata_category` ON `ds_maindata`.`cat_id` = `ds_maindata_category`.`id`
        LEFT JOIN `ds_maindata_hint` ON `ds_maindata`.`id` = `ds_maindata_hint`.`id`
        WHERE `ds_maindata`.`id` = ' . $id;
    $dbQuery->exec($sql);
    $data = $dbQuery->fetch();

    if(!$data) {
        engine_head();
        temp::display('404.index');
        exit();
    }

    core::$page_description = mb_substr($data['name'], 0, 200);
    $page_keys = preg_replace("/[ ]/", ", ", $data['name']);
    core::$page_keywords = mb_substr($page_keys, 0, 50);
    engine_head(mb_substr($data['name'], 0, 50) . "... " . lang('card_n') . $id);

    $tabledata = new tabledata(0, 0);
    $ds_maindata_organizers = new model(null, 'ds_maindata_organizers');
    $bankruptcy = new bankruptcy();
    $priceSchedule = new priceSchedule();

    if (isset($_POST['save_edit_admin'])) {
        $ds_maindata = new model(null, 'ds_maindata');
        $ds_maindata->updateByField('id', $id, [
            'place' => POST('place'),
            'cat_id' => POST('category'),
            'now_price' => POST('now_price'),
            'name' => htmlspecialchars(POST('name')),
            'address' => htmlspecialchars(POST('address'))
        ]);
        if (isset($_POST['emailot']) || isset($_POST['phoneot'])) {
            $ds_maindata_organizers->updateByField('id', $data['organizer'], [
                'mail' => htmlspecialchars(POST('emailot')),
                'phone' => htmlspecialchars(POST('phoneot'))
            ]);
        }
        func::notify('Лот обновлен', 'Обновление прошло успешно', '/card/' . $id);
    }

    if (isset($_POST['ruquestSubmit'])) {
        $mailer = mailer::factory();
        $mailer->setSubject($_POST['request_title'] . ', обратный адрес: ' . $_POST['request_email']);
        $mailer->Body = '<p style="color: red;">Прошу направить ответ на адрес электронной почты: ' .  $_POST['request_email'] . '</p>' . $_POST['request_text'];
        $mailer->isHTML(true);
        $mailer->addAddress($_POST['request_to']);
        if($mailer->send()) {
            func::notify('Сообщение отправлено', 'Сообщение отправлено', '/card/' . $id);
        } else {
            $log = $mailer->debug();
            func::notify('Произошла ошибка', 'Произошла ошибка', '/card/' . $id);
        }
    }

    $access = in_array(core::$rights, [10, 11, 70, 100]);

    temp::assign('id', $id);
    temp::assign('src', $src = $data['src']);
    $srcMap = ['bn' => 'Банкротство', 'cn' => 'Конфискат', 'pr' => 'Приватизация'];
    temp::assign('srcType', $srcMap[$src]);
    temp::assign('isBn', $isBn = $src == 'bn');
    temp::assign('isCn', $isCn = $src == 'cn');
    temp::assign('isPr', $isPr = $src == 'pr');
    $cid = (int)$data['cat_id'];

    $details = !$isBn ? (new view(null, 'ds_maindata_' . $src))->where('id', $id)->fetch() : [];
    $details = $details ? $details : [];

    $lotName = $tabledata->name($data['name'], 40, $data['id'], [], $data['description']);
    temp::HTMassign('lotdescr', $lotName['onlydata']);
    temp::assign('lotregion', $data['regionname']);
    temp::HTMassign('note', $data['note']);

    // фото
    $lot_fotos = new view(null, 'lot_fotos');
    if($items = $lot_fotos->where('lotid', $id)->limit(10)->fetchAll()){
        $photoArr = [];
        foreach ($items as $item){
            $photoArr[] = 'http://foto.bankrot-spy.ru' . $item['link'];
        }
        temp::HTMassign('photoArr', $photoArr);
    }

    // параметры пользователя
    if ($uid) {
        $where = ['item' => $id, 'user_id' => $uid];
        $ds_maindata_complaint = new view(null, 'ds_maindata_complaint');
        $in_complaint = $ds_maindata_complaint->where($where)->count();
    }
    temp::assign('lotFav', $uid ? count($bankruptcy->getDetailsArray('favorite', [$id], $uid)) : false);
    temp::assign('lotHide', $uid ? count($bankruptcy->getDetailsArray('hide', [$id], $uid)) : false);
    temp::assign('lotcomplaint', !!$in_complaint);

    // параметры лота
    temp::assign('category', $data['catname']);
    if(isset($details['subject']) && $details['subject']){
        temp::assign('subject', $details['subject']);
    }
    if(isset($details['property']) && $details['property']){
        temp::assign('property', $details['property']);
    }
    if(isset($details['final']) && $details['final']){
        temp::assign('final', $details['final']);
    }
    if(isset($details['decision']) && $details['decision']){
        temp::assign('decision', $details['decision']);
    }
    temp::assign('origin_address', $data['address']);
    if($data['address'] && (core::$rights == 100)){
        temp::assign('address_by', $data['address_by']);
    }
    temp::assign('lottype', $data['type_name']);

    $status = $tabledata->beforedate($data['start_time'], $data['end_time'], $data['status_name'], $data['status']);
    temp::assign('lotstatus', $status['col']);

    if(($data['type'] == 1) && in_array((int)$data['status'], [6, 8, 9, 10])){
        temp::assign('lotIsLiveAuction', true);
    }
    temp::assign('lotstarttime', ds_time($data['start_time']));
    temp::assign('lotendtime', ds_time($data['end_time']));

    // Цены
    temp::HTMassign('lotprice', f::prF($data['price'], 2));
    temp::HTMassign('nowprice', f::prF($data['now_price'], 2));
    if($data['grafik_state'] > 0){
        temp::HTMassign('priceSchedule', $priceSchedule->getPricesList($id, 'card'));
    } elseif ($data['grafik_state'] == -2) {
        temp::HTMassign('priceScheduleOrigin', $data['grafik1']);
    }

    if (!in_array($cid, [0, 2, 4, 8])) {
        temp::assign('priceExtra', true);
        $cPrice = $data['cadastr_price'];
        if(in_array($cid, [3, 5, 6])){
            if($cPrice > 0){
                if($data['market_price'] == 0) $data['market_price'] = $cPrice;
            } else {
                $cPrice = $data['market_price'];
            }
            temp::HTMassign('cadastr_price', f::prF($cPrice, 2));
        }
        temp::HTMassign('realprice', f::prF($data['market_price'], 2));
        temp::HTMassign('realPriceIsNumeric', is_numeric($data['market_price']));
        temp::HTMassign('profitrub', f::prF($data['profit_rub'], 2));
        temp::HTMassign('profitproc', f::prF($data['profit_proc']));
    }

    if ($cid == 2) {
        temp::assign('debExtra', true);
        $debpoints = $tabledata->debpoints($data['debpoints'], $data['debnotice'], CAN('scores_debtor'));
        temp::HTMassign('debpoints', $debpoints['col']);
        if($debpoints['addition']) {
            temp::HTMassign('additionhtmldeb', $debpoints['addition']);
        }
        if($debpoints['customclass']) {
            temp::HTMassign('customclassdeb', ' class="' . $debpoints['customclass'] . '" ');
        }
    }

    if (in_array($cid, [1, 5, 6, 7]) && !empty($data['hint'])) {
        temp::HTMassign('market_price_hint', preg_replace_callback('/^([^\d]*)(\d*)\s([^\s]*)([^.,]*)(.*)$/', function($m){
            return $m[1]
                . (CAN('cost_meter')
                    ? ($m[2] . ' объявлени'. f::w125($m[2], [1 => 'е', 'я', 5 => 'й']))
                    : '<i class="fa fa-lock"></i> объявлений')
                . $m[4];
        }, $data['hint']));
        temp::assign('market_price_link', $data['link']);
    }
    if(isset($details['deposit']) && (float)$details['deposit']){
        temp::HTMassign('deposit', f::prF($details['deposit'], 2));
    }
    if(isset($details['stepUp']) && (float)$details['stepUp']){
        temp::HTMassign('stepUp', f::prF($details['stepUp'], 2));
    }
    if(isset($details['stepDown']) && (float)$details['stepDown']){
        temp::HTMassign('stepDown', f::prF($details['stepDown'], 2));
    }
    if(isset($details['currency']) && $details['currency']){
        temp::assign('currency', $details['currency']);
    }
    if($data['depend']){
        temp::HTMassign('depend', $data['depend']);
    }

    // данные по банкроту
    if($isBn){
        $ds_maindata_debtors = new view(null, 'ds_maindata_debtors');
        if ($dataDebtor = $ds_maindata_debtors->where('id', $data['debtor'])->fetch()) {
            temp::assign('debtor', $dataDebtor['dept_name']);
            temp::assign('debtor_inn', $dataDebtor['inn']);
            if ($dataDebtor['debt_profile'] AND $dataDebtor['debt_profile'] != 'www1')
                temp::assign('debtor_profile', $dataDebtor['debt_profile']);
        }
        if($debtorLots = $bankruptcy->getLotsByDebtor($data['debtor'], [$data['id']])){
            $out = [];
            foreach ($debtorLots as $key => $value){
                $out[] = '<a href="/card/' . $key . '">#' . $key . '</a>'
                    . ((int)$value > 0 ? (' (' . $value . '%)') : '');
            }
            $out = join(', ', $out);
        } else {
            $out = 'нет';
        }
        temp::HTMassign('debtorOtherLots', $out);
        $debt_mess = new view(null, 'debt_mess');
        $out = [];
        $items = $debt_mess->where('debtor', $data['debtor'])->fetchAll();
        foreach ($items as $item){
            $tmp = [
                'date' => date('d.m.Y', strtotime($item['data'])),
                'title' => $item['tip'],
                'link' => $item['link']
            ];
            foreach (['оценщик', 'инвентаризац'] as $w){
                if(strpos($item['tip'], $w) != false){
                    $tmp['pr'] = 'prior';
                    break;
                }
            }
            $out[] = $tmp;
        }
        temp::HTMassign('debtorMsgs', $out);
    }

    // организатор торгов
    $dataOrg = $ds_maindata_organizers->where('id', $data['organizer'])->fetch();

    temp::HTMassign('organizer', (new organizer())->parse($dataOrg));

    $orgDocs = $dataOrg['totaldoc'] > 0;
    temp::assign('rating', $orgDocs ? $dataOrg['bal'] : 'Нет данных'); // рейтинг
    temp::assign('docs', $orgDocs ? $dataOrg['linkdocs'] : 'Нет данных'); // ссылка на документы

    temp::assign('inn_orgname', $dataOrg['inn']);
    temp::assign('oid', $dataOrg['id']);
    temp::assign('organizer_profile', $dataOrg['org_profile']);
    temp::assign('arbitr_profile', $dataOrg['arbitr_profile']);

    // внешние ресурсы
    temp::assign('platform_url', $access ? $data['platform_url'] : '');
    temp::assign('fedlink', $access ? $data['fedlink'] : -1);
    temp::assign('auct_link', $access ? $data['auct_link'] : -1);
    temp::assign('code_torg', $access ? $data['code'] : '-1');

    if(in_array((int)$data['status'], [1, 2])){
        $lot_views = new model(null, 'lot_views');
        $lot_views->insert([
            'lot_id' => $id,
            'view_date' => time()
        ]);
    }

    if (substr_count($data['case_number'], 'Арбитражныйсуд') > 0) {
        $data['case_number'] = 'Арбитражный суд ' . str_replace('Арбитражныйсуд', '', $data['case_number']);
    }
    temp::assign('case_number', $data['case_number']);

    temp::assign('reportLink', $data['reportlink']);
    temp::assign('lotnumber', (CAN('lot_number') ? ($data['code'] . ($data['lot_number'] ? (' / Лот №' . $data['lot_number']) : '')) : ''));
    temp::assign('request_doc', CAN('send_request'));
    temp::assign('id_user', $uid);

    // гистограмма аналогичных цен
    $graphType = in_array($cid, [5, 6]) ? 2 : 1; // 1- техника, 2 - недвижимость
    $similarDataPrice = [];
    $lot_prices = new view(null, 'lot_prices');
    $items = $lot_prices->where('id', $id)->order('`price` ASC')->fetchAll();
    if (count($items) > 1) { // надо минимум 2 значения для построения графика
        foreach ($items as $item){
            $item['average'] = ($graphType == 1) ? $data['market_price'] : $data['price_average'];
            $similarDataPrice[] = $item;
        }
        temp::assign('graphType', $graphType);
        temp::HTMassign('similarDataPrice', CAN('histogram_goods') ? json_encode($similarDataPrice) : 'access');
    }

    // редактирование лота
    if(in_array(core::$rights, [70, 100])){
        temp::assign('origin_name', $data['name']);
        temp::assign('origin_phoneot', $dataOrg['phone']);
        temp::assign('origin_emailot', $dataOrg['mail']);
        temp::assign('now_clear_price', $data['now_price']);
        $ds_maindata_regions = new view(null, 'ds_maindata_regions');
        temp::HTMassign('regions', $ds_maindata_regions->select(['number', 'mini', 'name'])->fetchAll());
        $ds_maindata_category = new view(null, 'ds_maindata_category');
        $categories = $ds_maindata_category->fetchAll();
        $categories[] = [
            'id' => 0,
            'name' => 'Прочее',
            'usort' => 10,
            'ssort' => 10
        ];
        temp::HTMassign('categories_select', $categories);
    }

    temp::display('cards.index');
    engine_fin();
