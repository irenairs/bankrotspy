<?php

defined('DS_ENGINE') or die('web_demon laughs');

$access = true;

if(!core::$user_id ) {
    $access = '<i class="fa fa-lock" onmouseout="toolTip()" onmouseover="toolTip(\'Информация доступна для зарегистрированных пользователей\')"></i>';
} else {
    $userQuery = core::$db->query('SELECT * FROM ds_users WHERE id = '.core::$user_id.' LIMIT 1;');
    $userData =  $userQuery->fetch_assoc();
    $tartime = $userData['desttime'] - time();

    if (!CAN('invest_view') || $tartime < 86410 ) { // только для юзеров с бодпиской больше чем на месяц
        if (core::$rights < 100) {
            $access = '<i class="fa fa-lock" onmouseout="toolTip()" onmouseover="toolTip(\'Информация доступна при VIP подписке на месяц и более\')"></i>';
        }
    }
}
