<?php
defined('DS_ENGINE') or die('web_demon laughs');

///////////////////////////////////////////////////////////////

if(core::$rights < 100)
  denied();

$error = [];
if(POST('submit'))
{
    $number = POST('number');
    if(!$number)
      $error[] = "Нет номера актива!";

    $active = POST('active');
    if(!$active)
      $error[] = "Нет названия актива!";
      
    $manager = POST('manager');
    if(!$manager)
      $error[] = "Нет ответственного менеджера!";

    $sum_invest = POST('sum_invest');
    if(!$sum_invest)
      $error[] = "Нет суммы инвестиций!";

    $time_invest = POST('time_invest');
    if(!$time_invest)
      $error[] = "Нет срока инвестиций!";

    $profit_proc = POST('profit_proc');
    if(!$profit_proc)
      $error[] = "Нет ориентира по доходности!";

    $present = POST('present');
    
   if(!$error)
   {
    core::$db->query('INSERT INTO `ds_invest_list` SET `number` = "'.core::$db->res($number).'", `active` = "'.core::$db->res($active).'",`sum_invest` = "'.core::$db->res($sum_invest).'", `time_invest` = "'.core::$db->res($time_invest).'", `profit_proc` = "'.core::$db->res($profit_proc).'", `manager` = "'.core::$db->res($manager).'", `present` = "'.core::$db->res($present).'";');
     func::notify(lang('pr_adminpanel'), lang('stat_added'), core::$home.'/control/invest', lang('continue'));
   } 
}  
if(POST('number'))
  temp::assign('number', POST('number'));
else
  temp::assign('number', $res['number']);

if(POST('active'))
  temp::assign('active', POST('active'));
else
  temp::assign('active', $res['active']);
if(POST('manager'))
   temp::assign('manager', POST('manager'));
else
  temp::assign('manager', $res['manager']);
if(POST('sum_invest'))
   temp::assign('sum_invest', POST('sum_invest'));
else
  temp::assign('sum_invest', $res['sum_invest']);
if(POST('time_invest'))
   temp::assign('time_invest', POST('time_invest'));
else
  temp::assign('time_invest', $res['time_invest']);
if(POST('profit_proc'))
   temp::assign('profit_proc', POST('profit_proc'));
else
  temp::assign('profit_proc', $res['profit_proc']);
if(POST('present'))
   temp::assign('present', POST('present'));
else
  temp::assign('present', $res['present']);


engine_head("Новый актив");

temp::HTMassign('error', $error);
temp::display('invest.add');
engine_fin();

