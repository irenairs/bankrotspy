<?php
defined('DS_ENGINE') or die('web_demon laughs');

// new nav(20);

$where = '1';

$order = 'org.number ASC'; 

$cntQuery = 'SELECT COUNT(*) AS cnt FROM ds_invest_list AS org WHERE ' . $where;

$query = "SELECT * "
    . " FROM"
        . " ds_invest_list AS org"
        . ' WHERE ' . $where
        . ' ORDER BY ' . $order ;

$cntQuery = core::$db->query($cntQuery);
$total = $cntQuery->fetch_assoc()['cnt'];
$res = core::$db->query( $query);

$data = [];
$i = 0;

while($row = $res->fetch_assoc()) {

    if (!empty($row['present'])) {
        $present = '<a class="namelink" href="' . $row['present'] . '" target="_blank">Смотреть</a>';
    } else {
        $present = 'Нет данных';
    }
    $data[$i]['manager'] = $row['manager'];
    $data[$i]['manager'] = text::out($data[$i]['manager'] ,0);
    $data[$i]['manager'] = preg_replace_callback('/\[(file|img)\=([^\n\&\/\"\\\\<\>\+\&\;\:]{1,200})\](.*?)\[\/\1\]/', array('pages', 'image_replace'), $data[$i]['manager']);

    
    $data[$i]['id'] = $row['id'];
    $data[$i]['number'] = $row['number'];
    $data[$i]['active'] = $row['active'];;
    $data[$i]['sum_invest'] = $row['sum_invest'];
    $data[$i]['time_invest'] = $row['time_invest'];
    $data[$i]['profit_proc'] = $row['profit_proc'];
    $data[$i]['manager'] = ($access === true) ? $data[$i]['manager'] : $access;
    $data[$i]['present'] = ($access === true) ? $present : $access;
    // $data[$i]['present'] = $row['present'];
    $i++;
}

$textQuery = core::$db->query('SELECT * FROM ds_pages WHERE id = "30" LIMIT 1;');
$textData =  $textQuery->fetch_assoc();
core::$db->query("INSERT INTO ds_counts(page, counttime, day, monthyear) VALUES (4,".time().",".date('j').", ".date('n').date('y')."4)");
engine_head($textData['name'], $textData['keywords'], $textData['description']);
temp::assign('title', $textData['name']);

$start = nav::$start;
$finish = $start + nav::$kmess;
if ($total < $finish ) {
    $finish = $total;
}

temp::assign('start', $start);
temp::assign('end', $finish);
temp::assign('total', $total);

temp::HTMassign('textData', text::out($textData['text'], 0));
temp::assign('access', $access);

temp::HTMassign('navigation', nav::display($total, core::$home.'/invest?'));
temp::HTMassign('data', $data);

temp::display('invest.index');

engine_fin();
