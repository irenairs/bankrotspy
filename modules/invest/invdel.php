<?php
defined('DS_ENGINE') or die('web_demon laughs');

if(core::$rights < 100)
  denied();

$id = intval(abs(GET('id')));
if($id)
{
  $res = core::$db->query('SELECT * FROM `ds_invest_list` WHERE `id` = "'.$id.'" LIMIT 1;');
  $res =  $res->fetch_assoc();
  if($res['id'])
  {
    if(POST('delete'))
    {
      core::$db->query('DELETE FROM `ds_invest_list` WHERE `id` = "'.$id.'" LIMIT 1;');
      func::notify("Удаление", lang('stat_deleted'), core::$home.'/control/invest', lang('continue'));
    }  
      
    engine_head($res['active']);
    temp::assign('active', $res['active']);
    temp::assign('id', $res['id']);
    temp::display('invest.invdel');
    engine_fin();
  }
  else
    func::notify(lang('stat'), lang('stat_no'), core::$home, lang('continue'));
}
else
  func::notify(lang('stat'), lang('stat_no'), core::$home, lang('continue'));