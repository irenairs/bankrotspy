<?php
error_reporting(E_ALL);
ini_set("display_errors", 0);
defined('DS_ENGINE') or die('web_demon laughs');

// доступ опционально для групп
if(!CAN('export_favorites')) {
    $response = array(
        'status'    => 1,
        'message'   => 'Данная функция доступна на тарифном плане VIP.'
    );
    ajax_response($response);
}

if(!empty($_SESSION['export'])&&$_SESSION['export']['data']>=date("Y-m-d H:i", strtotime("-10 minutes"))) {
    $response = array(
        'status'    => 1,
        'message'   => 'Вы экспортируете второй раз за 10 минут. Попробуйте позже'
    );
    ajax_response($response);
}

$_SESSION['export']['data']=date("Y-m-d H:i");

/*
//статусы ответа

0 - не корректные данные
1 - доступ запрещен
2 - прямая скачка файла
3 - файл отправлен на почту
*/



if(empty($_POST)) {
    $response = array(
        'status'    => 0,
        'message'   => 'Не корректные данные'
    );
    ajax_response($response);
}

// подключать через композер, переделать
require_once 'dscore/libs/PHPExcel.php';
require_once 'dscore/libs/phpmailer/PHPMailerAutoload.php';


$where = array('1');
$joins = array();
$selects = array('');
$orders = array();

$pst=$_POST['pst'];

foreach ($pst as $key=>$value){
    $pst_tmp[urldecode($key)]=urldecode($value);
}

$pst=$pst_tmp;


$search_name = trim(strip_tags($pst['search_name']));
$search_name = str_replace ( array('~', '>', '<', '?', ')', '(', '}', '{', '&', '^', '$', '#', '|', '_', '-', '/', '\\'), " ", $search_name);
$search_name = preg_replace( "/\s{2,}/", ' ', $search_name);

$all_keywords = trim(strip_tags($pst['all_keywords']));
if (isset($search_name) && !empty($search_name)) {

    $keywords = explode(' ', $search_name);
    $search_name_to_where = $search_name;

    if ($all_keywords) {
        $search_name_to_where = '+'.implode(' +', $keywords);
    }

    //Подключаем индекс
    $joins[] = 'LEFT JOIN `privatizacia_index` ON `privatizacia`.`id` = `privatizacia_index`.`lotID`';

    $selects[] = "MATCH (`privatizacia_index`.content) AGAINST ('" . $search_name . "') AS relSumm";
    $where[] = "MATCH (`privatizacia_index`.content) AGAINST ('" . $search_name_to_where . "' IN BOOLEAN MODE)";

    $relColumnNames = array();

    foreach ($keywords as $key => $keyword) {
        //Проверяем наличие каждого ключевого слова (1 или 0)
        $selects[] .= "MATCH (`privatizacia_index`.content) AGAINST ('" . $keyword . "' IN BOOLEAN MODE) > 0 AS rel_".$key;
        $relColumnNames[] = "rel_".$key;
    }
    $relColumnNamesString = implode(" + ", $relColumnNames);
    $orders[] = " ".$relColumnNamesString." DESC, relSumm DESC";
}

$reg_select = trim(strip_tags($pst['reg_select']));
if (isset($reg_select) && !empty($reg_select) && $reg_select != "any") {
    $where[] = "("
        . "privatizacia.region  LIKE '%".$reg_select."%'"
        . ")";
}

$cat_select = trim(strip_tags($pst['cat_select']));
if (isset($cat_select) && !empty($cat_select) && $cat_select != "any") {
    $where[] = "("
        . "privatizacia.tipi  LIKE '%".$cat_select."%'"
        . ")";
}


$begin_set_date = trim(strip_tags($pst['begin_set_date']));
if (isset($begin_set_date) && !empty($begin_set_date)) {
    $where[] = "("
        . "privatizacia.start_time  > '".strtotime($begin_set_date)."'"
        . ")";
}

$end_set_date = trim(strip_tags($pst['end_set_date']));
if (isset($end_set_date) && !empty($end_set_date)) {
    $where[] = "("
        . "privatizacia.end_time < '".strtotime($end_set_date)."'"
        . ")";
}

$begin_set_deposit = trim(strip_tags($pst['begin_set_deposit']));
if (isset($begin_set_deposit) && !empty($begin_set_deposit)) {
    $where[] = "("
        . "privatizacia.zadatok  > '".str_replace(" ","",$begin_set_deposit)."'"
        . ")";
}

$end_set_deposit = trim(strip_tags($pst['end_set_deposit']));
if (isset($end_set_deposit) && !empty($end_set_deposit)) {
    $where[] = "("
        . "privatizacia.zadatok < '".str_replace(" ","",$end_set_deposit)."'"
        . ")";
}

$price_start = trim(strip_tags($pst['price_start']));
if (isset($price_start) && !empty($price_start)) {
    $where[] = "("
        . "privatizacia.price  > '".str_replace(" ","",$price_start)."'"
        . ")";
}

$price_end = trim(strip_tags($pst['price_end']));
if (isset($price_end) && !empty($price_end)) {
    $where[] = "("
        . "privatizacia.price < '".str_replace(" ","",$price_end)."'"
        . ")";
}

$status_auct_1 = trim(strip_tags($pst['status_auct_1']));
$status_auct_2 = trim(strip_tags($pst['status_auct_2']));
if (isset($status_auct_1) && !empty($status_auct_1)) {
    $where[] = "(tradetip LIKE '%аукцион%')";
} elseif (!isset($status_auct_1) || empty($status_auct_1) && isset($status_auct_2) && !empty($status_auct_2)) {
    $where[] = "(tradetip NOT LIKE '%аукцион%')";
}

$sortOrder = $pst['sortOrder'];
$sortField = $pst['sortField'];

if ( isset($sortOrder) && ($sortOrder == 'ASC') ) {
    $sortOrder = 'ASC';
} else {
    $sortOrder = 'DESC';
}

if ( isset($sortField) && strlen($sortField) > 0) {
    array_unshift($orders, "privatizacia.".$sortField." ".$sortOrder);
} else {
    $orders[] = 'privatizacia.profit_proc'." ".$sortOrder;;
}


$countExport = 'SELECT COUNT(*) AS cnt FROM privatizacia
           '.implode(', ',$joins).'
            WHERE ' . implode(' AND ',$where);
$query = 'SELECT *'.implode(', ',$selects).' FROM privatizacia
           '.implode(', ',$joins).'
            WHERE ' . implode(' AND ',$where)
    . ' ORDER BY ' . implode(', ', $orders) . ' LIMIT 5000';


/*if($countExport >= 5) {
    $response = array(
        'status' => 3
    );

    echo json_encode($response);exit;
}*/

$res = core::$db->query( $query);

//массив полей
$fieldsHeader = array(
    'name'          => 'Описание',
    'type'          => 'Тип',
    'region'        => 'Регион',
    'begin_date'    => 'Дата начала',
    'end_date'      => 'Дата окончания',
    'status'        => 'Статус',
    'begin_price'   =>  'Начальная цена',
    'market_price'  =>  'Рыночная цена',
    'profit_rub'    =>  'Доход, руб.',
    'profit_proc'   =>  'Доходность, %',
    'zadatok'       =>  'Задаток',
    'obremenenie'   =>  'Обременение',
    'link'   =>  'Ссылка'
);

$fieldsCount = count($fieldsHeader);

// столбцы в которых текст по центру
$styleFields = array(
    'id',
    'type',
    'region',
    'status',
    'begin_date',
    'end_date',
    'begin_price'
);


while($data = $res->fetch_assoc()) {
    $lotsExport[] = $data['id'];

    $result[$i]['name'] = $data['name'];
    $result[$i]['type'] = (stristr($data['tradetip'], 'аукц') == true) ? "АО" : "ПП";
    $result[$i]['region'] = $data['region'];
    $result[$i]['begin_date'] = date('d.m.Y', $data['start_time']);
    $result[$i]['end_date'] = date('d.m.Y', $data['end_time']);
    $result[$i]['status'] = $data['status'];
    $result[$i]['begin_price'] = $data['price'];
    $result[$i]['market_price'] = $data['market_price'];
    $result[$i]['profit_rub'] = $data['profit_rub'];
    $result[$i]['profit_proc'] = $data['profit_proc'];
    $result[$i]['zadatok'] = $data['zadatok'];
    $result[$i]['obremenenie'] = $data['obremenenie'];
    $result[$i]['link'] = $data['link'];

    $result[$i]['id'] = $data['id'];

    $i++;
}

$totalRow = count($result);

if ($totalRow>5000){
    $response = array(
        'status'    => 1,
        'message'   => 'Количество лотов превышает допустимое число. Сократите объем и повторите экспорт.'
    );
    unset($_SESSION['export']);
    ajax_response($response);
}

$link_style_array = array(
    'font'  => array(
        'color' => array('rgb' => '0000FF'),
        'underline' => 'single'
    )
);

$excel = new PHPExcel();
$excel->setActiveSheetIndex(0);
//$excel->getActiveSheet()->setTitle('bankrot-spy.ru');
$sheet = $excel->getActiveSheet();

$sheet->freezePane('A3'); //закрепляем шапку
$sheet->getRowDimension(1)->setRowHeight(40); //высота шапки
$sheet->getRowDimension(2)->setRowHeight(20); //высота шапки

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setPath("data/bs.jpg");
$objDrawing->setWorksheet($sheet);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(10);
$objDrawing->setOffsetY(10);

$style_promo = array(
    // выравнивание
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
    ),
    'borders' => array (
        'allborders' => array (
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb' => 'ffffff'
            )
        ),
    )
);


$sheet->getStyle('A1:E1')->applyFromArray($style_promo);
$sheet->mergeCells('B1:c1');
$sheet->setCellValue('B1', 'www.bankrot-spy.ru');
$sheet->getCell('B1')->getHyperlink()->setUrl('http://www.bankrot-spy.ru');
$sheet->getStyle('B1')->applyFromArray($link_style_array);
//$sheet->setCellValue('A1', 'bankrot-spy.ru');


//$sheet->getStyle('A1')->applyFromArray($link_style_array);





$coord = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
$coordIndex = 0;
// шапка

$row = 2;
$col = 0;

foreach($fieldsHeader as $field => $data) {

    $cell = $coord[$coordIndex].$row;

    $sheet->setCellValue($cell, $data);
    $sheet->getColumnDimension($coord[$col])->setWidth(30);

    $col++;
    $coordIndex++;
}

//стиль шапки
$style_header = array(
    // выравнивание
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
    ),
    // заполнение цветом
    'fill' => array(
        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
        'color'=>array(
            'rgb' => '43464b'
        )
    ),
    // шрифт
    'font'=>array(
        'bold' => true,
        /* 'italic' => true, */
        'name' => 'Arial',
        'size' => 11,
        'color' => array(
            'rgb' => 'ffffff'
        )
    ),
    'borders' => array (
        'allborders' => array (
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb' => 'dddddd'
            )
        ),
    )
);
$sheet->getStyle('A2:' . $coord[$fieldsCount] .'2')->applyFromArray($style_header);

//список лотов
$coordIndex  = 0;

foreach($result as $row => $item) {
    $row = $row + 3;

    foreach($item as $field => $data) {
        $cell = $coord[$coordIndex].$row; //текущая ячейка

        //ссылка на площадку и фед. ресурс
        if ($field == 'id') {
            $sheet->setCellValue($cell, 'Карточка лота');
            $url = 'http://bankrot-spy.ru/privatized/' . $data;
            $sheet->getCell($cell)->getHyperlink()->setUrl($url);

            $sheet->getStyle($cell)->applyFromArray($link_style_array);
        } elseif($field == 'arbitrator') {
            $sheet->setCellValue($cell, $data['name']);
            $url = 'http://bankrot-spy.ru/amc/' . $data['id'];
            $sheet->getCell($cell)->getHyperlink()->setUrl($url);
            $sheet->getStyle($cell)->applyFromArray($link_style_array);
        } else {
            $sheet->setCellValue($cell, $data);
        }
        if (in_array($field, $styleFields)) {
            $sheet->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $coordIndex++;
    }
    $coordIndex  = 0;

}

$datetime = date('d.m.Y-H:i:s', time());

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
$filename = 'data/export/bankrotspy_privatization_'.core::$user_id.'_' . $datetime . '.xlsx';
$objWriter->save($filename);


if(isset($_GET['action']) && $_GET['action'] == 'download') {
    $response = array(
        'status' => 2,
        'file' => $filename
    );
    core::$db->query("INSERT INTO ds_counts(page, counttime, day, monthyear) VALUES (20,".time().",".date('j').", ".date('n').date('y')."20)");
    ajax_response($response);
} else {

    $body = array(
        'name' => core::$user_name,
        'date' => $datetime
    );

    //отправка на почту
    $mail = mailer::factory('./data/engine/');
    $mail->setSubject('Лоты');
    $mail->setBody('export', $body);
    $mail->addAttachment($filename);
    $mail->addAddress(core::$user_mail);

    if($mail->send()) {
        unlink($filename);
    }

    $response = array(
        'status' => 3,
        'message' => 'Файл отправлен на почту: ' . core::$user_mail
    );

    echo json_encode($response);
}