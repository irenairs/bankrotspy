<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    if($uid = core::$user_id){
        $def = defaultset::get();
        if(($data = json_decode(POST('jsettings'), 1)) && is_array($data)){
            $save = [];
            foreach($def AS $key => $value){
                if(isset($data[$key])){
                    if(is_array($data[$key]) && is_array($value)){
                        $tmp = [];
                        foreach($data[$key] AS $k => $v){
                            $tmp[$k] = $v ?  1 : 0;
                        }
                        $save[$key] = $tmp;
                    } elseif(!is_array($data[$key]) AND !is_array($value)) {
                        $save[$key] = $data[$key];
                    }
                } else {
                    $save[$key] = $value;
                }
            }
            $searchProfile = new searchProfile();
            (isset(core::$user_set['tabledata']) && core::$user_set['tabledata'])
                ? $searchProfile->upProfile(core::$user_set['tabledata'], $save)
                : $searchProfile->createProfile($save);
        } else {
            exit(400);
        }
    } else {
        exit(401);
    }
