<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    //Ключ, скорее для виду, сесcия доставит гораздо больше проблем =)
    $somevar = 'tvybunwedowhduw2397ey9hd8ybhb83wecugwvevct';
    if($somevar != POST('somevar'))
        exit('Glory Sithis!');

    $uid = core::$user_id;

    $columns = preg_split('/&/', POST('columns'));

    $prm = [
        'category' => POST('category'),
        'sortcolumn' => POST('sortcolumn'),
        'sorttype' => abs(intval(POST('sorttype'))),
        'new_lots' => POST('new_lots'),
        'favorite' => POST('favorite'),
        'hide' => POST('hide'),
        'type_price' => POST('type_price'),
        'price_start' => POST('price_start'),
        'price_end' => POST('price_end'),
        'altint' => POST('altint'),
        'data_filter_type' => POST('data_filter_type'),
        'begin_date' => POST('begin_date'),
        'end_date' => POST('end_date'),
        'types' => explode('|', POST('types')),
        'places' => explode('|', POST('places')),
        'platforms' => explode('|', POST('platforms')),
        'src' => explode('|', POST('src')),
        'status' => explode('|', POST('status')),
        'svalue' => POST('svalue'),
        'all_keywords' => POST('all_keywords'),
        'hasPhoto' => POST('hasPhoto'),
    ];

    $bankruptcy = new bankruptcy();

    //Постраничная навигация
    $page = ($page = abs(intval(POST('page')))) ? $page : 1;
    $size = (($size = abs(intval(POST('kmess')))) && ($size >= 20) && ($size <= 200)) ? $size : 20;
    $start =  ($page - 1) * $size;

    if(system::$config['mode']['search'] == 'sphinx'){
        $data = (new searchSphinx())->bankruptcy($prm, $uid, $start, $size);
    $res = $data[0];
    $count = $data[1][1]['Value'];
    } else {
        $dbQuery = new dbQuery();
        $cond = $bankruptcy->requestPrm($prm, $uid);

        //Счетчик
        $sql = 'SELECT COUNT(*) `count` FROM `ds_maindata`' . $cond['join'] . $cond['where'];
        $dbQuery->exec($sql);
        $count = $dbQuery->fetch('count');

        //Основной запрос
        $sql = 'SELECT `ds_maindata`.* ' . $cond['select'] . ' FROM `ds_maindata`' . $cond['join'] .
            $cond['where'] . $cond['order'] . ' LIMIT ' . $start . ', ' . $size;
        $dbQuery->exec($sql);
        $res = $dbQuery->fetchAll();
    }

    $category = (int)$prm['category'];
    $itemArr = isset($cond['queryStr']) ? explode(' ', $cond['query']) : [];

if (count($res)) {
    $statusMap = getStatusMap();
    $favArr = $uid ? $bankruptcy->getDetailsAllArray('favorite', $uid) : [];
    $favCount = 0;
    $hideArr = $uid ? $bankruptcy->getDetailsAllArray('hide', $uid) : [];

    $out = [];
    $out2 = [];

    $tabledata = new tabledata($prm['sortcolumn'], intval(abs($prm['sorttype'])));

    $nameLenght = (int)POST('more') == 1 ? 350 : 80; // Показывать расширенное имя лота или нет
    $idArr = [];
    foreach ($res as $item) {
        $idArr[] = $item['id'];
        $item['status_name'] = $statusMap[$item['status']];
        $favCount += ($item['item'] = in_array($item['id'], $favArr) ? 1 : 0);
        $item['hide'] = in_array($item['id'], $hideArr) ? 1 : 0;
        $out[] = $item;
    }

    $dataNotes = [];
    if(in_array('addition', $columns) && $uid){
        $lot_notes = new view(null, 'lot_notes');
        $dataNotes = $lot_notes->where([
            'user_id' => $uid,
            'lot_id' => $idArr
        ])->fetchAll('lot_id', 'text');
    }
    $lot_fotos = new view(null, 'lot_fotos');
    foreach ($out AS $key => $data) {
        $tmp = [];
        $id = $data['id'];
        $tmp['id'] = $id;
        $tmp['loadtime'] = $data['loadtime'] * 1e3;
        $tmp['last_update'] = $data['last_update'] * 1e3;
        $tmp['name'] = $tabledata->name($data['name'], $nameLenght, $id, $itemArr, $data['description'],
            $data['loadtime'], $lot_fotos->where('lotid', $id)->limit(1)->fetch('link'), $data['address'],
            $data['mail']);
        $tmp['type'] = $tabledata->type($data['type']);
        $tmp['place'] = $tabledata->place($data['place']);
        if(in_array('begindate', $columns)){
            $tmp['begindate'] = $tabledata->begindate($data['start_time']);
        }
        if(in_array('closedate', $columns)){
            $tmp['closedate'] = $tabledata->closedate($data['end_time']);
        }
        $tmp['beforedate'] = $tabledata->beforedate($data['start_time'], $data['end_time'], $data['status_name'], $data['status']);
        $tmp['beginprice'] = $tabledata->beginprice(round($data['price']));
        $tmp['nowprice'] = $tabledata->nowprice($data['now_price'], $data['platform_id'], $data['type'], ($data['grafik_state'] > 0) ? $id : 0, $data['price']);
        if(in_array('minprice', $columns)) {
            $tmp['minprice'] = $tabledata->minprice($data['min_price'], CAN('price_reduction_schedule'));
        }

        $access = in_array(core::$rights, [10,11,70,100]);
        /*
            0 - ? 
            2 - Деб. задолженность
            4 - Сельхоз. имущество
            5 - Недвиж. жилая
            8 - Обор. Инст. Мат.
        */
        if (!in_array($category, [0, 2, 4, 8])) {
            if(in_array('marketprice', $columns)) {
                if (in_array($category, [1, 5, 6, 7])) {
                    if (!CAN('scores_debtor')) {
                        $data['hint_text'] = 'Информация доступна на тарифном плане VIP';
                    }
                    if (CAN('market_price') && empty($data['market_price'])) {
                        $tmp['marketprice'] = $tabledata->marketprice($data['market_price'], $access, $data['hint_text'], true);
                    } else {
                        $tmp['marketprice'] = $tabledata->marketprice($data['market_price'], $access, $data['hint_text']);
                    }
                } else {

                    if (CAN('market_price') && empty($data['market_price'])) {
                        $tmp['marketprice'] = $tabledata->marketprice($data['market_price'], true, '', true);
                    } else {
                        $tmp['marketprice'] = $tabledata->marketprice($data['market_price'], $access);
                    }
                }
            }

            if(in_array('profitrub', $columns)) {
                if (CAN('profit_rub')) {
                    $tmp['profitrub'] = $tabledata->profitrub($data['profit_rub'], $data['platform_id'], $data['type'], true, $data['grafik1']);
                } else {
                    $tmp['profitrub'] = $tabledata->profitrub($data['profit_rub'], $data['platform_id'], $data['type'], false, $data['grafik1']);
                }
            }

            if(in_array('profitproc', $columns)) {
                if (CAN('profit_proc')) {
                    $tmp['profitproc'] = $tabledata->profitproc($data['profit_proc'], $data['platform_id'], $data['type'], true, $data['grafik1']);
                } else {
                    $tmp['profitproc'] = $tabledata->profitproc($data['profit_proc'], $data['platform_id'], $data['type'], false, $data['grafik1']);
                }
            }
        }

        if(in_array('pricediff', $columns)) {
            $tmp['pricediff'] = $tabledata->pricediff($data['price_diff'], $data['platform_id'], $data['type'], $data['grafik1']);
        }
        if(in_array('debpoints', $columns)) {
            if ($category == 2) {
                $tmp['debpoints'] = $tabledata->debpoints($data['debpoints'], $data['debnotice'], CAN('scores_debtor'));
            }
        }

        if(in_array('platform', $columns)) {
            $tmp['platform'] = $tabledata->platform($data['platform_id'], $data['auct_link'], $data['fedlink'], $access);
        }

        if(in_array('addition', $columns)) {
            $tmp['favorite'] = $tabledata->addition($id, $data['item'], isset($dataNotes[$id]) ? $dataNotes[$id] : '', $category, $data['hide']);
        }
        $out2[] = $tmp;
    }
    
    $outdata = array(
        'columns' => $tabledata->get_names(),
        'maindata' => $out2,
        'count' => $count,
        'prm' => [
            'allInFavorite' => count($out2) == $favCount
        ]
    );
} else {
    $outdata = array(
        'columns' => array('name' => array(
            'name' => (($category == '-1' AND !$uid) ? 'Добавление в избранное доступно только зарегистрированным пользователям!' : 'Ничего не найдено!'),
            'style' => 'background:white;border:0px;padding: 40px 0;font-size: 14px;'
        )),
        'maindata' => '',
        'count' => $count,
        'prm' => [
            'allInFavorite' => false
        ]
    );
}

function getStatusMap(){
    $ds_maindata_status = new view(null, 'ds_maindata_status');
    return $ds_maindata_status->fetchAll('id', 'status_name');
}

echo json_encode($outdata);
