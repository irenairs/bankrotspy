<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    if($uid = core::$user_id){
        if($name = trim(POST('profile_name'))){
            $def = defaultset::get();
            $searchProfile = new searchProfile();
            if(!isset(core::$user_set['tabledata']) || !core::$user_set['tabledata']){
                $searchProfile->createProfile($def);
            }
            $searchProfile->createProfile($def, htmlspecialchars($name));
            echo 'ok';
        } else {
            exit(400);
        }
    } else {
        exit(401);
    }
