<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    new nav; //Постраничная навигация

    $ds_platform_news = new view(null, 'ds_platform_news');
    if(core::$rights < 100){
        $ds_platform_news->where(['type' => [2]]);
    }
    $total = $ds_platform_news->count();

    $outNews = [];
    $res = $ds_platform_news->order('`time` DESC')->limit(nav::$start, nav::$kmess)->fetchAll();
    foreach ($res as $data){
        $tmp = [];
        $tmp['id'] = $data['id'];
        $tmp['time'] = ds_time($data['time'], '%H:%M');
        $tmp['data'] = ds_time($data['time'], '%d %B2 %Y');
        text::add_cache($data['cache']);
        $tmp['text'] = text::out($data['text'], 0);
        $outNews[] = $tmp;
    }

    engine_head(lang('pnews'));
    temp::HTMassign('total', $total);
    temp::HTMassign('outnews', $outNews);
    temp::HTMassign('navigation', nav::display($total, core::$home . '/pnews?'));
    temp::display('pnews.index');
    engine_fin();