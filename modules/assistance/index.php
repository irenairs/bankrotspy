<?php
defined('DS_ENGINE') or die('web_demon laughs');

require_once $_SERVER['DOCUMENT_ROOT'] . '/dscore/libs/phpmailer/PHPMailerAutoload.php';

$error = array();  

$lotid = GET('lotid');
$name   = POST('name');
$email  = POST('email');
$skype  = POST('skype');
$text   = POST('text');
$phone   = POST('phone');
$lotbs   = $_SERVER['HTTP_REFERER'];
//POST('lotbs');
if (GET('act')) {
    
    if(!$name) {
        $error[] = lang('empty_name');
    }
	//>>>>>>>>>>
	 
	
	
	//>>>>>>>>>>
    
    if(!$email) {
        $error[] = lang('empty_email');
    } elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error[] = lang('incorrect_email');
    }
    
    if(!$text) {
        $error[] = lang('empty_text');
    } elseif(mb_strlen($text) < 4) {
        $error[] = lang('length_text');
    }
    
    if(!func::capcha()) {
      $error[] = lang('miss_capcha');
    }
    
    if(!$skype) {
        $skype = 'не указан';
    }
  
    if(!$error) {
        
        $insertID = core::$db->insert('INSERT INTO `ds_assistance` SET
            `name`="'.core::$db->res($name).'",
            `email`="'.core::$db->res($email).'",
            `skype`="'.core::$db->res($skype).'",
            `phone`="'.core::$db->res($phone).'",
            `text`="'.core::$db->res($text).'",
			`lotbs`="'.core::$db->res($lotbs).'",
            `created`="'.time().'"
        ');
        
        $href= '';
        
        if(!empty($lotid)) {
            $lothref = core::$home . '/card/' . $lotid;
            $href = 'Лот: <a href="' . $lothref . '">' . $lothref . '</a><br/>';
        }
                
        $mail = mailer::factory('./data/engine/');
      //	'lotbs'  => $lotbs,  
        $body = array(
            'name'  => $name,		
            'phone'  => $phone,
            'email' => $email,
            'skype' => $skype,
            'href'  => $href,			
            'text'  => $text
        );
        
        $subject = array(
            'id' => $insertID
        );
        
        $mail->setSubject(lang('success_head'), $subject);
        $mail->setBody('assistance', $body);
        $mail->addAddress('sales@i-tt.ru');
		$mail->addAddress('task+1754@bankrotforum.planfix.ru');
        $mail->send();
       
        $data = array(
            'id' => $insertID,
            'name' => $name
        );
        
        $mail = mailer::factory('./data/engine/');
        
        if(!core::$user_id) {
            $mail->setSubject(lang('user_head'));
            $mail->setBody('assistance_registration', $data);
            $mail->addAddress($email);
            $mail->send();
        } else {
            $mail->setSubject(lang('user_head'));
            $mail->setBody('assistance_noregistration', $data);
            $mail->addAddress($email);
            $mail->send();
        }
        
        func::notify(lang('title'), lang('success'), core::$home);
    } 
}


$fil1 = core::$db->query('SELECT * FROM `ds_pages_files` WHERE `post` = "10";');
while ($fil = $fil1->fetch_assoc())
{
  $out_fil[$fil['filename']] = array();
  $out_fil[$fil['filename']]['filename'] = $fil['filename'];
  $out_fil[$fil['filename']]['name'] = $fil['name'];
  $out_fil[$fil['filename']]['id'] = $fil['id'];  
}

if(isset($out_fil))
  pages::$out_fil = $out_fil;



$textQuery = core::$db->query('SELECT * FROM `ds_pages` WHERE `id` = "10" LIMIT 1;');
$textData =  $textQuery->fetch_assoc();

engine_head($textData['name'], $textData['keywords'], $textData['description']);

temp::assign('title', $textData['name']);

temp::HTMassign('error',$error);

$to_text = text::out($textData['text'] ,0);
$to_text = preg_replace_callback('/\[(file|img)\=([^\n\&\/\"\\\\<\>\+\&\;\:]{1,200})\](.*?)\[\/\1\]/', array('pages', 'image_replace'), $to_text);

// $out['text'] = text::out(text::auto_cut($data['text'], 300), 0);
temp::HTMassign('page_text', $to_text);

temp::assign('name',$name);
temp::assign('email',$email);
temp::assign('phone',$phone);
temp::assign('skype',$skype);
temp::assign('text',$text);
temp::assign('lotbs',$lotbs);
temp::assign('lotid',$lotid);

temp::HTMassign('capcha',func::img_capcha());
temp::display('assistance.index');
// engine_fin();