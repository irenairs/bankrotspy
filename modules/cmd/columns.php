<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    $res['error'] = true;

    if (in_array(core::$rights, [10, 11, 100])) {
        $ds_users = new model(null, 'ds_users');
        core::$user_set['columns'][POST('type')] = POST('items');
        $ds_users->updateByField('id', core::$user_id, [
            'settings' => serialize(core::$user_set)
        ]);
        $res['error'] = false;
        $res['message'] = 'Изменение настроек таблицы прошло успешно.';
    } else {
        $res['message'] = 'Данная функция доступна на платной подписке!';
    }

    ajax_response($res);
