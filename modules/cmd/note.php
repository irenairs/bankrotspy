<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    $res['error'] = true;

    if (CAN('lot_note')) {
        if($id = (int)POST('id')){
            $text = htmlspecialchars($_POST['text']);
            $textL = strlen($text);
            if ($textL <= 400) {
                $target = new bankruptcy();
                if($target->checkLots([$id])){
                    $res['error'] = false;
                    $uid = core::$user_id;
                    $inNotes = $target->getDetailsArray('notes', [$id], $uid);
                    if($textL){
                        if($inNotes){
                            $target->upDetailsArray('notes', [$id], $uid, [
                                'text' => $text
                            ]);
                            $res['message'] = 'Комментарий обновлен.';
                        } else {
                            $target->setDetailsArray('notes', [$id], $uid, [
                                'text' => $text
                            ]);
                            $res['message'] = 'Комментарий добавлен.';
                        }
                    } else {
                        if($inNotes){
                            $target->delDetailsArray('notes', [$id], $uid);
                        }
                        $res['message'] = 'Комментарий удален.';
                    }
                } else {
                    $res['message'] = 'Этого лота не существует.';
                }
            } else {
                $res['message'] = 'Максимальное колличество символов 400.';
            }
        } else {
            $res['message'] = 'Не передан ID лота.';
        }
    } else {
        $res['message'] = 'Данная функция доступна на тарифном плане VIP';
    }

    ajax_response($res);
