<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    $res['error'] = true;

    if (CAN('add_favorites')) {
        if($items = (array)POST('items')){
            $target = new bankruptcy();
            if($target->checkLots($items)){
                $uid = core::$user_id;
                $inFavorite = $target->getDetailsArray('favorite', $items, $uid);
                if(POST('action') == 'del'){
                    if($inFavorite){
                        $target->delDetailsArray('favorite', $items, $uid);
                        $target->delDetailsArray('notes', $items, $uid);
                    }
                    $res['error'] = false;
                    $res['message'] = 'Удаление из избранного прошло успешно.';
                } else {
                    if(count($inFavorite) == count($items)){
                        $res['message'] = 'Это было добавлено в избранное реньше.';
                    } else {
                        $target->setDetailsArray('favorite', $items, $uid, [
                            'favtime' => time()
                        ]);
                        $res['error'] = false;
                        $res['message'] = 'Добавление в избранное прошло успешно.';
                    }
                }
            } else {
                $res['message'] = 'Одного или нескольких лотов не существует.';
            }
        } else {
            $res['message'] = 'Не переданы ID лотов.';
        }
    } else {
        $res['message'] = 'Данная функция доступна на платной подписке!';
    }

    ajax_response($res);
