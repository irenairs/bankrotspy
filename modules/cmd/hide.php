<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    $res['error'] = true;

    if (in_array(core::$rights, [10, 11, 100])) {
        if($items = (array)POST('items')){
            $target = new bankruptcy();
            if($target->checkLots($items)){
                $uid = core::$user_id;
                $inHide = $target->getDetailsArray('hide', $items, $uid);
                if(POST('action') == 'del'){
                    if($inHide){
                        $target->delDetailsArray('hide', $items, $uid);
                    }
                    $res['error'] = false;
                    $res['message'] = 'Удаление из мусора прошло успешно.';
                } else {
                    if(count($inHide) == count($items)){
                        $res['message'] = 'Это было добавлено в мусор реньше.';
                    } else {
                        $target->setDetailsArray('hide', $items, $uid, [
                            'hidetime' => time()
                        ]);
                        $res['error'] = false;
                        $res['message'] = 'Добавление в мусор прошло успешно.';
                    }
                }
            } else {
                $res['message'] = 'Одного или нескольких лотов не существует.';
            }
        } else {
            $res['message'] = 'Не переданы ID лотов.';
        }
    } else {
        $res['message'] = 'Данная функция доступна на платной подписке!';
    }

    ajax_response($res);
