<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    if(!isset($prm) || !isset($uid)){
        $message = 'Ошибка обработки данных!';
    } elseif((($prm['user'][0] == 'fav') && (count($prm['user']) == 1)) ? !CAN('export_favorites') : !CAN('export_all')){
        $message = 'Данная функция доступна на тарифном плане VIP';
    } else {
        $ds_user_export = new model(null, 'ds_user_export');
        $countExport = $ds_user_export->where([
            'user_id' => $uid,
            'datetime' => ['begin' => time() - 86400]
        ])->count();
        if($countExport >= 5) {
            $message = 'Разрешено не более 5 скачиваний в день';
        } else {
            $data = (new searchSphinx())->getLots($prm, $uid, 0, 1000);
            $res = $data[0];
            $count = $data[1][1]['Value'];

            $mapColumns = mapColumns::getForExport($prm['fields']);
            $fields = $mapColumns['fields'];
            $fields['id'] = [
                'title' => 'Ссылка на БС',
                'hc' => true
            ];
            $fieldsCount = count($fields);

            $ext = [];
            $mapExt = [
                'ds_maindata_category' => 'cat_id',
                'ds_maindata_status' => 'status',
                'ds_maindata_regions' => 'place',
                'ds_maindata_type' => 'type',
                'ds_maindata_debtors' => 'debtor',
                'ds_maindata_organizers' => 'organizer',
                'src' => 'src'
            ];
            foreach ($mapColumns['tables'] as $tbl => $f){
                $f[] = 'id';
                $ext[$tbl] = (new view(null, $tbl))->select($f)->where([
                    'id' => array_values(array_unique(array_column($res, $mapExt[$tbl])))
                ])->fetchAll('id');
            }
            if(isset($fields['src'])){
                $ext['src'] = ['bn' => 'Банкротство', 'cn' => 'Конфискация', 'pr' => 'Приватизация'];
                $fields['src']['table'] = 'src';
            }

            require_once system::realPath('dscore/libs/PHPExcel.php');

            $excel = new PHPExcel();
            $excel->setActiveSheetIndex(0);
            $sheet = $excel->getActiveSheet();

            $styleLink = [
                'font' => [
                    'color' => ['rgb' => '0000FF'],
                    'underline' => 'single'
                ]
            ];

            // шапка
            $sheet->freezePane('A3');
            $sheet->getRowDimension(1)->setRowHeight(40);
            $sheet->getRowDimension(2)->setRowHeight(20);

            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setPath("data/bs.jpg");
            $objDrawing->setWorksheet($sheet);
            $objDrawing->setCoordinates('A1');
            $objDrawing->setOffsetX(10);
            $objDrawing->setOffsetY(10);

            $sheet->getStyle('A1:E1')->applyFromArray([
                'alignment' => [
                    'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
                ],
                'borders' => [
                    'allborders' => [
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => [
                            'rgb' => 'ffffff'
                        ]
                    ],
                ]
            ]);

            $sheet->mergeCells('B1:C1');
            $sheet->setCellValue('B1', 'www.bankrot-spy.ru');
            $sheet->getCell('B1')->getHyperlink()->setUrl('http://www.bankrot-spy.ru');
            $sheet->getStyle('B1')->applyFromArray($styleLink);

            $xMap = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z'];

            $x = 0;
            $y = 2;
            foreach($fields as $field) {
                $cell = $xMap[$x] . $y;
                $sheet->setCellValue($cell, $field['title']);
                $sheet->getColumnDimension($xMap[$x])->setWidth(30);
                $x++;
            }

            $sheet->getStyle('A2:' . $xMap[$fieldsCount - 1] .'2')->applyFromArray([
                'alignment' => [
                    'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
                ],
                'fill' => [
                    'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
                    'color' => ['rgb' => '43464b']
                ],
                'font' => [
                    'bold' => true,
                    'name' => 'Arial',
                    'size' => 11,
                    'color' => ['rgb' => 'ffffff']
                ],
                'borders' => [
                    'allborders' => [
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => ['rgb' => 'dddddd']
                    ],
                ]
            ]);

            $lotsExport = [];
            foreach ($res as $item){
                $lotsExport[] = $item['id'];
                $x = 0;
                $y++;
                foreach ($fields as $name => $pr){
                    $cell = $xMap[$x] . $y;
                    $url = false;
                    switch ($name){
                        case 'id':
                            $value = 'Карточка лота';
                            $url = 'https://bankrot-spy.ru/card/' . $item['id'];
                            break;
                        case 'auct_link':
                        case 'fedlink':
                            if($item[$name]){
                                $value = 'Ссылка на ' . ($name == 'auct_link' ? 'площадку' : 'федресурс');
                                $url = $item[$name];
                            } else {
                                $value = '';
                            }
                            break;
                        case 'manager':
                            $tbl = $pr['table'];
                            $index = $item[$mapExt[$tbl]];
                            if(isset($ext[$tbl][$index])){
                                $value = $ext[$tbl][$index];
                                $value = isset($pr['fld']) ? $value[$pr['fld']] : $value;
                                $url = 'https://bankrot-spy.ru/amc/' . $index;
                            } else {
                                $value = '';
                            }
                            break;
                        case 'start_time':
                        case 'end_time':
                            $value = date('d.m.Y', $item[$name]);
                            break;
                        case 'now_price':
                        case 'min_price':
                        case 'market_price':
                        case 'profit_rub':
                            $value = number_format($item[$name], 0, '', ' ');
                            break;
                        default:
                            if(isset($pr['table'])){
                                $tbl = $pr['table'];
                                $index = $item[$mapExt[$tbl]];
                                if(isset($ext[$tbl][$index])){
                                    $value = $ext[$tbl][$index];
                                    $value = isset($pr['fld']) ? $value[$pr['fld']] : $value;
                                } else {
                                    $value = '';
                                }
                            } else {
                                $value = $item[$name];
                            }
                    }
                    $sheet->setCellValue($cell, $value);
                    if($url){
                        $sheet->getCell($cell)->getHyperlink()->setUrl($url);
                        $sheet->getStyle($cell)->applyFromArray($styleLink);
                    }
                    if($pr['hc']){
                        $sheet->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }
                    $x++;
                }
            }

            $datetime = date('d.m.Y-H:i:s', time());

            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            $filename = 'data/export/bankrotspy_'.core::$user_id.'_' . $datetime . '.xlsx';
            $objWriter->save($filename);

            if($lotsExport) {
                $ds_user_export->insert([
                    'user_id' => $uid,
                    'lots' => json_encode($lotsExport),
                    'datetime' => time()
                ]);
            }

            $ds_counts = new model(null, 'ds_counts');
            $ds_counts->insert([
                'page' => 20,
                'counttime' => time(),
                'day' => date('j'),
                'monthyear' => date('n') . date('y')
            ]);

            if($prm['action'] == 'download'){
                $file = $filename;
            } else {
                require_once (system::realPath('dscore/classes/mailer.php'));
                $body = [
                    'name' => core::$user_name,
                    'date' => $datetime
                ];
                $mailer = mailer::factory(system::realPath('data/engine') . '/');
                $mailer->setSubject('Экспорт лотов');
                $mailer->setBody('export', $body);
                $mailer->addAddress(core::$user_mail);
                $mailer->addAttachment($filename);
                if($mailer->send()) {
                    unlink($filename);
                }
                $message = 'Файл отправлен на почту: ' . core::$user_mail;
            }
        }
    }
