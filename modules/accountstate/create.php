<?php

if(!core::$user_id)
{
    func::notify("Нет доступа", "Чтобы получить доступ к данной странице, вам необходимо зарегистрироваться", core::$home.'/user/register', "Регистрация");
    return 0;
}

//Список тарифов из базы
$res = core::$db->query('SELECT * FROM `ds_tariffs` WHERE price != 0 ORDER BY price ASC');
$tariffs = array();
while ($data = $res->fetch_array()){
    
    $term = '-';
    if($data['typetime'] == 1){
        $term = $data['longtime'];
        if((int)$data['longtime'] == 1){
            $term .= ' месяц';
        } elseif((int)$data['longtime'] > 1 && (int)$data['longtime'] < 5){
            $term .= ' месяца';
        } elseif((int)$data['longtime'] >= 5){
            $term .= ' месяцев';
        }
    } elseif($data['typetime'] == 0 && $data['longtime'] > 0){
       $term = $data['longtime'];
        if((int)$data['longtime'] == 1){
            $term .= ' день';
        } elseif((int)$data['longtime'] > 1 && (int)$data['longtime'] < 5){
            $term .= ' дня';
        } elseif((int)$data['longtime'] >= 5){
            $term .= ' дней';
        } 
    }
    $data['term'] = $term;
    if(empty($data['spec_descr'])){
       $data['spec_descr'] = '--'; 
    } else {
       $data['spec_descr'] = htmlspecialchars($data['spec_descr']); 
    }
    $tariffs[] = $data;
}
//Выводим страничку
engine_head('Выписка счета');
//temp::HTMassign('array_for_zaya',$array_for_zaya);
temp::HTMassign('tariffs', $tariffs);
temp::display('accountstate.create');
engine_fin();
