<?php
defined('DS_ENGINE') or die('web_demon laughs');
if(!core::$user_id)
{
    func::notify("Нет доступа", "Чтобы получить доступ к данной странице, вам необходимо зарегистрироваться", core::$home.'/user/register', "Регистрация");
    return 0;
}
$doc_count_id = GET('id');

if(!$doc_count_id)
    denied();

$req = core::$db->query('SELECT * FROM ds_count_data WHERE `id` = "'.$doc_count_id.'" LIMIT 1;');

if($req->num_rows)
{
    $data = $req->fetch_assoc();
}
else
    denied();


//Выводим страничку
engine_head('Редактирование выписки счета');
temp::HTMassign('data', $data);
temp::display('accountstate.edit');
engine_fin();