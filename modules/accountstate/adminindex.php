<?php
defined('DS_ENGINE') or die('web_demon laughs');

if(!core::$user_id)
{
    func::notify("Нет доступа", "Чтобы получить доступ к данной странице, вам необходимо зарегистрироваться", core::$home.'/user/register', "Регистрация");
    return 0;
}

new nav(30);

$prodcode['T1']='Тариф - 1 день';
$prodcode['T2']='Тариф - 1 месяц';
$prodcode['T3']='Анализ дебиторки 1';
$prodcode['T4']='Анализ дебиторки 2';
$prodcode['T5']='Доступ на 3 месяца';
$prodcode['T6']='VIP-доступ на 1 месяц';
$prodcode['T7']='Коммерсант_1';
$prodcode['T8']='Поиск дебиторки 2';
$prodcode['T9']='Доступ на 6 месяцев к материалам сайта';
$prodcode['T10']='Доступ на 12 месяцев к материалам сайта';
$prodcode['T11']='VIP-доступ на 3 месяца';
$prodcode['T12']='Обучение "Специалист"';
$prodcode['T13']='VIP-доступ на 6 месяцев';
$prodcode['T14']='Поиск дебиторки 1';
$prodcode['T15']='VIP-доступ на 12 месяцев';
$prodcode['T16']='Коммерсант_2';
$prodcode['T17']='Коммерсант_3';
$prodcode['T18']='Поиск дебиторки 3';
$prodcode['T19']='Обучение "Эксперт"';


$user_id = core::$user_id;

$cntQuery = 'SELECT COUNT(*) AS cnt FROM ds_count_data';
$cntQuery = core::$db->query($cntQuery);
$total = $cntQuery->fetch_assoc()['cnt'];

$where = "";
$sql = "SELECT  id,compname,prodcode, datecreate  FROM ds_count_data AS cd  ORDER BY `id` DESC LIMIT ". nav::$start . ', ' . nav::$kmess ;
$res = core::$db->query($sql);


$data = [];
$i=0;
while($row = $res->fetch_assoc()) {

    $data[$i]['id'] = $row['id'];
    $data[$i]['prodcode'] = $prodcode[$row['prodcode']];
//    $data[$i]['datecreate'] = $row['datecreate'];
    $data[$i]['filename']="Счет № ".$row['id']." от ".$row['datecreate'].", ". $row ['compname'];
    $data[$i]['compname'] = $row['compname'];
    $i++;
}


//Выводим страничку
engine_head('Выписка счета');
$start = nav::$start;
$finish = $start + nav::$kmess;
if ($total < $finish ) {
    $finish = $total;
}
$admin = 1;

temp::assign('start', $start);
temp::assign('end', $finish);
temp::assign('total', $total);
temp::assign('admin', $admin);
temp::HTMassign('data', $data);
temp::HTMassign('navigation', nav::display($total, core::$home.'/accountstate?', '', '',null));
temp::display('accountstate.index');
engine_fin();