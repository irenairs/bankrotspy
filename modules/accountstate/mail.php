<?php
defined('DS_ENGINE') or die('web_demon laughs');
if(!core::$user_id)
{
    func::notify("Нет доступа", "Чтобы получить доступ к данной странице, вам необходимо зарегистрироваться", core::$home.'/user/register', "Регистрация");
    return 0;
}

require_once 'dscore/libs/phpmailer/PHPMailerAutoload.php';

$datetime = date('d.m.Y-H:i:s', time());
$body = array(
    'name' => core::$user_name,
    'date' => $datetime
);
$id = GET('id');
$filename = "data/docflow_files/".$id.".docx";
//отправка на почту
$mail = mailer::factory('./data/engine/');
$mail->setSubject('Выписка счета');
$mail->setBody('maildoc', $body);
$mail->addAttachment($filename,"bspy_счет_№ ".$id.".docx");
$mail->addAddress(core::$user_mail);


$response = array(
    'status' => 3,
    'message' => 'Файл отправлен на почту: ' . core::$user_mail,
);

if($mail->send()) {
    echo json_encode($response);
}




