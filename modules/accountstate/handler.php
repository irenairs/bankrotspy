<?php

if(!core::$user_id)
{
    func::notify("Нет доступа", "Чтобы получить доступ к данной странице, вам необходимо зарегистрироваться", core::$home.'/user/register', "Регистрация");
    return 0;
}

$res = core::$db->query('SELECT * FROM `ds_tariffs` WHERE price != 0 ORDER BY price ASC');
$prodcode = array();
$prt = array();
while ($data = $res->fetch_array()){
   $prodcode['T'.$data['id']] = $data['name']; 
   $prt['T'.$data['id']] = $data['price'];
}

/*$prodcode['T1']='Тариф - 1 день';
$prodcode['T2']='Тариф - 1 месяц';
$prodcode['T3']='Анализ дебиторки 1';
$prodcode['T4']='Анализ дебиторки 2';
$prodcode['T5']='Доступ на 3 месяца';
$prodcode['T6']='VIP-доступ на 1 месяц';
$prodcode['T7']='Коммерсант_1';
$prodcode['T8']='Поиск дебиторки 2';
$prodcode['T9']='Доступ на 6 месяцев к материалам сайта';
$prodcode['T10']='Доступ на 12 месяцев к материалам сайта';
$prodcode['T11']='VIP-доступ на 3 месяца';
$prodcode['T12']='Обучение "Специалист"';
$prodcode['T13']='VIP-доступ на 6 месяцев';
$prodcode['T14']='Поиск дебиторки 1';
$prodcode['T15']='VIP-доступ на 12 месяцев';
$prodcode['T16']='Коммерсант_2';
$prodcode['T17']='Коммерсант_3';
$prodcode['T18']='Поиск дебиторки 3';
$prodcode['T19']='Обучение "Эксперт"';

$prt['T1']='90';
$prt['T2']='550';
$prt['T3']='600';
$prt['T4']='900';
$prt['T5']='1200';
$prt['T6']='1500';
$prt['T7']='1500';
$prt['T8']='1500';
$prt['T9']='2200';
$prt['T10']='3500';
$prt['T11']='3500';
$prt['T12']='4000';
$prt['T13']='5000';
$prt['T14']='5000';
$prt['T15']='6900';
$prt['T16']='8000';
$prt['T17']='9500';
$prt['T18']='10000';
$prt['T19']='19000';*/


function num2str($num) {
    $nul='ноль';
    $ten=array(
        array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
        array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
    );
    $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
    $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
    $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
    $unit=array( // Units
        array('копейка' ,'копейки' ,'копеек',	 1),
        array('рубль'   ,'рубля'   ,'рублей'    ,0),
        array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
        array('миллион' ,'миллиона','миллионов' ,0),
        array('миллиард','милиарда','миллиардов',0),
    );
    //
    list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub)>0) {
        foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
            if (!intval($v)) continue;
            $uk = sizeof($unit)-$uk-1; // unit key
            $gender = $unit[$uk][3];
            list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
            // mega-logic
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
            else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            // units without rub & kop
            if ($uk>1) $out[]= morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
        } //foreach
    }
    else $out[] = $nul;
    $out[] = morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
    $out[] = $kop.' '.morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
}

/**
 * Склоняем словоформу
 * @ author runcore
 */
function morph($n, $f1, $f2, $f5) {
    $n = abs(intval($n)) % 100;
    if ($n>10 && $n<20) return $f5;
    $n = $n % 10;
    if ($n>1 && $n<5) return $f2;
    if ($n==1) return $f1;
    return $f5;
}

$user_id = core::$user_id;

//$compname;
//$inn;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $compname =  core::$db->res(text::st(trim(strip_tags($_POST['name']))));

    if(strpos($_POST['inn'], '_'))
    $inn =  substr($_POST['inn'], 0, strpos($_POST['inn'], '_' ));
    else
        $inn = $_POST['inn'];

    $prc = $_POST['paymentType'];
    $price=   $prt[$prc];

}

$datecreate = date("Y-m-d");
//  запись в базу реквизитов для счета
$sql="INSERT INTO `ds_count_data`(`user_id`,`inn`,`compname`,`prodcode`,`prodprice`,`datecreate`) VALUES(".$user_id.",'".$inn."','".$compname."','".$prc."',".$price.",'".$datecreate."')";
core::$db->query($sql);
$doc_num = core::$db->insert_id;


// сгенерировали документ word
//
$in_words = num2str($price);
$words = explode(" ",$in_words);
$words[0]  = mb_convert_case($words[0],MB_CASE_TITLE);

$phpdocx = new phpdocx("data/t.docx");
$datecreate_to_doc = date("d.m.Y");

//$phpdocx->assign("#INN#",$inn);
if (!empty($inn)) $inn = " ИНН ".$inn;
$phpdocx->assign("#NAME#",$compname.$inn);
$phpdocx->assign("#PRICE#",$price.",00");
$phpdocx->assign("#PRODUCTNAME#",$prodcode[$prc]);
$phpdocx->assign("#COUNTNUM#",$doc_num);
$phpdocx->assign("#DATE#",$datecreate_to_doc);
$phpdocx->assign("#INWORD#",implode(" ",$words));

$phpdocx->save("data/docflow_files/".$doc_num.".docx");

header('Location:' . core::$home.'/accountstate');
