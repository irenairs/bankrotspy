<?php

if(!core::$user_id)
{
    func::notify("Нет доступа", "Чтобы получить доступ к данной странице, вам необходимо зарегистрироваться", core::$home.'/user/register', "Регистрация");
    return 0;
}
$doc_account_id = GET('id');

if(!$doc_account_id)
    denied();

new download("data/docflow_files/".$doc_account_id.".docx", text::unhtmlentities("bspy_счет_№ ".$doc_account_id.".docx"));