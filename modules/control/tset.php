<?php
defined('DS_ENGINE') or die('web_demon laughs');



//Для сохранения признака страниц (по ajax)
if(isset($_GET['page_update'])){
    $page = $_GET['page_update'];
    $id = (int)$_GET['id'];
    $check = $_GET['check'];
    $query = "UPDATE `ds_tariffs` SET `{$page}` = '{$check}' WHERE `id` = '{$id}'";
    
    core::$db->query($query);
    exit('Настройки тарифа изменены!');
}



$res = core::$db->query('SELECT * FROM `ds_tariffs` ORDER BY `price` ASC;');
$rmenu = array();

$typetime = array(
    'День',
    'Месяц',
    'Час'
);

while($data = $res->fetch_array())
{
  $loc = array();
  $loc['id'] = $data['id'];
  $loc['name'] =  $data['name'];
  text::add_cache($data['cache']);
  $loc['subtext'] = text::out($data['descr'], 0, $data['id']);
  // $loc['spec_descr'] = $data['spec_descr'];
  $loc['spec_descr'] = text::out($data['spec_descr'], 0, $data['id']);
  $loc['longtime'] = $data['longtime'];
  $loc['typetime'] = $typetime[$data['typetime']];
  $loc['price'] = $data['price'];
  $loc['page_main'] = $data['page_main'];
  $loc['page_tariff'] = $data['page_tariff'];
  $rmenu[] = $loc;
}


if(POST('newtabletarif')){
    
    $text = text::st(POST('text'));
        $link = text::st(POST('link'));
        $turn = 0;
        if(POST('turn')){        
            $turn = (int)POST('turn');
        }
        $name_link = text::st(POST('name_link'));
        $free = 0;
        $plain = 0;
        $vip = 0;
        
        if(POST('free') !== false){
            $free = 1;
        }
        if(POST('plain') !== false){
            $plain = 1;
        }
        if(POST('vip') !== false){
            $vip = 1;
        }
        
        $sql_i = "INSERT INTO `ds_tariffs_table` (`text`, `link`,`name_link`,`turn`,`free`, `plain`, `vip`) VALUES ('{$text}', '{$link}','{$name_link}','{$turn}', '{$free}', '{$plain}', '{$vip}')";
        
        $id_t = core::$db->insert($sql_i);
       
        if($turn == 0){
            core::$db->query("UPDATE `ds_tariffs_table` SET `turn`={$id_t} WHERE id = {$id_t}");
        }
        func::notify(lang('t_settings'), lang('settings_saved'), core::$home . '/control/tset', lang('continue'));
    }
    
    if(POST('edittabletarif')){
        $up = '';
       // var_dump(POST('text'));
        foreach(POST('id') as $key => $id){
            $text = text::st(POST('text')[$id][0]);
            $link = text::st(POST('link')[$id][0]);
            $turn = $id;
            if(POST('turn')[$id][0]){        
                $turn = (int)POST('turn')[$id][0];
            }
            $name_link = text::st(POST('name_link')[$id][0]);
            $free = 0;
            $plain = 0;
            $vip = 0;
            
            if(POST('free')[$id] !== false && POST('free')[$id] !== NULL){
                $free = 1;
            }
            if(POST('plain')[$id] !== false && POST('plain')[$id] !== NULL){
                $plain = 1;
            }
            if(POST('vip')[$id] !== false && POST('vip')[$id] !== NULL){
                $vip = 1;
            }
            $up .= "UPDATE `ds_tariffs_table` SET `text` = '{$text}', `link` = '{$link}',`name_link` = '{$name_link}',`turn` = '{$turn}',`free`='{$free}', `plain`='{$plain}', `vip`={$vip} WHERE id = {$id};";
        }
        
        core::$db->multi_query($up);
        core::$db->multi_free();
        func::notify(lang('t_settings'), lang('settings_saved'), core::$home . '/control/tset', lang('continue'));
    }
    
    $tt = array();
    $tt_res = core::$db->query("SELECT * FROM `ds_tariffs_table` order by turn");
    while($r_tt = $tt_res->fetch_assoc()){
        $tt[] = $r_tt;
    }


if(POST('submit'))
{
     
        
        $merchant_key = text::st(POST('merchant_key'));
        $market_id = text::st(POST('market_id'));
        $market_prefix = text::st(POST('prefix'));
        $mess = text::st(POST('mess'));

        $query = 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($merchant_key).'" WHERE `key` = "merchant_key";';
        $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($market_id).'" WHERE `key` = "market_id";';
        $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($market_prefix).'" WHERE `key` = "market_prefix";';

        core::$db->multi_query($query);
        core::$db->multi_free();

        core::$db->query('UPDATE `ds_reg_page` SET
                `text` = "'.core::$db->res($mess).'",
                `cache` = "'.core::$db->res(text::presave($mess)).'"
                WHERE `id`="2" LIMIT 1;');
    
  func::notify(lang('t_settings'), lang('settings_saved'), core::$home . '/control/tset', lang('continue'));
}

$res = core::$db->query('SELECT * FROM  `ds_reg_page` WHERE `id` = "2";');
$data = $res->fetch_assoc();

engine_head(lang('base_settings'));
temp::HTMassign('rmenu',$rmenu);
temp::HTMassign('tt',$tt);
temp::HTMassign('merchant_key', core::$set['merchant_key']);
temp::HTMassign('market_id', core::$set['market_id']);
temp::HTMassign('market_prefix', core::$set['market_prefix']);
temp::assign('text',$data['text']);

temp::display('control.tset');
engine_fin();