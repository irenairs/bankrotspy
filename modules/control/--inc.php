<?php
defined('DS_ENGINE') or die('web_demon laughs');

if(core::$rights < 100)
  denied();


//исходне права для групп
$defaultRights = array (
    'common' => array(
        'edit_user' => 0,
        'del_user' => 0,
        'edit_smiles' => 0,
        'stats_create' => 0,
        'stats_edit' => 0,
        'stats_delete' => 0,
        'comm_view' => 0,
        'comm_edit' => 0,
        'comm_delete' => 0,
        'ban_users' => 0,
        'comm_self_edit' => 1,
        'comm_self_delete' => 1,
        'create_comm' => 1,
        'tech_support' => 0,
        'news_create' => 0,
        'news_edit' => 0,
        'news_delete' => 0,
        'lot_edit' => 0,
    ),
    // платный контент, управление доступом в админке у групп пользователей
    'paid' => array(
        'export_favorites'      => 0, // экспорт избранных лотов
        'export_all'      => 0, // экспорт избранных лотов
        'add_favorites'         => 0, // добавление в избранные лоты
        'toplots_view'          => 0, // Топ лотов
        'stats_view'            => 0, // Статистика
        'winners_view'          => 0, // Рейтинг победителей
        'rating_arbitration'    => 0, // Рейтинг АУ
        'platforms_view'        => 0, // Торговые площадки
        'debtors_view'          => 0, // Должники
        'scores_debtor'         => 0, // Баллы в деб. задолжности
        'histogram_goods'       => 0, // Гистограмма аналогичных товаров
        'cost_meter'            => 0, // Стоимость М2
        'planner_lots'          => 0, // Планировщик избранных лотов
        'document_creation'     => 0, // Заявка на участие в торгах
        'send_request'          => 0, //отправка запроса организатору торгов
        'get_lot_price'         => 0, // Поиск цены для лотов без РЦ
        'lot_note'              => 0, // комментарии к избранным лотам
        'confiscat_view'        => 0, // Просмотр лотов конфиската
        'privatized_view'       => 0, // Просмотр лотов приватизации
        'invest_view'           => 0, // Просмотр списка активов
        'subscribe_lots'        => 0, // Рассылка лотов на email (обычная подписка)
        'subscribe_lots_vip'    => 0, // Рассылка лотов на email (VIP)
        'market_price'        => 0, // Рыночная цена лота
        'profit_rub'    => 0, // Доход руб
        'profit_proc'    => 0, // Доходность %
        'price_reduction_schedule'  => 0,//График понижения цен
        'delo_number' => 0, //Дело №
        'inn_bancrot' => 0, //ИНН банкрота
        'phone_email_ot' => 0, //Телефон и маил OT
        'lot_number' => 0, // Номер лота
        'lot_complain' => 0, //Пожаловаться на лот
        'lot_info' => 0, //Информация о лоте
    )
);