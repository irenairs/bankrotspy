<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    $ds_tariffs = new view(null, 'ds_tariffs');
    $ds_paid = new model(null, 'ds_paid');

    if(GET('action') == 'addPay'){
        if(GET('fio') == '') {
            func::notify('', 'Вы не указали плательщика!!!', core::$home . '/control/pays');
        } elseif(!((int)GET('summa') > 0)) {
            func::notify('', 'Некорректная сумма платежа!!!', core::$home . '/control/pays');
        } elseif(GET('tarid') == 0) {
            func::notify('', 'Вы не выбрали услугу!!!', core::$home . '/control/pays');
        } else {
            $ds_users = new view(null, 'ds_users');
            $uid = $ds_users->where('login', GET('fio'))->fetch('id');
            $tar_name = $ds_tariffs->where('id', GET('tarid'))->fetch('name');
            if($ds_paid->insert([
                'tarid' => GET('tarid'),
                'userid' => $uid ? $uid : 0,
                'username' => GET('fio'),
                'paytime' => GET('date') ? strtotime(GET('date')) : time(),
                'summ' => GET('summa'),
                'comm' => $ds_tariffs->where('id', GET('tarid'))->fetch('name')
            ])){
                header('Location:/control/pays');
            }
        }
    }

    $dbQuery = new dbQuery();
    $select = '`ds_paid`.*';
    $from = '`ds_paid`';
    $where = '`summ` > 0';

    if(isset($_GET['search']) && !empty($search = trim(strip_tags($_GET['search'])))) {
        $search = htmlspecialchars($search);
        $select .= ', `ds_users`.`mail`';
        $from .= ' LEFT JOIN `ds_users` ON `ds_paid`.`userid` = `ds_users`.`id`';
        $where .= ' AND ((`username` LIKE "%' . $search . '%") OR (`mail` LIKE "%' . $search . '%"))';
    }

    $total = ($dbQuery->exec('SELECT COUNT(*) as `count` FROM ' . $from . ' WHERE ' . $where))
        ? $dbQuery->fetch('count')
        : 0;
    $dbQuery->exec('SELECT ' . $select . ' FROM ' . $from .' WHERE ' . $where . ' ORDER BY `paytime` DESC');
    $res = $dbQuery->fetchAll();

$i = 0;
$arr = array();
$month_names = array(
    1 => 'Январь',
    2 => 'Февраль',
    3 => 'Март',
    4=> 'Апрель',
    5=> 'Май',
    6=> 'Июнь',
    7=> 'Июль',
    8=> 'Август',
    9=> 'Сентябрь',
    10=> 'Октябрь',
    11=> 'Ноябрь',
    12=> 'Декабрь'
);

$month_summ = array();

// Перебор всех полученных транзакций и установка доплнительных значений
foreach ($res as $data) {
    $out = array();

    $out['id'] =  $data['id'];
    $out['userid'] = $data['userid'];
    $out['username'] = $data['username'];
    if(isset($data['mail'])){
        $out['mail'] = $data['mail'];
    }
    $out['paidid'] =  $data['paidid'];
    $out['paytime'] =  ds_time($data['paytime'], '%d %B2 %Y');
    $out['time'] =  date('h:m:s', $data['paytime']);
    $out['summ'] =  $data['summ'];
    $out['comm'] =  $data['comm'];

    $month = date('n', $data['paytime']);
    $year = date('Y', $data['paytime']);

    $month_summ[$year][$month] = @$month_summ[$year][$month] + $data['summ'];

    $arr[$year][$month][] = $out;
    $i++;
}


//hohol добавление списка услуг
$tarrif = core::$db->query('SELECT * FROM `ds_tariffs`');
$tarrif_select = "<select name='tarid' id='tarid'>";
$tarrif_select.= "<option value='0'>Не выбран</option>";
while($data = $tarrif->fetch_assoc()) {
    $tarrif_select.= "<option value='".$data['id']."'>".$data['name']."</option>";
}
$tarrif_select.= "</select>";

//hohol добавление списка услуг
/*$tarrif = core::$db->query('SELECT * FROM `ds_tariffs`');
$tarrif_select = "<select name='tarid' id='tarid'>";
$tarrif_select.= "<option value='0'>0</option>";
while($data = $tarrif->fetch_assoc()) {
    $tarrif_select.= "<option value='".$data['name']."'>".$data['name']."</option>";
}
$tarrif_select.= "</select>";
*/


// select count(distinct userid) from `ds_paid` where summ != '0' and tarid not in (19,15,12,20,21,10,11,12,22,23,24)

$clients_with = core::$db->query("select count(userid) as count from `ds_paid` where summ != '0' and tarid in (select id from ds_tariffs where typetime != 0)")->count();

$clients = core::$db->query("select count(distinct userid) from `ds_paid` where summ != '0' and tarid not in (select id from ds_tariffs where typetime = 0)")->count();

// $clients = core::$db->query("select count(distinct userid) from `ds_paid` where summ != '0'")->count();

$str = "select sum(cnt) as count from (
        select userid, t.id, (count(userid)-1) as cnt from ds_paid as p 
        join ds_tariffs as t on p.tarid = t.id
        where summ !='0' and t.longtime != 0
        group by userid having cnt > 0
        ) as t";

// $repays = core::$db->query($str)->count();

$str =  "select (avg(endtime - regtime))/8640 as count from
            (select 
                userid, 
                (u.time/10) as regtime, 
                typetime, 
                max(paytime) as lasttime, 
                CASE typetime
                    WHEN '0' THEN (  (longtime * 3600) + max(paytime)  ) / 10
                    WHEN '1' THEN (  (longtime * 86400) + max(paytime)  ) / 10
                    WHEN '2' THEN (  (longtime * 2592000) + max(paytime)  ) / 10
                END AS endtime

                from ds_paid 
                join  ds_tariffs t on tarid=t.id 
                join ds_users u on userid=u.id 
                where summ != '0' 
                group by userid having typetime > 0
            ) t";

$repays = $clients_with - $clients;
$max_repays = core::$db->query("select max(cnt) as count from (select username, count(username) as cnt from ds_paid group by username) as t")->count();
$avg_live = (int)core::$db->query($str)->count();


// Выдем переменные в шаблон
engine_head(lang('u_online'));
if(isset($search)!='')
temp::assign('search', $search);


temp::HTMassign('tarrif_select', $tarrif_select);
temp::HTMassign('out', $arr);
temp::HTMassign('marr', $month_names);
temp::HTMassign('msumm', $month_summ);
//temp::HTMassign('date', $date);
temp::assign('uall', $total);
temp::assign('clients_with', $clients_with);
temp::assign('clients', $clients);
temp::assign('repays', $repays);
temp::assign('max_repays', $max_repays);
temp::assign('avg_live', $avg_live);

temp::HTMassign('navigation', nav::display($total, core::$home.'/control/pays?', '', '', array('search'=>isset($search)?$search:'')));
//temp::HTMassign('navigation', nav::display($total, core::$home.'/control/pays?', '', '', array('search'=>$search,'date'=>$date)));

temp::display('control.pays');
engine_fin();