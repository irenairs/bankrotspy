<?php
defined('DS_ENGINE') or die('web_demon laughs');

$res = core::$db->query('SELECT *, `ds_maindata_category`.`name` FROM `ds_maindata_category_keys` LEFT JOIN `ds_maindata_category` ON `ds_maindata_category_keys`.`id` = `ds_maindata_category`.`id` ORDER BY `ds_maindata_category_keys`.`sort` ASC;');
$cats = array();
while($data = $res->fetch_array())
{
  $loc = array();
  $loc['id'] = $data['id'];
  $loc['name'] = $data['name'];
  $keywords = explode("\n", htmlspecialchars_decode(str_replace("\r\n", "\n", $data['keys'])));
  $loc['undtext'] =implode(', ', $keywords);
  $cats[] = $loc;
}

engine_head(lang('admin_control'));
temp::HTMassign('cats',$cats);
temp::display('control.keys');
engine_fin();