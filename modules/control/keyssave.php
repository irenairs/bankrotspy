<?php
defined('DS_ENGINE') or die('web_demon laughs');

  $res = core::$db->query('SELECT *, `ds_maindata_category`.`name` FROM `ds_maindata_category_keys` LEFT JOIN `ds_maindata_category` ON `ds_maindata_category_keys`.`id` = `ds_maindata_category`.`id` ORDER BY `ds_maindata_category_keys`.`sort` ASC;');
  $cats = array();
  $cat = (int) $_POST['cat'];
  $key = text::st(POST('key'));
  while($data = $res->fetch_array())
  {
    $loc = array();
    $loc['id'] = $data['id'];
    $loc['name'] = $data['name'];
    $keywords = explode("\n", htmlspecialchars_decode(str_replace("\r\n", "\n", $data['keys'])));
    if (in_array($key, $keywords)) {
      print json_encode(array('status'=>'error','message'=>'Ключ уже существует в категории: "'.$data['name'].'"'));
      exit;
    }
    $loc['keys'] = $keywords;
    $cats[$data['id']] = $loc;
  }
  $keywords = $cats[$cat]['keys'];
  $keywords[] = $key;
  array_walk($keywords, 'trim_value');
  $keywords = array_diff($keywords, array(''));
  sort($keywords);
  $keywords = implode("\n", $keywords);
  core::$db->query('UPDATE `ds_maindata_category_keys` SET `keys` = "' . core::$db->res(text::st($keywords)) . '" WHERE `id` = "' . $cat . '";');
  print json_encode(array('status'=>'success','message'=>'Ключ добавлен в категорию: "'.$cats[$cat]['name'].'"'));


function trim_value(&$value)
{
  $value = trim($value);
}