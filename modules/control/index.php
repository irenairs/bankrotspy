<?php
defined('DS_ENGINE') or die('web_demon laughs');

if( POST('submit') ) {
    $site_domain = text::st(POST('site_domain'));
    $site_name = text::st(POST('site_name'));
    $site_keywords = text::st(POST('site_keywords'));
    $site_description = text::st(POST('site_description'));
    $main_adv_text = text::st(POST('main_adv_text'));
    $private_attention = text::st(POST('private_attention'));
    $bun_visits_guest = text::st(POST('bun_visits_guest'));
    $number_attempts = text::st(POST('number_attempts'));
    $time_attempts = text::st(POST('time_attempts'));
    $bans_period = text::st(POST('bans_period'));

    $query = 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($site_domain).'" WHERE `key` = "site_name";';
    $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($site_name).'" WHERE `key` = "site_name_main";';
    $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($site_description).'" WHERE `key` = "description";';
    $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($site_keywords).'" WHERE `key` = "keywords";';
    $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($main_adv_text).'" WHERE `key` = "main_adv_text";';
    $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($private_attention).'" WHERE `key` = "private_attention";';
    
    //Проверка, есть ли в таблице настроек ключ bun_visits_guest
    $sql = "SELECT * FROM `ds_settings` WHERE `key` = 'bun_visits_guest'";
    $q = core::$db->query($sql);
    $rows = $q -> num_rows;
    if($rows == 0){
        $sql_u = "INSERT INTO `ds_settings` (`key`, `val`) VALUES ('bun_visits_guest', '{$bun_visits_guest}')"; 
        core::$db -> query($sql_u);
    } else {
    
        $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($bun_visits_guest).'" WHERE `key` = "bun_visits_guest";';
    }
    
    //Проверка, есть ли в таблице настроек ключ number_attempts
    $sql = "SELECT * FROM `ds_settings` WHERE `key` = 'number_attempts'";
    $q = core::$db->query($sql);
    $rows = $q -> num_rows;
    if($rows == 0){
        $sql_u = "INSERT INTO `ds_settings` (`key`, `val`) VALUES ('number_attempts', '{$number_attempts}')"; 
        core::$db -> query($sql_u);
    } else {
    
        $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($number_attempts).'" WHERE `key` = "number_attempts";';
    }
    
    //Проверка, есть ли в таблице настроек ключ bun_visits_guest
    $sql = "SELECT * FROM `ds_settings` WHERE `key` = 'time_attempts'";
    $q = core::$db->query($sql);
    $rows = $q -> num_rows;
    if($rows == 0){
        $sql_u = "INSERT INTO `ds_settings` (`key`, `val`) VALUES ('time_attempts', '{$time_attempts}')"; 
        core::$db -> query($sql_u);
    } else {
    
        $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($time_attempts).'" WHERE `key` = "time_attempts";';
    }
    
    //Проверка, есть ли в таблице настроек ключ bun_visits_guest
    $sql = "SELECT * FROM `ds_settings` WHERE `key` = 'bans_period'";
    $q = core::$db->query($sql);
    $rows = $q -> num_rows;
    if($rows == 0){
        $sql_u = "INSERT INTO `ds_settings` (`key`, `val`) VALUES ('bans_period', '{$bans_period}')"; 
        core::$db -> query($sql_u);
    } else {
    
        $query .= 'UPDATE `ds_settings` SET `val` = "'.core::$db->res($bans_period).'" WHERE `key` = "bans_period";';
    }
    core::$db->multi_query($query);
    core::$db->multi_free();

    $art_text = POST('mess');
    core::$db->query('UPDATE `ds_reg_page` SET
            `text` = "'.core::$db->res($art_text).'",
            `cache` = "'.core::$db->res(text::presave($art_text)).'"
            WHERE `id`="1" LIMIT 1;');

    func::notify(lang('base_settings'), lang('settings_saved'), core::$home . '/control', lang('continue'));
}

$res = core::$db->query('SELECT * FROM  `ds_reg_page` WHERE `id` = "1";');
$data = $res->fetch_assoc();
$error = array();

engine_head(lang('base_settings'));
temp::HTMassign('site_domain', core::$set['site_name']);
temp::HTMassign('site_name', core::$set['site_name_main']);
temp::HTMassign('site_keywords', core::$set['keywords']);
temp::HTMassign('site_description', core::$set['description']);
temp::HTMassign('main_adv_text', core::$set['main_adv_text']);
temp::HTMassign('private_attention', core::$set['private_attention']);
temp::assign('text',$data['text']);
temp::HTMassign('bun_visits_guest', core::$set['bun_visits_guest']);
temp::HTMassign('number_attempts', core::$set['number_attempts']);
temp::HTMassign('time_attempts', core::$set['time_attempts']);
temp::HTMassign('bans_period', core::$set['bans_period']);
temp::display('control.index');
engine_fin();