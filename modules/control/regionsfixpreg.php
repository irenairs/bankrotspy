<?php
defined('DS_ENGINE') or die('web_demon laughs');
set_time_limit(120);
$n = 0;
$messages = FALSE;
$succes = FALSE;
// Выбираем порцию лотов, у которых не определен регион
$lots = core::$db->query('SELECT * FROM `ds_maindata` WHERE place=0 OR place =85');

//Получаем id регионов
$regions = core::$db->query('SELECT id,number FROM `ds_maindata_regions` WHERE number>0');

//Выбираем все ключевики для регионов
while($region = $regions->fetch_array()){
	$regions_add = core::$db->query("SELECT name FROM `ds_maindata_regions_add` WHERE region_id=".$region['id']);
	while($reg_add = $regions_add->fetch_array()){
		$key_words[] = array('name' =>$reg_add['name'], 'id' =>$region['id']);	
	}
}

//Поиск полей в ds_maindata по ключевым словам регионов
while($lot = $lots->fetch_array()){
	 $save = False;
	 $lot_quanty++;
	if($lot['cat_id']==3 OR $lot['cat_id']==5 OR $lot['cat_id']==6){

		$reg = regFind($key_words,$lot['name']);
		$des = regFind($key_words,$lot['description']);

		if($reg!=FALSE){
			$save =htmlview($lot['id'],$reg,$lot['name']);
		}
		elseif($des!=FALSE){
			$save = htmlview($lot['id'],$des,$lot['description']); 
		}
	}
	else{
		$reg = regFind($key_words,$lot['name']);
		$des = regFind($key_words,$lot['description']);
		$sud = regFind($key_words,$lot['rawsud']);
		
		if($reg!=FALSE){
			$save =htmlview($lot['id'],$reg,$lot['name']);
		}
		elseif($des!=FALSE){
		$save = htmlview($lot['id'],$des,$lot['description']); 			
		}
		elseif($sud!=FALSE){
			$save = htmlview($lot['id'],$sud,$lot['rawsud']); 
		}
	}
	if($save){
		
		if(isset($_GET['save'])){
			
			core::$db->query('UPDATE `ds_maindata` SET `place`='.$save['id'].' WHERE `id`='.$lot['id']);
			$succes = 'Регионы успешно переопределены!';
		}
		$messages[] = $save;
		$n++;
	}
	
}

engine_head(lang('admin_control'));
temp::HTMassign('lot_quanty',$lot_quanty);
temp::HTMassign('succes',$succes);
temp::HTMassign('messages',$messages);
temp::HTMassign('count',$n);
temp::display('control.regionsfixpreg');
engine_fin();

//Формирование массива для вывода

function htmlview($lot,$id,$field){
	$nameRegs = core::$db->query('SELECT name FROM `ds_maindata_regions` WHERE id='.$id);
	
	
	while($nameReg2 = $nameRegs->fetch_object()){
		$nameReg =$nameReg2->name;	
	}
	if(!$nameReg)
	{
		die('Ошибка! ID - '.$id.' не соответствует не одному из регионов!');
	}
	return array('id'=>$id,'name'=>$nameReg,'field'=>$field,'lot'=>$lot);
	}
	
	
	
//Функция поиска названия региона в поле таблицы ds_maindata через регулярные выражения
 function regFind($kw,$desc)
	{
	
		foreach ($kw as $key => $qw) {
			
			$find = preg_match_all("/".$qw['name']."/Usu", $desc);
			if($find){
				$find_id = $qw['id'];
				$find = false;
				return $find_id;
			}
			
		}
		return false;
	}
	
	
	
	
//Функция посимвольного поиска названия региона в поле таблицы ds_maindata
function search_region($array,$string){

	for($i=0;$i<=count($array);$i++){
		$c=0;

		if(!empty($array[$i])){
			$s = mb_strstr($string,$array[$i]['name']);		
			if($s!=FALSE){
				$c++;
				break;
			}
		}
	}
	if($c>0 ){
		return $array[$i]['id'];
	}	
	return FALSE;	
}

