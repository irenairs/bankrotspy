<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    $refer = new refer();
    $key = GET('key');
    if(POST('action') == 'addPay'){
        $refer->payOut($key, POST('sum'), POST('date') ? strtotime(POST('date')) : time());
        header('Location:/control/refer?key=' . $key);
    }
    $data = $refer->getSummary($key);

    engine_head();
    temp::assignHTMulti([
        'paySummary' => $data['data'][$key],
        'pays' => $refer->getPayments($key)
    ]);
    temp::assignMulti([
        'referKey' => $key,
        'curWait' => $data['wait']['curWait'],
        'nextWait' => $data['wait']['nextWait']
    ]);
    temp::display('control.refer');
    engine_fin();