<?php 
	defined('DS_ENGINE') or die('web_demon laughs');
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	ini_set('memory_limit', '-1');
	set_time_limit (3000);
	$off=0;
	if (isset($_POST['p'])) {
		$off = (int)$_POST['p'];
	}
	$old = array();
	$all = array();
	if (isset($_POST['old'])) {
		foreach ($_POST['old'] as $key => $cat) {
			$old[$key] = explode('_+_',$cat);
			$all = array_merge($all,$old[$key]); 
		}
	} 
	$events = core::$db->query(
      'SELECT *
       FROM `ds_maindata` 
       ORDER BY `id` DESC
       limit 1000 OFFSET '.($off* 1000).';');
	$res = core::$db->query('SELECT *, `ds_maindata_category`.`name` FROM `ds_maindata_category_keys` LEFT JOIN `ds_maindata_category` ON `ds_maindata_category_keys`.`id` = `ds_maindata_category`.`id` ORDER BY `ds_maindata_category_keys`.`sort` ASC;');
	$keys = array();
	$cats = array();
	while($data = $res->fetch_array())
	{
	  $loc = array();
	  $loc['id'] = $data['id'];
	  $loc['name'] = $data['name'];
	  $keywords = explode("\n", htmlspecialchars_decode(str_replace("\r\n", "\n", $data['keys'])));
	  if ($data['id'] == $_POST['cat']) {
			$keywords[] = $_POST['key'];
	  }
	  foreach ($keywords as $key => $kt) {
			if (strpos($kt, '__') === 0) {
				$loc['anti'][] = $kt;
			} else {
				$loc['keys'][] = $kt;
			}
		}
	  $keys[] = $loc;
	  $ct = array(
	  	'name' => $data['name'],
	  	'id' => $data['id'],
	  	'items' =>array()
	  	);
	  $cats[$data['id']] = $ct;
	}
	$diff = array();
	$res = array();
	while ($event = $events->fetch_assoc()) {
		$desc = (strlen($event['name']) >= strlen($event['description'])?$event['name'].' '.$event['description']:$event['description'].' '.$event['name']);
		$fd = false;
		foreach ($keys as $kk => $kw) {
			$find = catFind($kw,$desc,$event,$cats);
			if ($find) {
				if (!in_array($event['id'], $old[$kw['id']])) {
					$diff[] = '<b style="color:green">'.$kw['name'].'</b> '.$find.'<br>';
				}
				$fd = true;
				break;
			}
		}
	}
	print json_encode($diff);

	function catFind($kw,$desc,$event,$cats)
	{
		if (isset($kw['anti']) && count($kw['anti'])) {
			foreach ($kw['anti'] as $key => $value) {
				$qw = substr($value, 2);
				$big = preg_match_all("/[А-Я]/Usu",$qw);
				if ($big) {
					$findanti = preg_match_all("/".$qw."/Usu", $desc);
				} else {
					//print $qw.'<br>';
					$findanti = preg_match_all("/".$qw."/Uisu", $desc);
				}
				if ($findanti) {
					return false;
				}
			}
		}
		foreach ($kw['keys'] as $key => $qw) {
			//$zap = '"/'.$qw.''
			$big = preg_match_all("/[А-Я]/Usu",$qw);
			if ($big) {
				$find = preg_match_all("/".$qw."/Usu", $desc);
			} else {
				//print $qw.'<br>';
				$find = preg_match_all("/".$qw."/Uisu", $desc);
			}
			if ($find) {
				$catName = 'Нераспределено';
				if ($event['cat_id']) {
					$catName = $cats[$event['cat_id']]['name'];
				}
				return $qw.' '.$find.' <b style="color:red">'.$catName.'</b> <a href="/card/'.$event['id'].'">'.$event['id'].'</a> - '.$desc;
			}
		}
		return false;
	}
 ?>