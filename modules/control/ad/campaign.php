<?php
    defined('DS_ENGINE') or die('access denied');

    engine_head('Редактирование рекламной кампании');

    $ad_campaigns = new model(null, 'ad_campaigns');
    $ad_places = new model(null, 'ad_places');
    
    temp::HTMassign('places', $ad_places->select(['id', 'descr'])->order('`descr` ASC')->fetchAll('id', 'descr'));
    
    $mail_mailing = new model(null, 'mail_mailing');
    $mail_mailing_files = new model(null, 'mail_mailing_files');

    if(!empty($_POST)) {
        $prm = [
            'pid' => $_POST['pid'],
            'tm_from' => strtotime($_POST['tm_from']),
            'tm_to' => strtotime($_POST['tm_to']),
            'priority' => $_POST['priority'],
            'link' => htmlspecialchars($_POST['link']),
            'img' => $_POST['img'],
            'text' => htmlspecialchars($_POST['text']),
            'btn' => htmlspecialchars($_POST['btn']),
            'active' => isset($_POST['active']) ? 1 : 0
        ];
        $cid = !empty($_POST['id']) ? intval($_POST['id']) : 0;
    
        if(!empty($cid)) {
            $dirTmp = 'data/ad/' . $cid;
            $dir = system::realPath($dirTmp);
            $ad_campaigns->updateByField('id', $cid, $prm);
            $msg = 'Рекламная кампания успешно обновлена';
        } else {
            if($ad_campaigns->insert($prm)){
                $cid = $ad_campaigns->insertId();
                $dirTmp = 'data/ad/' . $cid;
                $dir = system::realPath($dirTmp);
                mkdir($dir, 0777);
            }
            $msg = 'Рекламная кампания успешно создана';
    
            if ($image = $_POST['image']) {
                $tmpFile = system::realPath('data/ad/tmp/' . $image);
                $file = $dir . '/' . $image;
                copy($tmpFile, $file);
                unlink($tmpFile);
                $ad_campaigns->updateByField('id', $cid, [
                    'img' => '/data/ad/' . $cid . '/' . $image
                ]);
            }
        }
    }

    if(isset($_GET['id']) && isset($_GET['action'])){
        $id = intval($_GET['id']);
        switch ($_GET['action']){
            case 'edit':
                temp::assignHTMulti([
                    'campaign' => $ad_campaigns->where('id', $id)->fetch()
                ]);
                break;
        }
    }
    
    if(isset($msg)){
        func::notify('Реклама', $msg, '/control/ad');
    }

    temp::display('control/ad/campaign');
    engine_fin();
