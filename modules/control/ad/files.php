<?php
    defined('DS_ENGINE') or die('access denied');

    $response = [];

    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    $filePath = ($_GET['action'] == 'delete')
        ? $_GET['file']
        : ('data/ad/' . ($id ? $id : 'tmp') . '/' . strtolower(basename($_FILES['image']['name'])));
    $fileDest = system::realPath($filePath);

    $ad_campaigns = new model(null, 'ad_campaigns');

    if($_GET['action'] == 'upload') {
        $fileSource = $_FILES['image']['tmp_name'];
        if(upload($fileSource, $fileDest)) {
            $response = [
                'error' => 0,
                'name' => '/' . $filePath,
                'info' => $_FILES
            ];
        } else {
            $response = [
                'error' => 1,
                'src' => $fileSource,
                'info' => $_FILES
            ];
        }
    }

    if($_GET['action'] == 'delete') {
        unlink($fileDest);
        if($id){
            $ad_campaigns->updateByField('id', $id, [
                'img' => ''
            ]);
        }
    }

    ajax_response($response);

    function upload($source, $destination) {
        return !!move_uploaded_file($source, $destination);
    }