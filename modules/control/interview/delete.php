<?php

defined('DS_ENGINE') or die('access denied');

if (isset($_POST['delete'])){
    core::$db->query('delete from interview where id="'.$_POST['id'].'"');
    core::$db->query('delete from answer where interview_id="'.$_POST['id'].'"');
    func::notify('Опрос', 'Опрос удален', core::$home . '/control/interview');
}

$answer=[];
$res = core::$db->query('SELECT * FROM `interview` where id="'.$_REQUEST['id'].'"');
$data = $res->fetch_array();

$res2 = core::$db->query('SELECT * FROM `answer` where interview_id="'.$_REQUEST['id'].'"');
while ($data2 = $res2->fetch_array()){
    $answer[]=$data2;
}

engine_head('Удаление опроса');
temp::HTMassign('data', $data);
temp::HTMassign('answer', $answer);
temp::display('control/interview/delete');
engine_fin();
