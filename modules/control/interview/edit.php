<?php

defined('DS_ENGINE') or die('access denied');

if (isset($_POST['edit'])){
    if (count($_POST['question'])<2){
        func::notify('Ошибка', 'Ответов должно быть минимум 2', core::$home . '/control/interview/edit');
    } else {
        core::$db->insert('UPDATE `interview` set `name`="'.$_POST['name'].'", `status`="'.$_POST['status'].'" where id="'.$_POST['id'].'"');
        $max=$_POST['id'];
        core::$db->query("delete from answer where interview_id='$max'");
        foreach ($_POST['question'] as $q){
            core::$db->insert('INSERT INTO `answer` (`answer`, `interview_id`) 
                                VALUES ("'.$q.'", "'.$max.'")');
        }
        func::notify('Опрос', 'Опрос обновлен', core::$home . '/control/interview');
    }
}

$answer=[];
$res = core::$db->query('SELECT * FROM `interview` where id="'.$_REQUEST['id'].'"');
$data = $res->fetch_array();

$res2 = core::$db->query('SELECT * FROM `answer` where interview_id="'.$_REQUEST['id'].'"');
while ($data2 = $res2->fetch_array()){
    $answer[]=$data2;
}

$res3 = core::$db->query('SELECT * FROM `answer` where interview_id="' . $_REQUEST['id'] . '"');
$max=0;
while ($data3 = $res3->fetch_array()) {
    $id=$data3['id'];
    $rz=core::$db->query("select count(*) from answer_result where interview_id='".$_REQUEST['id']."' and `value`='$id'");
    $rz=$rz->fetch_array();
    $max=$rz[0]>$max?$rz[0]:$max;
    $answer2[]=[
        'name'=>$data3['answer'],
        'count'=>$rz[0],
    ];
}

engine_head('Редактирование опроса');
temp::HTMassign('data', $data);
temp::HTMassign('answer', $answer);
temp::HTMassign('answer2', $answer2);
temp::HTMassign('max', $max);
temp::display('control/interview/edit');
engine_fin();
