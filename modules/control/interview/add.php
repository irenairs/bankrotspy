<?php

defined('DS_ENGINE') or die('access denied');

if (isset($_POST['add'])){
    $_SESSION['opr']=$_POST;
    if (count($_POST['question'])<2){
        func::notify('Ошибка', 'Ответов должно быть минимум 2', core::$home . '/control/interview/add');
    } else {
        core::$db->insert('INSERT INTO `interview` (`name`, `data`, `status`) 
                                VALUES ("'.htmlspecialchars($_POST['name']).'", NOW(), "'.$_POST['status'].'")');
        $max=core::$db->query('SELECT LAST_INSERT_ID()');
        $max=$max->fetch_all();
        $max=$max[0]['LAST_INSERT_ID()'];
        foreach ($_POST['question'] as $q){
            core::$db->insert('INSERT INTO `answer` (`answer`, `interview_id`) 
                                VALUES ("'.htmlspecialchars($q).'", "'.$max.'")');
        }
        func::notify('Опрос', 'Опрос добавлен', core::$home . '/control/interview');
        unset($_SESSION['opr']);
    }
}

engine_head('Добавление опроса');
temp::display('control/interview/add');
engine_fin();
