<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    $refer = new refer();
    $data = $refer->getSummary();

    engine_head();
    temp::assignHTMulti([
        'data' => $data['data']
    ]);
    temp::assignMulti([
        'curWait' => $data['wait']['curWait'],
        'nextWait' => $data['wait']['nextWait']
    ]);
    temp::display('control.refersum');
    engine_fin();