<?php
defined('DS_ENGINE') or die('web_demon laughs');
$error = array();
$id = abs(intval(GET('id')));

//Запрос к таблице регионов с данным id региона (ожидается одна запись в ответе mysql)
$res1 = core::$db->query('SELECT * FROM `ds_maindata_regions` WHERE `number`='.$id);
if ($res1) {
   $data1 = $res1->fetch_assoc();
   if(!$data1['mini'])
      denied();
}

//Запрос к таблице ключевых слов для определения регионов
$keywords = '';
$res2 = core::$db->query('SELECT * FROM `ds_maindata_regions_add` WHERE `region_id`='.$id);
if (!$res2) {
   $error[] = lang('no_keys');
}
while ($data2 = $res2->fetch_assoc()) {
   $keywords .= $data2['name']."\r\n"; //СОбираем все ключевые слова в строку $keywords
}

//Действие по сохранению формы с ключевыми словами в таблицу ключевых слов
if(POST('submit'))
{
  $keywords1 = text::st(POST('keywords'));
  if(!$keywords1)
    $error[] = lang('no_keys');

  if(!$error)
  {
    //Сначала удаляем все клчевые слова по данному региону из таблицы ключевых слов 
    $q1 = 'DELETE FROM `ds_maindata_regions_add` WHERE `region_id`='.$id;
    core::$db->query($q1);

    $keywords_array = explode ( "\n", $keywords1 );

    //Затем создаём заново записи с ключевыми словами по текущему региону (по каждой ключевой фразе отдельная запись).
    foreach ( $keywords_array as $one_keyword ) {
         $keyword = trim($one_keyword);
         // Запрос на то, что такая ключевая фраза уже сущетвует
         $q2 = 'SELECT * FROM `ds_maindata_regions_add` WHERE `name` LIKE "'.$one_keyword.'";';
         $res2 = core::$db->query($q2);
         $row2 = $res2->fetch_array();
         if ($row2['name']===NULL) { //Если такой ключевой фразы нет в таблице, то ...
            $q3 = 'INSERT INTO `ds_maindata_regions_add` (`region_id`, `name`) VALUES ( '.$id.', "'.core::$db->res(text::st($keyword)).'");';
            core::$db->query($q3);
         }
    }
    //Выдаём пользователю админки сообщение что всё сохранено удачно.
    func::notify(lang('edit_item'), lang('new_item_changed'), core::$home . '/control/regions', lang('continue'));
  }
}


function trim_value(&$value)
{
  $value = trim($value);
}

engine_head(lang('edit_item'));
temp::assign('number', $data1['number']);
temp::assign('name', $data1['mini']);
temp::assign('keywords', $keywords);
temp::HTMassign('error', $error);
temp::display('control.editregion'); //Вызываем view для данной модели.
engine_fin();
