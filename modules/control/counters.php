<?php
defined('DS_ENGINE') or die('web_demon laughs');


// Подсует и выборка транзакций
$total = core::$db->query('SELECT COUNT(*) FROM `ds_counts`;')->count();

$res = core::$db->query('SELECT *, count(*) as cnt FROM `ds_counts` GROUP BY monthyear ORDER BY `counttime` DESC');

$i = 0;
$arr = array();
$month_names = array(
    1 => 'Январь',
    2 => 'Февраль',
    3 => 'Март',
    4=> 'Апрель',
    5=> 'Май',
    6=> 'Июнь',
    7=> 'Июль',
    8=> 'Август',
    9=> 'Сентябрь',
    10=> 'Октябрь',
    11=> 'Ноябрь',
    12=> 'Декабрь'
);

$page_names = array(
    1 => array('Новости', 'http://bankrot-spy.ru/news'),
    2 => array('Тарифы', 'http://bankrot-spy.ru/tariffs'),
    3 => array('Услуги', 'http://bankrot-spy.ru/pages/services'),
    4=> array('Для инвесторов', 'http://bankrot-spy.ru/invest'),
    5=> array('Мануал', 'http://bankrot-spy.ru/pages/manual-bankrot-spy'),
    6=> array('Топ лотов', 'http://bankrot-spy.ru/toplots'),
    7=> array('Статистика', 'http://bankrot-spy.ru/stats'),
    8=> array('Победители', 'http://bankrot-spy.ru/winners'),
    9=> array('Арбитражные управляющие', 'http://bankrot-spy.ru/amc'),
    10=> array('Торговые площадки', 'http://bankrot-spy.ru/platforms'),
    11=> array('Должники', 'http://bankrot-spy.ru/debtors'),
    12=> array('Чат для клиентов', 'http://bankrot-spy.ru/forum'),
    13=> array('Регистрация', 'http://bankrot-spy.ru/register'),
    14=> array('Регистрация: отправка формы', 'http://bankrot-spy.ru/register'),
    15=> array('Регистрация: подтверждение', '---'),
    16=> array('Неудачные попытки оплаты тарифов', '---'),
    17=> array('Создание поисковых профилей', '---'),
    18=> array('Подписки на рассылку лотов', '---'),
    19=> array('Оформление заявки для ОТ', 'http://bankrot-spy.ru/zayavka'),
    20=> array('Экспорт в Иксель', '---'),
    21=> array('Приватизация', 'http://bankrot-spy.ru/privatized'),
    22=> array('Конфискат', 'http://bankrot-spy.ru/confiscated'),
    23=> array('Банкротство', 'http://bankrot-spy.ru/torgi'),
    24=> array('Главная', 'http://bankrot-spy.ru'),
    25=> array('Техподдержка', 'http://bankrot-spy.ru/support'),
    26=> array('Статьи', 'http://bankrot-spy.ru/articles'),

);

function cmp($a, $b)
{
    if ($a['cnt'] == $b['cnt']) {
        return 0;
    }
    return ($a['cnt'] < $b['cnt']) ? 1 : -1;
}

while($data = $res->fetch_assoc()) {
    $out = array();

    $out['id'] =  $data['id'];
    $out['page'] = $data['page'];
    $out['counttime'] =  ds_time($data['counttime'], '%d %B2 %Y');
    $out['time'] =  date('h:m:s', $data['counttime']);
    $out['cnt'] = $data['cnt'];
    $month = date('n', $data['counttime']);
    $year = date('Y', $data['counttime']);

    $arr[$year][$month][$out['page']] = $out;
    $i++;
}

foreach ($arr as $year => $data) {
    foreach ($data as $month => $value) {
        usort($arr[$year][$month], "cmp");
    }
}


// Выдем переменные в шаблон
engine_head(lang('counters'));
temp::HTMassign('out', $arr);
temp::HTMassign('marr', $month_names);
temp::HTMassign('parr', $page_names);

temp::display('control.counters');
engine_fin();