<?php
defined('DS_ENGINE') or die('web_demon laughs');
/*error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1); // включаем отображение ошибок*/
//Выбираем статистику за 5 дней

//$dateStart = date_format(date_create(date("Y-m-d", strtotime('-4 days'))." 00:00:00"),'U');
$dateEnd = date_format(date_create(date("Y-m-d")),'U');
$result = [];
for($i = 0; $i < 5; $i++){
   $dateStart = date_format(date_create(date("Y-m-d", strtotime('-'.$i.' days'))),'U'); 
    $query = "SELECT t.user_id, t.user_ip, t.day,  SUM( t.count ) AS total, u.login, u.mail, u.rights
        FROM  `ds_users_traffic` AS t
        LEFT JOIN ds_users AS u ON t.user_id = u.id
        WHERE day = '{$dateStart}'
        GROUP BY user_id, user_ip, day
        ORDER BY day DESC, total DESC LIMIT 20";
    $day = date('d.m.Y', $dateStart);    
    $result[$day] = [];
    $req = core::$db->query($query);
    
    while ($r = $req -> fetch_assoc()){
        
       $day = date('d.m.Y', $r['day']);
       //if(!isset($result[$day])) $result[$day] = [];
       //Страницы для данного пользователя
       $sql = "SELECT `page`, `count` FROM `ds_users_traffic` WHERE day = '{$r['day']}'"
                . "AND user_id = '{$r['user_id']}' "
                . " AND user_ip = '{$r['user_ip']}' "
                . " ORDER BY `count` DESC";
        $q = core::$db->query($sql);
        $r['pages'] = [];
        while ($row = $q -> fetch_assoc()){
            $r['pages'][] = $row;
        }
        $result[$day][] = $r; 
    }
}

//Удаляем лишнее
if(isset($dateStart)){
    $query = "DELETE FROM `ds_users_traffic` WHERE `day` < '{$dateStart}'";
    $q = core::$db->query($query);
}    

// Выдем переменные в шаблон
engine_head(lang('traffic'));
temp::HTMassign('array_for_traffic',$result);
temp::HTMassign('dateStart',$dateStart);
temp::HTMassign('dateEnd',$dateEnd);
temp::display('control.traffic');
engine_fin();

