<?php 

	defined('DS_ENGINE') or die('web_demon laughs');
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	ini_set('memory_limit', '-1');
	set_time_limit (3000);
	$revent = $data = array();
	$off=0;
	if (isset($_GET['p'])) {
		$off = (int)$_GET['p'];
	}
	$filtr = 0;
	if (isset($_GET['f'])) {
		$filtr = (int) ($_GET['f']);
	}
	$update = 0;
	if (isset($_POST['update']) && isset($_POST['update'])) {
		$update = (int) $_POST['update'];

		$events = core::$db->query(
	      'SELECT *
	       FROM `ds_maindata` 
	       WHERE `loadtime` >= '.$update.';');
	} else {
		$events = core::$db->query(
	      'SELECT *
	       FROM `ds_maindata` 
	       ORDER BY `id` DESC
	       limit 1000 OFFSET '.($off* 1000).';');
	}

	$res = core::$db->query('SELECT *, `ds_maindata_category`.`name` FROM `ds_maindata_category_keys` LEFT JOIN `ds_maindata_category` ON `ds_maindata_category_keys`.`id` = `ds_maindata_category`.`id` ORDER BY `ds_maindata_category_keys`.`sort` ASC;');
	$cats = array();
	$keys = array();
	while($cat = $res->fetch_assoc())
	{
		$cats[$cat['id']]['id'] = $cat['id'];
		$cats[$cat['id']]['name'] = $cat['name'];
		$cats[$cat['id']]['items'] = array();
		$loc = array();
		$loc['id'] = $cat['id'];
		$loc['name'] = $cat['name'];
		$loc['anti'] = array();
		$keywords = explode("\n", htmlspecialchars_decode(str_replace("\r\n", "\n", $cat['keys'])));
		foreach ($keywords as $key => $kt) {
			if (strpos($kt, '__') === 0) {
				$loc['anti'][] = $kt;
			} else {
				$loc['keys'][] = $kt;
			}
		}
		$keys[$cat['id']] = $loc;
	}
	$count = core::$db->query(
      'SELECT count(*) as total
       FROM `ds_maindata`;');
	$dt=$count->fetch_assoc();
	$count = $dt['total'];
	$pages = (int) ($count/1000);
	while ($event = $events->fetch_assoc()) {
		$desc = (strlen($event['name']) >= strlen($event['description'])?$event['name'].' '.$event['description']:$event['description'].' '.$event['name']);
		
		$fd = false;
		foreach ($keys as $kk => $kw) {
			$find = catFind($kw,$desc,$event,$cats);
			if ($find) {
				if (!$filtr || ($kw['id'] != $event['cat_id'])) {
					$cats[$kw['id']]['items'][] = $event['id'];
					$revent[$event['id']] = $find;
					if ($update) {
						core::$db->query('UPDATE `ds_maindata` SET `cat_id` = "' . $kw['id'] . '" WHERE `id` = "' . $event['id'] . '";');
					}
				}
				$cats[$kw['id']]['all'][] = $event['id'];
				$fd = true;
				break;
			}
		}
		if (!$fd) {
			if (!$filtr || $event['cat_id']) {
				$catName = 'Нераспределено';
				if ($event['cat_id'] && isset($cats[$event['cat_id']])) {
					$catName = $cats[$event['cat_id']]['name'];
				}
				if ($update) {
					core::$db->query('UPDATE `ds_maindata` SET `cat_id` = "0" WHERE `id` = "' . $event['id'] . '";');
				}
			$data[] = '<a href="/card/'.$event['id'].'">'.$event['id'].'</a> <b style="color:red">'.$catName.'</b> - '.$desc;
			}
		}
	}
	
	engine_head(lang('title'),lang('keys'),lang('description'));
	temp::HTMassign('cats', $cats);
	temp::HTMassign('revent', $revent);
	temp::HTMassign('data', $data);
	temp::HTMassign('pages', $pages);
	temp::HTMassign('filtr', $filtr);
	temp::HTMassign('off', $off);
	temp::display('control.keysfix');

	function catFind($kw,$desc,$event,$cats)
	{
		if (isset($kw['anti']) && count($kw['anti'])) {
			foreach ($kw['anti'] as $key => $value) {
				$qw = substr($value, 2);
				$big = preg_match_all("/[А-Я]/Usu",$qw);
				if ($big) {
					$findanti = preg_match_all("/".$qw."/Usu", $desc);
				} else {
					//print $qw.'<br>';
					$findanti = preg_match_all("/".$qw."/Uisu", $desc);
				}
				if ($findanti) {
					return false;
				}
			}
		}
		foreach ($kw['keys'] as $key => $qw) {
			$big = preg_match_all("/[А-Я]/Usu",$qw);
			if ($big) {
				$find = preg_match_all("/".$qw."/Usu", $desc);
			} else {
				$find = preg_match_all("/".$qw."/Uisu", $desc);
			}
			if ($find) {
				$catName = 'Нераспределено';
				if ($event['cat_id']) {
					$catName = $cats[$event['cat_id']]['name'];
				}
				return $qw.' '.$find.' <b style="color:red">'.$catName.'</b> <a href="/card/'.$event['id'].'">'.$event['id'].'</a> - '.$desc;
			}
		}
		return false;
	}
 ?>