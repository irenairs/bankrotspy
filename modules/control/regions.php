<?php
defined('DS_ENGINE') or die('web_demon laughs');

//Берём все регионы в массив
$res1 = core::$db->query('SELECT * FROM `ds_maindata_regions` ORDER BY `short` ASC;');

$rmenu = array();
while($region = $res1->fetch_array())
{
   if ( $region['number'] > 0 ) {
     $loc = array();
     $loc['id'] = $region['id'];
     $loc['number'] = $region['number'];
     $loc['name'] = $region['mini'];
     $keywords = array();
     //Выбираем все ключевые слова, у которых регион равен текущему региону.
     $res2 = core::$db->query('SELECT * FROM `ds_maindata_regions_add` WHERE `region_id`='.$region['number'].' ORDER BY `id` ASC;');
     while ($data = $res2->fetch_array()) {
         $keywords[] = $data['name'];//Добавляем клучевые слова в массив $keywords
     }
     $loc['undtext'] =implode(', ', $keywords); //Преобразуем массив ключевых слов в строку ключевых слов, разделённых запятыми.
     $rmenu[] = $loc;
   }  
}

engine_head(lang('admin_control'));
temp::HTMassign('rmenu',$rmenu);
temp::display('control.regions');//Вызываем view для данной модели.
engine_fin();
