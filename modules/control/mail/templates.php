<?php
    defined('DS_ENGINE') or die('access denied');

    $mail_templates = new model(null, 'mail_templates');
    if(isset($_GET['save'])) {
        foreach ($_POST['template'] as $id => $data) {
            $mail_templates->updateByField('id', $id, [
                'subject' => htmlspecialchars($data['subject']),
                'template' => htmlspecialchars($data['template'])
            ]);
        }
    }

    $templates = $mail_templates->reset()->order('`sort` ASC')->fetchAll();

    engine_head('Управление шаблонами');
    temp::HTMassign('templates', $templates);
    temp::display('control/mail/templates');
    engine_fin();
