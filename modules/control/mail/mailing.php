<?php
    defined('DS_ENGINE') or die('access denied');

    engine_head('Редактирование рассылки');

    $mail_mailing = new model(null, 'mail_mailing');
    $mail_mailing_files = new model(null, 'mail_mailing_files');
    $ds_users = new view(null, 'ds_users');

    function prepare_text($text, $images, $dir){
        $replace_files = function($match) use ($images, $dir) {
            if(in_array($match[2], $images)) {
                $link = core::$home . '/' . $dir . '/' .$match[2];
                $image = '<a target="_blank" href="'.$link.'"><img style="width:100%; height:auto;" alt="'.$match[3].'" src="'.$link.'"/></a>';
                return $image;
            }
            return $match[2];
        };

        $text = preg_replace_callback('/\[(file|img)\=([^\n\&\/\"\\\\<\>\+\&\;\:]{1,200})\](.*?)\[\/\1\]/', $replace_files, $text);
        return $text;
    }

    if(!empty($_POST)) {
        $subject = htmlspecialchars($_POST['subject']);
        $text = htmlspecialchars($_POST['text_source']);
        $groups = $_POST['groups'];
        $mailId = !empty($_POST['id']) ? intval($_POST['id']) : 0;
    
        if(!empty($mailId)) { // обновляем рассылку
            $dirTmp = 'data/mailing/' . $mailId;
            $dir = system::realPath($dirTmp);
            $text_compiled = prepare_text(text::out($text), $_POST['images'], $dirTmp);
            $mail_mailing->updateByField('id', $mailId, [
                'text_source' => $text,
                'text_compiled' => $text_compiled,
                'subject' => $subject,
                'groups' => $groups
            ]);
            $msg = 'Рассылка успешно обновлена';
        } else {
            if($mail_mailing->insert([
                'text_source' => $text,
                'created' => time(),
                'subject' => $subject,
                'groups' => $groups
            ])){
                $mailId = $mail_mailing->insertId();
                $dirTmp = 'data/mailing/' . $mailId;
                $dir = system::realPath($dirTmp);
                mkdir($dir, 0777);
                $text_compiled = prepare_text(text::out($text), $_POST['images'], $dirTmp);
                $mail_mailing->reset()->updateByField('id', $mailId, [
                    'text_compiled' => $text_compiled
                ]);
            }
            $msg = 'Рассылка успешно создана';
    
            if (!empty($_POST['images'])) {
                foreach ($_POST['images'] as $image) {
                    $mail_mailing_files->insert([
                        'mail_id' => $mailId,
                        'name' => $image,
                        'type' => 'image'
                    ]);
                    $tmpFile = system::realPath('data/mailing/tmp/' . $image);
                    $file = $dir . '/' . $image;
                    copy($tmpFile, $file);
                    unlink($tmpFile);
                }
            }
    
            if (!empty($_POST['files'])) {
                foreach ($_POST['files'] as $file) {
                    $mail_mailing_files->insert([
                        'mail_id' => $mailId,
                        'name' => $file,
                        'type' => 'attachments'
                    ]);
                    $tmpFile = system::realPath('data/mailing/tmp/' . $file);
                    $file = $dir . '/' . $file;
                    copy($tmpFile, $file);
                    unlink($tmpFile);
                }
            }
        }
    }

    if(isset($_GET['id']) && isset($_GET['action'])){
        $id = intval($_GET['id']);
        switch ($_GET['action']){
            case 'test':
                $mail = $mail_mailing->where('id', $id)->limit(1)->fetch();
                $users = $ds_users->where(['mail' => system::$config['email']['test_mailing']])->fetchAll();
                $mail_mailing_files = new view(null, 'mail_mailing_files');
                $files = [];
                if($attachments = $mail_mailing_files->where([
                    'mail_id' => $id,
                    'type' => 'attachments'
                ])->fetchAll()){
                    foreach ($attachments as $item){
                        $files[] = [
                            'path' => system::realPath('data/mailing/' . $id . '/' . $item['name']),
                            'name' => $item['name']
                        ];
                    }
                }
                foreach ($users as $user){
                    $body = [
                        'host'  => core::$home,
                        'text'  => $mail['text_compiled'],
                        'hash'  => system::hash($user['id'])
                    ];
                    $mailer = mailer::factory(system::realPath('engine/templates') . '/');
                    $mailer->setSubject($mail['subject']);
                    $mailer->setBody('mailing', $body);
                    $mailer->addAddress($user['mail']);
                    foreach ($files as $file) {
                        $mailer->addAttachment($file['path'], $file['name']);
                    }
                    $mailer->send();
                }
                exit();
                break;
            case 'edit':
                temp::assignHTMulti([
                    'mail' => $mail_mailing->where('id', $id)->limit(1)->fetch(),
                    'images' => $mail_mailing_files->where(['type' => 'image', 'mail_id' => $id])->fetchAll(),
                    'attachments' => $mail_mailing_files->where(['type' => 'attachments', 'mail_id' => $id])->fetchAll()
                ]);
                break;
            case 'start':
                $task = $mail_mailing->where('id', $id)->limit(1)->fetch();
                $upDate = ['status' => 1];
                if(!(int)$task['start_time']){
                    $upDate['start_time'] = time();
                }
                $mail_mailing->updateByField('id', $id, $upDate);
                $msg = 'Рассылка запущена';
                break;
            case 'stop':
                $mail_mailing->updateByField('id', $id, ['status' => 2]);
                $msg = 'Рассылка остановлена';
                break;
            case 'delete':
                $mail_mailing->deleteByField('id', $id);
                $files = $mail_mailing_files->select('name')->where('mail_id', $id)->fetchAll(null, 'name');
                foreach ($files as $file_name){
                    unlink(system::realPath('data/mailing/' . $id . '/' . $file_name));
                }
                rmdir(system::realPath('data/mailing/' . $id));
                $mail_mailing_files->deleteByField('mail_id', $id);
                $msg = 'Рассылка успешно удалена';
                break;
        }
    }
    
    if(isset($msg)){
        func::notify('Рассылка', $msg, core::$home . '/control/mail/mailinglist');
    }

    temp::display('control/mail/create');
    engine_fin();
