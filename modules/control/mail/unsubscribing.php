<?php
    defined('DS_ENGINE') or die('access denied');

    $ds_users = new view(null, 'ds_users');
    $users = $ds_users->select(['id', 'login'])->where('`subscribe` = "0"')->fetchAll();

    engine_head('Отписавшиеся');
    temp::HTMassign('users', $users);
    temp::display('control/mail/unsubscribing');
    engine_fin();
