<?php
    defined('DS_ENGINE') or die('access denied');

    $mailing = [];
    $statusMap = [
        '0' => 'Новая',
        '1' => 'Запущена',
        '2' => 'Остановлена',
        '3' => 'Завершена'
    ];
    
    $mail_mailing = new model(null, 'mail_mailing');
    $items = $mail_mailing->order('`id` DESC')->fetchAll();

    $ds_users = new view(null, 'ds_users');
    $totalUsers = $ds_users->where('subscribe', 1)->count();

    $i = 0;
    foreach ($items as $mail) {
        $mailing[] = [
            'id' => $mail['id'],
            'subject' => $mail['subject'],
            'status' => $statusMap[$mail['status']],
            'status_act' => $mail['status'],
            'created' => !empty($mail['created']) ? date('d.m.Y H:i:s', $mail['created']) : '-',
            'start' => !empty($mail['start_time']) ? date('d.m.Y H:i:s', $mail['start_time']) : '-',
            'end' => !empty($mail['end_time']) ? date('d.m.Y H:i:s', $mail['end_time']) : '-',
            'totalsent' => $mail['user_ok'],
            'totaluser' => $mail['status'] < 3 ? $totalUsers : $mail['user_total']
        ];
    }

    engine_head('Cписок рассылок');
    temp::HTMassign('mailing', $mailing);
    temp::display('control/mail/mailinglist');
    engine_fin();