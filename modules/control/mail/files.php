<?php
    defined('DS_ENGINE') or die('access denied');

    $response = [];

    $fileType = isset($_GET['type']) ? $_GET['type'] : 'file';
    $fileName = ($_GET['action'] == 'delete') ? $_GET['file'] : strtolower(basename($_FILES[$fileType]['name']));

    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    $fileDest = system::realPath('data/mailing/' . ($id ? $id : 'tmp')) . '/' . $fileName;

    $mail_mailing_files = new model(null, 'mail_mailing_files');

    if($_GET['action'] == 'upload') {
        $fileSource = $_FILES[$fileType]['tmp_name'];
        if(upload($fileSource, $fileDest)) {
            if($id){
                $mail_mailing_files->insert([
                    'mail_id' => $id,
                    'name' => $fileName,
                    'type' => $fileType
                ]);
            }
            $response = [
                'error' => 0,
                'name' => $fileName,
                'info' => $_FILES
            ];
        } else {
            $response = [
                'error' => 1,
                'src' => $fileSource,
                'info' => $_FILES
            ];
        }
    }

    if($_GET['action'] == 'delete') {
        unlink($fileDest);
        if($id){
            $mail_mailing_files->deleteByField([
                'name' => $_GET['file'],
                'mail_id' => $id
            ]);
        }
    }

    ajax_response($response);

    function upload($source, $destination) {
        return !!move_uploaded_file($source, $destination);
    }