<?php
defined('DS_ENGINE') or die('web_demon laughs');
$error = array();
$id = abs(intval(GET('id')));

if(!$id)
  denied();

$req = core::$db->query('SELECT *, `ds_maindata_category`.`name` FROM `ds_maindata_category_keys` LEFT JOIN `ds_maindata_category` ON `ds_maindata_category_keys`.`id` = `ds_maindata_category`.`id` WHERE `ds_maindata_category_keys`.`id` = "'.$id.'" LIMIT 1 ;');
if(!$req->num_rows)
  denied();

$data = $req->fetch_assoc();
if(POST('submit'))
{
  $name = text::st(POST('name'));
  if(!$name)
    $error[] = lang('no_name');
  $keys = text::st(POST('keywords_lot'));
  if(!$keys)
    $error[] = lang('no_keys');

  if(!$error)
  {
    if($name != $data['name'])
      core::$db->query('UPDATE `ds_maindata_category` SET `name` = "' . core::$db->res(text::st($name)) . '" WHERE `id` = "' . $id . '";');

    if($keys != $data['keys'])
    {
      $keywords = explode("\n", str_replace("\r\n", "\n", $keys));
      array_walk($keywords, 'trim_value');
      $keywords = array_diff($keywords, array(''));
      sort($keywords);
      $keywords = implode("\n", $keywords);
      core::$db->query('UPDATE `ds_maindata_category_keys` SET `keys` = "' . core::$db->res(text::st($keywords)) . '" WHERE `id` = "' . $id . '";');
    }
    func::notify(lang('edit_item'), lang('new_item_changed'), core::$home . '/control/keys', lang('continue'));
  }
}

function trim_value(&$value)
{
  $value = trim($value);
}
$data['keys'] = htmlspecialchars_decode($data['keys']);
engine_head(lang('edit_item'));
temp::assign('name', $data['name']);
temp::assign('keywords', $data['keys']);
temp::assign('id', $data['id']);
temp::HTMassign('error', $error);
temp::display('control.editkeys');
engine_fin();