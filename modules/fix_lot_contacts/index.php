<?php
/**
 * bankrot-spy.sitecreate54.ru
 * Author: Vladimir S. <guyasyou@gmail.com>
 * www.SiteCreate54.ru
 * © 2016
 */
defined('DS_ENGINE') or die('web_demon laughs');

// Если это не админ, выходим из скрипта
if (core::$rights < 100) {
    header( "refresh:5;url=".core::$home );
    die('Только администраторы могут запускать этот скрипт! Вы будете направлены на главную страницу через несколько секунд');
}

$fixedContacts = array();
//что индексируем
$target = GET('target');
if ($target && array_search($target, array('email', 'phone')) !== false) {
    $offset = abs(intval(GET('offset'))); //Отступ не указан, начинаем сначала
    $total = abs(intval(GET('total'))); //Сколько всего
    $repaired = abs(intval(GET('repaired'))); //Сколько успешно починено лотов
    if (!$total) {
        $total = helper::getLotsWithoutContactsCount($target);

        if ($total == 0) {
            die('Ошибка! Нет лотов для исправления');
        }
    }


    $lotIDs = helper::getLotsWithoutContact($target, $offset);

    switch ($target) {
        case 'email':
            $fixedContacts = helper::fixLotEmails($lotIDs);
            break;
        case 'phone':
            $fixedContacts = helper::fixLotPhones($lotIDs);
            break;
    }

    if (is_array($fixedContacts)) {
        $repaired = $repaired + count($fixedContacts);
    }


    if ($offset < $total) {
        //Сдвигаем отступ
        $offset = $offset + helper::fix_limit;
        if ($offset > $total) {
            $offset = $total;
        }
        //Редиректим на себя с новым отступом
        header( "refresh:1;url=/control/categories/fix_lot_".$target."/".$offset."/".$total."/".$repaired."" );
    } else {
        $finished = true;
    }

    ?>
    <h1 style="margin-top: 80px; text-align: center;">Исправление контактной информации лотов</h1>
    <p style="text-align: center;">Исправляется "<?=$target?>"</p>
    <div style="width: 400px; margin: 0 auto; padding: 20px; background-color: #efefef; border: 1px solid #d6d6d6;">
        <h2 style="text-align: center;">Обработано <?=round($offset*100/$total, 2)?>%</h2>
        <p style="text-align: center;"><?=$offset?> лотов из <?=$total?></p>
        <p style="text-align: center;">Успешно поправлено <?=$repaired?> лотов</p>
        <?php
        if ($finished) {?>
            <h2 style="color: #06ba00; text-align: center">Исправление завершено!</h2>
            <p style="text-align: center;"><a href="/control/keys">Назад в админку</a></p>
        <?php
        }
        ?>
    </div>
    <h4>Исправленные контакты:</h4><?php
    if (is_array($fixedContacts)) {
        foreach ($fixedContacts as $fixedContact) {
            echo $fixedContact.'<br />';
        }
    }
} else {
    die('Ошибка! Не указано что фиксить');
}