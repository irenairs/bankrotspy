<?php

//Пропускает запросты только от сервера!
if ($_SERVER['REMOTE_ADDR'] != '188.40.109.69') {
    die('Access denied!');
}

if(!isset($_GET['data'])) {
    die('Bad data!');
}

//$request = json_decode('{"notification_type":"p2p-incoming","operation_id":"1011496739808126017","amount":9.95,"withdraw_amount":10,"currency":null,"datetime":"2016-01-10T13:39:29Z","sender":"410013399813469","codepro":"false","label":"bspy;392;1;1452433156","sha1_hash":"2dcd0fc23fbd663adfe84b2515005f3ab7afdaab"}', true);

$request = json_decode($_GET['data'], true);

$order_id = $request['operation_id']; // номер платежа

$order = explode(';', $request['label']);

$market = $order[0];
$user_id = intval($order[1]);
$tariff_id = intval($order[2]);
$pay_date = time();

$query = core::$db->query('SELECT * FROM `ds_tariffs` WHERE `id` = "'.$tariff_id.'"');
$tariff = $query->fetch_assoc();



/*
if(floor($request['amount']) < intval($tariff['price'])) {
    die('incorrect sum: ' . $request['amount']);
}*/

if ($market === core::$set['market_prefix'] && user::exists_id($user_id) && !empty($tariff)) {
    
    $query = core::$db->query('SELECT * FROM `ds_users` WHERE `id` = "'.$user_id.'"');
    $user = $query->fetch_assoc();
       

    //Обработка подписки, только если покупка подразуевает права
    if ($tariff['rights'] > 0) {
        //подписка 0 - дней, 1 - месяцев
        if ($tariff['typetime'] == 0) {
            $days_k = 1;
        } elseif ($tariff['typetime'] == 1){
            $days_k = 31;
        }

        //Расчет времени конца купленной подписки
        $end_date = $pay_date + $tariff['longtime'] * $days_k * 24 * 3600;

        //echo 'Текущий '.date('d-m-Y', $user['desttime']).'<br/>';
        //echo 'Тариф '.date('d-m-Y', $end_date).'<br/>';


        //Если еще есть текущая подписка, нужно продлить купленную соответственно
        if ($user['rights'] == 10 || $user['rights'] == 11) {

            //Вычисляем остаток текущей подписки. $Конец_подписки - $Сегодня
            $current_tariff_duration = $user['desttime'] - $pay_date;

            //Если остаток больше нуля, тоесть есть
            if ($current_tariff_duration > 0) {
                //Коэфициент между 10 и 11.
                //Используется для конфертации оставшихся обычных дней подписки в ВИП дни
                $default_k = 1; //коэфициент обычного тарифа.
                $VIP_k = 3; //1 день ВИП равен 3 обычным.

                //Находим коэфициент для конвертации остатков.
                switch ($tariff['rights']) {
                    case 10:
                        $new_tariff_k = $default_k;
                        break;
                    case 11:
                        $new_tariff_k = $VIP_k;
                        break;
                }

                switch ($user['rights']) {
                    case 10:
                        $current_k = $default_k;
                        break;
                    case 11:
                        $current_k = $VIP_k;
                        break;
                }
                $k = $new_tariff_k / $current_k;

                //Конвертированный остаток в секундах
                $current_tariff_duration = $current_tariff_duration / $k;

                //Смещаем конец подписки на конвертированный остаток текущей подписки
                $end_date = $end_date + $current_tariff_duration;

                //echo 'Суммарный '.date('d-m-Y', $end_date).'<br/>';
            }

        }


        core::$db->query('UPDATE `ds_users` SET
                            `rights` = "'.$tariff['rights'].'",
                            `ordertimeid` = "'.$pay_date.'",
                            `desttime` = "'.$end_date.'",
                            `ordercode` = "'.$request['label'].'"
                        WHERE `id` = "'.$user_id.'";');
    }

    core::$db->query('INSERT INTO `ds_paid` SET
                        `tarid` = "'.$tariff['id'].'",
                        `userid` = "'.$user_id.'",
                        `username` = "'.core::$db->res($user['login']).'",
                        `paidid` = "'.core::$db->res($order_id).'",
                        `summ` = "'.$request['amount'].'",
                        `paytime` = "'.$pay_date.'",
                        `comm` = "'.core::$db->res($tariff['name']).'";');

    (new refer())->pay($user_id, $request['amount']);

    $query = core::$db->query('SELECT * FROM `mail_templates` WHERE name = "payment"');
    $data = $query->fetch_assoc();

    $body = array(
        'name'      => $user['login'],
        'taiff'     => $tariff['name'],
        'orderid'    => $order_id,
        'date'      => date('d.m.Y', $pay_date),
        'enddate'   => date('d.m.Y', $end_date)
    );
    exit;
    $mail = mailer::factory();
    $mail->setSubject($data['subject']);
    $mail->setBody($data['template'], $body);
    $mail->addAddress($user['mail']);
    $mail->send();

    //системная почта
    $mail = mailer::factory();
    $mail->setSubject('Оплата подписки');
    $mail->setBody('Клиент: {$name}<br/>Тариф: {$taiff}<br/>Дата: {$date}', $body);

    $mail->addAddress('sales@i-tt.ru');
    $mail->addAddress('techsupport@i-tt.ru');
    $mail->send();

    echo 'ok';
}