<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    $ds_pages = new view(null, 'ds_pages');
    $textData = $ds_pages->where('id', 19)->limit(1)->fetch();
    temp::GLBassign('torgAcademy', true);
    engine_head($textData['name'], $textData['keywords'], $textData['description']);
    temp::assign('title', $textData['name']);
    temp::HTMassign('textData', text::out($textData['text'], 0));

    (new refer())->enter();

    $ds_counts = new model(null, 'ds_counts');
    $ds_counts->insert([
        'page' => 23, 
        'counttime' => time(), 
        'day' => date('j'), 
        'monthyear' => date('n') . date('y')    
    ]);

    // Вывод рекламного блока
    if ( isset(core::$set['main_adv_text']) && (core::$set['main_adv_text'] != '') ) {
        temp::HTMassign('main_adv_text', text::out(core::$set['main_adv_text'], 0));
    }

    temp::HTMassign('counts', cache::get('counts_torg'));
    temp::display('torgi.index');
    // engine_fin();