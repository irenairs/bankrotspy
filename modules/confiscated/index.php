<?php
defined('DS_ENGINE') or die('web_demon laughs');

$more = (int)GET('more');
$fav = (int)GET('fav');

$nav_select = trim(strip_tags(GET('nav_select')));
if (isset($nav_select) && !empty($nav_select)) {
    $nav_amount = intval($nav_select);
} else {
    $nav_amount = 20;
}

new nav($nav_amount);

$where = array();
$joins = array();
$selects = array('');
$orders = array();

$search_name = trim(strip_tags(GET('search_name')));
$search_name = str_replace ( array('~', '>', '<', '?', ')', '(', '}', '{', '&', '^', '$', '#', '|', '_', '-', '/', '\\'), " ", $search_name);
$search_name = preg_replace( "/\s{2,}/", ' ', $search_name);

$all_keywords = trim(strip_tags(GET('all_keywords')));
if (isset($search_name) && !empty($search_name)) {

    if(preg_match('/^(\d+:){1,3}$/', $search_name)){
        //режим поиска по кадастровому региону
        $where[] = '`cadastr_num` LIKE "' . $search_name . '%"';
    } else {
        $keywords = explode(' ', $search_name);
        $search_name_to_where = $search_name;

        if ($all_keywords) {
            $search_name_to_where = '+' . implode(' +', $keywords);
        }
        //Подключаем индекс
        $joins[] = 'LEFT JOIN `konfiscat2_index` ON `konfiscat2`.`id` = `konfiscat2_index`.`lotID`';

        $selects[] = "MATCH (`konfiscat2_index`.content) AGAINST ('" . $search_name . "') AS relSumm";
        $where[] = "MATCH (`konfiscat2_index`.content) AGAINST ('" . $search_name_to_where . "' IN BOOLEAN MODE)";

        $relColumnNames = array();

        foreach ($keywords as $key => $keyword) {
            //Проверяем наличие каждого ключевого слова (1 или 0)
            $selects[] .= "MATCH (`konfiscat2_index`.content) AGAINST ('" . $keyword . "' IN BOOLEAN MODE) > 0 AS rel_" . $key;
            $relColumnNames[] = "rel_" . $key;
        }
        $relColumnNamesString = implode(" + ", $relColumnNames);
        $orders[] = " " . $relColumnNamesString . " DESC, relSumm DESC";
    }
}

$reg_select = trim(strip_tags(GET('reg_select')));
if (isset($reg_select) && !empty($reg_select) && $reg_select != "any") {
    $where[] = "("
            . "konfiscat2.region  LIKE '%".$reg_select."%'"
            . ")";
}

$cat_select = trim(strip_tags(GET('cat_select')));
if (isset($cat_select) && !empty($cat_select) && $cat_select != "any") {
    $where[] = "("
            . "konfiscat2.tipi  LIKE '%".$cat_select."%'"
            . ")";
}

$data_filter_type = trim(strip_tags(GET('data_filter_type')));
$altintconf = trim(strip_tags(GET('altintconf')));

if(isset($data_filter_type) && !empty($data_filter_type)){

    if($data_filter_type == 'by_start'){

        if(isset($altintconf) && !empty($altintconf)){
            $where[] = "("
                . "DATE_FORMAT(FROM_UNIXTIME(konfiscat2.start_time), '%d.%m.%Y') = '".date("d.m.Y", (time()+3600*24* $altintconf))."'"
                . ")";

        }else {
            $begin_set_date = trim(strip_tags(GET('begin_set_date')));
            if (isset($begin_set_date) && !empty($begin_set_date)) {
                $where[] = "("
                    . "konfiscat2.start_time  >= '" . strtotime($begin_set_date) . "'"
                    . ")";
            }

            $end_set_date = trim(strip_tags(GET('end_set_date')));
            if (isset($end_set_date) && !empty($end_set_date)) {
                $where[] = "("
                    . "konfiscat2.start_time <= '" . strtotime($end_set_date) . "'"
                    . ")";
            }

        }

    }else{
        if(isset($altintconf) && !empty($altintconf)){
            $where[] = "("
                . "DATE_FORMAT(FROM_UNIXTIME(konfiscat2.end_time), '%d.%m.%Y') = '".date("d.m.Y", (time()+3600*24* $altintconf))."'"
                . ")";

        }else {
            $begin_set_date = trim(strip_tags(GET('begin_set_date')));
            if (isset($begin_set_date) && !empty($begin_set_date)) {
                $where[] = "("
                    . "konfiscat2.end_time  >= '" . strtotime($begin_set_date) . "'"
                    . ")";
            }

            $end_set_date = trim(strip_tags(GET('end_set_date')));
            if (isset($end_set_date) && !empty($end_set_date)) {
                $where[] = "("
                    . "konfiscat2.end_time <= '" . strtotime($end_set_date) . "'"
                    . ")";
            }
        }
    }
}


$begin_set_deposit = trim(strip_tags(GET('begin_set_deposit')));
if (isset($begin_set_deposit) && !empty($begin_set_deposit)) {
    $where[] = "("
            . "konfiscat2.zadatok  > '".str_replace(" ","",$begin_set_deposit)."'"
            . ")";
}

$end_set_deposit = trim(strip_tags(GET('end_set_deposit')));
if (isset($end_set_deposit) && !empty($end_set_deposit)) {
    $where[] = "("
            . "konfiscat2.zadatok < '".str_replace(" ","",$end_set_deposit)."'"
            . ")";
}

$price_start = trim(strip_tags(GET('price_start')));
if (isset($price_start) && !empty($price_start)) {
    $where[] = "("
            . "konfiscat2.price  > '".str_replace(" ","",$price_start)."'"
            . ")";
}

$price_end = trim(strip_tags(GET('price_end')));
if (isset($price_end) && !empty($price_end)) {
    $where[] = "("
            . "konfiscat2.price < '".str_replace(" ","",$price_end)."'"
            . ")";
}

$status_auct_1 = trim(strip_tags(GET('status_auct_1')));
$status_auct_2 = trim(strip_tags(GET('status_auct_2')));

if(isset($_GET['status_auct_1'])){
    $status_auct_1 = 'on';
}else{
    $status_auct_2 = 'on';
}
if(!isset($_GET['status_auct_1']) && !isset($_GET['status_auct_2'])){
    $status_auct_1 = 'on';
    $status_auct_2 = 'on';
}

if(isset($_GET['search_form_extend']) && $_GET['search_form_extend']==1){
if (isset($status_auct_1) && !empty($status_auct_1)) {
    $where[] = "(tradetip LIKE '%аукцион%')";
} elseif (!isset($status_auct_1) || empty($status_auct_1) && isset($status_auct_2) && !empty($status_auct_2)) {
    $where[] = "(tradetip NOT LIKE '%аукцион%')";
}
}

    $stActive = (int)GET('status_auct_4');
    $stOver = (int)GET('status_auct_5');
    if($stOver){
        if(!$stActive){
            $where[] = "(konfiscat2.end_time <= '" . strtotime(date("Y-m-d H:i:s")) . "')";
        }
    } else {
        $stActive = 1;
        $where[] = "(konfiscat2.end_time > '" . strtotime(date("Y-m-d H:i:s")) . "')";
    }

    $uid = core::$user_id;
    if($uid){
        if($fav){
            $selects[] = '`ds_maindata_favorive_cn`.`item`';
            $joins[] = 'LEFT JOIN `ds_maindata_favorive_cn` ON `konfiscat2`.`id` = `ds_maindata_favorive_cn`.`item` AND `ds_maindata_favorive_cn`.`user_id` = ' . $uid;
            $where[] = '`ds_maindata_favorive_cn`.`user_id` = ' . $uid;
        }
        $selects[] = '`ds_maindata_hide_cn`.`hidetime` AS `hidetime`';
        $joins[] = "LEFT JOIN `ds_maindata_hide_cn` ON `ds_maindata_hide_cn`.`item` = `konfiscat2`.`id` AND `ds_maindata_hide_cn`.`user_id` = " . $uid;
        $where[] = "`hidetime` IS NULL ";
        $selects[] = '`lot_notes_cn`.`text` AS note';
        $joins[] = 'LEFT JOIN `lot_notes_cn` ON `lot_notes_cn`.`lot_id` = `konfiscat2`.`id` AND `lot_notes_cn`.`user_id` = ' . $uid;
    }

$sortOrder = GET('sortOrder');
$sortField = GET('sortField');

if ( isset($sortOrder) && ($sortOrder == 'ASC') ) {
    $sortOrder = 'ASC';
} else {
    $sortOrder = 'DESC';
}

if ( isset($sortField) && strlen($sortField) > 0) {
    array_unshift($orders, "konfiscat2.".$sortField." ".$sortOrder);
} else {
    $orders[] = 'konfiscat2.profit_proc'." ".$sortOrder;;
}

$cntQuery = 'SELECT COUNT(*) AS cnt FROM konfiscat2
           '.implode(' ',$joins).'
            WHERE ' . implode(' AND ',$where)." and status='"."Объявлен"."'";
$query = 'SELECT `konfiscat2`.*'.implode(', ',$selects).' FROM konfiscat2
           '.implode(' ',$joins).'
            WHERE ' . implode(' AND ',$where)." and status='"."Объявлен"."'"
        . ' ORDER BY ' . implode(', ', $orders)
        . ' LIMIT ' . nav::$start . ', ' . nav::$kmess;

$cntQuery = core::$db->query($cntQuery);
$total = $cntQuery->fetch_assoc()['cnt'];
$res = core::$db->query( $query);

    $uid = core::$user_id;
    $confiscation = new confiscation();
    $favArr = $uid ? $confiscation->getDetailsAllArray('favorite', $uid) : [];
    $favCount = 0;
    $hideArr = $uid ? $confiscation->getDetailsAllArray('hide', $uid) : [];

$data = [];
$i = 0;
$type = 'cn';

while($row = $res->fetch_assoc()) {
    $tmp = [];
    $id = $row['id'];
    if (mb_strlen($row['name']) > 70 && $more==0){
        $tmp['name'] = mb_substr($row['name'], 0, 70) . "<span class='show_more moreattr_".$row['id']."'>...</span><span class='text_all allattr_".$row['id']."' >".mb_substr($row['name'], 70, strlen($row['name']))."</span>";
    } else {
        $tmp['name'] = $row['name'];
    }
    $tmp['lotid'] = $id;
    $tmp['nomeri'] = $row['nomeri'];
    $tmp['kolvo'] = $row['kolvo'];
    $tmp['predmet'] = $row['predmet'];
    $tmp['mesto'] = $row['mesto'];
    $tmp['region'] = $row['region'];
    $tmp['vidsob'] = $row['vidsob'];
    $tmp['tipi'] = $row['tipi'];
    $tmp['obremenenie'] = $row['obremenenie'];
    $tmp['tradetip'] = (stristr($row['tradetip'], 'аукц') == true) ? "АО" : "ПП";
    $tmp['price'] = $row['price'];
    $tmp['market_price'] = $row['market_price'];
    if($row['market_price'] == 0 && $row['cadastr_price'] > 0) $tmp['market_price'] = $row['cadastr_price'];
    $tmp['cadastr_price'] = $row['cadastr_price'];
    $tmp['profit_rub'] = $row['profit_rub'];
    $tmp['profit_proc'] = $row['profit_proc'];
    $tmp['start_time'] = date("d.m.Y H:i:s",$row['start_time']);
    $tmp['end_time'] = date("d.m.Y H:i:s",$row['end_time']);
    $tmp['status'] = $row['status'];

    $tmp['profit_rub'] = ($access === true) ? $row['profit_rub'] : $access;
    $tmp['profit_proc'] = ($access === true) ? $row['profit_proc'] : $access;
    $tmp['zadatok'] = ($access === true) ? $row['zadatok'] : $access;
    $tmp['link'] = ($access === true) ? $row['link'] : false;
    if(in_array($id, $favArr)){
        $tmp['fh'][] = '<i cmd="fav|' . $type . '|del|' . $id . '" title="Удалить лот из избранного" class="icon-star-clicked"></i>';
        $favCount++;
    } else {
        $tmp['fh'][] = '<i cmd="fav|' . $type . '||' . $id . '" title="Добавить лот в избранное" class="icon-star-empty"></i>';
    }
    if(in_array($id, $hideArr)){
        $tmp['fh'][] = '<i cmd="hide|' . $type . '|del|' . $id . '" title="Достать лот из мусора" class="icon-forward"></i>';
        $favCount++;
    } else {
        $tmp['fh'][] = '<i cmd="hide|' . $type . '||' . $id . '" title="Отправить лот в мусор" class="icon-delete"></i>';
    }
    $note = '<i data-note="' . ($row['note'] ? $row['note'] : '') . '" cmd="note_pop|' . $type . '|' . $id . '" title="Комментарий к лоту" class="fa fa-sticky-note';
    if($row['note']){
        $note .= '"></i>';
    } elseif($fav && in_array($id, $favArr)) {
        $note .= '-o"></i>';
    } else {
        $note = false;
    }
    if($note){
        $tmp['fh'][] = $note;
    }
    $tmp['fh'] = join(' <br/> ', $tmp['fh']);
    $data[] = $tmp;
}


$noacc = $access;

$query = "SELECT region, id FROM konfiscat2 GROUP BY region";
$res = core::$db->query($query);
$data_regions = [];
$i = 0;
while ($row = $res->fetch_assoc()) {
    if(!empty($row["region"])) {
        $data_regions[$i] = $row["region"];
        $i++;
    }
}

$query = "SELECT tipi, id FROM konfiscat2 GROUP BY tipi";
$res = core::$db->query($query);
$data_cats = [];
$i = 0;
while ($row = $res->fetch_assoc()) {
    if(!empty($row["tipi"])) {
        $data_cats[$i] = $row["tipi"];
        $i++;
    }
}

    $ds_pages = new view(null, 'ds_pages');
    $textData = $ds_pages->where('id', 20)->limit(1)->fetch();
    engine_head($textData['name'], $textData['keywords'], $textData['description']);
    temp::assign('title', $textData['name']);
    temp::HTMassign('textData', text::out($textData['text'], 0));

$start = nav::$start;
$finish = $start + nav::$kmess;
if ($total < $finish ) {
    $finish = $total;
}
temp::assign('start', $start);
temp::assign('end', $finish);
temp::assign('total', $total);

temp::assign('sortOrder', $sortOrder);
temp::assign('sortField', $sortField);
temp::assign('search_name', $search_name);
temp::assign('all_keywords', GET('all_keywords'));
temp::assign('begin_set_date', isset($begin_set_date) ? $begin_set_date : '');
temp::assign('end_set_date', isset($end_set_date) ? $end_set_date : '');
temp::assign('altintconf', $altintconf);
temp::assign('begin_set_deposit', $begin_set_deposit);
temp::assign('end_set_deposit', $end_set_deposit);
temp::assign('price_start', $price_start);
temp::assign('price_end', $price_end);

temp::assign('status_auct_1', $status_auct_1);
temp::assign('status_auct_2', $status_auct_2);
temp::assign('status_auct_4', $stActive);
temp::assign('status_auct_5', $stOver);
temp::assign('reg_select', $reg_select);
temp::assign('cat_select', $cat_select);
temp::assign('data_filter_type', $data_filter_type);

temp::HTMassign('navigation', nav::display($total, core::$home.'/confiscated/?'));
temp::HTMassign('data', $data);
temp::HTMassign('data_regions', $data_regions);
temp::HTMassign('data_cats', $data_cats);
temp::HTMassign('noacc', $noacc);
temp::HTMassign('more', $more);
temp::HTMassign('fav', $fav);
temp::HTMassign('favAll', count($data) == $favCount);

temp::assign('nav_select', $nav_select);

    //Новости по лотам
    $outNews = [];
    $ds_platform_news = new view(null, 'ds_platform_news');
    $res = $ds_platform_news->where(['type' => [2]])->order('`time` DESC')->limit(10)->fetchAll();
    foreach ($res as $data){
        $tmp = [];
        $tmp['id'] = $data['id'];
        $tmp['time'] = ds_time($data['time'], '%H:%M');
        $tmp['data'] = ds_time($data['time'], '%d %B2 %Y');
        text::add_cache($data['cache']);
        $tmp['text'] = text::out($data['text'], 0);
        $outNews[] = $tmp;
    }
    temp::HTMassign('outnews', $outNews);

    $ds_counts = new model(null, 'ds_counts');
    $ds_counts->insert([
        'page' => 22,
        'counttime' => time(),
        'day' => date('j'),
        'monthyear' => date('n') . date('y')
    ]);

    temp::HTMassign('counts', cache::get('counts_torg'));

// Вывод рекламного блока
if ( isset(core::$set['main_adv_text']) && (core::$set['main_adv_text'] != '') ) {
    temp::HTMassign('main_adv_text', text::out(core::$set['main_adv_text'], 0));
//    temp::HTMassign('main_adv_text', core::$set['main_adv_text']);
}

temp::display('confiscated.index');
// engine_fin();