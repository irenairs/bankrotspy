<?php

defined('DS_ENGINE') or die('web_demon laughs');

$id = intval(GET('id'));
$uid = core::$user_id;

$res = core::$db->query('SELECT 
       `konfiscat2`.*,
       `lot_notes_cn`.`text` AS `note`
        FROM `konfiscat2`
        LEFT JOIN `lot_notes_cn` ON `lot_notes_cn`.`lot_id` = `konfiscat2`.`id` AND `lot_notes_cn`.`user_id` = "' . $uid . '"
        WHERE `konfiscat2`.`id` = ' . $id);
    
$data = [];
$i = 0;

while($row = $res->fetch_assoc()) {
    $data['id'] = $row['id'];
    $data['name'] = $row['name'];
    $data['nomeri'] = $row['nomeri'];
    $data['kolvo'] = $row['kolvo'];
    $data['predmet'] = $row['predmet'];
    $data['mesto'] = $row['mesto'];
    $data['region'] = $row['region'];
    $data['organizer'] = $row['organizer'];
    $data['mestosrok'] = $row['mestosrok'];
    $data['vidsob'] = $row['vidsob'];
    $data['tipi'] = $row['tipi'];
    $data['obremenenie'] = $row['obremenenie'];
    $data['tradetip'] = $row['tradetip'];
    $data['price'] = $row['price'];
    $data['market_price'] = $row['market_price'];
    if($row['market_price'] == 0 && $row['cadastr_price'] > 0) $data['market_price'] = $row['cadastr_price'];
    $data['cadastr_price'] = $row['cadastr_price'];
    $data['profit_rub'] = $row['profit_rub'];
    $data['profit_proc'] = $row['profit_proc'];
    $data['zadatok'] = $row['zadatok'];
    $data['start_time'] = ds_time($row['start_time']);
    $data['end_time'] = ds_time($row['end_time']);
    $data['status'] = $row['status'];
    $data['link'] = $row['link'];
    $data['note'] = $row['note'];
    $i++;
}

//тип графика (техника)
$graphType = 1;

// Формируем и передаем meta_description в движок
core::$page_description = mb_substr($data['name'], 0, 200);

// Формируем и передаем meta_keywords в движок
$page_keys = preg_replace("/[ ]/",", ", $data['name']);
$page_keys = mb_substr($page_keys, 0, 50);
core::$page_keywords = $page_keys;

// если категория недвижимость берем среднее метр квадратный
if(stripos($data['tipi'], 'Недвиж') !== false) {
    $m2price = $query = core::$db->query('SELECT * FROM `ds_maindata_hint_konfiscat2` WHERE `id` = "' . $id . '" LIMIT 1');
    $average = $m2price->fetch_assoc();
    $data['average'] = $average['text'];
    $data['average_link'] = $average['link'];
    $graphType = 2;//недвижимость
} else {
    $m2price = $query = core::$db->query('SELECT * FROM `ds_maindata_hint_konfiscat2` WHERE `id` = "' . $id . '" LIMIT 1');
    $average = $m2price->fetch_assoc();
    $data['average'] = $average['text'];
    $data['average_link'] = $average['link'];
    $graphType = 1;
}


$query = core::$db->query('SELECT * FROM `lot_prices_konfiscat2` WHERE `id` = "' . $id . '" ORDER BY price ASC');
$countLot = $query->num_rows;
$similarDataPrice = array();

// надо минимум 2 значения для построения графика
if($countLot > 1 ) { 
    while($row = $query->fetch_assoc()) {
        // если категория недвижимость берем среднее метр квадратный иначе рыночная стоимость
        if($graphType == 2) {
            $row['average'] = $average['price'];
        } else {
            $row['average'] = $data['market_price'];
        }
        $similarDataPrice[] = $row;
    }
}


$data['mestosrok'] = ($access === true) ? $data['mestosrok'] : $access;
$data['zadatok'] = ($access === true) ? $data['zadatok'] : $access;
$data['mesto'] = ($access === true) ? $data['mesto'] : $access;
$data['average'] = ($access === true) ? $data['average'] : $access;
$data['link'] = ($access === true) ? $data['link'] : false;
$noacc = $access;

$confiscation = new confiscation();

engine_head(mb_substr($data['name'], 0, 50)."... ".lang('card_n').''.$id);

temp::assign('type', 'cn');
temp::assign('id', $id);
temp::assign('lotFav', $uid ? count($confiscation->getDetailsArray('favorite', [$id], $uid)) : false);
temp::assign('lotHide', $uid ? count($confiscation->getDetailsArray('hide', [$id], $uid)) : false);
temp::HTMassign('note', $data['note']);

if(!empty($similarDataPrice) && CAN('histogram_goods')) {
    temp::HTMassign('similarDataPrice', json_encode($similarDataPrice));
} elseif(!empty($similarDataPrice) && !CAN('histogram_goods')) {
    temp::HTMassign('similarDataPrice', 'access');
}
temp::assign('graphType', $graphType);
temp::assign('title', lang('title'));
temp::HTMassign('noacc', $noacc);
temp::HTMassign('data', $data);
temp::display('confiscated.card');
engine_fin();
