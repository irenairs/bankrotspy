<?php

  defined('DS_ENGINE') or die('web_demon laughs');
  ini_set('error_reporting', E_ALL);
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  ini_set('memory_limit', '-1');
  set_time_limit (3000);
  require_once 'dscore/libs/PHPExcel.php';
  
  $now=time();
  $regionsDb = core::$db->query('SELECT * FROM `ds_maindata_regions`;'); //выгружаем регионы
  $regions = array();
  while ($region = $regionsDb->fetch_assoc()) {
      $regions[$region['number']] = array('name' => $region['name'], 'events'=> array()); // создаем структуру вида [region_id] = array (name,events) где events список аукционов в этом регионе
  }
  $catName = '';
  if (isset($_POST['catid'])) {
     $total = core::$db->query('SELECT count(*) FROM `ds_maindata` 
       WHERE  ( `cat_id`='.((int)$_POST['catid']).') AND  `status` < 3 AND `end_time` > '.$now.' ;')->count(); //количество аукционов
    $newEvents = core::$db->query(
        'SELECT * FROM `ds_maindata` 
         WHERE  ( `cat_id`='.((int)$_POST['catid']).') AND  `status` < 3 AND `end_time` > '.$now.' ;'); //массив аукционов
    $catName = $_POST['catname'];
  } else {
    $total = core::$db->query('SELECT count(*) FROM `ds_maindata` 
         WHERE  ( `cat_id`=6 or `cat_id`=5 or `cat_id`=3) AND  `status` < 3 AND `end_time` > '.$now.' ;')->count(); //количество аукционов
    $newEvents = core::$db->query(
        'SELECT * FROM `ds_maindata` 
         WHERE  ( `cat_id`=6 or `cat_id`=5 or `cat_id`=3) AND  `status` < 3 AND `end_time` > '.$now.' ;'); //массив аукционов
  }

if(!isset($_POST['catid']) || $_POST['catid']==3 || $_POST['catid']==5 || $_POST['catid']==6){
    $inputFileName = './data/xls/template_ned.xlsx';

    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName); //загружаем xlsx шаблон
    
    while ($newEvent = $newEvents->fetch_assoc()) { // сортируем аукционы по регионам
    
        for ($i=0; $i < 18; $i++) {
            $event[$i] = '';
        }
        $event[0] = $newEvent['id'];
        if (strlen($newEvent['description'])>strlen($newEvent['name'])) {
            $discription = $newEvent['description'];
        } else {
            $discription = $newEvent['name'];
        }
        $discription = str_replace(['#','^',';',"\n","\t"],['','','','',''],$discription);
        $event[1] = $discription;
        $event[2] = str_replace(['#','^',';',"\n","\t"],['','','','',''],$newEvent['address']);
        $area = '';
        $pieces = explode(" ", $discription);
        //print_r($pieces);die();
        for ($i=0;$i<count($pieces);$i++) {  // пытаемся найти значение площади в описании
            if (strpos($pieces[$i],"кв.") !== false) {
                
                $area = (float) str_replace(',','.',$pieces[$i]);
                if (!$area && $i) {
                    $area = (float) str_replace(',','.',$pieces[$i-1]);
                }
                break;
            }
        }
        $event[3] = $area; // площадь
        $event[4] = $newEvent['price']; // начальная цена
        if ($area && $newEvent['price']) {
            $event[5] = intval($newEvent['price']/$area); // цена за 1.кв
        }
    
        $scheduleDrop = $scheduleLines = array(); // парсим расписание снижения цены
        $scheduleArr = explode("span class=\"pp_data\">",$newEvent['grafik1']);
        foreach ($scheduleArr as $key => $value) {
            if ($key) {
                $priceDrop = '';
                if (preg_match("|<s[^>]*pp_cena[^>]*>(.*)</s|Uis", $value, $res)) {
                    $priceDrop = str_replace(',','.',$res[1]);
                }
                $scheduleDrop[] = array('date' => explode('</span>',$value)[0],
                    'price' => $priceDrop);
                $scheduleLines[] = explode('</span>',$value)[0]. ' - '.$priceDrop;
            }
        }
        if (count($scheduleDrop)) { // находим ближайшее актуальное снижение
            $nowF = date('Ymd');
            foreach ($scheduleDrop as $key => $sch) {
                $dt = explode(' ',$sch['date']);
                $day = explode('.',$dt[0]);
                if (isset($day[2]) && (int) $day[2]) {
                    $dt = $day[2].$day[1].$day[0];
                    if ($dt >= $nowF) {
                        $event[6] = $sch['price'];
                        break;
                    }
                }
            }
        }
       // print_r($newEvent);
        //print_r($event[4]."  ".$event[3]);
       if($event[3]!=0)
        $event[5] = intval(($event[4]/$event[3]));
       
        if (count($scheduleLines)) { // заносим расписание в аукцион
            $event[6] = implode("\r\n",$scheduleLines);
        }
        /*if ($area && $event[6]) { //вычисляем площадь после снижения, если нашли ближайшее снижение и площадь
            $event[7] = $event[6]/$area;
        }*/
       
        
        $event[7] = $newEvent['now_price']<1? $newEvent['price']:$newEvent['now_price']; // текущая цена, руб.
        if ((float) $event[4] && (float) $event[7]) { // % снижения
            $event[8] = intval(($event[4]-$event[7])/$event[4]*100);
        }
        
        if($event[3]!=0)
        $event[9] = intval(($event[7])/$event[3]);
        $event[10] = $newEvent['market_price']; // Рыночная цена, руб.
        if($event[10] == 0 && $newEvent['cadastr_price'] > 0){
            $event[10] = $newEvent['cadastr_price'];
        }
        if($event[10] == 0) $event[10] = '-';
        if($event[3]!=0)
        $event[11] = intval(($event[10])/$event[3]);
        
        $event[12] = $newEvent['profit_rub']==0 ? "-":$newEvent['profit_rub'];     // Доход, руб.
        $event[13] = $newEvent['profit_proc']==0 ? "-":$newEvent['profit_proc'];   // Доходность, %
        $event[14] = date('d.m.Y',$newEvent['start_time']); // начало аукциона
        $event[15] = date('d.m.Y',$newEvent['end_time']); // конец аукциона
        $event[16] = $newEvent['fedlink']; // ссылка на федрезерв
        $event[17] = $newEvent['auct_link']; // ссылка на аукцион
        $event[18] = date('d.m.Y',$newEvent['last_update']); // последнее обновление
    //print_r($event);die();
        $regions[$newEvent['place']]['events'][] = $event; // добавляем аукцион к региону
    }
}else{
    $inputFileName = './data/xls/template.xlsx';
    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName); //загружаем xlsx шаблон    
    
    
    while ($newEvent = $newEvents->fetch_assoc()) { // сортируем аукционы по регионам
    
        for ($i=0; $i < 15; $i++) {
            $event[$i] = '';
        }
        $event[0] = $newEvent['id'];
        if (strlen($newEvent['description'])>strlen($newEvent['name'])) {
            $discription = $newEvent['description'];
        } else {
            $discription = $newEvent['name'];
        }
        $discription = str_replace(['#','^',';',"\n","\t"],['','','','',''],$discription);
        $event[1] = $discription;
        $event[2] = str_replace(['#','^',';',"\n","\t"],['','','','',''],$newEvent['address']);
        $area = '';
        $pieces = explode(" ", $discription);
        $event[3] = $newEvent['price']; // начальная цена
        $scheduleDrop = $scheduleLines = array(); // парсим расписание снижения цены
        $scheduleArr = explode("span class=\"pp_data\">",$newEvent['grafik1']);
        foreach ($scheduleArr as $key => $value) {
            if ($key) {
                $priceDrop = '';
                if (preg_match("|<s[^>]*pp_cena[^>]*>(.*)</s|Uis", $value, $res)) {
                    $priceDrop = str_replace(',','.',$res[1]);
                }
                $scheduleDrop[] = array('date' => explode('</span>',$value)[0],
                    'price' => $priceDrop);
                $scheduleLines[] = explode('</span>',$value)[0]. ' - '.$priceDrop;
            }
        }
        if (count($scheduleDrop)) { // находим ближайшее актуальное снижение
            $nowF = date('Ymd');
            foreach ($scheduleDrop as $key => $sch) {
                $dt = explode(' ',$sch['date']);
                $day = explode('.',$dt[0]);
                if (isset($day[2]) && (int) $day[2]) {
                    $dt = $day[2].$day[1].$day[0];
                    if ($dt >= $nowF) {
                        $event[4] = $sch['price'];
                        break;
                    }
                }
            }
        }
        /*if ($area && $event[4]) { //вычисляем площадь после снижения, если нашли ближайшее снижение и площадь
            $event[7] = $event[4]/$area;
        }*/
       /* if ((float) $event[4] && (float) $event[6]) { // % снижения
            $event[8] = 100-$event[6]*100/$event[4];
        }*/
        if (count($scheduleLines)) { // заносим расписание в аукцион
            $event[4] = implode("\r\n",$scheduleLines);
        }
        $event[5] = $newEvent['now_price']<1? $newEvent['price']:$newEvent['now_price']; // текущая цена, руб.
        if($event[3]!=0)
        $event[6] = intval(($event[3]-$event[5])/$event[3]*100); // Снижение %.
        
        $event[7] = $newEvent['market_price']==0 ? "-":$newEvent['market_price']; // Рыночная цена, руб.
        $event[8] = $newEvent['profit_rub']==0 ? "-":$newEvent['profit_rub'];     // Доход, руб.
        $event[9] = $newEvent['profit_proc']==0 ? "-":$newEvent['profit_proc'];   // Доходность, %
    
        $event[10] = date('d.m.Y',$newEvent['start_time']); // начало аукциона
        $event[11] = date('d.m.Y',$newEvent['end_time']); // конец аукциона
        $event[12] = $newEvent['fedlink']; // ссылка на федрезерв
        $event[13] = $newEvent['auct_link']; // ссылка на аукцион
        $event[14] = date('d.m.Y',$newEvent['last_update']); // последнее обновление

        $regions[$newEvent['place']]['events'][] = $event; // добавляем аукцион к региону
    }
    
}
  

  foreach ($regions as $key => $region) {   
    if (count($region['events'])) {
      if (!isset($region['name'])) {  // фикс отсутствия региона с номером 81
        $region['name'] = 'ХМАО Югра';
      }
      $objClonedWorksheet = clone $objPHPExcel->getSheetByName('template'); // клонируем лист 
      $titl = $region['name'];
      if (strpos($titl, 'Республика Северная Осетия - Алания') !== false) {
        $titl = 'Северная Осетия - Алания';
      } 
      $objClonedWorksheet->setTitle($titl);
    //  print_r($region['events']);die();
      $objClonedWorksheet->fromArray($region['events'],NULL,'A4'); // заполняем таблицу
      $objPHPExcel->addSheet($objClonedWorksheet); 

    } 
  }
  $sheetIndex = $objPHPExcel->getIndex(
    $objPHPExcel->getSheetByName('template') // удаляем лист шаблона из книги
  );
  
  $objPHPExcel->removeSheetByIndex($sheetIndex);
  $objPHPExcel->getProperties()->setCreated();
  $objPHPExcel->getProperties()->setModified();
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
  $objWriter->save('./data/xls/export_baspy_'.$catName.'_'.$total.'.xlsx'); //сохраняем файл на сервере
  $file = './data/xls/export_baspy_'.$catName.'_'.$total.'.xlsx';
  header('Content-Description: File Transfer');
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename="'.basename($file).'"');
  header('Expires: 0');
  header('Cache-Control: must-revalidate');
  header('Pragma: public');
  header('Content-Length: ' . filesize($file));
  readfile($file); //отдаем файл
  exit();