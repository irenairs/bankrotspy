<?php
defined('DS_ENGINE') or die('web_demon laughs');

if (!core::$user_id) {
    func::notify(
        'Нет доступа',
        'Чтобы получить доступ к данной странице, вам необходимо зарегистрироваться',
        core::$home.'/user/register',
        'Регистрация'
    );

    return 0;
}

new nav(50);

$where = '';
$search = trim(strip_tags(GET('search')));

if (!empty($search)) {
    $where .= ' WHERE (';

    // для ускорения и разгрузки БД предварительно проверяем запрос на соответствие формату искомых полей
    if (is_int($search) ) {
        // если запрос полностью число то это либо ИНН, либо номер телефона
        $where .= 'org.inn LIKE "%'.$search.'%" OR org.phone LIKE "%'.$search.'%"';
    } elseif (mb_strpos($search, '@')) {
        // если в запросе есть символ @ то это наверняка email
        $where .= 'org.mail LIKE "%'.$search.'%"';
    } elseif (preg_match('/^[^a-zA-Zа-яА-Я ]+/', $search)) {
        // если в запросе есть любые символы кроме букв то это навернка номер телефона
        $where .= 'org.phone LIKE "%'.$search.'%"';
    } else {
        // если же ни один из шаблонов не подошел к запросу скорее всего это одно из оставшихся полей
        $where .= 'org.org_name LIKE "%'.$search.'%" 
                    OR org.contact_person LIKE "%'.$search.'%" 
                    OR org.manager LIKE "%'.$search.'%"';
    }

    $where .= ')';
} 

// задаем сортировку полученного ответа
$sortOrder = GET('sortOrder');
if (!empty($sortOrder) && $sortOrder == 'ASC') {
    $sortOrder = 'ASC';
} else {
    $sortOrder = 'DESC';
}

$sortField = GET('sortField');
if (!empty($sortField)) {
    switch ($sortField) {
        case 'name':
            $order = 'org.org_name';
            break;
        case 'cnt':
            $order = 'cnt';
            break;
        default:
            $order = 'org.bal';

            break;
    }
} else {
    $sortField = 'bal';
    $order = 'org.bal';
}

$order .= ' ' . $sortOrder . ', org.totaldoc DESC';

$cntQuery = 'SELECT COUNT(*) AS cnt FROM ds_maindata_organizers AS org' . $where;

$query = 'SELECT org.*, ifnull(lot.cnt, 0) AS cnt
          FROM ds_maindata_organizers AS org
          LEFT JOIN (select organizer, count(*) AS cnt FROM ds_maindata WHERE status = "3" group by organizer) AS lot ON org.id = lot.organizer
          ' . $where . '
          ORDER BY ' . $order . '
          LIMIT ' . nav::$start . ', ' . nav::$kmess;

$cntQuery = core::$db->query($cntQuery);
$total = $cntQuery->fetch_assoc()['cnt'];
$res = core::$db->query($query);

$data = [];
$i = 0;

foreach ( $res as $row ) {
    if ($row['totaldoc'] > 0 && $row['totaldoc'] < 3) {
        $rating = 'Мало данных';
    } elseif ($row['totaldoc'] == 0) {
        $rating = 'Нет данных';
    } elseif ($row['bal'] > 5 ) {
        $rating = '<a class="plus" href="' . core::$home . '/amc/' . $row['id'] . '" target="_blank">' . $row['bal'] . '</a>';
    } else {
        $rating = '<a class="minus" href="' . core::$home . '/amc/' . $row['id'] . '" target="_blank">' . $row['bal'] . '</a>';
    }
    
    if($row['cnt']) {
        $cnt = $row['cnt'];
    } else {
        $cnt = 0;
    }

    if (!empty($row['totaldoc'])) {
        $linkdocs = '<a class="namelink" href="' . $row['linkdocs'] . '" target="_blank">Смотреть</a>';
    } else {
        $linkdocs = 'Нет данных';
    }
    
    if(!empty($row['fasdocs'])) {
        $fasdocs = '<a class="namelink" href="' . $row['fasdocs'] .'" target="_blank">Смотреть</a>';
    } else {
        $fasdocs = 'Нет данных';
    }
    
    if(!empty($row['org_profile'])) {
        $org_profile = '<a class="namelink" href="' . $row['org_profile'] .'" target="_blank">Смотреть</a>';
    } else {
        $org_profile = 'Нет данных';
    }

    $data[$i]['id'] = $row['id'];
    $data[$i]['name'] = str_replace(['ИП ', 'ИП'], '', $row['org_name']);

    if ($access === true) {
        $data[$i]['phone'] = $row['phone'];
        $data[$i]['rating'] = $rating;
        $data[$i]['cnt'] = $cnt;
        $data[$i]['fasdocs'] = $fasdocs;
        $data[$i]['linkdocs'] = $linkdocs;
        $data[$i]['org_profile'] = $org_profile;
        $data[$i]['totaldoc'] = $row['totaldoc'];
        $data[$i]['email'] = $row['mail'];
    } else {
        $data[$i]['phone'] = $access;
        $data[$i]['rating'] = $access;
        $data[$i]['cnt'] = $access;
        $data[$i]['fasdocs'] = $access;
        $data[$i]['linkdocs'] = $access;
        $data[$i]['org_profile'] = $access;
        $data[$i]['totaldoc'] = $access;
        $data[$i]['email'] = $access;
    }

    $i++;
}

$textQuery = core::$db->query('SELECT * FROM ds_pages WHERE id = "9" LIMIT 1;');
$textData = $textQuery->fetch_assoc();

engine_head($textData['name'], $textData['keywords'], $textData['description']);
temp::assign('title', $textData['name']);

$start = nav::$start;
$finish = $start + nav::$kmess;
if ($total < $finish) {
    $finish = $total;
}

core::$db->query("INSERT INTO ds_counts(page, counttime, day, monthyear) VALUES (9,".time().",".date('j').", ".date('n').date('y')."9)");

temp::assign('start', $start);
temp::assign('end', $finish);
temp::assign('total', $total);

temp::HTMassign('textData', text::out($textData['text'], 0));
temp::assign('sortOrder', $sortOrder);
temp::assign('sortField', $sortField);
temp::assign('search', $search);
temp::assign('access', $access);

temp::HTMassign('navigation', nav::display($total, core::$home.'/amc/?', '', '', array('search'=>$search,'sortOrder'=>$sortOrder,'sortField'=>$sortField)));
temp::HTMassign('data', $data);

temp::display('amc.view');

engine_fin();