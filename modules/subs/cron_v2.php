<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require 'vendor/autoload.php';
use phpmailer\phpmailer;

$conn = new mysqli('localhost', 'bankrotspy', 'E80KbRS8', 'bankrotspy', 6000);//'bankrotspy','E80KbRS8');
if ($conn->connect_error) {
throw new Exception('Connection Error: ['.$conn->connect_errno.'] '.$conn->connect_error, $conn->connect_errno);
}

$conn->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");


$mail_temp = "<p align=\"center\"><a href=\"http://bankrot-spy.ru/\"><img src=\"http://bankrot-spy.ru/themes/web/default/images/logo.png\" width=\"392\" height=\"70\" /></a></p>" 
."<p align=\"center\"><span style=\"color: #696969;\"><span style=\"font-size: x-large;\"><strong>Новости по новым лотам</strong></span></span></p>"
."<br>"
."<table style=\"border-color: #ссс;\" border=\"3\">"
."<tbody>"
."<tr><th><strong>Название</strong></th><th><strong>Цена, руб</strong></th><th><strong>Добавлен</strong></th></tr>";


$main_query = 'SELECT'
        .' ds_search_profiles.id,'
        .' ds_search_profiles.userid,'  
        .' ds_search_profiles.profile,'
        .' ds_search_profiles.pname,'
        .' ds_users.mail'
        .' FROM'
        .' ds_search_profiles,' 
        .' ds_users' 
    .' WHERE'
        .' ds_search_profiles.subscribe = 1'
        .' AND ds_search_profiles.userid =  ds_users.id';
// получаем массив всех подписок
$main_res = $conn->query($main_query);

// преобразование массива в письмо
while ($pdata = $main_res->fetch_assoc()) {
    // преобразуем в массив
    $tset = json_decode($pdata['profile'], 1);
    $data = $tset;
    $user_id = $pdata['userid'];

    //Основной запрос
    $lots_query = getLotsByProfile($tset, $pdata['userid']);

    $res = $conn->query($lots_query);
    if ($res->num_rows) {
        $loc = [];
        $i = 0;
        while ($getdata = $res->fetch_assoc()) {
            $loc[$i] = [];
            $loc[$i]['id'] = $getdata['id'];
            $loc[$i]['name'] = $getdata['name'];
            if (mb_strlen($getdata['name']) > 70){
                $loc[$i]['name'] = mb_substr($getdata['name'], 0, 70) . "...";
            } else {
                $loc[$i]['name'] = $getdata['name'];
            }   
            $loc[$i]['price'] = $getdata['price'];
            $loc[$i]['loadtime'] = ds_time2($getdata['loadtime']);
            $i++;
        }
        $mailer = mailer::factory();
        $mailer->setSubject("Новости по лотам (".$pdata['pname'].")");
        $body = $mail_temp;
        foreach ($loc as $key => $value) {
            $body .= "<tr>";
            $body .= "<td style=\"padding: 10px;\"><strong>&nbsp;<span style=\"color: #ff9900;\"><a style=\"text-decoration:none; color: #C77216;\" href=\"http://bankrot-spy.ru/card/".$value['id']."\">".$value['name']."</a></span></strong></td>";
            $body .= "<td style=\"padding: 10px; text-align: center;\"><strong>".$value['price']."</strong></td>";
            $body .= "<td style=\"padding: 10px; text-align: center;\"><strong>".$value['loadtime']."</strong></td>";
            $body .= "</tr>";
        }
        $body .= "</tbody></table>";
        $mailer->setBody($body);
        $mailer->addAddress($pdata['mail']);
        $mailer->send();
    }
}

// получение массива лотов по профилю
function getLotsByProfile($data, $user_id) {
    //Условия для WHERE (компилятся через AND)
    $conditions = array();

    //Условия для WHERE (компилятся через OR)
    $conditions_or = array();

    //Дополнительные условия для LEFT JOIN
    $join_conditions = array();

    //Условия для выборки
    $selects = array();

    //Условия для сортировки
    $order_conditions = array();

    $begin_date = abs(intval($data['begin_date']));
    $end_date = abs(intval($data['begin_date']));

    $data['platforms'] = check_platforms($data['platforms']);
    $data['places'] = check_places($data['places']);
    $data['types'] = check_types($data['types']);

    if ($end_date AND $begin_date) {
        if ($end_date < $begin_date) {
            $end_date = 0;
            $begin_date = 0;
        }
    }

    // // Есть ли фото
    // if ($data['cntPhoto'] && $data['cntPhoto'] > 0) {
    //     $conditions['cntPhoto'] = "`ds_maindata`.`cntPhoto` > 0";
    // }

    // Номер дела
    if ($data['case_number'] && $data['case_number'] != '' ) {
        $conditions['case_number'] = " `ds_maindata`.`case_number` LIKE '%" . $data['case_number'] . "%'"; 
    }

    // ИНН или ФИО должника
    if ($data['inn'] && $data['inn'] != '' ) {
        $join_conditions['inn'] = 'LEFT JOIN `ds_maindata_debtors` ON `ds_maindata`.`debtor` = `ds_maindata_debtors`.`id`';
        if ( is_numeric($inn) ) {
            $conditions['inn'] = " `ds_maindata_debtors`.`inn` = '" . $data['inn'] . "'";
        } else {
            $conditions['inn'] = " `ds_maindata_debtors`.`dept_name` LIKE '%" . $data['inn'] . "%'";
        }
    }

    //выборка по категориям
    if ($data['category'] == '-1') {
        $conditions['fav_sql'] = '`ds_maindata_favorive`.`user_id` = "'.$user_id.'" ';
        $join_conditions['fav_sql'] = 'LEFT JOIN `ds_maindata_favorive` ON `ds_maindata`.`id` = `ds_maindata_favorive`.`item` AND `ds_maindata_favorive`.`user_id` = "'.$user_id.'"';
        $selects['fav_sql'] = ' `ds_maindata_favorive`.`item` ';
    } elseif($data['category'] === 5 || $data['category'] === 1 || $data['category'] === 7 || $data['category'] === 6) {
        $conditions['hint'] = ' `ds_maindata`.`cat_id` = "'.$data['category'].'" ';
        $join_conditions['hint']= 'LEFT JOIN `ds_maindata_hint` ON `ds_maindata`.`id` = `ds_maindata_hint`.`id`';
        $selects['hint'] = ' `ds_maindata_hint`.`text` AS hint_text';
    } elseif ($data['category'] >= 0) {
        $conditions['category'] = ' `ds_maindata`.`cat_id` = "'.$data['category'].'" ';
    }

    //Фильтрация по типам
    if($data['types']) {
        $conditions['types'] = ' `type` IN ('.implode(', ', $data['types']).') ';
    }
    //Фильтрация по регионам
    if(count($data['places']) AND $data['places']) {
        $conditions['places'] = ' `ds_maindata`.`place` IN ('.implode(', ', $data['places']).') ';
    }
    //Фильтрация по платформам
    if(count($data['platforms']) AND $data['platforms']) {
        $conditions['platforms'] = ' `ds_maindata`.`platform_id` IN ('.implode(', ', $data['platforms']).') ';
    }
    //Дата начала и окончания торгов
    if($begin_date) {
        $conditions['starttime'] = ' `ds_maindata`.`start_time` > "' . $begin_date . '" ';
    }
    if($end_date) {
        $conditions['endtime'] = ' `ds_maindata`.`start_time` < "' . $end_date . '" ';
    }

    if($data['price_start']) {
        $conditions['price_start'] = ' `ds_maindata`.`price` > "' . $data['price_start'] . '" ';
    }
    if($data['price_end']){
      $conditions['price_end'] = ' `ds_maindata`.`price` < "' . $data['price_end'] . '" ';
    }

    $conditions['loadtime'] = ' FROM_UNIXTIME(`ds_maindata`.`loadtime`) > NOW() - INTERVAL 1 DAY ';
    $order_cond = ' ORDER BY `ds_maindata`.`loadtime` DESC ';

    //Компилим условия
    $where_cond = '';
    if($conditions OR $conditions_or) {
        $where_and = '';
        $where_or = '';
        if($conditions) {
            $where_and = '('.implode(' AND ', $conditions).')';
        }
        if($conditions_or) {
            $where_or = '('.implode(' OR ', $conditions_or).')';
        }

        $where_cond = ' WHERE '.$where_and.' '.(($where_and AND $where_or) ? ' AND ' : '').' '.$where_or;
    }

    $join_cond = '';
    if($join_conditions) {
        $join_cond = ' '.implode(' ', $join_conditions);
    }

    $select_cond = '';
    if( $selects ) {
        $select_cond = ' , '.implode(' , ', $selects);
    }

    //Основной запрос
    $lots_query = 'SELECT `ds_maindata`.* ' . $select_cond .' FROM `ds_maindata`' . $join_cond . '' . $where_cond . '' . $order_cond . ' LIMIT 20;';
    return $lots_query;
}

function check_platforms($platforms) {
    $out = array();
    foreach ($platforms as $key => $value) {
        $out[$key] = $key;
    }
    return $out;
}

function check_places($places) {
    $out = array();
    foreach ($places as $key => $value) {
        $out[$key] = $key;
    }
    return $out;
}

function check_types($types)
{
    $out = array();
    foreach ($types as $key => $value) {
        $out[$key] = $key;
    }
    return $out;
}

function ds_time2($timestamp, $format = '')
{
  if(!$format)
    $format = '%d %B2 %Y, %H:%M';

  if (strpos($format, '%B2') === FALSE)
    return strftime($format, $timestamp);
  $month_number = date('n', $timestamp);
  $rusformat = str_replace('%B2', lang('month_r_'.$month_number), $format);
  return strftime($rusformat, $timestamp);
}