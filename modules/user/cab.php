<?php

defined('DS_ENGINE') or die('web_demon laughs');

if (core::$user_id) {

/**************Платежи*****************/

  //$res_pay = core::$db->query('SELECT * FROM `ds_paid` WHERE `summ` > 0 AND userid = 306 ORDER BY `paytime` DESC');
    $res_pay = core::$db->query('SELECT * FROM `ds_paid` WHERE `summ` > 0 AND userid = "'.core::$user_id.'"  ORDER BY `paytime` DESC');

    $i = 0;
    $arr = array();
    $month_names = array(
        1 => 'Январь',
        2 => 'Февраль',
        3 => 'Март',
        4=> 'Апрель',
        5=> 'Май',
        6=> 'Июнь',
        7=> 'Июль',
        8=> 'Август',
        9=> 'Сентябрь',
        10=> 'Октябрь',
        11=> 'Ноябрь',
        12=> 'Декабрь'
    );

    $month_summ = array();

// Перебор всех полученных транзакций и установка доплнительных значений
    while($data_pay = $res_pay->fetch_assoc()) {
        $out = array();

        $out['id'] =  $data_pay['id'];
        $out['userid'] = $data_pay['userid'];
        $out['username'] = $data_pay['username'];
        $out['paidid'] =  $data_pay['paidid'];
        $out['paytime'] =  ds_time($data_pay['paytime'], '%d %B2 %Y');
        $out['time'] =  date('h:m:s', $data_pay['paytime']);
        $out['summ'] =  $data_pay['summ'];
        $out['comm'] =  $data_pay['comm'];

        $month = date('n', $data_pay['paytime']);
        $year = date('Y', $data_pay['paytime']);

        $month_summ[$year][$month] = @$month_summ[$year][$month] + $data_pay['summ'];

        $arr[$year][$month][] = $out;
        $i++;
    }

/*******************Платежи*************************/

    $res = core::$db->query('SELECT * FROM `ds_users` WHERE `id` = "'.core::$user_id.'" LIMIT 1');

    $data = $res->fetch_array();
    $user_info = unserialize($data['info']);
    $user_set = unserialize($data['settings']);

    $eng_right = user::get_rights();
    $avatar = user::get_avatar(core::$user_id, $data['avtime'], 1);

    if ($data['ordercode']) {
        $tar_tariff = array();
        $res = core::$db->query('SELECT * FROM `ds_tariffs` ORDER BY `price` ASC;');
        if ($res->num_rows) {
        
            while ($dat = $res->fetch_array()) {
                $tar_tariff[$dat['id']] = $dat['name'];
            }
            
            $ord_arr = explode(';', $data['ordercode']);
            $ord_arr[1] = abs(intval($ord_arr[1]));
            $ord_arr[2] = abs(intval($ord_arr[2]));
            $ord_arr[3] = abs(intval($ord_arr[3]));

            $tariff = $tar_tariff[$ord_arr[2]];
        } else {
            $tariff = lang('uncknown_tarif');
        }
    }

    if($data['desttime'])
        $desttime = ds_time($data['desttime']);

    //Достаем все профили
    if (core::$user_id AND isset(core::$user_set['tabledata']) AND core::$user_set['tabledata']) {
        $outprofiles = (new searchProfile())->getProfiles(core::$user_id);
    }


    engine_head(lang('cab'));
    temp::assign('user_ip', core::$ip);
    temp::assign('user_ua', core::$ua);
    temp::assign('avatar', $avatar);
    temp::HTMassign('refer', (new refer())->getDetails($data['refer_key']));
    temp::HTMassign('out', $arr);
    temp::HTMassign('marr', $month_names);
    temp::HTMassign('msumm', $month_summ);

    if(isset($tariff))
        temp::assign('tariff', $tariff);

    if(isset($outprofiles))
        temp::HTMassign('outprofiles', $outprofiles);

    temp::assign('online', user::is_online($data['lastvisit']));
    //temp::assign('ordercode', $data['ordercode']);
    if(isset($desttime))
        temp::assign('desttime', $desttime);
    temp::assign('change_nick', preg_match('/^#User\d+/u', $data['login']));
    if($data['type'] == 'email'){
        temp::assign('change_pass', true);
    }
    temp::assign('rights',$eng_right[$data['rights']]);
    temp::display('user.cab');
    engine_fin();



} else {
  denied();
}



