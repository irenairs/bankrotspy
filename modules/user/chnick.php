<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    if($uid = core::$user_id){
        $ds_users = new model(null, 'ds_users');
        if(GET('act')){
            $error = [];
            $nick = htmlspecialchars(POST('nick'));
            if(!$nick) {
                $error[] = lang('empty_nick');
            } elseif(!user::len_nick($nick)) {
                $error[] = lang('nick_wrong_len');
            } elseif(!user::valid_nick($nick)) {
                $error[] = lang('nick_wrong');
            } elseif(user::exists($nick)) {
                $error[] = lang('nick_ex');
            }

            if(!$error){
                $ds_users->updateByField('id', $uid, ['login' => $nick]);
                header('Location:'.core::$home . '/user/cab');
                exit();
            } else {
                engine_head(lang('changenick'));
                temp::HTMassign('error',$error);
                temp::assign('nick', $nick);
                temp::display('user.chnick');
                engine_fin();
            }
        } else {
            $nick = $ds_users->select('login')->where('id', $uid)->fetch('login');
        }
        engine_head(lang('changenick'));
        temp::assign('nick', $nick);
        temp::display('user.chnick');
        engine_fin();
    } else {
        header('Location:'.core::$home);
        exit();
    }