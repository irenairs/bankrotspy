<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    if($uid = core::$user_id) {
        $error = [];
        if(POST('submit')){
            if($name = trim(POST('profile_name'))){
                $def = defaultset::get();
                $searchProfile = new searchProfile();
                if(!isset(core::$user_set['tabledata']) || !core::$user_set['tabledata']){
                    $searchProfile->createProfile($def);
                }
                $searchProfile->createProfile($def, htmlspecialchars($name));
                func::notify(lang('newprofile'), lang('new_item_added'), '/user/cab', lang('continue'));
            } else {
                $error[] = lang('no_profile_name');
            }
        }
    } else {
        denied();
    }

    engine_head(lang('newprofile'));
    temp::HTMassign('error', $error);
    temp::display('user.newprofile');
    engine_fin();