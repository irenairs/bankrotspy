<?php
defined('DS_ENGINE') or die('web_demon laughs');
$id = abs(intval(GET('id')));

if(!core::$user_id)
    denied();

$check = core::$db->query('SELECT * FROM `ds_search_profiles` WHERE `userid` = "'.core::$user_id.'" AND `subscribe` = 1;');
// если есть хотя бы 1 подписка и это не vip
if(CAN('subscribe_lots') OR CAN('subscribe_lots_vip')) {
    // если обычный тариф с существующей рассылкой
    if($check->num_rows >= 2 AND !CAN('subscribe_lots_vip', 0)) {
        func::notify(lang('need_vip'), lang('need_vip_desc'), core::$home.'/tariffs', "Приобрести");  
        return 0;
    // если вип с 5 рассылками
    } elseif ($check->num_rows >= 5 AND CAN('subscribe_lots_vip')) {
        func::notify(lang('subscribe_limit'), lang('subscribe_limit_desc'), core::$home.'/user/cab', "Назад");  
        return 0;
    }
} else {
    func::notify(lang('need_tariff'), lang('need_tariff_desc'), core::$home.'/tariffs', "Приобрести");  
    return 0;
}

if(!$id)
    denied();

$req = core::$db->query('SELECT * FROM `ds_search_profiles` WHERE `id` = "' . $id . '" AND `userid` = "'.core::$user_id.'" LIMIT 1 ;');
if(!$req->num_rows)
    denied();

$data = $req->fetch_assoc();

if($id == core::$user_set['defprofile'])
    denied();

if(POST('submit'))
{
    core::$db->query('UPDATE `ds_search_profiles` SET `subscribe`= 1 WHERE `id` = "'.$id.'";');
    core::$db->query("INSERT INTO ds_counts(page, counttime, day, monthyear) VALUES (18,".time().",".date('j').", ".date('n').date('y')."18)");
    func::notify(lang('subscribe_profile'), lang('new_subscribe_created'), core::$home . '/user/cab', lang('continue'));
}

engine_head(lang('subscribe_profile'));
temp::assign('id', $data['id']);
temp::assign('pname', $data['pname']);
temp::display('user.subscribeprofile');
engine_fin();