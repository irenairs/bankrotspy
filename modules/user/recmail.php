<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    if(mb_strlen($key = htmlspecialchars(GET('id'))) != 32){
        denied();
    }

    $ds_chmail = new model(null, 'ds_chmail');
    if($data = $ds_chmail->where('key', $key)->fetch()){
        $ds_users = new model(null, 'ds_users');
        $ds_users->updateByField('id', $data['user_id'], ['mail' => $data['mail']]);
        if($data['oldmail']){
            $ds_depr_emails = new model(null, 'ds_depr_emails');
            $ds_depr_emails->insert(['mail' => $data['oldmail']]);
        }
        $ds_chmail->deleteByField('id', $data['id']);
        func::notify(lang('activate'), lang('act_succ'), core::$home, lang('continue'));
    } else {
        denied();
    }
