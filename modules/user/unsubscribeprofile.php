<?php
defined('DS_ENGINE') or die('web_demon laughs');
$id = abs(intval(GET('id')));

if(!core::$user_id)
  denied();

if(!$id)
  denied();

$req = core::$db->query('SELECT * FROM `ds_search_profiles` WHERE `id` = "' . $id . '" AND `userid` = "'.core::$user_id.'" LIMIT 1 ;');
if(!$req->num_rows)
  denied();

$data = $req->fetch_assoc();

if($id == core::$user_set['defprofile'])
  denied();

if(POST('submit'))
{
  core::$db->query('UPDATE `ds_search_profiles` SET `subscribe`= 0 WHERE `id` = "'.$id.'";');

  func::notify(lang('unsubscribe_profile'), lang('new_subscribe_deleted'), core::$home . '/user/cab', lang('continue'));
}

engine_head(lang('unsubscribe_profile'));
temp::assign('id', $data['id']);
temp::assign('pname', $data['pname']);
temp::display('user.unsubscribeprofile');
engine_fin();