<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    if(!core::$user_id){
        if(core::$set['reg']){
            if(GET('act')){
                $ds_users = new model(null, 'ds_users');
                $ds_users_inactive = new model(null, 'ds_users_inactive');
                $key = md5(func::passgen(rand(15, 30)));
                function genNick(){
                    while(user::exists($nick = '#User' . rand(1e6, 1e7 - 1)));
                    return $nick;
                }
                if(!isset($_POST['type'])) {
                    $_POST['type'] = 'email';
                }
                if(($_POST['type'] == 'google') && isset($_POST['user'])) {
                    $auth = $_POST['user'];
                    $sid = 'g' . $auth['id'];
                    if(!($user = $ds_users->where(['sid' => $sid, 'type' => 'google'])->fetch())) {
                        $key = md5($sid);
                        $hasRealEmail = isset($auth['emails'][0]['value']);
                        $ds_users_inactive->insert([
                            'login' => genNick(),
                            'password' => md5(md5(md5($sid))),
                            'sid' => $sid,
                            'key' => $key,
                            'mail' => isset($auth['emails'][0]['value']) ? $auth['emails'][0]['value'] : '',
                            'time' => time(),
                            'ip' => core::$ipl,
                            'ua' => core::$ua,
                            'lang' => core::$lang
                        ]);
                        $res = ['url' => core::$home . '/user/rec?g=' . $key];
                    } else {
                        $res = ['cmd' => 'auth'];
                    }
                    header('Content-type: application/json; charset: utf-8', true);
                    echo json_encode($res);
                    exit();
                }elseif(($_POST['type'] == 'facebook') && isset($_POST['authResponse'])) {
                    $auth = $_POST['authResponse'];
                    $sid = 'fb' . $auth['userID'];
                    if(!($user = $ds_users->where(['sid' => $sid, 'type' => 'facebook'])->fetch())) {
                        $key = md5($sid);
                        $ds_users_inactive->insert([
                            'login' => genNick(),
                            'password' => md5(md5(md5($sid))),
                            'sid' => $sid,
                            'key' => $key,
                            'mail' => '',
                            'time' => time(),
                            'ip' => core::$ipl,
                            'ua' => core::$ua,
                            'lang' => core::$lang
                        ]);
                        $res = ['url' => core::$home . '/user/rec?fb=' . $key];
                    } else {
                        $res = ['cmd' => 'auth'];
                    }
                    header('Content-type: application/json; charset: utf-8', true);
                    echo json_encode($res);
                    exit();
                }elseif(($_POST['type'] == 'vk') && isset($_POST['session'])) {
                    $auth = $_POST['session']['user'];
                    $sid = 'vk' . $auth['id'];
                    if(!($user = $ds_users->where(['sid' => $sid, 'type' => 'vk'])->fetch())) {
                        $key = md5($sid);
                        $ds_users_inactive->insert([
                            'login' => genNick(),
                            'password' => md5(md5(md5($sid))),
                            'sid' => $sid,
                            'key' => $key,
                            'mail' => '',
                            'time' => time(),
                            'ip' => core::$ipl,
                            'ua' => core::$ua,
                            'lang' => core::$lang
                        ]);
                        $res = ['url' => core::$home . '/user/rec?vk=' . $key];
                    } else {
                        $res = ['cmd' => 'auth'];
                    }
                    header('Content-type: application/json; charset: utf-8', true);
                    echo json_encode($res);
                    exit();
                }

                $error = [];
                $nick = htmlspecialchars(POST('nick'));
                if(!$nick) {
                    $error[] = lang('empty_nick');
                } elseif(!user::len_nick($nick)) {
                    $error[] = lang('nick_wrong_len');
                } elseif(!user::valid_nick($nick)) {
                    $error[] = lang('nick_wrong');
                } elseif(user::exists($nick)) {
                    $error[] = lang('nick_ex');
                }

                $pass = POST('pass');
                if(!$pass) {
                    $error[] = lang('empty_pass');
                } elseif(mb_strlen($pass) < 4 or mb_strlen($pass) > 15){
                    $error[] = lang('pass_wrong_len');
                    $pass = '';
                }

                $pass_rep = POST('pass_rep');
                if($pass != $pass_rep){
                    $error[] = lang('miss_pass');
                    $pass = '';
                    $pass_rep = '';
                }

                $mail = POST('mail');
                if(!$mail) {
                    $error[] = lang('miss_mail');             //Регулярка отсюда: http://fightingforalostcause.net/misc/2006/compare-email-regex.php
                } elseif(!preg_match('/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD',$mail)) {
                    $error[] = lang('wrong_mail');
                } elseif(func::denied_mail($mail)) {
                    $error[] = lang('error_mail');
                } elseif(user::mail_exists($mail)) {
                    $error[] = lang('mail_ex');
                }

                if(!func::capcha()) {
                    $error[] = lang('miss_capcha');
                }

                if(!$error){
                    $pass_mail = md5(func::passgen(rand(15, 30)));

                    $mailTemplate = (new model(null, 'mail_templates'))->where('name', 'register')->fetch();

                    $body = array(
                        'site'      => core::$home,
                        'link'      => core::$home.'/user/rec?id='.$pass_mail,

                    );
                    $mailer = mailer::factory();
                    $mailer->setSubject($mailTemplate['subject']);
                    $mailer->setBody($mailTemplate['template'], $body);
                    $mailer->addAddress($mail);
                    $mailer->send();

                    $ds_users_inactive->insert([
                        'login' => $nick,
                        'password' => md5(md5($pass)),
                        'key' => $pass_mail,
                        'mail' => $mail,
                        'time' => time(),
                        'ip' => core::$ipl,
                        'ua' => core::$ua,
                        'lang' => core::$lang
                    ]);
                    func::notify(lang('register'), lang('reg_succ'), core::$home);
                } else {
                    engine_head(lang('register'));
                    temp::HTMassign('error',$error);
                    temp::assign('nick', $nick);
                    temp::assign('pass', $pass);
                    temp::assign('pass_rep', $pass_rep);
                    temp::assign('mail', $mail);
                }
            }

            $ds_reg_page = new view(null, 'ds_reg_page');
            $data = $ds_reg_page->where('id', 1)->fetch();
            text::add_cache($data['cache']);
            $text = text::out($data['text'], 0, $data['id']);

            if(!isset($error)) {
                engine_head(lang('register'));
            }
            temp::HTMassign('text', $text);
            temp::HTMassign('capcha', func::img_capcha());
            temp::GLBassign('disableCounters', true);
            temp::display('user.register');
            engine_fin();
        } else {
            func::notify(lang('register'), lang('reg_den'), core::$home, lang('reg_back'));
        }

        (new model(null, 'ds_counts'))->insert([
            'page' => GET('act') ? 14 : 13,
            'counttime' => time(),
            'day' => date('j'),
            'monthyear' => date('ny')
        ]);

    } else {
        header('Location:'.core::$home);
        exit();
    }