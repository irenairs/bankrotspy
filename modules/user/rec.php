<?php
    defined('DS_ENGINE') or die('web_demon laughs');

    if($uid = core::$user_id){
        denied();
    } elseif(GET('id')){
        if(mb_strlen($key = htmlspecialchars(GET('id'))) != 32){
            denied();
        }
        $type = 'email';
    } elseif(GET('g')) {
        $key = htmlspecialchars(GET('g'));
        $type = 'google';
    } elseif(GET('fb')) {
        $key = htmlspecialchars(GET('fb'));
        $type = 'facebook';
    } elseif(GET('vk')) {
        $key = htmlspecialchars(GET('vk'));
        $type = 'vk';
    } else {
        denied();
    }

    $ds_users_inactive = new model(null, 'ds_users_inactive');
    if($user = $ds_users_inactive->where('key', $key)->fetch()){
        $ds_users = new model(null, 'ds_users');
        if($ds_users->insert([
            'login' => $user['login'],
            'sid' => $user['sid'],
            'password' => $user['password'],
            'mail' => $user['mail'],
            'type' => $type,
            'rights' => 0,
            'time' => $user['time'],
            'subscribe' => 1,
            'ip' => core::$ipl,
            'ua' => core::$ua
        ])){
            if($uid = $ds_users->insertId()){

                (new refer())->reg($uid);

                $pay_date = time();
                $tariff = (new model(null, 'ds_tariffs'))->where('id', 9)->fetch();
                $periods = [24*3600, 31*24*3600, 3600];
                $end_date = $pay_date + $tariff['longtime'] * $periods[$tariff['typetime']];
                $ds_users->updateByField('id', $uid, [
                    'rights' => $tariff['rights'],
                    'ordercode' => join(';', ['bspy', $uid, $tariff['id'], $pay_date]),
                    'desttime' => $end_date
                ]);

                (new model(null, 'ds_paid'))->insert([
                    'tarid' => $tariff['id'],
                    'userid' => $uid,
                    'username' => $user['login'],
                    'paidid' => $pay_date,
                    'summ' => $tariff['price'],
                    'paytime' => $pay_date,
                    'comm' => $tariff['name']
                ]);

                if($user['mail']){
                    $mailTemplate = (new model(null, 'mail_templates'))->where('name', 'payment_free')->fetch();
                    $body = array(
                        'name'      => $user['login'],
                        'taiff'     => $tariff['name'],
                        'orderid'   => $pay_date,
                        'date'      => date('d.m.Y', $pay_date),
                        'enddate'   => date('d.m.Y', $end_date)
                    );
                    $mailer = mailer::factory();
                    $mailer->setSubject($mailTemplate['subject']);
                    $mailer->setBody($mailTemplate['template'], $body);
                    $mailer->addAddress($user['mail']);
                    $mailer->send();
                }

                setcookie('force', $uid, time() + 300, '/', system::$config['mode']['host']);
                temp::GLBassign('forceUserName', $user['login']);
            }
        }
        $ds_users_inactive->deleteByField('id', $user['id']);
        (new model(null, 'ds_counts'))->insert([
            'page' => 15,
            'counttime' => time(),
            'day' => date('j'),
            'monthyear' => date('ny')
        ]);
        func::notify(lang('activate'), lang('act_succ'), core::$home . '/torgi', lang('home'));
    } else {
        denied();
    }
