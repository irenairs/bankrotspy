<?php
defined('DS_ENGINE') or die('web_demon laughs');

class column_debpoints
{
    private $price;
    private $debnotice;
    private $access;

    function __construct($params)
    {
        $this->price = isset($params[0]) ? $params[0] : '';
        $this->debnotice = isset($params[1]) ? $params[1] : '';
        $this->access = !empty($params[2]) ? true : false;
    }

    public function before_load()
    {
        return array(
            'sortcolumn' => ' `ds_maindata`.`debpoints` '
        );
    }

    public function name()
    {
        return array(
            'name' => 'Баллы',
            'addhtml' => ' onmousemove="tdMouseMove(\'Рейтинг ликвидности лота, рассчитанный на основе 3-х критериев:<br/>3 - высокое качество лота<br/>2 - среднее качество лота<br/>0-1 - низкое качество лота\',this)" onmouseout="tdMouseOut()" '
        );
    }

    public function process()
    {
        $price = $this->price;
        $access = $this->access;
        
        $class = '';
        $addition = '';
        
        if ($price == -1 && $access) {
            $price = '<i onmousemove="tdMouseMove(\'У этого лота пока нет баллов\',this)" onmouseout="tdMouseOut()" class="icon-help"></i>';
        } else {
            if($this->debnotice && $access) {
                $class = 'cell_with_notify';
                $addition = ' onmousemove="tdMouseMove(\''.text::st(str_replace("\n", '<br/>', str_replace("\r\n", "\n", $this->debnotice))).'\',this)" onmouseout="tdMouseOut()" ';
            } else {
                $price = '<i class="fa fa-lock"></i>';
                $addition = ' onmouseover = "tdMouseOver(\'Информация доступна на платной подписке, <a href=/tariffs>Тарифы</a>\', this)" onmouseout= "tdMouseOut()" ';
            }
        }

        return array(
            'col' => $price,
            'style' => 'text-align:center;padding: 0px;',
            'customclass' => $class,
            'addition' => $addition
        );
    }
}