<?php
defined('DS_ENGINE') or die('web_demon laughs');

class column_minprice{

    private $price;
    private $access;
    private $fixTo;

    function __construct($params){
        $this->price = isset($params[0]) ? ((float)$params[0] ? $params[0] : '-') : '-';
        $this->access = isset($params[1]) ? $params[1] : false;
        $this->fixTo = isset($params[2]) ? $params[2] : 0;
    }

    public function before_load(){
        return ['sortcolumn' => ' IF(`ds_maindata`.`min_price` IS NULL,1,0), `ds_maindata`.`min_price` '];
    }

    public function name(){
        return ['name' => 'Минимальная цена, руб.'];
    }

    public function process() {
        $price = $this->price;
        $access = $this->access;
        $addition = '';
        if($access) {
            $out_price = is_numeric($price) ? number_format($price, $this->fixTo, '.', '&nbsp;') : $price;
        } else {
            $out_price = '<i class="fa fa-lock"></i>';
            $addition = 'onmouseover = "tdMouseOver(\'Информация будет доступна после <a href=/user/register>регистрации</a>\', this)" onmouseout= "tdMouseOut()"';
        }
        return [
            'col' => $out_price,
            'addition' => $addition,
            'style' => 'text-align:center;'
        ];
    }
}

