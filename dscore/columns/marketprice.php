<?php
defined('DS_ENGINE') or die('web_demon laughs');

class column_marketprice
{
    private $price;
    private $access;
    private $hint;
    private $getPrice = false;
    private $fixTo;

    public function __construct($params)
    {
        $this->price = isset($params[0]) ? ((float)$params[0] ? $params[0] : '-') : '-';
        $this->access = !empty($params[1]) ? true : false;
        $this->hint = !empty($params[2]) ? $params[2] : false;
        $this->getPrice = !empty($params[3]) ? $params[3] : false;
        $this->fixTo = isset($params[4]) ? $params[4] : 0;
    }

    public function before_load()
    {
        return array(
            'sortcolumn' => ' `ds_maindata`.`market_price` '
        );
    }

    public function name()
    {
        return array(
            'name' => 'Рыночная цена, руб.',
            'addhtml' => ' onmouseover="tdMouseMove(\'Рыночная цена лота на рынке\',this)" onmouseout="tdMouseOut()" '
        );
    }

    public function process(){
        $addition = '';

        if($this->access){
            if($this->price){
                $out_price = is_numeric($this->price) ? number_format((float)$this->price, $this->fixTo, '.', '&nbsp;') : $this->price;
                if(!empty($this->hint)) {
                    $addition = ' onmouseover="tdMouseMove(\''.$this->hint.'\',this)" onmouseout="tdMouseOut()" ';
                }
            } else {
                if ($this->getPrice) {
                    $out_price = '<a class="get_lot_price"><span>Узнать</span></a>';
                } else {
                    $out_price = '<i class="fa fa-lock"></i>';
                    $addition = 'onmouseover = "tdMouseOver(\'Информация доступна на платной подписке, <a href=/tariffs>Тарифы</a>\', this)" onmouseout= "tdMouseOut()"';
                }
            }
        } else {
            $out_price = '<i class="fa fa-lock"></i>';
            $addition = 'onmouseover = "tdMouseOver(\'Информация доступна на платной подписке, <a href=/tariffs>Тарифы</a>\', this)" onmouseout= "tdMouseOut()"';
        }

        return array(
            'col' => $out_price,
            'style' => 'text-align:center;',
            'addition' => $addition
        );
    }
}