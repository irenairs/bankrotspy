<?php
defined('DS_ENGINE') or die('web_demon laughs');

class column_beginprice{

    private $price;
    private $fixTo;

    function __construct($params){
        $this->price = isset($params[0]) ? $params[0] : '';
        $this->fixTo = isset($params[1]) ? $params[1] : 0;
    }

    public function before_load(){
        return ['sortcolumn' => ' `ds_maindata`.`price` '];
    }

    public function name(){
        return ['name' => 'Начальная цена, руб.'];
    }

    public function process() {
        return [
            'col' => is_numeric($this->price) ? number_format((float)$this->price, $this->fixTo, '.', '&nbsp;') : $this->price,
            'style' => 'text-align:center;'
        ];
    }
}

