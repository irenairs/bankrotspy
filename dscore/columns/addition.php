<?php
defined('DS_ENGINE') or die('web_demon laughs');

class column_addition
{
    private $id;
    private $note;
    private $favorite;
    private $hide;
    public $category;

    public function __construct( $data = array() )
    {
        
        $this->id = isset($data[0]) ? $data[0] : null;
        $this->favorite = isset($data[1]) ? $data[1] : null;
        $this->note = isset($data[2]) ? $data[2] : null;
        $this->category = isset($data[3]) ? $data[3] : null;
        $this->hide = isset($data[4]) ? $data[4] : null;
       
    }

    public function before_load()
    {
        return array(
            'sortcolumn' => ' `ds_maindata`.`id` ' //'sortcolumn' => ' `ds_maindata_favorive`.`item` '
        );
    }

    public function name()
    {
        return array(
            'name' => '<i title="Дополнительно" class=""><i cmd="fav|bn|" title="Добавить все лоты в избранное" class="icon-star-empty"></i></i>', //А, че, оказывается можно и иконку засунуть
            'style' => 'max-width: 50px;min-width: 40px;',
            'nosort' => 1
        );
    }

    public function process()
    {
        if($this->favorite)
            $favStar = '<i cmd="fav|bn|del|' . $this->id . '" title="Удалить лот из избранного" class="icon-star-clicked"></i>';
        else
            $favStar = '<i cmd="fav|bn||' . $this->id . '" title="Добавить лот в избранное" class="icon-star-empty"></i>';
        
        if( $this->hide )
            $hideStar = '<i cmd="hide|bn|del|' . $this->id . '" title="Достать лот из мусора" class="icon-forward"></i>';
        else
            $hideStar = '<i cmd="hide|bn||' . $this->id . '" title="Отправить лот в мусор" class="icon-delete"></i>';
        
        $note = '<i data-note="'.$this->note.'" cmd="note_pop|bn|' . $this->id . '" title="Комментарий к лоту" class="fa fa-sticky-note';
        if($this->note)
            $note .= '"></i>';
        elseif($this->category == '-1' && $this->favorite)
            $note .= '-o"></i>';
        else 
            $note = '';
        
        return [
            'col' => implode('<br/>', [$favStar, $hideStar, $note]),
            'style' => 'text-align:center;'
        ];
    }
}
