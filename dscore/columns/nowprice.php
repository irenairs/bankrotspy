<?php
defined('DS_ENGINE') or die('web_demon laughs');

class column_nowprice {
    private $price;
    private $platform_id;
    private $type;
    private $itemId;
    private $fixTo;
    private $priceSchedule;

    function __construct($params) {

        $this->price = isset($params[0]) ? ((float)$params[0] ? $params[0] : '-') : '-';
        $this->platform_id = isset($params[1]) ? $params[1] : '';
        $this->type = isset($params[2]) ? $params[2] : '';
        $this->itemId = isset($params[3]) ? $params[3] : 0;
        $this->fixTo = isset($params[4]) ? $params[4] : 0;
        $this->priceSchedule = new priceSchedule();
    }

    public function before_load() {
        return ['sortcolumn' => ' IF(`ds_maindata`.`now_price` IS NULL,1,0), `ds_maindata`.`now_price` '];
    }

    public function name() {
        return array(
            'name' => 'Текущая цена, руб.'
        );
    }

    public function process() {
        if(($this->platform_id >= 16) && $this->itemId && ($price = $this->priceSchedule->getActualPrice($this->itemId))){
            $this->price = $price;
        }

        $out_price = is_numeric($this->price) ? number_format($this->price, $this->fixTo, '.', '&nbsp;') : $this->price;
        $style = 'text-align: center;';
        $isCalculated = 0;
        
        $man_plf = func::get_manual_platforms();
        
        if(in_array($this->platform_id, $man_plf) && $this->type == 2 && !$this->itemId) {
            $out_price = "Уточните цену на площадке";
            $style .= ' color: #d27600;';
            $isCalculated = 1;
        }

        return array(
            'isCalculated' => $isCalculated,
            'col' => $out_price,
            'style' => $style
        );
    }
}