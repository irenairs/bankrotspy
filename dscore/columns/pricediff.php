<?php
defined('DS_ENGINE') or die('web_demon laughs');

class column_pricediff
{
    private $price;
    private $platform_id;
    private $type;
    private $schedule;
    
    function __construct($params)
    {
        $this->price = isset($params[0]) ? (is_numeric($params[0]) ? number_format($params[0], 0, '.', '&nbsp;') : '-') : '';
        $this->platform_id = isset($params[1]) ? $params[1] : '';
        $this->type = isset($params[2]) ? $params[2] : '';
        $this->schedule = isset($params[3]) ? $params[3] : false;
    }

    public function before_load()
    {
        return array(
            'sortcolumn' => ' `price_diff` '
        );
    }

    public function name()
    {
        return array(
            'name' => 'Понижение<br/> цены, %',
            'addhtml' => '  onmousemove="tdMouseMove(\'Разница между начальной и текущей ценой\',this)" onmouseout="tdMouseOut()" '
        );
    }

    public function process()
    {
        $price = $this->price;

        $color_red = false;
        
        if($price < 0)
            $color_red = true;

        if($price === '')
            $price = '-';

        if($color_red)
            $price = '<span style="color:#ff7863">' .$price.'</span>';

        $man_plf = func::get_manual_platforms();
        
        if(in_array($this->platform_id, $man_plf) AND $this->type == 2 && strlen($this->schedule) < 10)
            $price = '<i onmousemove="tdMouseMove(\'Не рассчитывается, т.к. цена определяется вручную\',this)" onmouseout="tdMouseOut()" class="icon-help"></i>';

        return array(
            'col' => $price,
            'style' => 'text-align:center;'
        );
    }
}