<?php
defined('DS_ENGINE') or die('web_demon laughs');

class column_profitrub
{
    private $price;
    private $platform_id;
    private $type;
    private $access;
    private $schedule;
    private $fixTo;

    function __construct($params)
    {
        $this->price = isset($params[0]) ? ((float)$params[0] ? $params[0] : '-') : '-';
        $this->platform_id = isset($params[1]) ? $params[1] : '';
        $this->type = isset($params[2]) ? $params[2] : '';
        $this->access = !empty($params[3]) ? true : false;
        $this->schedule = isset($params[4]) ? $params[4] : false;
        $this->fixTo = isset($params[5]) ? $params[5] : 0;
    }

    public function before_load()
    {
        return array(
            'sortcolumn' => ' `ds_maindata`.`profit_rub` '
        );
    }

    public function name()
    {
        return array(
            'name' => 'Доход, руб.',
            'addhtml' => '  onmouseover="tdMouseMove(\'Разница между ценой на рынке и на торгах\',this)" onmouseout="tdMouseOut()" '
        );
    }

    public function process(){
        $addition = '';
        
        $color_red = is_numeric($this->price) && ($this->price < 0);

        if ($access = $this->access) {
            $out_price = is_numeric($this->price) ? number_format((float)$this->price, $this->fixTo, '.', '&nbsp;') : $this->price;
        } else {
            $out_price = '<i class="fa fa-lock"></i>';
        }
    
        if($color_red){
            $out_price = '<span style="color:#ff7863">' .$out_price.'</span>';
        }

        $man_plf = func::get_manual_platforms();
        if(in_array($this->platform_id, $man_plf) && $this->type == 2 && strlen($this->schedule) < 10) {
            if($access) {
                $out_price = '<i onmouseover="tdMouseMove(\'Не рассчитывается, т.к. цена определяется вручную\',this)" onmouseout="tdMouseOut()" class="icon-help"></i>';
            } else {
                $addition = 'onmouseover = "tdMouseOver(\'Информация доступна на платной подписке, <a href=/tariffs>Тарифы</a>\', this)" onmouseout= "tdMouseOut()"';
            }
        } elseif(!$access) {
            $addition = 'onmouseover = "tdMouseOver(\'Информация доступна на платной подписке, <a href=/tariffs>Тарифы</a>\', this)" onmouseout= "tdMouseOut()"';
        }
    
        return array(
            'col' => $out_price,
            'style' => 'text-align:center;',
            'addition' => $addition
        );
    }
}