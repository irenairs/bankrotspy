<?php

class mapColumns {

    private static $settings = [
        'export' => [
            [
                'title' => 'Основные',
                'items' => [
                    [
                        'name' => 'name',
                        'title' => 'Описание'
                    ], [
                        'name' => 'src',
                        'title' => 'Тип имущества',
                        'hc' => true
                    ], [
                        'name' => 'type',
                        'title' => 'Тип торгов',
                        'hc' => true,
                        'table' => 'ds_maindata_type',
                        'fld' => 'type_name'
                    ], [
                        'name' => 'place',
                        'title' => 'Регион',
                        'hc' => true,
                        'table' => 'ds_maindata_regions',
                        'fld' => 'name'
                    ], [
                        'name' => 'cat_id',
                        'title' => 'Категория',
                        'hc' => true,
                        'table' => 'ds_maindata_category',
                        'fld' => 'name'
                    ], [
                        'name' => 'status',
                        'title' => 'Статус',
                        'hc' => true,
                        'table' => 'ds_maindata_status',
                        'fld' => 'status_name'
                    ],
                ]
            ], [
                'title' => 'Даты',
                'items' => [
                    [
                        'name' => 'start_time',
                        'title' => 'Дата начала',
                        'hc' => true
                    ], [
                        'name' => 'end_time',
                        'title' => 'Дата окончания',
                        'hc' => true
                    ],
                ]
            ], [
                'title' => 'Цены, руб.',
                'items' => [
                    [
                        'name' => 'now_price',
                        'title' => 'Текущая цена',
                        'hc' => true
                    ], [
                        'name' => 'min_price',
                        'title' => 'Минимальная цена',
                        'hc' => true
                    ], [
                        'name' => 'market_price',
                        'title' => 'Рыночная цена',
                        'hc' => true
                    ],
                ]
            ], [
                'title' => 'Аналитика',
                'items' => [
                    [
                        'name' => 'profit_rub',
                        'title' => 'Доход, руб',
                        'hc' => true
                    ], [
                        'name' => 'profit_proc',
                        'title' => 'Доходность, %',
                        'hc' => true
                    ], [
                        'name' => 'price_diff',
                        'title' => 'Понижение цены, %',
                        'hc' => true
                    ],
                ]
            ], [
                'title' => 'Банкрот',
                'items' => [
                    [
                        'name' => 'dept_name',
                        'title' => 'Банкрот',
                        'table' => 'ds_maindata_debtors',
                    ], [
                        'name' => 'case_number',
                        'title' => 'Дело №',
                        'hc' => true
                    ], [
                        'name' => 'd_inn',
                        'title' => 'ИНН банкрота',
                        'hc' => true,
                        'table' => 'ds_maindata_debtors',
                        'fld' => 'inn'
                    ],
                ]
            ], [
                'title' => 'Организатор',
                'items' => [
                    [
                        'name' => 'org_name',
                        'title' => 'Организатор торгов',
                        'table' => 'ds_maindata_organizers',
                    ], [
                        'name' => 'o_inn',
                        'title' => 'ИНН организатора',
                        'hc' => true,
                        'table' => 'ds_maindata_organizers',
                        'fld' => 'inn'
                    ], [
                        'name' => 'manager',
                        'title' => 'Арбитражный управляющий',
                        'table' => 'ds_maindata_organizers',
                    ], [
                        'name' => 'contact_person',
                        'title' => 'Контактное лицо',
                        'table' => 'ds_maindata_organizers',
                    ],
                ]
            ], [
                'title' => 'Дополнительно',
                'items' => [
                    [
                        'name' => 'auct_link',
                        'title' => 'Торги на площадке',
                        'hc' => true
                    ], [
                        'name' => 'fedlink',
                        'title' => 'Лот на федресурсе',
                        'hc' => true
                    ],
                ]
            ],
        ],
        'view' => [
            [
                'title' => 'Даты',
                'items' => [
                    [
                        'name' => 'begindate',
                        'title' => 'Дата начала',
                        'def' => false
                    ], [
                        'name' => 'closedate',
                        'title' => 'Дата окончания',
                        'def' => false
                    ],
                ]
            ], [
                'title' => 'Цены, руб.',
                'items' => [
                    [
                        'name' => 'minprice',
                        'title' => 'Минимальная цена',
                        'def' => false,
                        'can' => 'price_reduction_schedule',
                        'bl' => 'pay'
                    ], [
                        'name' => 'marketprice',
                        'title' => 'Рыночная цена',
                        'def' => true,
                        'can' => 'market_price',
                        'bl' => 'pay'
                    ],
                ]
            ], [
                'title' => 'Аналитика',
                'items' => [
                    [
                        'name' => 'profitrub',
                        'title' => 'Доход, руб',
                        'def' => true,
                        'can' => 'profit_rub',
                        'bl' => 'vip'
                    ], [
                        'name' => 'profitproc',
                        'title' => 'Доходность, %',
                        'def' => false,
                        'can' => 'profit_proc',
                        'bl' => 'vip'
                    ], [
                        'name' => 'pricediff',
                        'title' => 'Понижение цены, %',
                        'def' => true
                    ], [
                        'name' => 'debpoints',
                        'title' => 'Баллы',
                        'def' => true,
                        'can' => 'scores_debtor',
                        'bl' => 'vip'
                    ],
                ]
            ], [
                'title' => 'Дополнительно',
                'items' => [
                    [
                        'name' => 'platform',
                        'title' => 'Федресурс, Площадка',
                        'def' => true,
                        'groups' => [10,11,70,100],
                        'bl' => 'pay'
                    ], [
                        'name' => 'addition',
                        'title' => 'Избранное',
                        'def' => true,
                        'can' => 'add_favorites',
                        'bl' => 'pay'
                    ],
                ]
            ],
        ]
    ];

    public static function get($mode) {
        return self::$settings[$mode];
    }

    public static function getForExport($fields){
        $res = [
            'fields' => [],
            'tables' => []
        ];
        foreach (self::$settings['export'] as $group){
            foreach ($group['items'] as $item){
                if(in_array($item['name'], $fields)){
                    $res['fields'][$item['name']] = [
                        'title' => $item['title'],
                        'hc' => isset($item['hc']) && $item['hc']
                    ];
                    if(isset($item['table'])){
                        $tbl = $item['table'];
                        $res['fields'][$item['name']]['table'] = $tbl;
                        if(!isset($res['tables'][$tbl])){
                            $res['tables'][$tbl] = [];
                        }
                        $fld = isset($item['fld']) ? $item['fld'] : $item['name'];
                        $res['tables'][$tbl][] = $fld;
                        $res['fields'][$item['name']]['fld'] = $fld;
                    }
                }
            }
        }
        return $res;
    }
}