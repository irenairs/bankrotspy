<?php
/**
 * bankrot-spy.ru
 * Author: Vladimir S. <guyasyou@gmail.com>
 * www.SiteCreate54.ru
 * © 2016
 */
class helper {

    const fix_limit = 5000;
    const email_pattern = '/([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)/';
    const phone_pattern = '/(?<country>\+?7|8){0,1}\s?(?<code>\({1}\d{3,4}\){1})(?<number>\d{2,3}[\s-]?\d{2,4}[\s|\-\d]{0,3})/';

    //---------------------------------------
    //Исправление email адресов
    //---------------------------------------
    public static function fixLotEmail($lotID) {
        $lotData = self::getLotTextAndOrg($lotID);
        if ($lotData) {
            $text = $lotData['name'].$lotData['description'];
            if (preg_match(self::email_pattern, $text, $matches)) {
                $email = trim($matches[0]);
                core::$db->query('UPDATE `ds_maindata_organizers`
                  SET `mail`="'.core::$db->res($email).'"
                  WHERE `ds_maindata_organizers`.`id` = '.$lotData['org_id'].';');
                return $email;
            }
        }
        return false;
    }

    public static function fixLotEmails($lotIDs = array()) {
        $fixedEmails = array();
        if (is_array($lotIDs)) {
            foreach ($lotIDs as $lotID) {
                $fixedEmail = self::fixLotEmail($lotID['id']);
                if ($fixedEmail) {
                    $fixedEmails[] = $fixedEmail;
                }
            }
        }
        return $fixedEmails;
    }


    //---------------------------------------
    //Исправление номеров телефонов
    //---------------------------------------
    public static function fixLotPhone($lotID) {
        $lotData = self::getLotTextAndOrg($lotID);
        if ($lotData) {
            $text = $lotData['name'].$lotData['description'];
            if (preg_match(self::phone_pattern, $text, $matches)) {
                $phone = trim($matches[0]);
                core::$db->query('UPDATE `ds_maindata_organizers`
                  SET `phone`="'.core::$db->res($phone).'"
                  WHERE `ds_maindata_organizers`.`id` = '.$lotData['org_id'].';');
                return $phone;
            }
        }
        return false;
    }

    public static function fixLotPhones($lotIDs = array()) {
        $fixedPhones = array();
        if (is_array($lotIDs)) {
            foreach ($lotIDs as $lotID) {
                $fixedPhone = self::fixLotPhone($lotID['id']);
                if ($fixedPhone) {
                    $fixedPhones[] = $fixedPhone;
                }
            }
        }
        return $fixedPhones;
    }



    //---------------------------------------
    //Общие метоыды для исправления контактной информации
    //---------------------------------------
    public static function lotHaveEmail($lotID) {
        $result = core::$db->query('SELECT `ds_maindata`.`id`
          FROM `ds_maindata`
          LEFT JOIN `ds_maindata_organizers` ON `ds_maindata`.`organizer` = `ds_maindata_organizers`.`id`
          WHERE (`ds_maindata_organizers`.`id` > 0)
          AND (`ds_maindata_organizers`.`mail` IS NOT NULL AND TRIM(`ds_maindata_organizers`.`mail`) <> "")
          AND (`ds_maindata`.`id` = '.$lotID.');');
        if ($result->num_rows != 0) {
            return true;
        }
        return false;
    }

    public static function lotHavePhone($lotID) {
        $result = core::$db->query('SELECT `ds_maindata`.`id`
          FROM `ds_maindata`
          LEFT JOIN `ds_maindata_organizers` ON `ds_maindata`.`organizer` = `ds_maindata_organizers`.`id`
          WHERE (`ds_maindata_organizers`.`id` > 0)
          AND (`ds_maindata_organizers`.`phone` IS NOT NULL AND TRIM(`ds_maindata_organizers`.`phone`) <> "")
          AND (`ds_maindata`.`id` = '.$lotID.');');
        if ($result->num_rows != 0) {
            return true;
        }
        return false;
    }

    public static function getLotsWithoutContact($type = 'email', $offset = 0, $limit = self::fix_limit) {
        /** @var SQLResult $result */
        switch ($type) {
            case 'email':
                $column = 'mail';
                break;
            case 'phone':
                $column = 'phone';
                break;
        }
        $result = core::$db->query('SELECT `ds_maindata`.`id`
          FROM `ds_maindata`
          LEFT JOIN `ds_maindata_organizers` ON `ds_maindata`.`organizer` = `ds_maindata_organizers`.`id`
          WHERE (`ds_maindata_organizers`.`id` > 0)
          AND (`ds_maindata_organizers`.`'.$column.'` IS NULL OR `ds_maindata_organizers`.`'.$column.'` = "")
          LIMIT '.$offset.','.$limit.';');
        if ($result->num_rows != 0) {
            return $result->fetch_all();
        }
        return false;
    }

    public static function getLotsWithoutContactsCount($type = 'email') {
        /** @var SQLResult $result */
        switch ($type) {
            case 'email':
                $column = 'mail';
                break;
            case 'phone':
                $column = 'phone';
                break;
        }
        $result = core::$db->query('SELECT COUNT(`ds_maindata`.`id`)
          FROM `ds_maindata`
          LEFT JOIN `ds_maindata_organizers` ON `ds_maindata`.`organizer` = `ds_maindata_organizers`.`id`
          WHERE (`ds_maindata_organizers`.`id` > 0)
          AND (`ds_maindata_organizers`.`'.$column.'` IS NULL OR `ds_maindata_organizers`.`'.$column.'` = "");');
        if ($result->num_rows != 0) {
            return $result->count();
        }
        return 0;
    }

    public static function getLotTextAndOrg($lotID) {
        /** @var SQLResult $result */
        $result = core::$db->query('SELECT `ds_maindata_organizers`.`id` AS `org_id`, `ds_maindata`.`name`, `ds_maindata`.`description`
          FROM `ds_maindata`
          LEFT JOIN `ds_maindata_organizers` ON `ds_maindata`.`organizer` = `ds_maindata_organizers`.`id`
          WHERE `ds_maindata`.`id` = '.$lotID.';');
        if ($result->num_rows != 0) {
            return $result->fetch();
        }
        return false;
    }


    //Выдает страницу ошибки 404
    public static function dispatch404() {
        header("HTTP/1.0 404 Not Found");
        engine_head('Страница не найдена');
        temp::HTMassign('navigation', nav::display(1, core::$home.'/404?'));
        temp::display('404.index');
        exit;
    }
}