<?php
/**
 * bankrot-spy.ru
 * Author: Vladimir S. <guyasyou@gmail.com>
 * www.SiteCreate54.ru
 * © 2016
 */
abstract class data_index {

    public static function get($type = '') {
        switch ($type) {
            case 'bankrot':
                return new data_index_bankrot();
            case 'konfiscat':
                return new data_index_konfiscat();
            case 'privatizacia':
                return new data_index_privatizacia();
            default:
                return false;
        }
    }

    /**
     * @return string
     */
    abstract protected function getSourceTableName();

    /**
     * @return string
     */
    abstract protected function getIndexTableName();

    /**
     * @return string
     */
    abstract protected function getIndexKeyColumnName();

    /**
     * @return string
     */
    abstract protected function getIndexContentColumnName();

    /**
     * Возвращает массив данных о лоте по номеру
     * @param integer $lotID
     * @return array
     */
    abstract protected function getData($lotID);


    //Шаг выборки
    //количество лотов которые будут обработаны в одном запросе
    public function rowsPerStep() {
        return 5000;
    }

    /**
     * Возвращает массив с lotID и контентом индекса
     * @param integer $lotID
     * @return array
     */
    public function getIndexByLotID($lotID) {
        /** @var SQLResult $result */
        if (is_numeric($lotID)) {
            $result = core::$db->query('SELECT * FROM `'.$this->getIndexTableName().'` WHERE `'.$this->getIndexKeyColumnName().'` = '.$lotID.';');
            if ($result->num_rows != 0) {
                return $result->fetch_assoc();
            }
        }
    }

    public function getLotsCount() {
        /** @var SQLResult $result */
        $result = core::$db->query('SELECT COUNT(`'.$this->getSourceTableName().'`.id) FROM `'.$this->getSourceTableName().'`;');
        $lots_number = $result->fetch_row()[0];
        return $lots_number;
    }

    public function fullReindex($offset = 0, $limit = 100) {
        /** @var SQLResult $result */
        $result = core::$db->query('SELECT `'.$this->getSourceTableName().'`.id FROM `'.$this->getSourceTableName().'` LIMIT '.$offset.','.$limit.';');
        if ($result->num_rows != 0) {
            $lotIDs = $result->fetch_all();
            foreach ($lotIDs as $lotID) {
                $this->reindex($lotID['id']);
            }
        }
    }

    /**
     * Создает или обновляет индек лота по его номеру
     * @param integer $lotID
     */
    public function reindex($lotID = 0) {
        if ($lotID && is_numeric($lotID)) {
            //Получаем данные лота
            $lotDataArray = $this->getData($lotID);
            if ($lotDataArray) {
                //Склеиваем данные в одну строку
                $lotDataString = implode(' ', $lotDataArray);
                if ($this->getIndexByLotID($lotID)) {
                    //Если индекс уже есть, обновляем его
                    $this->update($lotID, $lotDataString);
                } else {
                    //Если индекса нет, создаем новую запись
                    $this->create($lotID, $lotDataString);
                }
            }
        }
    }

    /**
     * Создает новый индекс
     * @param integer $lotID
     * @param string $lotDataString
     */
    protected function create($lotID, $lotDataString) {
        core::$db->query('INSERT INTO `'.$this->getIndexTableName().'` (`'.$this->getIndexKeyColumnName().'`, `'.$this->getIndexContentColumnName().'`) VALUES ('.$lotID.', "'.core::$db->res($lotDataString).'");');
    }

    /**
     * Обновляет существующий индекс
     * @param integer $lotID
     * @param string $lotDataString
     */
    protected function update($lotID, $lotDataString) {
        core::$db->query('UPDATE `'.$this->getIndexTableName().'` SET `'.$this->getIndexContentColumnName().'`="'.core::$db->res($lotDataString).'" WHERE `'.$this->getIndexKeyColumnName().'`='.$lotID.';');
    }
}