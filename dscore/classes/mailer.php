<?php

require_once system::realPath('dscore/libs/phpmailer/PHPMailerAutoload.php');

class Mailer extends Phpmailer{
    protected $path;
    protected $template;
    public  $log;
    
    public static function factory($path = './data/engine/'){
        return new Mailer($path);
    }
    
    public function __construct($path = ''){
        parent::__construct();
        $cnf = system::$config['email']['config'];
        $this->SMTPDebug = 3;
        $this->Debugoutput = [$this, 'log'];
        
        $this->path = $path;
        
        $this->isSMTP();
        $this->SMTPAuth = true;

        $this->Host = $cnf['host'];
        $this->Port = $cnf['port'];

        $this->Username = $cnf['login'];
        $this->Password = $cnf['pass'];
        //$this->SMTPSecure = 'tls';

        $this->isHTML(true);
        $this->CharSet = 'UTF-8';
        $this->setFrom($cnf['login'], system::$config['mode']['name']);
    }
    
    public function log($str, $level) {
        $this->log .= $str;
    }
    
    public function setBody($template, array $data = array()){
        $file = $this->path . $template . '.tpl';
        if(file_exists($file)) {
            $template = file_get_contents($file);
        }
        
        $this->Body = $this->parseStr($template, $data);
    }
    
    public function setSubject($str, array $data = array())
    {
        $this->Subject = $this->parseStr($str, $data);
    }
    
    public function parseStr($str, $data)
    {
        if(!empty($data)) {
            foreach($data as $key => $value) {
                $str = $this->replace($str, $key, $value);
            }
        }
        return $this->clear($str);
    }
    
    public function replace($str, $key, $value)
    {
        return str_replace('{$'.$key.'}', $value, $str);
    }
    
    public function clear($str)
    {
        return preg_replace('#{[^}]+}#is', '', $str);
    }
    
    public function debug()
    {
        return $this->log;
    }
}