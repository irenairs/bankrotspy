<?php
/**
 * bankrot-spy.ru
 * Author: Vladimir S. <guyasyou@gmail.com>
 * www.SiteCreate54.ru
 * © 2016
 */

class data_index_privatizacia extends data_index {

    /**
     * @return string
     */
    protected function getSourceTableName()
    {
        return 'privatizacia';
    }

    /**
     * @return string
     */
    protected function getIndexTableName()
    {
        return 'privatizacia_index';
    }

    /**
     * @return string
     */
    protected function getIndexKeyColumnName()
    {
        return 'lotID';
    }

    /**
     * @return string
     */
    protected function getIndexContentColumnName()
    {
        return 'content';
    }

    /**
     * Возвращает массив данных о лоте по номеру
     * @param integer $lotID
     * @return array
     */
    protected function getData($lotID) {
        /** @var SQLResult $result */
        $result = core::$db->query('
            SELECT
               `'.$this->getSourceTableName().'`.nomeri,
               `'.$this->getSourceTableName().'`.name,
               `'.$this->getSourceTableName().'`.mesto,
               `'.$this->getSourceTableName().'`.region,
               `'.$this->getSourceTableName().'`.organizer,
               `'.$this->getSourceTableName().'`.tipi
            FROM
               `'.$this->getSourceTableName().'`
            WHERE `'.$this->getSourceTableName().'`.`id` = "'.$lotID.'"
        ;');
        if ($result->num_rows != 0) {
            return $result->fetch_assoc();
        }
    }
}