<?php

class defaultset {

    public static function get() {
        $defSet = cache::get('settings_default');
        return [
            'categories' => $defSet['categories'],
            'page' => 1,
            'kmess' => 20,
            'svalue' => '',
            'search_mode' => 'any',
            'types' => $defSet['type'],
            'date_mode' => 'date',
            'date_range' => 'begin',
            'begin_date' => '',
            'end_date' => '',
            'altint' => '',
            'price_start' => '',
            'price_end' => '',
            'type_price' => 1,
            'sortcolumn' => '',
            'sorttype' => '',
            'places' => $defSet['place'],
            'platforms' => $defSet['platform'],
            'src' => $defSet['src'],
            'status' => [1 => 1, 2 => 1, 3 => 0],
            'new_lots' => 0,
            'more' => 1,
            'hasPhoto' => 0,
            'user' => $defSet['user'],
            'filterActive' => ['text' => 1]
        ];
    }
}