<?php
/**
 * bankrot-spy.ru
 * Author: Vladimir S. <guyasyou@gmail.com>
 * www.SiteCreate54.ru
 * © 2016
 */

class data_index_bankrot extends data_index {

    /**
     * @return string
     */
    protected function getSourceTableName()
    {
        return 'ds_maindata';
    }

    /**
     * @return string
     */
    protected function getIndexTableName()
    {
        return 'ds_maindata_index';
    }

    /**
     * @return string
     */
    protected function getIndexKeyColumnName()
    {
        return 'lotID';
    }

    /**
     * @return string
     */
    protected function getIndexContentColumnName()
    {
        return 'content';
    }

    /**
     * Возвращает массив данных о лоте по номеру
     * @param integer $lotID
     * @return array
     */
    protected function getData($lotID) {
        /** @var SQLResult $result */
        $result = core::$db->query('
            SELECT
               `ds_maindata`.name,
               `ds_maindata`.description,
               `ds_maindata`.code,
               `ds_maindata`.case_number,
               `ds_maindata_regions`.name AS region,
               `ds_maindata_debtors`.dept_name,
               `ds_maindata_debtors`.inn AS dept_inn,
               `ds_maindata_organizers`.org_name,
               `ds_maindata_organizers`.inn AS org_inn
            FROM
               `ds_maindata`
                LEFT JOIN `ds_maindata_regions` ON `ds_maindata`.`place` = `ds_maindata_regions`.`number`
                LEFT JOIN `ds_maindata_debtors` ON `ds_maindata`.`debtor` = `ds_maindata_debtors`.`id`
                LEFT JOIN `ds_maindata_organizers` ON `ds_maindata`.`organizer` = `ds_maindata_organizers`.`id`
            WHERE `ds_maindata`.`id` = "'.$lotID.'"
        ;');
        if ($result->num_rows != 0) {
            return $result->fetch_assoc();
        }
    }
}