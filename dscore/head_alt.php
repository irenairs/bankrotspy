<?php
defined('DS_ENGINE') or die('web_demon laughs');

if ((stristr(core::$ua, 'msie') && stristr(core::$ua, 'windows')))     //or stristr(core::$ua, "chrome")
{
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Content-type: text/html; charset=UTF-8'); 
}
else
{
    header("Cache-Control: public");
    header('Content-type: text/html; charset=UTF-8');
    //header('Content-type: application/xhtml+xml; charset=UTF-8');
}
header('Expires: ' . date('r',  time() + 60));

temp::GLBassign('themepath', core::$theme_path);
temp::GLBassign('new_mail', core::$new_mail);
temp::GLBassign('home',  core::$home);
temp::assign('title', core::$page_title);
if(isset($rmenu)){
    temp::HTMassign('rmenu',  $rmenu);
}
if(core::$user_id)
  temp::GLBassign('user_id', core::$user_id);
if(core::$user_name)
  temp::GLBassign('user_name', core::$user_name);
if(core::$module != core::$set['module'] or core::$action != core::$set['action'])
  temp::GLBassign('noindex',1);
if(core::$page_keywords)
  temp::assign('keywords', core::$page_keywords);
elseif(core::$module=='index' AND core::$action=='index')
  temp::HTMassign('keywords', core::$set['keywords']);
if(core::$page_description)
  temp::assign('description', core::$page_description);
elseif(core::$module=='index' AND core::$action=='index')
  temp::HTMassign('description', core::$set['description']);
temp::GLBassign('ad', (new ad())->getAdTarget());
temp::display('core.head_alt');
