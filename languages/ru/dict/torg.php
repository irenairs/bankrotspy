<?php
return [
    'filter' => [
        'text' => [
            'title' => 'Описание лота',
            'icon' => 'icon-pencil',
            'schema' => 'text',
            'r' => 0,
            'map' => [
                'all' => 'Все слова',
                'any' => 'Любое из слов',
                'target' => 'Точная фраза',
                'list' => ['target', 'all', 'any']
            ]
        ],
        'category' => [
            'title' => 'Категория',
            'icon' => 'icon-list',
            'schema' => 'tree',
            'r' => 0,
            'prm' => 'categories',
            'mode' => 'view',
        ],
        'date' => [
            'title' => 'Расписание',
            'icon' => 'icon-calendar',
            'schema' => 'date',
            'r' => 0,
            'mapMode' => [
                'date' => 'Дата',
                'wait' => 'Дней до',
                'list' => ['wait', 'date']
            ],
            'mapRange' => [
                'begin' => 'начала',
                'end' => 'окончания',
                'list' => ['end', 'begin']
            ]
        ],
        'price' => [
            'title' => 'Цена',
            'icon' => 'icon-rouble',
            'schema' => 'price',
            'r' => 0,
            'map' => [
                '1' => 'Текущая',
                '2' => 'Минимальная',
                '3' => 'Рыночная',
                'list' => [3, 2, 1]
            ]
        ],
        'region' => [
            'title' => 'Регион',
            'icon' => 'icon-location',
            'schema' => 'tree',
            'r' => 0,
            'prm' => 'places',
            'mode' => 'view',
        ],
        'platform' => [
            'title' => 'Площадка',
            'icon' => 'icon-globe-set',
            'schema' => 'tree',
            'r' => 0,
            'prm' => 'platforms',
            'mode' => 'view',
        ],
        'type' => [
            'title' => 'Тип торгов',
            'icon' => 'icon-hammer-set',
            'schema' => 'group',
            'r' => 0,
            'prm' => 'types',
            'map' => [
                '1' => 'Аукцион',
                '2' => 'Публичное предложение',
                'list' => [1, 2]
            ]
        ],
        'status' => [
            'title' => 'Статус',
            'icon' => 'icon-clock',
            'schema' => 'group',
            'r' => 0,
            'prm' => 'status',
            'map' => [
                '1' => 'Объявленные',
                '2' => 'Приём заявок',
                '3' => 'Окончены',
                'list' => [1, 2, 3]
            ]
        ],
        'src' => [
            'title' => 'Имущество',
            'icon' => 'icon-share',
            'schema' => 'group',
            'r' => 0,
            'prm' => 'src',
            'map' => [
                'bn' => 'Банкротство',
                'cn' => 'Конфискат',
                'pr' => 'Приватизация',
                'list' => ['bn', 'cn', 'pr']
            ]
        ],
        'extra' => [
            'title' => 'Дополнительно',
            'icon' => 'icon-ok',
            'schema' => 'group',
            'r' => 0,
            'map' => [
                'hasPhoto' => 'Фото',
                'more' => 'Подробнее',
                'new_lots' => 'Новые лоты',
                'list' => ['new_lots', 'more', 'hasPhoto']
            ]
        ],
        'user' => [
            'title' => 'Метки',
            'icon' => 'icon-tags',
            'schema' => 'group',
            'r' => 10,
            'prm' => 'user',
            'map' => [
                'all' => 'Без меток',
                'fav' => '<i class="icon-star"></i> Избранное',
                'trash' => '<i class="icon-trash"></i> Мусор',
                'list' => ['all', 'fav', 'trash']
            ]
        ],
        'list' => ['text', 'category', 'date', 'price', 'region', 'platform', 'type', 'status', 'src', 'extra', 'user']
    ],
    'column' => [
        'name' => [
            'title' => 'Лот',
            'sort' => true
        ],
        'type' => [
            'title' => 'Тип',
            'sort' => true
        ],
        'place' => [
            'title' => 'Регион',
            'sort' => false
        ],
        'begindate' => [
            'title' => 'Дата начала',
            'sort' => true
        ],
        'closedate' => [
            'title' => 'Дата окончания',
            'sort' => true
        ],
        'beforedate' => [
            'title' => 'Статус',
            'sort' => true
        ],
        'beginprice' => [
            'title' => 'Начальная цена, руб.',
            'sort' => true
        ],
        'nowprice' => [
            'title' => 'Текущая цена, руб.',
            'sort' => true
        ],
        'minprice' => [
            'title' => 'Минимальная цена, руб.',
            'sort' => true
        ],
        'marketprice' => [
            'title' => 'Рыночная цена, руб.',
            'sort' => true
        ],
        'profitrub' => [
            'title' => 'Доход, руб.',
            'sort' => true
        ],
        'profitproc' => [
            'title' => 'Доходность, %',
            'sort' => true
        ],
        'pricediff' => [
            'title' => 'Понижение<br/>цены, %',
            'sort' => true
        ],
        'debpoints' => [
            'title' => 'Баллы',
            'sort' => true
        ],
        'platform' => [
            'title' => 'Федресурс,<br/>Площадка',
            'sort' => true
        ],
        'addition' => [
            'title' => '<i cmd="fav|bn|" title="Добавить все лоты в избранное" class="icon-star-empty"></i>',
            'sort' => false
        ],
        'list' => ['name', 'type', 'place', 'begindate', 'closedate', 'beforedate',
            'beginprice', 'nowprice', 'minprice', 'marketprice', 'profitrub', 'profitproc',
            'pricediff', 'debpoints', 'platform', 'addition']
    ],
    'tools' => [
        'profile' => [
            'url' => '/pages/skan-bankrot-spy'
        ]
    ],
    'btn_alerts' => 'Оповещения',
    'btn_news' => 'Показать все',
    'btn_create' => 'Создать',
    'btn_save' => 'Сохранить',
    'btn_remove' => 'Удалить',
    'btn_cancel' => 'Отмена',
    'btn_login' => 'Авторизация',
    'btn_reg' => 'Регистрация',
    'btn_subscr' => 'Оформить подписку',
    'btn_later' => 'Возможно, позже',
    'msg_info_header' => 'Полезная информация',
    'msg_guest_header' => 'Действие заблокировано',
    'msg_guest_text' => 'Доступно только для зарегистрированных пользователей. Войдите на сайт или зарегистрируйтесь.',
    'msg_over_header' => 'Действие заблокировано',
    'msg_over_text' => 'Можно создать не более трех профилей.',
    'msg_premium_header' => 'Платный контент',
    'msg_premium_text' => 'Доступно только по подписке.',
    'msg_profile_add_header' => 'Создание нового профиля',
    'msg_profile_add_placeholder' => 'введите название профиля',
    'msg_profile_rn_header' => 'Изменение названия текущего профиля',
    'msg_profile_rm_header' => 'Удаление профиля',
    'msg_profile_rm_text' => 'Вы действительно хотите удалить этот профиль?',
    'msg_mc_header' => 'Filter setting for market cap',
    'msg_ch_header' => 'Timeframe setting for price change',
    'msg_imp_header' => 'Timeframe setting for impulse signals',
    'msg_sign_header' => 'Timeframe setting for TB and DTDB signals',
    'msg_vsp_header' => 'Timeframe setting for volatiliy and Prevailing trend',
];
